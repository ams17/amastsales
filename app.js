const env = process.env.NODE_ENV || 'dev';
const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');

const accountRouter = require('./routes/a_account');
const apiAllotmentRouter = require('./routes/api_allotment');
const apiAuthRouter = require('./routes/api_auth');
const apiFinanceRouter = require('./routes/a_finance');
const apiAFinanceRouter = require('./routes/api_finance');
const apiInventoryRouter = require('./routes/a_inventory');
const apiALookupRouter = require('./routes/api_lookup');
const apiAOutletRouter = require('./routes/api_outlet');
const apiOutletRouter = require('./routes/a_outlet');
const apiLookupRouter = require('./routes/a_lookup');
const apiReportRouter = require('./routes/a_report');
const apiPicklistRouter = require('./routes/a_picklist');
const apiPriceRouter = require('./routes/a_price');
const apiDeviceManagementRouter = require('./routes/a_device_management');
const apiLov = require('./routes/a_lov');
const apiAPriceRouter = require('./routes/api_price');
const apiAProductRouter = require('./routes/api_product');
const apiSkuRouter = require('./routes/a_sku');
const apiAStockRouter = require('./routes/api_stock');
const apiVanRouter = require('./routes/a_van');
const apiAVanStockRouter = require('./routes/api_van_stock');
const apiAVisitRouter = require('./routes/api_visit');
const apiReconciliationRouter = require('./routes/a_reconciliation');
const indexRouter = require('./routes/index');
const loginRouter = require('./routes/login');
const siteRouter = require('./routes/a_site');
const userRouter = require('./routes/a_user');
const notificationRouter = require('./routes/a_notification');
const dashboardRouter = require('./routes/a_dashboard');

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json({limit: '5mb'}));
app.use(express.urlencoded({limit: '5mb', extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/dayjs', express.static(path.join(__dirname, 'node_modules/dayjs')));
app.use('/decimal.js', express.static(path.join(__dirname, 'node_modules/decimal.js')));
app.use('/sortablejs', express.static(path.join(__dirname, 'node_modules/sortablejs')));
app.use('/select2', express.static(path.join(__dirname, 'node_modules/select2')));
app.use('/jquery-sortablejs', express.static(path.join(__dirname, 'node_modules/jquery-sortablejs')));
app.use('/printjs', express.static(path.join(__dirname, 'node_modules/print-js/dist')));
app.use('/chart', express.static(path.join(__dirname, 'node_modules/chart.js/dist')));
app.use('/font-awesome', express.static(path.join(__dirname, 'node_modules/@fortawesome/fontawesome-free')));

app.use('/', indexRouter);
app.use('/a/account', accountRouter);
app.use('/a/finance/', apiFinanceRouter);
app.use('/a/inventory', apiInventoryRouter);
app.use('/a/outlet', apiOutletRouter);
app.use('/a/picklist', apiPicklistRouter);
app.use('/a/price', apiPriceRouter);
app.use('/a/device_management', apiDeviceManagementRouter);
app.use('/a/lov', apiLov);
app.use('/a/site', siteRouter);
app.use('/a/sku', apiSkuRouter);
app.use('/a/user', userRouter);
app.use('/a/notification', notificationRouter);
app.use('/a/dashboard', dashboardRouter);
app.use('/a/van', apiVanRouter);
app.use('/a/reconciliation', apiReconciliationRouter);
app.use('/a/lookup', apiLookupRouter);
app.use('/a/report', apiReportRouter);
app.use('/api/auth', apiAuthRouter);
app.use('/api/allotment', apiAllotmentRouter);
app.use('/api/dataLookup',apiALookupRouter);
app.use('/api/finance',apiAFinanceRouter);
app.use('/api/outlet',apiAOutletRouter);
app.use('/api/price',apiAPriceRouter);
app.use('/api/product',apiAProductRouter);
app.use('/api/stock',apiAStockRouter);
app.use('/api/van/stock',apiAVanStockRouter);
app.use('/api/visit',apiAVisitRouter);
app.use('/login', loginRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  if(err.status == 404) {
    if(req.originalUrl.startsWith('/a/')){
      return res.status(404).send('API not found');
    }
    return res.redirect('/login');
  }

  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page  
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
