const env = process.env.NODE_ENV || 'dev';
const conf = require('../config/config.json')[env];
const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const {Parser} = require('json2csv');
const jwtAuth = require('../common/jwtAuth');
const Constants = require('../common/constants');
const Password = require('../common/password');
const db = require('../model/db.js');
const Op = db.Sequelize.Op;
const sq = db.sequelize;
const df = 'YYYY-MM-DD';
const limit = 20;

const Sites = {};

Sites.list= async function(req,res){
  let page = req.query.page;
  let status = req.query.status || 'all';
  let role = req.query.role;
  let keyword = req.query.keyword;
  let searchby = req.query.searchby || 'id';
  let where = {};

  status = status.toLowerCase();
  page = (!page || isNaN(page) || parseInt(page) < 0)? 0 : parseInt(page) - 1;
  let offset = page * limit;

  if(searchby == 'id' && keyword)  where.id = {[Op.iLike]: `${keyword}%`};
  where.active = {[Op.eq]: true}
  let sites;
  try{
    sites = await db.sites.findAndCountAll({
      attributes: ['id', 'name', 'address', 'postcode', 'city', 'state', 'created_by', 'created_at'],
      order: [['id']],
      where, offset, limit,
      raw: true
    });
    sites.limit = limit;
    sites.offset = offset;
  }catch(e){
    console.error(e);
    return res.status(500).send({status: 'failed', errMsg: 'Failed to get sites.'});
  }
  return res.send({status: 'success', sites});
}

Sites.create = async function(req,res){
  let id  = req.body.id;
  let name = req.body.name;
  let address = req.body.address;
  let postcode = req.body.postcode;
  let city = req.body.city;
  let state = req.body.state;
  let status = req.body.status;
  let logo = req.body.logo;
  let isDefault = req.body.isDefault;
  let {user_id, role} = req.token;

  if(!id) res.status(500).send({errMsg:'Please enter site id.'});
  if(!name) res.status(500).send({errMsg:'Please enter site name.'});
  if(!address) res.status(500).send({errMsg:'Please enter address.'});
  if(!postcode) res.status(500).send({errMsg:'Please enter postcode.'});
  if(!city) res.status(500).send({errMsg:'Please enter city.'});
  if(!state) res.status(500).send({errMsg:'Please enter state.'});
  if(!status) res.status(500).send({errMsg:'Please enter status.'});

  let site;

  try {
    site = await db.sites.findOne({
      where: {
        id : {[Op.eq]: id}
      },
      raw: true
    });

    if(site) return res.status(422).send({errMsg:`Site id ${id} already used. Please enter a new id.`});

    let convertedLogo = null;
    console.log(isDefault);
    console.log(logo);
    if (logo != null && isDefault == 'false'){
      let base64Result = logo;
      base64result = base64Result.substr(base64Result.indexOf(',') + 1);

      let data = await sq.query(qDecodeImage,{
        type: sq.QueryTypes.SELECT,
        replacements: {logo: base64result},
        raw: true
      });
      convertedLogo = data[0].logo;
    }
    
    await db.sites.create({
      id: id,
      name: name,
      address: address,
      postcode: postcode,
      city: city,
      state: state,
      active: status,
      created_by : user_id,
      created_at : new Date(),
      logo : convertedLogo
    });

  } catch(e) {
    // statements
    console.error(e);
    return res.status(500).send({errMsg: 'Failed to create new site.'})
  }

  return res.send({msg: `Site ${id} create successfully.`})
}

let qSite = `select *,'data::image/jpg;base64,' || encode(logo, 'base64') as logo from sites where id = :id`;

Sites.read = async function(req,res){
  let id = req.params.id;
  if(id == null) return res.status(422).send({status: 'failed', errMsg:'Site ID not provided.'});

  let site,states,cities;
  try{
    let data = await sq.query(qSite,{
      type: sq.QueryTypes.SELECT,
      replacements: {id},
      raw: true
    });

    site = data[0];

    if(!site) return res.status(404).send({status: 'failed', errMsg: `Site #${id} not found.`});
    let selectedStates = site.state;
    states = await Constants.states;
    cities = await Constants.cities[selectedStates];    
    console.log('test',selectedStates,states,cities);
  }catch(e){
    console.error(e);
    return res.status(500).send({status: 'failed', errMsg: `Failed to get site #${id}`})
  }
  return res.send({status:'success', site,states,cities});
}

let qDecodeImage = `select decode(:logo, 'base64') as logo`;

Sites.update = async function(req, res){
  let id  = req.body.id;
  let {user_id,role} = req.token;

  // if(!id) return res.status().send({errMsg:'Site ID not provided'});
  let name = req.body.name;
  let address = req.body.address;
  let postcode = req.body.postcode;
  let city = req.body.city;
  let state = req.body.state;
  let status = req.body.status;
  let isDefault = req.body.isDefault;

  if(!id) res.status(500).send({errMsg:'Please enter site id.'});
  if(!name) res.status(500).send({errMsg:'Please enter site name.'});
  if(!address) res.status(500).send({errMsg:'Please enter address.'});
  if(!postcode) res.status(500).send({errMsg:'Please enter postcode.'});
  if(!city) res.status(500).send({errMsg:'Please enter city.'});
  if(!state) res.status(500).send({errMsg:'Please enter state.'});
  if(!status) res.status(500).send({errMsg:'Please enter status.'});

  let site;

  try {
    site = await db.sites.findOne({
      where: {
        id : {[Op.eq]: id}
      }
    })
    
    if(!site) return res.status(404).send({status: 'failed', errMsg: `Site #${id} not found.`});

    if("name" in req.body){
      if(req.body.name != null){
        if(req.body.name.toString().trim().length != 0){
          site.name = req.body.name
        }
      }
    }
    if("address" in req.body){site.address = req.body.address}
    if("postcode" in req.body){site.postcode = req.body.postcode}
    if("city" in req.body){site.city = req.body.city}
    if("state" in req.body){site.state = req.body.state}
    if('status' in req.body){
      let {status} = req.body;
      site.active = status;
    }

    if("logo" in req.body){
      if (req.body.logo != null && isDefault == 'false'){
        let base64Result = req.body.logo;
        base64result = base64Result.substr(base64Result.indexOf(',') + 1);

        let data = await sq.query(qDecodeImage,{
          type: sq.QueryTypes.SELECT,
          replacements: {logo: base64result},
          raw: true
        });
        site.logo = data[0].logo;
      }
    }
    site.save();

  } catch(e) {
    // statements
    console.error(e);
  }

  return res.send({msg:'Updated site successfully.'})
}

Sites.delete = async function(req, res){
  let id  = req.body.id;
  let {user_id,role} = req.token;

  if(!id) return res.status().send({errMsg:'Site ID not provided'});

  let site;

  try {
    site = await db.sites.findOne({
      where: {
        id : {[Op.eq]: id}
      }
    })
    
    if(!site) return res.status(404).send({status: 'failed', errMsg: `Site #${id} not found.`});
    site.active = false;
    site.save();
    // site.destroy();

  } catch(e) {
    // statements
    console.error(e);
  }

  return res.send({msg:'Site deleted successfully.'})
}

Sites.listmin = async function(req, res){
  let sites;
  try{
    sites = await db.sites.findAll({ attributes:['id'], order: ['id'], raw: true });
  }catch(e){
    console.error(e);
    return res.status(500).send({errMsg: 'Failed to get list of sites.'});
  }
  return res.send({sites});
}

Sites.getDetails = async function(req, res){
  let states;
  try{
    states = await Constants.states;
  }catch(e){
    return res.status(500).send({errMsg: 'Failed to get states'});
  }
  return res.send({states});
}

Sites.getCities = async function(req, res){
  let states;
  let cities;
  try{
    // console.log(req.body);
    // console.log(req.params);
    let state  = req.params.id;
    // console.log('state',state);
    cities = await Constants.cities[state];
    // console.log(cities);
    // if(!Constants.cities.includes(state)) {
    //     return res.status(422).send({errMsg: `State '${state}' is not valid.`});
    //   }else{
    //     cities = await Constants.cities.state;
    //   }
  }catch(e){
    return res.status(500).send({errMsg: 'Failed to get states'});
  }
  return res.send({cities});
}

module.exports = Sites;