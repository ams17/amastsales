const env = process.env.NODE_ENV || 'dev';
const conf = require('../config/config.json')[env];
const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const dayjs = require('dayjs');
const {Parser} = require('json2csv');
const jwtAuth = require('../common/jwtAuth');
const Password = require('../common/password');
const db = require('../model/db.js');
const Op = db.Sequelize.Op;
const sq = db.sequelize;
const df = 'YYYY-MM-DD';
const limit = 50;

let Void = {};

const searchByOpts = ['id', 'outlet_id', 'van_id'];
Void.list = async function(req, res){
  let page = req.query.page;
  let {keyword, site_id, date}  = req.query;
  let searchby = req.query.searchby;
  let where = {};

  page = (!page || isNaN(page) || parseInt(page) < 0)? 0 : parseInt(page) - 1;
  let offset = page * limit;

  let keywords = keyword? keyword.split(',').map(k=> k.trim()) : [];
  
  if(keyword != null){
    let col = searchByOpts.includes(searchby)? searchby : 'id';
    where[col] = keywords.length === 1? {[Op.iLike]: `${keyword}%`} : {[Op.in]: keywords}; 
  }

  if(site_id && site_id != 'all') where.site_id = {[Op.iLike]: `${tag}`};
  if(date){
    let d = date.split(',');
    if(d.length == 1){
      if(!dayjs(d[0], df).isValid()) return res.status(422).send({errMsg: 'Date format is invalid.'});
      where.date = {[Op.gte]: d[0]};
    } else{
      if(!dayjs(d[0], df).isValid() || !dayjs(d[1], df).isValid()) return res.status(422).send({errMsg: 'Date format is invalid.'});
      where.date = { [Op.between]:[ 
        dayjs(d[0]).startOf('day').toDate(), 
        dayjs(d[1]).endOf('day').toDate() 
      ]}
    }
  }

  let void_invoices;
  try{
    void_invoices = await db.void_invoices.findAndCountAll({
      order: [['date','desc'], 'van_id', 'outlet_id', 'created_at'],
      where, offset, limit, 
      raw: true,
      logging: console.log
    });
    void_invoices.limit = limit;
    void_invoices.offset = offset;
  }catch(e){
    console.error(e);
    return res.status(500).send({status: 'failed', errMsg: 'Failed to get void_invoices.'});
  }
  return res.send({status: 'success', void_invoices});
}


Void.read = async function(req,res){
  let id = req.params.id;
  if(id == null) return res.status(422).send({status: 'failed', errMsg:'Void Invoice ID not provided.'});

  let void_invoice, details;
  try{

    void_invoice = await db.void_invoices.findOne({
      where: {id: {[Op.eq]: id}},
      raw: true
    });

    if(!void_invoice){
      return res.status(404).send({status: 'failed', errMsg: `Void Invoice #${id} not found.`});
    }

    details = await db.invoice_details.findAll({
      where: {invoice_id: void_invoice.invoice_id},
      raw: true
    });

  }catch(e){
    console.error(e);
    return res.status(500).send({status: 'failed', errMsg: `Failed to get void invoice #${id}`})
  }

  return res.send({status:'success', void_invoice, details});
}

module.exports = Void;