const env = process.env.NODE_ENV || 'dev';
const conf = require('../config/config.json')[env];
var express = require('express');
var router = express.Router();
const db = require('../model/db.js');
const Op = db.Sequelize.Op;
const sq = db.sequelize;

const limit = 20;

let Lov = {};

Lov.list = async function(req,res){
  let group_id = req.query.group_id;
  let selected_id = req.query.selected_id;
  let is_default = req.query.is_default;
  let keywords = req.query.keywords;
  let page = req.query.page;

  let data;
  let where = {};

  if(!group_id) return res.status(422).send({error:"Please enter a group ID."})

  page = (!page || isNaN(page) || parseInt(page) < 0)? 0 : parseInt(page) - 1;
  let offset = page * limit;

  let responseJson = {
    results: [],
    pagination: {
      more: false
    }
  }

  where.group_id = group_id;
  where.active = true;

  if (keywords != null) {
    where.display = {[Op.iLike]: `%${keywords}%`}
  }

  if (selected_id != null) {
    where.id = selected_id;
  }

  if (is_default != null) {
    where.is_default = is_default;
  }

  try {
    data = await db.lovs.findAndCountAll({
      where: where,
      attributes: ['id', ['display', 'text']],
      order: [['sequence', 'asc']],
      limit: limit,
      offset: offset,
      raw: true
    });

    if (offset < data.rows.count) {
      responseJson.pagination.more = true;
    }

    responseJson.results = data.rows;
    
  } catch(e) {
    // statements
    console.error(e);
    return res.status(500).send({error:'Failed to get lovs list.'});
  }
  
  return res.send(responseJson)
}
Lov.lists = async function(req,res){
  let group_id = (req.query.group_id)?(req.query.group_id):null;
  let selected_id = req.query.selected_id;
  let is_default = req.query.is_default;
  let keywords = req.query.keywords;
  let page = req.query.page;

  let data;
  let where = {};

  // if(!group_id) return res.status(422).send({error:"Please enter a group ID."})

  page = (!page || isNaN(page) || parseInt(page) < 0)? 0 : parseInt(page) - 1;
  let offset = page * limit;

  let responseJson = {
    results: [],
    pagination: {
      more: false
    }
  }
  if(group_id != null)
  where.group_id = group_id;

  where.active = true;

  if (keywords != null) {
    where.display = {[Op.iLike]: `%${keywords}%`}
  }

  if (selected_id != null) {
    where.id = selected_id;
  }

  if (is_default != null) {
    where.is_default = is_default;
  }
  where.group_id = 'test';
  try {
    data = await db.lovs.findAndCountAll({
      logging: console.log,
      where: where,
      attributes: ['id','group_id','value','display','sequence','is_default','active'],
      order: [['group_id','asc'],['sequence', 'asc']],
      limit: limit,
      offset: offset,
      raw: true
    });
    // if (offset < data.rows.count) {
    //   data.pagination.more = true;
    // }
    data.limit = limit;
    data.offset = offset;
    
  } catch(e) {
    // statements
    console.error(e);
    return res.status(500).send({error:'Failed to get lovs list.'});
  }
  
  return res.send({status:'success', data})
}

Lov.listAndroid = async function(req,res){
  let group_id = req.body.group_id;
  let data;

  if(!group_id) return res.status(422).send({error:"Please enter a group ID."})
  try {
    data = await db.lovs.findAndCountAll({
      where:{
        group_id : group_id,
        active: true
      },
      attributes: ['id', 'display', 'is_default'],
      order: [['sequence', 'asc']],
      raw: true
    });
  } catch(e) {
    // statements
    console.error(e);
    return res.status(500).send({error:'Failed to get lovs list.'});
  }

  return res.send({status:'success',data})
}


Lov.create = async function(req,res){
    let {role, user_id} = req.token;  

    let sequence        = req.body.sequence;
    let displayValue    = req.body.displayValue;
    let groupID         = req.body.groupID;
    let id              = req.body.id;
    let value           = req.body.value;
    let isDefault       = req.body.isDefault;
    let active          = req.body.active;

    // console.log(req.body);
    // console.log(typeof(isDefault));return false;

    if(!sequence) return res.status(500).send({errMsg: 'Please enter UU ID'});
    if(!displayValue) return res.status(500).send({errMsg: 'Please enter IME ID'});
    if(!groupID) return res.status(500).send({errMsg: 'Please select made.'});
    if(!id) return res.status(500).send({errMsg: 'Please select model.'});
    if(!value) return res.status(500).send({errMsg: 'Please enter Tel No'});

    let Lov;
    try {
        let notUniqueid = await db.lovs.findOne({ where:{ id: {[Op.eq]: id} }});
        if(notUniqueid) return res.status(422).send({status:'failed', errMsg:'ID is already taken. Please try another ID.'})

        let notUniqueSequence = await db.lovs.findOne({ where:{ sequence: {[Op.eq]: sequence},group_id:{[Op.eq]: groupID} }});
        if(notUniqueSequence) return res.status(422).send({status:'failed', errMsg:'Sequence is already taken. Please try another Sequence.'})

        Lov = await db.lovs.create({
            sequence: sequence,
            display : displayValue,
            group_id : groupID,
            id : id,
            value : value,
            is_default : isDefault,
            active : active,
            created_by: req.token.user_id,
            updated_by: req.token.user_id,
            created_at: sq.now   
        });
    } catch(e) {
        console.error(e);
        return res.status(500).send({errMsg: 'Failed to create LOV.'});
    }
    return res.send({Lov});
}

Lov.update = async function(req, res){
  let id = req.body.rowId;
  let {role, user_id} = req.token;
  // console.log(req.body);return false;
  let lov;
  try{
    lov = await db.lovs.findOne({ 
      where: {id: {[Op.eq]: id}} 
    });
    if(!lov) return res.status(404).send({errMsg: 'LOV not found.'});

    if('sequence' in req.body){
      let sequence = req.body.sequence;
      if(sequence !== null && sequence.trim().length == 0) {
        return res.status(422).send({errMsg: 'Sequence cannot be empty.'})
      }
      lov.sequence = sequence;
    }
    if('displayValue' in req.body){
      let displayValue = req.body.displayValue;
      if(displayValue !== null && displayValue.trim().length == 0) {
        return res.status(422).send({errMsg: 'Display value cannot be empty.'})
      }
      lov.display = displayValue;
    }
    if('groupID' in req.body){
      let groupID = req.body.groupID;
      if(groupID !== null && groupID.trim().length == 0) {
        return res.status(422).send({errMsg: 'Group ID cannot be empty.'})
      }
      lov.group_id = groupID;
    }
    if('id' in req.body){
      let id = req.body.id;
      if(id !== null && id.trim().length == 0) {
        return res.status(422).send({errMsg: 'Year purchased cannot be empty.'})
      }
      lov.id = id;
    }
    if('value' in req.body){
      let value = req.body.value;
      if(value !== null && value.trim().length == 0) {
        return res.status(422).send({errMsg: 'Made cannot be empty.'})
      }
      lov.value = value;
    }
    if('isDefault' in req.body){
      let isDefault = req.body.isDefault;      
      lov.is_default = isDefault;
    }
    if('active' in req.body){
      let active = req.body.active;
      lov.active = active;
    }

    lov.updated_by =  user_id;
    lov.updated_at =  sq.now
    await lov.save();
    console.log(lov);
  }catch(e){
    console.error(e);
    return res.status(500).send({errMsg: 'Failed to update LOV.'});
  }

  return res.send({lov});
}

Lov.delete = async function(req, res){
  let id  = req.body.id;
  let {user_id,role} = req.token;

  if(!id) return res.status().send({errMsg:'Price ID not provided'});

  let price;

  try {
    price = await db.price_list.findOne({
      where: {
        id : {[Op.eq]: id}
      }
    })
    
    if(!price) return res.status(404).send({status: 'failed', errMsg: `User #${id} not found.`});
    price.active = false;
    price.save();
    // user.destroy();

  } catch(e) {
    // statements
    console.error(e);
  }

  return res.send({msg:'User deleted successfully.'})
}

Lov.read = async function(req, res){
  let id = req.params.id;

  let lovs;
  try{
    lovs = await db.lovs.findOne({
      where: {id: {[Op.eq]: id} },
      raw: true
    });

    if(!lovs) return res.status(404).send({errMsg: 'LOV not found.'});
  }catch(e){
    console.error(e);
    return res.status(500).send({errMsg: 'Failed to find LOV.'})
  }

  return res.send({
    data:{
        lovs : lovs
    }
  });
}
module.exports = Lov;