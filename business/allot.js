const env = process.env.NODE_ENV || 'dev';
const conf = require('../config/config.json')[env];
const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const dayjs = require('dayjs');
const {Parser} = require('json2csv');
const jwtAuth = require('../common/jwtAuth');
const Password = require('../common/password');
const db = require('../model/db.js');
const Op = db.Sequelize.Op;
const sq = db.sequelize;
const df = 'YYYY-MM-DD';
const limit = 50;

let Allots = {};

Allots.list = async function(req, res){
  let page = req.query.page;
  let {keyword, tag, status, date}  = req.query;
  let searchby = req.query.searchby || 'allotId';
  let where = {};

  page = (!page || isNaN(page) || parseInt(page) < 0)? 0 : parseInt(page) - 1;
  let offset = page * limit;

  let keywords = keyword? keyword.split(',').map(k=> k.trim()) : [];

  if (searchby == 'vanId' && keyword){
    where.van_id = keywords.length === 0? {[Op.iLike]: `${keyword}%`} : {[Op.in]: keywords};
  } else if(searchby == 'allotId' && keyword){
    where.id = keywords.length === 0? {[Op.iLike]: keyword} : {[Op.in]: keywords};
  } else if(searchby == 'do' && keyword){
    where.delivery_order_id = keywords.length === 0? {[Op.eq]: keyword} : {[Op.in]: keywords};
  }

  if(tag && tag != 'all') where.tag = {[Op.iLike]: `${tag}`};
  if(status && status != 'all') where.status = {[Op.iLike]: `${status}`};
  if(date){
    let d = date.split(',');
    if(d.length == 1){
      if(!dayjs(d[0], df).isValid()) return res.status(422).send({errMsg: 'Date format is invalid.'});
      where.date = {[Op.gte]: d[0]};
    } else{
      if(!dayjs(d[0], df).isValid() || !dayjs(d[1], df).isValid()) return res.status(422).send({errMsg: 'Date format is invalid.'});
      where.date = { [Op.between]:[ 
        dayjs(d[0]).startOf('day').toDate(), 
        dayjs(d[1]).endOf('day').toDate() 
      ]}
    }
  }

  let allotments;
  try{
    allotments = await db.van_allot.findAndCountAll({
      attributes: ['id', 'date', 'van_id', 'tag', 'status', 'created_at'],
      order: [['date', 'desc'],['van_id'],['tag', 'desc']],
      where, offset, limit, 
      raw: true,
      logging: console.log
    });
    allotments.limit = limit;
    allotments.offset = offset;
  }catch(e){
    console.error(e);
    return res.status(500).send({status: 'failed', errMsg: 'Failed to get allotments.'});
  }
  return res.send({status: 'success', allotments});
}

const qVanAllotPlan = `select sku_id, uom, array_to_json(array[max(mon), max(tue), max(wed), max(thu), max(fri), max(sat), max(sun)]) as quantity 
from (
  with plan as (
    select :van_id::text as van_id, suc.sku_id, suc.to_uom_id as uom, days.day, coalesce(vap.quantity,0) as quantity from sku_uom_conversions suc
    inner join (select 'MON' as day union select 'TUE' union select 'WED' union select 'THU' union select 'FRI' union select 'SAT' union select 'SUN') days on true
    left join van_allot_plan vap 
    on vap.sku_Id = suc.sku_id
    and vap.day = days.day
    and van_id = :van_id
    where multiply_by = 1
  )
  select van_id, sku_id, uom, quantity as mon, 0 as tue, 0 as wed, 0 as thu, 0 as fri, 0 as sat, 0 as sun from plan where day = 'MON' 
  union select van_id, sku_id, uom, 0 as mon, quantity as tue, 0 as wed, 0 as thu, 0 as fri, 0 as sat, 0 as sun from plan where day = 'TUE'
  union select van_id, sku_id, uom, 0 as mon, 0 as tue, quantity as wed, 0 as thu, 0 as fri, 0 as sat, 0 as sun from plan where day = 'WED'
  union select van_id, sku_id, uom, 0 as mon, 0 as tue, 0 as wed, quantity as thu, 0 as fri, 0 as sat, 0 as sun from plan where day = 'THU'
  union select van_id, sku_id, uom, 0 as mon, 0 as tue, 0 as wed, 0 as thu, quantity as fri, 0 as sat, 0 as sun from plan where day = 'FRI'
  union select van_id, sku_id, uom, 0 as mon, 0 as tue, 0 as wed, 0 as thu, 0 as fri, quantity as sat, 0 as sun from plan where day = 'SAT'
  union select van_id, sku_id, uom, 0 as mon, 0 as tue, 0 as wed, 0 as thu, 0 as fri, 0 as sat, quantity as sun from plan where day = 'SUN'
)a
group by van_id, sku_id, uom 
order by sku_id;`

Allots.planList = async function(req,res){
  let {van_id} = req.params;
  let {user_id, role} = req.token; 
  if(!van_id) return res.status(422).send({status: 'failed', errMsg:'Please enter Van ID.'});

  let plan;
  try {
    let van = await db.vans.findOne({
      where: {id: {[Op.eq]: van_id}},
      raw: true
    });
    if(!van) return res.status(404).send({errMsg: 'Van ID not found.'});

    plan = await sq.query(qVanAllotPlan,{
      type: sq.QueryTypes.SELECT,
      replacements: {van_id},
      raw: true
    });
  } catch(e) {
    console.error(e);
    return res.status(500).send({errMsg:'Failed to get Van Allotment Plan list.'});
  }
  res.send({van_id, plan, desc: 'planned quantity arrayed from Mon to Sun.'});
}

const days = ['MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN'];
const qSkuBaseUom = `select json_object_agg(sku_Id, to_uom_id) as uoms from sku_uom_conversions where multiply_by = 1;`;

Allots.planSave = async function(req,res){
  let van_id = req.body.van_id;
  let plan = req.body.plan;
  let {user_id, role} = req.token;

  if(!van_id) return res.status(422).send({errMsg:'Please enter Van ID.'});
  if(!plan) return res.status(422).send({errMsg:'Please submit an Allocation Plan.'});
  //if(!quantity || isNaN(quantity)) return res.status(422).send({errMsg:'Please enter quantity'});
  //if(quantity < 0 ) return res.status(422).send({errMsg:'Quantity cannot lower then 0'});

  let van;
  let vanAllotPlan;
  let transaction;

  try {
    van = await db.vans.findOne({
      where:{id: {[Op.eq]: van_id}},
      raw: true
    });
    if(!van) return res.status(404).send({errMsg:'Van ID not found.'});

    let baseUoms = await sq.query(qSkuBaseUom, {type:sq.QueryTypes.SELECT, raw : true});
    let uoms = baseUoms[0].uoms;

    let vanAllotPlan = [];
    for(let p of plan){
      if(!(p.sku_id in uoms)) return res.status(422).send({errMsg: `Base UOM for SKU ID (${p.sku_id}) not found.`});
      let uom_id = uoms[p.sku_id];

      for(let i = 0; i< p.quantity.length; i++){
        if(p.quantity[i] <= 0) continue;

        vanAllotPlan.push({
          van_id: van_id,
          sku_id: p.sku_id, 
          uom_id: uom_id,
          day: days[i],
          quantity: p.quantity[i] || 0,
          created_by: req.token.user_id,
          created_at: sq.now,
          updated_at: sq.now
        });  
      }
    }

    transaction = await sq.transaction();
    await db.van_allot_plan.destroy({
      where: {van_id: {[Op.eq]: van_id}},
      transaction
    });
    await db.van_allot_plan.bulkCreate(vanAllotPlan, {transaction, logging: console.log});
    await transaction.commit();
    
  } catch(e) {
    if(transaction) await transaction.rollback();
    console.error(e);
    return res.status(500).send({errMsg:'Failed to update allotment plan.'});
  }

  return res.send({msg:'Update allotment plan successfully.'})
};

const qCreateSkuList = `select suc.sku_id, to_uom_id as uom from sku_uom_conversions suc 
  inner join sku_uoms su on su.sku_id = suc.sku_id and su.uom_id = suc.to_uom_id and su.active = true
  where multiply_by = 1 order by suc.sku_id`;

Allots.createSkuList = async function(req, res){
  let skus;
  try{
    skus = await sq.query(qCreateSkuList,{ type: sq.QueryTypes.SELECT, raw: true});
  }catch(e) {
    console.error(e);
    return res.status(500).send({errMsg:'Failed to get SKU list for additional allotment.'});
  }
  res.send({skus});
}

const qCreateSkuListPlanned = `select 
  suc.sku_id, to_uom_id as uom, 
  array_to_json(array[coalesce(vap.quantity,0)]) as quantity
from sku_uom_conversions suc
inner join sku_uoms su 
  on su.sku_id = suc.sku_id 
  and su.uom_id = suc.to_uom_id 
  and su.active = true
left join van_allot_plan vap 
  on van_id = :van_id 
  and vap.sku_id = suc.sku_id
  and vap.uom_id = suc.to_uom_id 
  and day = to_char(:date::date,'DY')
where multiply_by = 1 
order by suc.sku_id`;

Allots.createSkuListPlanned = async function(req, res){
  let {date, van_id} = req.params;

  if(!date) return res.status(422).send({errMsg: 'Please enter a date.'});
  if(!dayjs(date, df).isValid()) return res.status(422).send({errMsg: 'Please enter a valid date format.'});
  if(!van_id) return res.status(422).send({errMsg: 'Please select a Van ID.'});

  let skus;
  try{
    skus = await sq.query(qCreateSkuListPlanned,{ 
      type: sq.QueryTypes.SELECT, 
      replacements: {date, van_id},
      raw: true
    });
  }catch(e) {
    console.error(e);
    return res.status(500).send({errMsg:'Failed to get SKU list for planned allotment.'});
  }
  res.send({skus});
}

const qListVanStatus = `select v.id, case when count(va.id) > 0 then 'allotted' else 'unallotted' end as status from vans v
left join van_allot va on va.van_id = v.id and date = :date and tag = 'planned'
where v.site_id = :site_id and active = true group by v.id order by v.id`;

Allots.listVanStatus = async function(req, res){
  let {date, site_id} = req.params;
  let {role, user_id} = req.token;

  if(!date) return res.status(422).send({errMsg: 'Please enter a date.'});
  if(!dayjs(date, df).isValid()) return res.status(422).send({errMsg: 'Please enter a valid date format.'});

  if(!site_id) return res.status(422).send({errMsg: 'Please select a site id.'});

  let vans;
  try{

    if(role != 'superadmin'){
      // check site_id
    }

    vans = await sq.query(qListVanStatus,{ 
      type: sq.QueryTypes.SELECT, 
      replacements: {date, site_id},
      raw: true
    });
  }catch(e) {
    console.error(e);
    return res.status(500).send({errMsg:'Failed to get list of Van Allotment status.'});
  }
  res.send({vans});
}

const qNextAllotment = `SELECT nextval('seq_allotment') as allot_id`;

Allots.create = async function(req, res){
  let {date, van_id, tag, allot} = req.body;
  let {role, user_id} = req.token;

  if(!date) return res.status(422).send({errMsg: 'Please enter a date.'});
  if(!dayjs(date, df).isValid()) return res.status(422).send({errMsg: 'Please enter a valid date format.'});
  if(!van_id) return res.status(422).send({errMsg: 'Please select a Van ID.'});
  if(!tag) return res.status(422).send({errMsg: 'Please enter allotment tag.'});
  if(!allot || allot.length == 0) return res.status(422).send({errMsg: 'Please enter allotment details.'});
  if(!Array.isArray(allot)) return res.status(422).send({errMsg: 'Expected an array for Allotment details.'});
  if(allot.filter(a => a.quantity > 0).length == 0) return res.status(422).send({errMsg: 'Please enter SKU quantity for allotment.'})

  const validTags = ['planned', 'additional'];
  if(!validTags.includes(tag)) return res.status(422).send({errMsg: `Valid value for tag are [${validTags}]`});

  let allotHeader, allotDetails;
  let transaction;
  console.log('ok')
  try{
    let van = await db.vans.findOne({ where: {id: {[Op.eq]: van_id} }});
    if(!van) return res.status(404).send({errMsg: 'Van ID not found.'});

    transaction = await sq.transaction();

    if(tag == 'planned'){ 
      let plannedExists = await db.van_allot.findOne({
        where: {van_id: {[Op.eq]: van_id}, date: date, tag: tag},
        transaction: transaction
      });
      if(plannedExists) return res.status(422).send({errMsg: `Planned allotment already created for ${van_id} for ${date}: ${plannedExists.id}`});
    }

    let baseUoms = await sq.query(qSkuBaseUom, {type:sq.QueryTypes.SELECT, raw : true});
    let uoms = baseUoms[0].uoms;

    let nv = await sq.query(qNextAllotment,{type: sq.QueryTypes.SELECT, raw: true});
    let allot_id = 'A' + nv[0].allot_id;

    allotDetails = [];
    for(let a of allot){
      allotDetails.push({
        van_allot_id: allot_id,
        sku_id: a.sku_id,
        uom_id: uoms[a.sku_id],
        allotted_quantity: a.quantity,
        created_at: sq.now, 
        updated_at: sq.now
      });
    }

    allotHeader = await db.van_allot.create({
      id: allot_id,
      van_id: van_id, 
      date: date, 
      status: 'allotted',
      tag: tag, 
      created_by: user_id
    }, {transaction: transaction});
    
    await db.van_allot_details.bulkCreate(allotDetails, {transaction, logging: console.log});

    //await transaction.rollback();
    await transaction.commit();
  }catch(e){
    console.trace(e);
    if(transaction) await transaction.rollback();
    return res.status(500).send({errMsg:'Failed to create Van Allotment.'});
  }
  res.send({allotment: allotHeader, details: allotDetails});
}

const qPlannedQty = `select v.id as van_id, coalesce(quantity, 0) as quantity
from vans v
left join (
  select van_id, sum(quantity) quantity from van_allot_plan
  where van_id in (:vans)
  and day = to_char(:date::date,'DY')
  group by van_id
) vap on vap.van_id = v.id
where id in (:vans)`;

const qBulkAllot = `with allot as(
  insert into van_allot (id, van_id, date, status, tag, created_by, created_at, updated_at)
  select 'A'||nextval('seq_allotment'), v.id, :date, 'allotted', 'planned', :user_id, current_timestamp, current_timestamp
  from vans v
  inner join (
      select van_id, sum(quantity) quantity from van_allot_plan vap
      where van_id in (:vans)
      and day = to_char(:date::date,'DY')
      group by van_id having sum(quantity) > 0
  ) vap 
  on vap.van_id = v.id
  where id in (:vans)
  returning id, van_id, date
)
insert into van_allot_details (van_allot_id, sku_id, uom_id, requested_quantity, allotted_quantity, created_at, updated_at)
select allot.id, sku_id, uom_id, 0, vap.quantity, current_timestamp, current_timestamp 
from van_allot_plan vap 
inner join allot 
on vap.van_id = allot.van_id 
and day = to_char(allot.date::date,'DY')
returning van_allot_id, sku_id, uom_id, allotted_quantity as quantity`;

Allots.bulkCreate = async function(req, res){
  let {date, vans} = req.body;
  let {role, user_id} = req.token;
  
  if(!date) return res.status(422).send({errMsg: 'Please enter a date.'});
  if(!dayjs(date, df).isValid()) return res.status(422).send({errMsg: 'Please enter a valid date format.'});
  if(!vans || vans.length == 0) return res.status(422).send({errMsg: 'Please enter list of vans.'});
  if(!Array.isArray(vans)) return res.status(422).send({errMsg: 'Expected an array for vans.'});

  let allots;
  let allotCount;
  let unplannedVans;
  let transaction;
  try{
    transaction = await sq.transaction();

    // check vans
    let _vans = await db.vans.findAll({ where: {id: {[Op.in]: vans}}, raw: true, transaction });
    console.log({_vans});

    if(vans.length != _vans.length){
      let invalidVans = _vans.filter(v => !vans.includes(v));
      return res.status(404).send({errMsg: `Vans not found: [${invalidVans.join(', ')}]`});
    }

    // check if vans already have planned allot for the given date
    let _allot = await db.van_allot.findAll({
      where: {
        van_id: {[Op.in]: vans},
        date: {[Op.eq]: date},
        tag: 'planned'
      }, raw: true, transaction
    })
    console.log({_allot});

    if(_allot && _allot.length > 0){
      return res.status(422).send({errMsg: `These planned allotment already created for these vans:]\n${_allot.map(a => a.van_id)}` });
    }

    // check that planned allotment has positive quantity.
    let plans = await sq.query(qPlannedQty,{
      type: sq.QueryTypes.SELECT,
      replacements: {vans, date},
      raw: true, transaction
    });
    console.log({plans});
    unplannedVans = plans.filter(p => p.quantity == 0).map(v => v.van_id);

    // execute bulk allotment
    allots = await sq.query(qBulkAllot, {
      type: sq.QueryTypes.SELECT,
      replacements: {vans, date, user_id},
      raw: true, transaction
    });

    allots = [... new Set(allots.map(e => e.van_allot_id))];

    await transaction.commit();
  }catch(e){
    console.trace(e);
    if(transaction) await transaction.rollback();
    return res.status(500).send({errMsg:'Failed to create Bulk Allotment.'});
  }

  res.send({allotCount, unplanned_vans: unplannedVans, allots});
}

Allots.read = async function(req, res){
  let id = req.params.id;

  let allotH, allotD;
  try{
    allotH = await db.van_allot.findOne({
      where: {id: {[Op.eq]: id}},
      raw: true
    });
    if(!allotH) return res.status(404).send({errMsg: 'Allotment not found.'});

    allotD = await db.van_allot_details.findAll({
      attributes: ['sku_id', 'uom_id', [sq.literal('array_to_json(array[requested_quantity, allotted_quantity])'), 'quantity']],
      where: {van_allot_id: {[Op.eq]: id}},
      raw: true
    });
    
  }catch(e){
    console.error(e);
    return res.status(500).send({errMsg: 'Failed to find allotment.'})
  }

  return res.send({allotment: allotH, details: allotD});
}

const qAllotPacked = `select van_allot_id as allot_id, allot_details
from ( 
  select vad.van_allot_id, va.van_id, va.status, 
  json_agg( json_build_object('sku_id', sku_id, 'uom_id', uom_id, 'quantity', allotted_quantity))as allot_details
  from van_allot_details vad
  inner join van_allot va on vad.van_allot_id = va.id and status = 'packed' and van_id = :van_id
  where va.date <= current_date
  group by vad.van_allot_id, van_id, status order by va.van_id, vad.van_allot_id 
)a;`

Allots.packed = async function(req,res){
  let {user_id, van_id, role} = req.token;
  let rows;

  try {
    rows = await sq.query(qAllotPacked,{
      type: sq.QueryTypes.SELECT,
      replacements: {van_id},
      raw: true
    });

  } catch(e) {
    // statements
    console.error(e);
    return res.status(500).send({error: "Failed to get packed allotment."})
  }

  return res.send({
    status:'success',
    data: {
      count: rows.length,
      rows: rows
    }
  });
}


module.exports = Allots;