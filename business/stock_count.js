const env = process.env.NODE_ENV || 'dev';
const conf = require('../config/config.json')[env];
const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const moment = require('moment');
const dayjs = require('dayjs');
const {Parser} = require('json2csv');
const jwtAuth = require('../common/jwtAuth');
const Password = require('../common/password');
const { v4: uuidv4 } = require('uuid');
const db = require('../model/db.js');
const Op = db.Sequelize.Op;
const sq = db.sequelize;
const df = 'YYYY-MM-DD';
const limit = 20;

let StockCount = {};

StockCount.list = async function(req,res){
  let date = req.body.date;
  let outlet_id = req.body.outlet_id;
  let where = {};
  let stock_count;

  if(!outlet_id) return res.status(422).send({error: 'Please enter a outlet ID.'});

  where.outlet_id = {[Op.eq]: outlet_id};
  if(date || dayjs(date,df).isVaild()) where.date = {[Op.eq]: date}

  try {
    data = await db.stock_count.findAndCountAll({ where, raw:true });
  } catch(e) {
    // statements
    console.error(e);
    return res.status(500).send({error: 'Failed to get Stock Count list.'})
  }
  return res.send({data})
}

const qSkus = `select json_object_agg(id,true) as product from skus`;

StockCount.save = async function(req,res) {
  let outlet_id = req.body.outletId;
  let skus = req.body.skus;
  let {user_id, role} = req.token;

  let product;
  let outlet;
  let stock_count=[];

  try {
    outlet = await db.outlets.findOne({ where : { id: {[Op.eq]: outlet_id} }, raw:true });

    if(!outlet) return res.status(404).send({error: 'Outlet ID not found.'});

    product = await sq.query(qSkus,{
      type: sq.QueryTypes.SELECT,
      raw : true
    })

    product = product[0].product; 
    let uuid = uuidv4();

    for(let e of skus){
      if(e.quantity<0) return res.status(422).send({error: `${e.sku_id} quantity cannot lower than 0.`});

      if(!product[e.sku_id]) return res.status(404).send({error: `SKU ID #${e.sku_id} is not found.`});
      
      stock_count.push({
        id : uuid,
        date : dayjs().format(df),
        outlet_id : outlet_id,
        sku_id : e.sku_id,
        quantity : e.quantity,
        created_by : user_id
      });
      
    }

    await db.stock_count.bulkCreate(stock_count)

  } catch(e) {
    // statements
    console.error(e);
    return res.status(500).send('Failed to save stock count.');
  }

  return res.send({status:'success'});

}

module.exports = StockCount;