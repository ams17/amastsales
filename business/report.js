const env = process.env.NODE_ENV || 'dev';
const conf = require('../config/config.json')[env];
var express = require('express');
var router = express.Router();
const db = require('../model/db.js');
const Op = db.Sequelize.Op;
const sq = db.sequelize;

const fetch = require('node-fetch');

const {pipeline} = require('stream');
const {promisify} = require('util');
const streamPipeline = promisify(pipeline);

const limit = 20;

let Report = {};

const qCheckViewInvoicePermission = `select id
from invoices i
where id = :report_id
 and site_id in (select site_id from user_sites us where user_id = :user_id);`

Report.invoice = async function(req,res){
  let {user_id, role} = req.token; 

  let report_name = 'invoice';
  let report_id = req.query.report_id;

  if(!report_id) return res.status(422).send({error:"Please enter a report ID."})

  let invoice;

  try{

    invoice = await sq.query(qCheckViewInvoicePermission,{
        type: sq.QueryTypes.SELECT,
        replacements: {report_id, user_id},
        raw : true
      });
    
    if(!invoice){
      return res.status(404).send({status: 'failed', errMsg: `Invoice #${report_id} not found.`});
    }
    
  }catch(e){
    console.error(e);
    return res.status(500).send({status: 'failed', errMsg: `Failed to get Invoice #${report_id}`})
  }

  let parameters = [
    `REPORT_ID=${report_id}`
  ]

  return downloadReport(req, res, report_name, parameters);
}

const qCheckViewCreditNotePermission = `select id
from credit_notes i
where id = :report_id
 and site_id in (select site_id from user_sites us where user_id = :user_id);`

Report.credit_note = async function(req,res){
  let {user_id, role} = req.token; 

  let report_name = 'credit_notes';
  let report_id = req.query.report_id;

  if(!report_id) return res.status(422).send({error:"Please enter a report ID."})

  let credit_note;

  try{

    credit_note = await sq.query(qCheckViewCreditNotePermission,{
        type: sq.QueryTypes.SELECT,
        replacements: {report_id, user_id},
        raw : true
      });
    
    if(!credit_note){
      return res.status(404).send({status: 'failed', errMsg: `Credit Note #${report_id} not found.`});
    }
    
  }catch(e){
    console.error(e);
    return res.status(500).send({status: 'failed', errMsg: `Failed to get Credit Note #${report_id}`})
  }

  let parameters = [
    `REPORT_ID=${report_id}`
  ]

  return downloadReport(req, res, report_name, parameters);
}

const qCheckViewReceiptPermission = `select id
from receipts i
where id = :report_id
 and site_id in (select site_id from user_sites us where user_id = :user_id);`

Report.receipt = async function(req,res){
  let {user_id, role} = req.token; 

  let report_name = 'receipts';
  let report_id = req.query.report_id;

  if(!report_id) return res.status(422).send({error:"Please enter a receipt ID."})

  let receipt;

  try{

    receipt = await sq.query(qCheckViewReceiptPermission,{
        type: sq.QueryTypes.SELECT,
        replacements: {report_id, user_id},
        raw : true
      });
    console.log(receipt);
    if(!receipt){
      return res.status(404).send({status: 'failed', errMsg: `Receipt #${report_id} not found.`});
    }
    
  }catch(e){
    console.error(e);
    return res.status(500).send({status: 'failed', errMsg: `Failed to get Receipt #${report_id}`})
  }

  let parameters = [
    `Receipt_No=${report_id}`
  ]

  return downloadReport(req, res, report_name, parameters);
}

const qCheckViewOutletAccStmtPermission = `select id
from outlets o
where id = :outlet_id
 and site_id in (select site_id from user_sites us where user_id = :user_id);`

Report.outlet_acc_stmt = async function(req,res){
  let {user_id, role} = req.token; 

  let report_name = 'Statement_of_Account';
  let outlet_id = req.query.outlet_id;

  if(!outlet_id) return res.status(422).send({error:"Please enter a outlet ID."})

  let outlet_acc_stmt;

  try{

    outlet_acc_stmt = await sq.query(qCheckViewOutletAccStmtPermission,{
        type: sq.QueryTypes.SELECT,
        replacements: {outlet_id, user_id},
        raw : true
      });
    
    if(!outlet_acc_stmt){
      return res.status(404).send({status: 'failed', errMsg: `Statement of Account for outlet #${outlet_id} not found.`});
    }
    
  }catch(e){
    console.error(e);
    return res.status(500).send({status: 'failed', errMsg: `Failed to get Statement of Account for outlet #${outlet_id}`})
  }

  let parameters = [
    `OUTLET_ID=${outlet_id}`
  ]
  
  return downloadReport(req, res, report_name, parameters);
}

const qCheckViewVoidInvoicePermission = `select id
from void_invoices i
where id = :report_id
 and site_id in (select site_id from user_sites us where user_id = :user_id);`

Report.void_invoice = async function(req,res){
  let {user_id, role} = req.token; 

  let report_name = 'void_invoice';
  let report_id = req.query.report_id;

  if(!report_id) return res.status(422).send({error:"Please enter a report ID."})

  let void_invoice;

  try{

    void_invoice = await sq.query(qCheckViewVoidInvoicePermission,{
        type: sq.QueryTypes.SELECT,
        replacements: {report_id, user_id},
        raw : true
      });
    
    if(!void_invoice){
      return res.status(404).send({status: 'failed', errMsg: `Void Invoice #${report_id} not found.`});
    }
    
  }catch(e){
    console.error(e);
    return res.status(500).send({status: 'failed', errMsg: `Failed to get Void Invoice #${report_id}`})
  }

  let parameters = [
    `REPORT_ID=${report_id}`
  ]

  return downloadReport(req, res, report_name, parameters);
}

const qCheckViewDebitNotePermission = `select id
from debit_notes i
where id = :report_id
 and site_id in (select site_id from user_sites us where user_id = :user_id);`

Report.debit_note = async function(req,res){
  let {user_id, role} = req.token; 

  let report_name = 'debit_notes';
  let report_id = req.query.report_id;

  if(!report_id) return res.status(422).send({error:"Please enter a report ID."})

  let debit_note;

  try{

    debit_note = await sq.query(qCheckViewDebitNotePermission,{
        type: sq.QueryTypes.SELECT,
        replacements: {report_id, user_id},
        raw : true
      });
    
    if(!debit_note){
      return res.status(404).send({status: 'failed', errMsg: `Debit Note #${report_id} not found.`});
    }
    
  }catch(e){
    console.error(e);
    return res.status(500).send({status: 'failed', errMsg: `Failed to get Debit Note #${report_id}`})
  }

  let parameters = [
    `REPORT_ID=${report_id}`
  ]

  return downloadReport(req, res, report_name, parameters);
}

const qCheckViewRefundPermission = `select id
from refunds i
where id = :report_id
 and id in (select site_id from user_sites us where user_id = :user_id);`

Report.refund = async function(req,res){
  let {user_id, role} = req.token; 

  let report_name = 'refunds';
  let report_id = req.query.report_id;

  if(!report_id) return res.status(422).send({error:"Please enter a report ID."})

  let refund;

  try{

    refund = await sq.query(qCheckViewRefundPermission,{
        type: sq.QueryTypes.SELECT,
        replacements: {report_id, user_id},
        raw : true
      });
    
    if(!refund){
      return res.status(404).send({status: 'failed', errMsg: `Refund #${report_id} not found.`});
    }
    
  }catch(e){
    console.error(e);
    return res.status(500).send({status: 'failed', errMsg: `Failed to get Refund #${report_id}`})
  }

  let parameters = [
    `Refund_No=${report_id}`
  ]

  return downloadReport(req, res, report_name, parameters);
}

const qCheckViewTransferInPermission = `select id
from transfer_ins
where id = :report_id
 and site_id in (select site_id from user_sites us where user_id = :user_id);`

Report.transfer_in = async function(req,res){
  let {user_id, role} = req.token; 

  let report_name = 'transfer_in';
  let report_id = req.query.report_id;

  if(!report_id) return res.status(422).send({error:"Please enter a report ID."})

  let refund;

  try{

    refund = await sq.query(qCheckViewTransferInPermission,{
        type: sq.QueryTypes.SELECT,
        replacements: {report_id, user_id},
        raw : true
      });
    
    if(!refund){
      return res.status(404).send({status: 'failed', errMsg: `Transfer In #${report_id} not found.`});
    }
    
  }catch(e){
    console.error(e);
    return res.status(500).send({status: 'failed', errMsg: `Failed to get Transfer In #${report_id}`})
  }

  let parameters = [
    `TRANSFER_IN_ID=${report_id}`
  ]

  return downloadReport(req, res, report_name, parameters);
}

const qCheckViewTransferOutPermission = `select id
from transfer_outs
where id = :report_id
 and from_site_id in (select site_id from user_sites us where user_id = :user_id);`

Report.transfer_out = async function(req,res){
  let {user_id, role} = req.token; 

  let report_name = 'transfer_out';
  let report_id = req.query.report_id;

  if(!report_id) return res.status(422).send({error:"Please enter a report ID."})

  let refund;

  try{

    refund = await sq.query(qCheckViewTransferOutPermission,{
        type: sq.QueryTypes.SELECT,
        replacements: {report_id, user_id},
        raw : true
      });
    
    if(!refund){
      return res.status(404).send({status: 'failed', errMsg: `Transfer Out #${report_id} not found.`});
    }
    
  }catch(e){
    console.error(e);
    return res.status(500).send({status: 'failed', errMsg: `Failed to get Transfer Out #${report_id}`})
  }

  let parameters = [
    `REPORT_ID=${report_id}`
  ]

  return downloadReport(req, res, report_name, parameters);
}

const qCheckViewWriteOffPermission = `select id
from write_offs
where id = :report_id
 and site_id in (select site_id from user_sites us where user_id = :user_id);`

Report.write_off = async function(req,res){
  let {user_id, role} = req.token; 

  let report_name = 'write_off';
  let report_id = req.query.report_id;

  if(!report_id) return res.status(422).send({error:"Please enter a report ID."})

  let refund;

  try{

    refund = await sq.query(qCheckViewWriteOffPermission,{
        type: sq.QueryTypes.SELECT,
        replacements: {report_id, user_id},
        raw : true
      });
    
    if(!refund){
      return res.status(404).send({status: 'failed', errMsg: `Write Off #${report_id} not found.`});
    }
    
  }catch(e){
    console.error(e);
    return res.status(500).send({status: 'failed', errMsg: `Failed to get Write Off #${report_id}`})
  }

  let parameters = [
    `REPORT_ID=${report_id}`
  ]

  return downloadReport(req, res, report_name, parameters);
}

async function downloadReport(req, res, report_name, parameters){

  let reportServerHost = `${conf.reportServer.host}`;
  let reportServerPort = `${conf.reportServer.port}`;
  let reportUser = `${conf.reportServer.user}`;
  let reportPass = `${conf.reportServer.pass}`;

  let loginApi = `http://${reportServerHost}:${reportServerPort}/jasperserver/rest_v2/login?j_username=${reportUser}&j_password=${reportPass}`;
  let reportApi = `http://${reportServerHost}:${reportServerPort}/jasperserver/rest_v2/reports/reports/${report_name}.pdf`;

  if (parameters.length > 0) reportApi += `?${parameters.join('&')}`;
  
  var options = {
    method: 'POST',
  };

  const loginResponse = await fetch(loginApi, options);
  cookies = loginResponse.headers.get('set-cookie');

  if (loginResponse == null || loginResponse.status != 200) {
    return res.status(422).send({error:"Error connect to Report Server"});
  }
  
  var getOptions = {
    method: 'GET',
    headers: new fetch.Headers([['Cookie', cookies]])
  };

  const reportResponse = await fetch(reportApi, getOptions);

  if (reportResponse == null || reportResponse.status != 200) {

    return res.status(422).send({error:"Report not available"});
  }

  res.setHeader('Content-disposition', `attachment; filename=report.pdf`);
  res.set('Content-Type', 'application/pdf');

  return await streamPipeline(reportResponse.body, res);
}

module.exports = Report;