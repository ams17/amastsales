const env = process.env.NODE_ENV || 'dev';
const conf = require('../config/config.json')[env];
const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const {Parser} = require('json2csv');
const jwtAuth = require('../common/jwtAuth');
const Password = require('../common/password');
const db = require('../model/db.js');
const Op = db.Sequelize.Op;
const sq = db.sequelize;
const df = 'YYYY-MM-DD';
const limit = 10;

const Notifications = {};

const qNotificationCount = `select count(1)
from (
  with sites as(
     select site_id from user_sites us, sites s where s.id = us.site_id and us.user_id = :user_id
  ) 
  select 'vr', date,id, van_id, site_id, status from "returns" where status = 'pending' and site_id in (select site_id from sites)
  union all
  select 'tr', date, id, from_site_id, to_site_id, status from transfer_outs where status = 'in transit' and to_site_id in(select site_id from sites)
) as a`;

Notifications.count = async function(req,res){
  let {user_id} = req.token;
  let notification = {};
  try {
    let data = await sq.query(qNotificationCount,{
      type: sq.QueryTypes.SELECT,
      replacements: {user_id},
      raw: true
    });

    notification.count = data[0].count;

  } catch(e) {
    console.error(e);
    return res.status(500).send({errMsg:'Failed to get Notification Count.'});
  }
  res.send({notification, desc: 'Notification count'});
}

const qNotificationList = `select type, date, id, sender, receiver, status
from (
  with sites as(
     select site_id from user_sites us, sites s where s.id = us.site_id and us.user_id = :user_id
  ) 
  select 'vr' as type, date, id, van_id as sender, site_id as receiver, status from "returns" where status = 'pending' and site_id in (select site_id from sites)
  union all
  select 'tr' as type, date, id, from_site_id as sender, to_site_id as receiver, status from transfer_outs where status = 'in transit' and to_site_id in(select site_id from sites)
) as a
where (
        :keyword IS null OR 
        UPPER(id) LIKE '%' || UPPER(:keyword) || '%' OR 
        UPPER(status) LIKE '%' || UPPER(:keyword) || '%'
      )
ORDER BY date desc
LIMIT :limit 
OFFSET :offset`;

Notifications.list = async function(req,res){
  let {user_id} = req.token;
  let {keyword, page}  = req.query;

  page = (!page || isNaN(page) || parseInt(page) < 0)? 0 : parseInt(page) - 1;
  let offset = page * limit;

  if (!keyword) {
    keyword = null;
  }

  let notifications = {};
  try {
    let data = await sq.query(qNotificationList,{
      type: sq.QueryTypes.SELECT,
      replacements: {user_id, limit, offset, page, keyword},
      raw: true
    });

    notifications.offset = offset;
    notifications.limit= limit;
    notifications.count = data.length;
    notifications.rows = data;

  } catch(e) {
    console.error(e);
    return res.status(500).send({errMsg:'Failed to get Notification List.'});
  }
  res.send({notifications, desc: 'Notification list'});
}

const qNotificationDetail = `select *
from (
  with sites as(
     select site_id from user_sites us, sites s where s.id = us.site_id and us.user_id = :user_id
  ) 
  select 'vr' as type, date,id, van_id as sender, site_id as receiver, status from "returns" where status = 'pending' and site_id in (select site_id from sites)
  union all
  select 'tr' as type, date, id, from_site_id as sender, to_site_id as receiver, status from transfer_outs where status = 'in transit' and to_site_id in(select site_id from sites)
) as a
where id = :id`;

Notifications.read = async function(req,res){
  let {user_id} = req.token;
  let {id}  = req.params;
  let notification = {};
  try {
    let data = await sq.query(qNotificationDetail,{
      type: sq.QueryTypes.SELECT,
      replacements: {user_id, id},
      raw: true
    });

    notification = data[0];

  } catch(e) {
    console.error(e);
    return res.status(500).send({errMsg:'Failed to get Notification detail.'});
  }
  res.send({notification, desc: 'Notification detail'});
}

module.exports = Notifications;