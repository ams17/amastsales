const env = process.env.NODE_ENV || 'dev';
const conf = require('../config/config.json')[env];
const express = require('express');
const router = express.Router();
const dayjs = require('dayjs'); 
const bcrypt = require('bcrypt');
const {Parser} = require('json2csv');
const jwtAuth = require('../common/jwtAuth');
const Constants = require('../common/constants');
const Password = require('../common/password');
const db = require('../model/db.js');
const Op = db.Sequelize.Op;
const sq = db.sequelize;
const df = 'YYYY-MM-DD';
const limit = 20;

let WriteOff= {};

const qSkus = `select json_object_agg(sku_Id, to_uom_id) as uoms from sku_uom_conversions where multiply_by = 1;`

const qNextWoID = `select nextVal('seq_write_off') as WoID; `;

const qWriteOffInsertILedger = `INSERT INTO public.inventory_ledger (id, site_id, "date", sku_id, uom_id, "condition", operation, reference_id, prev_quantity, movement, new_quantity, "current", description, created_by, created_at, updated_at)
  select uuid_generate_v4() as id, :site_id as site_id, current_date, wod.sku_id, wod.uom_id, wod."condition", 'write off' as operation, :woID as reference_id, coalesce(il.new_quantity, 0) as prev_quantity, -wod.quantity, coalesce(il.new_quantity, 0) - wod.quantity as new_quantity, true as current, :remark as description, :user_id as created_by, current_timestamp, current_timestamp 
  from write_off_details wod
  left join inventory_ledger il 
  on (wod.sku_id = il.sku_id and wod.uom_id = il.uom_id and wod."condition" = il."condition")
  and il.current = true
  and il.site_id = :site_id 
  where wod.write_off_id = :woID;`

const qWriteOffUpdateICurrentLedger = `update inventory_ledger il
  set "current" = false
  from write_off_details wod 
  where (wod.sku_id = il.sku_id and wod.uom_id = il.uom_id and wod."condition" = il."condition")
  and reference_id != wod.write_off_id
  and il.site_id = :site_id 
  and wod.write_off_id = :woID
  and current = true;`

const qWriteOffUpdateILDaily = `update inventory_ledger_daily ild set movement = ild.movement + il.movement, new_quantity = il.new_quantity 
  from inventory_ledger il
  where il.current = true
  and reference_id = :woID
  and (il.site_id = ild.site_id and il.date = ild.date and il.sku_id = ild.sku_id and il.uom_id = ild.uom_id and il.condition = ild.condition);`

const qWriteOffInsertILDaily = `INSERT INTO public.inventory_ledger_daily ( site_id, "date", sku_id, uom_id, "condition", prev_quantity, movement, new_quantity, created_at, updated_at)
  select 
    il.site_id , il.date, il.sku_id, il.uom_id, il."condition", 
    coalesce(il.prev_quantity, 0) as prev_quantity, 
    il.movement, 
    coalesce(il.prev_quantity, 0) + il.movement as new_quantity, 
    current_timestamp, current_timestamp 
  from inventory_ledger il
  where il.current = true
  and reference_id = :woID
  and (site_id, date, sku_id, uom_id, condition) not in (select site_id, date, sku_id, uom_id, condition from inventory_ledger_daily ild );`

const qWriteOffUpdateInventory = `update inventory i set quantity = il.new_quantity, updated_at = current_timestamp, updated_by = :user_id
  from inventory_ledger il 
  where reference_id = :woID
  and (i.sku_id = il.sku_id and i.uom_id = il.uom_id and i."condition" = il."condition" and i.site_id = il.site_id);`;

const qInventory= `select json_object_agg(sku_id,inventory) as inventory from (
  select sku_id ,json_object_agg(condition ,quantity) as inventory from (
    with reserved_stock as (      
      select vad.sku_id, vad.uom_id, 'fresh'::text as condition, sum(vad.allotted_quantity) as quantity from van_allot va 
      inner join van_allot_details vad on vad.van_allot_id = va.id and status in ('sent for picking', 'packed')
      inner join vans v on v.id = va.van_id and va.site_id = :site_id
      group by vad.sku_id, vad.uom_id
    )
    select i.sku_id, i.uom_id, i.condition, i.quantity, coalesce(rs.quantity, 0) reserved_quantity, i.quantity - coalesce(rs.quantity, 0) available_quantity 
    from inventory i 
    left join reserved_stock rs on rs.sku_id = i.sku_id and rs.uom_id = i.sku_id and rs.condition = i.condition 
    where site_id = :site_id
    and (:inventoryConditions)
    order by sku_id, case 
      when i.condition = 'fresh' then 1 
      when i.condition = 'damaged' then 2 
      when i.condition = 'old' then 3 
      else 4 end
  ) a group by sku_id
) b`;

WriteOff.create = async function (req,res) {
  let skus = req.body.skus;
  let site_id = req.body.site_id;
  let remark = req.body.remark;
  let {user_id,role} = req.token;
  let dateToday = dayjs().format(df);
  let woID;

  if(!skus || skus.length == 0) return res.status(422).send({errMsg:'Please enter an sku.'});

  if(!site_id) return res.status(422).send({errMsg:'Please enter a site ID.'});

  let writeOff;
  let data = [];
  let inventoryConditions = [];
  let user_site;
  let inventory;
  let transaction;

  try {
    
    if(role != 'superadmin'){
      user_site = await db.user_sites.findOne({
        where:{
          user_id : { [Op.eq] : user_id },
          site_id : { [Op.eq] : site_id }
        }
      });
      if(!user_site) return res.status(404).send({errMsg:'Invalid site ID for user.'});
    }  

    let uoms = await sq.query(qSkus,{type : sq.QueryTypes.SELECT, raw: true});
    uoms = uoms[0].uoms;

    woID = await sq.query(qNextWoID,{type: sq.QueryTypes.SELECT, raw: true});

    woID = `WO_${site_id}_${woID[0].woid}`;

    for(let e of skus){
      if(!uoms[e.sku_id] || uoms[e.sku_id] == null){
        return res.status(404).send({errMsg:`Base UOM for ${e.skuid} is not found.`});
      }

      for(let i = 0; i < e.quantity.length; i++){
        if(e.quantity[i] <= 0) continue;

        data.push({
          write_off_id : woID,
          sku_id : e.sku_id,
          uom_id : uoms[e.sku_id],
          condition : Constants.conditions[i],
          quantity : e.quantity[i]
        });

        inventoryConditions.push(`(i.sku_id = '${e.sku_id}' and i.uom_id = '${uoms[e.sku_id]}' and i.condition = '${Constants.conditions[i]}')`);
      }
    }

    inventoryConditions = inventoryConditions.join('or');
    let qInvt = qInventory.replace(':inventoryConditions', inventoryConditions);

    transaction = await sq.transaction();

    inventory = await sq.query(qInvt,{
      type: sq.QueryTypes.SELECT,
      replacements : {site_id},
      raw : true,
      transaction
    });

    inventory = inventory[0].inventory;
    if(!inventory) throw 'Inventory empty';

    for(let e of skus){
      for(let i = 0; i < e.quantity.length; i++){
        let qty = e.quantity[i];
        let sku_id = inventory[e.sku_id];
        let condition = Constants.conditions[i];

        if(qty <= 0) continue;
        if(!sku_id) throw `Insufficient stock for ${inventory[e.sku_id]}`;
        if(!sku_id[condition]) throw `Insufficient stock for ${condition} ${sku_id}`;
        if(sku_id[condition] < qty) throw `Current stock balance for ${e.sku_id} is ${sku_id[condition]}. Cannot transfer out more.`;  
      }
    }
    
    writeOff = await db.write_offs.create({
      id : woID,
      date : dateToday,
      site_id : site_id,
      remark : remark,
      created_by : user_id
    },{ transaction });

    
    await db.write_off_details.bulkCreate(data,{transaction});

    
    await sq.query(qWriteOffInsertILedger,{
      type: sq.QueryTypes.INSERT,
      replacements: {site_id,woID,remark,user_id},
      transaction
    })

    await sq.query(qWriteOffUpdateICurrentLedger,{
      type: sq.QueryTypes.UPDATE,
      replacements: {site_id,woID},
      transaction
    });

    await sq.query(qWriteOffUpdateILDaily,{
      type: sq.QueryTypes.UPDATE,
      replacements: {woID},
      transaction
    });
    
    await sq.query(qWriteOffInsertILDaily,{
      type: sq.QueryTypes.INSERT,
      replacements: {woID},
      transaction
    })

    await sq.query(qWriteOffUpdateInventory,{
      type: sq.QueryTypes.UPDATE,
      replacements: {woID, user_id},
      transaction
    });

    await transaction.commit();

  } catch(e) {
    // statements
    console.error(e);
    if(transaction) await transaction.rollback();
    return res.status(500).send({errMsg:`Failed to Write Off. ${e}`})
  }

  return res.send({writeOff, msg: 'Write-off successful.'});
}

WriteOff.list = async function(req,res){
  let page = req.query.page;
  let keyword = req.query.keyword;
  let site_id =  req.query.site_id || 'all';
  let date = req.query.date;
  let where = {};

  page = (!page || isNaN(page) || parseInt(page) < 0)? 0 : parseInt(page) - 1;
  let offset = page * limit;

  let writeOff;

  if(keyword) where.id = {[Op.iLike] : `%${keyword}%`};
  if(site_id != 'all') where.site_id = {[Op.eq] : site_id}

  if(date){
    let d = date.split(',');

    if(d.length== 1){
      if(!dayjs(d[0], df).isValid()) return res.status(422).send({errMsg: 'Date format is invalid.'});
      where.date = {[Op.eq]: d[0]};
    }else{
      if(!dayjs(d[0], df).isValid() || !dayjs(d[1], df).isValid()) return res.status(422).send({errMsg: 'Date format is invalid.'});
      where.date = { [Op.between]:[ d[0].format(df), d[1].format(df) ] }
    }
  }

  try {
    writeOff = await db.write_offs.findAndCountAll({
      attributes: ['id', 'date', 'site_id', 'created_by', 'created_at'],
      order : [['created_at','DESC']],
      where,offset,limit,
      raw : true
    });

    writeOff.limit = limit;
    writeOff.offset = offset;

  } catch(e) {
    // statements
    console.error(e);
    return res.status(500).send({errMsg:'Failed to get Write Off list.'})
  }

  return res.send({writeOff});
}

const qWriteOffDetails = `select sku_id, uom_id, array_to_json(array[max(fresh_qty), max(damaged_qty), max(old_qty), max(recalled_qty)]) as quantity from 
(with ti as (select * from write_off_details tid where write_off_id = :woid)
select sku_id, uom_id, quantity fresh_qty, 0 damaged_qty, 0 old_qty, 0 recalled_qty from ti where condition = 'fresh'
union select sku_id, uom_id, 0 fresh_qty, quantity damaged_qty, 0 old_qty, 0 recalled_qty from ti where condition = 'damaged'
union select sku_id, uom_id, 0 fresh_qty, quantity damaged_qty, 0 old_qty, 0 recalled_qty from ti where condition = 'old'
union select sku_id, uom_id, 0 fresh_qty, quantity damaged_qty, 0 old_qty, 0 recalled_qty from ti where condition = 'recalled'
) a group by sku_id, uom_id order by sku_id`;

WriteOff.details = async function(req,res){
  let woid = req.params.woid;
  let {user_id,role} = req.token;

  let write_off;
  let details;

  if(!woid) return res.status(422).send({errMsg:'Please enter a Write Off ID.'});

  try{

    write_off = await db.write_offs.findOne({ where:{ id: {[Op.eq]: woid}}, raw: true });
    if(!write_off) return res.status(404).send({errmsg: 'Write Off ID not found.'});

    details = await sq.query(qWriteOffDetails, {
      type : sq.QueryTypes.SELECT, 
      replacements: {woid},
      raw: true
    });


  }catch(e){
    console.error(e)
    return res.status(500).send({errMsg:'Failed to get Write Off details.'})
  }

  return res.send({write_off, details});
}

module.exports = WriteOff;