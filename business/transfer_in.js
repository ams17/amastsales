const env = process.env.NODE_ENV || 'dev';
const conf = require('../config/config.json')[env];
const express = require('express');
const router = express.Router();
const dayjs = require('dayjs'); 
const bcrypt = require('bcrypt');
const {Parser} = require('json2csv');
const jwtAuth = require('../common/jwtAuth');
const Constants = require('../common/constants');
const Password = require('../common/password');
const db = require('../model/db.js');
const Op = db.Sequelize.Op;
const sq = db.sequelize;
const df = 'YYYY-MM-DD';
const limit = 20;

let TrxIn = {};

const qSkus = `select json_object_agg(sku_Id, to_uom_id) as uoms from sku_uom_conversions where multiply_by = 1;`

const qNextTiID = `select nextVal('seq_transfer_in') as tiID; `;

const qTrxInInsertILedger = `INSERT INTO public.inventory_ledger (
    id, site_id, "date", sku_id, uom_id, "condition", operation, reference_id, 
    prev_quantity, movement, new_quantity, "current", description, created_by, created_at, updated_at
  )
  select uuid_generate_v4() as id, :site_id as site_id, current_date, tid.sku_id, tid.uom_id, tid."condition", 
    'transfer in' as operation, :tiID as reference_id, coalesce(il.new_quantity, 0) as prev_quantity, tid.quantity, 
    tid.quantity + coalesce(il.new_quantity, 0) as new_quantity, true as current, :remark as description, 
    :user_id as created_by, current_timestamp, current_timestamp
  from transfer_in_details tid
  left join inventory_ledger il 
  on (tid.sku_id = il.sku_id and tid.uom_id = il.uom_id and tid."condition" = il."condition")
  and il.current = true
  and il.site_id = :site_id 
  where receiving = true and tid.transfer_in_id = :tiID;`

const qTrxInUpdateICurrentLedger = `update inventory_ledger il
  set "current" = false
  from transfer_in_details tid 
  where (tid.sku_id = il.sku_id and tid.uom_id = il.uom_id and tid."condition" = il."condition")
  and reference_id != tid.transfer_in_id
  and il.site_id = :site_id 
  and tid.transfer_in_id = :tiID
  and tid.receiving = true
  and current = true;`

const qTrxInUpdateILDaily = `update inventory_ledger_daily ild set movement = ild.movement + il.movement, new_quantity = il.new_quantity 
  from inventory_ledger il
  where il.current = true
  and reference_id = :tiID
  and (il.site_id = ild.site_id and il.date = ild.date and il.sku_id = ild.sku_id and il.uom_id = ild.uom_id and il.condition = ild.condition);`

const qTrxInInsertILDaily = `INSERT INTO public.inventory_ledger_daily ( site_id, "date", sku_id, uom_id, "condition", prev_quantity, movement, new_quantity, created_at, updated_at)
  select 
    il.site_id , il.date, il.sku_id, il.uom_id, il."condition", 
    coalesce(il.prev_quantity, 0) as prev_quantity, 
    il.movement, 
    coalesce(il.prev_quantity, 0) + il.movement as new_quantity, 
    current_timestamp, current_timestamp 
  from inventory_ledger il
  where il.current = true
  and reference_id = :tiID
  and (site_id, date, sku_id, uom_id, condition) not in (select site_id, date, sku_id, uom_id, condition from inventory_ledger_daily ild );`

const qTrxInUpdateInventory = `update inventory i set quantity = il.new_quantity 
  from inventory_ledger il 
  where reference_id = :tiID
  and (i.sku_id = il.sku_id and i.uom_id = il.uom_id and i."condition" = il."condition" and i.site_id = il.site_id);`;

TrxIn.create = async function(req,res){
  let {site_id, type, ref_id, remark, receivingSkus, nonReceivingSkus} = req.body;
  let {user_id,role} = req.token;
  let dateToday = dayjs().format(df);
  let tiID;

  console.log(req.body);

  if(!type) return res.status(422).send({errMsg:'Please enter a receiving type.'});
  if(!site_id) return res.status(422).send({errMsg:'Please enter a site ID.'});
  if(!ref_id) return res.status(422).send({errMsg:'Please enter a reference ID.'});
  if(!remark) return res.status(422).send({errMsg:'Please enter a remark.'});
  if(!receivingSkus || receivingSkus.length == 0) return res.status(422).send({errMsg:'Please enter receiving sku quantity.'});
  
  let ti, tOut, vRet;
  let data = [];
  let user_site;
  let transaction;

  try {

    if(role != 'superadmin'){
      user_site = await db.user_sites.findOne({
        where:{
          user_id : { [Op.eq] : user_id },
          site_id : { [Op.eq] : site_id }
        }
      });
      if(!user_site) return res.status(404).send({errMsg:'Invalid site ID for user.'});
    }
      
    let uoms = await sq.query(qSkus,{type : sq.QueryTypes.SELECT, raw: true});
    uoms = uoms[0].uoms;

    tiID = await sq.query(qNextTiID,{type: sq.QueryTypes.SELECT, raw: true});

    tiID = `IN_${site_id}_${tiID[0].tiid}`;

    function loadData(arr, receiving){
      for(let e of arr){
        if(!uoms[e.sku_id] || uoms[e.sku_id] == null) {
          return res.status(404).send({errMsg:`Base UOM for ${e.sku_id} is not found.`});
        }

        for(let i = 0; i < e.quantity.length; i++){
          if(e.quantity[i] <= 0) continue;        

          data.push({
            transfer_in_id : tiID,
            receiving: receiving,
            sku_id: e.sku_id,
            uom_id: uoms[e.sku_id],
            condition: Constants.conditions[i],
            quantity: e.quantity[i]
          });
        }
      }
    }

    loadData(receivingSkus, true);
    loadData(nonReceivingSkus, false);

    if(data.length == 0){
      return res.status(422).send({errMsg: `SKU quantity cannot be 0.`})
    }

    if(type == 'transfer'){
      tOut = await db.transfer_outs.findOne({
        where: {id: {[Op.eq]: ref_id}, status: 'in transit'}
      });

      if(!tOut) return res.status(404).send({errMsg: `Transfer Out id not found.`});
      tOut.status = 'received';

    } else if (type == 'van return'){
      vRet = await db.returns.findOne({
        where: {id: {[Op.eq]: ref_id}, status: 'pending'}
      });

      if(!vRet) return res.status(404).send({errMsg: `Return id not found.`});
      vRet.status = 'received';
    }

    transaction = await sq.transaction();

    ti = await db.transfer_ins.create({
      id : tiID,
      date : dateToday,
      site_id : site_id,
      type: type, 
      ref_id: ref_id,
      remark : remark,
      created_by : user_id
    },{ transaction, raw: true });

    if(tOut) await tOut.save({transaction});
    if(vRet) await vRet.save({transaction});

    await db.transfer_in_details.bulkCreate(data,{transaction});

    ///TODO
    await sq.query(qTrxInInsertILedger,{
      type: sq.QueryTypes.INSERT,
      replacements: {site_id,tiID,remark,user_id},
      transaction
    })

    await sq.query(qTrxInUpdateICurrentLedger,{
      type: sq.QueryTypes.UPDATE,
      replacements: {site_id,tiID},
      transaction
    });

    await sq.query(qTrxInUpdateILDaily,{
      type: sq.QueryTypes.UPDATE,
      replacements: {tiID},
      transaction
    });

    await sq.query(qTrxInInsertILDaily,{
      type: sq.QueryTypes.INSERT,
      replacements: {tiID},
      transaction
    })

    await sq.query(qTrxInUpdateInventory,{
      type: sq.QueryTypes.UPDATE,
      replacements: {tiID},
      transaction
    });

    await transaction.commit();

  } catch(e) {
    console.error(e);
    if(transaction) await transaction.rollback();
    return res.status(500).send({errMsg:'Failed to Transfer In.'});
  }

  return res.send({ti, msg: 'Transfer In created successfully.'});
}

TrxIn.list = async function(req,res){
  let page = req.query.page;
  let keyword = req.query.keyword;
  let site_id =  req.query.site_id || 'all';
  let date = req.query.date;
  let where = {};

  page = (!page || isNaN(page) || parseInt(page) < 0)? 0 : parseInt(page) - 1;
  let offset = page * limit;

  let transferIn;

  if(keyword) where.id = {[Op.iLike] : `%${keyword}%`};
  if(site_id != 'all') where.site_id = {[Op.eq] : site_id}

  if(date){
    let d = date.split(',');

    if(d.length== 1){
      if(!dayjs(d[0], df).isValid()) return res.status(422).send({errMsg: 'Date format is invalid.'});
      where.date = {[Op.eq]: d[0]};
    }else{
      if(!dayjs(d[0], df).isValid() || !dayjs(d[1], df).isValid()) return res.status(422).send({errMsg: 'Date format is invalid.'});
      where.date = { [Op.between]:[ d[0].format(df), d[1].format(df) ] }
    }
  }

  try {
    transferIn = await db.transfer_ins.findAndCountAll({
      attributes: ['id', 'date', 'site_id', 'created_by', 'created_at'],
      order : [['created_at','DESC']],
      where,offset,limit,
      raw : true
    });

    transferIn.limit = limit;
    transferIn.offset = offset;

  } catch(e) {
    // statements
    console.error(e);
    return res.status(500).send({errMsg:'Failed to get Transfer In list.'})
  }

  return res.send({transferIn});
}

const qTransferInDetails = `select sku_id, uom_id, array_to_json(array[max(fresh_qty), max(damaged_qty), max(old_qty), max(recalled_qty)]) as quantity from 
(with ti as (select * from transfer_in_details tid where transfer_in_id = :tiid and receiving = :receiving)
select sku_id, uom_id, quantity fresh_qty, 0 damaged_qty, 0 old_qty, 0 recalled_qty from ti where condition = 'fresh'
union select sku_id, uom_id, 0 fresh_qty, quantity damaged_qty, 0 old_qty, 0 recalled_qty from ti where condition = 'damaged'
union select sku_id, uom_id, 0 fresh_qty, quantity damaged_qty, 0 old_qty, 0 recalled_qty from ti where condition = 'old'
union select sku_id, uom_id, 0 fresh_qty, quantity damaged_qty, 0 old_qty, 0 recalled_qty from ti where condition = 'recalled'
) a group by sku_id, uom_id order by sku_id`;

TrxIn.details = async function(req,res){
  let tiid = req.params.tiid;
  let {user_id,role} = req.token;

  let transfer_in;
  let received, unreceived;

  if(!tiid) return res.status(422).send({errMsg:'Please enter a Transfer In ID.'});

  try{

    transfer_in = await db.transfer_ins.findOne({
      where:{
        id: {[Op.eq] : tiid}
      },
      raw : true
    });

    if(!transfer_in) return res.status(404).send({errMsg: 'Transfer In ID not found.'});

    received = await sq.query(qTransferInDetails, {
      type : sq.QueryTypes.SELECT, 
      replacements: {tiid, receiving: true},
      raw: true
    });

    unreceived = await sq.query(qTransferInDetails, {
      type : sq.QueryTypes.SELECT, 
      replacements: {tiid, receiving: false},
      raw: true
    });

  }catch(e){
    console.error(e)
    return res.status(500).send({errMsg:'Failed to get Transfer In details.'})
  }

  return res.send({transfer_in, received, unreceived});
}

module.exports = TrxIn;