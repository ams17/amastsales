const env = process.env.NODE_ENV || 'dev';
const conf = require('../config/config.json')[env];
const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const {Parser} = require('json2csv');
const dayjs = require('dayjs');
const jwtAuth = require('../common/jwtAuth');
const Password = require('../common/password');
const db = require('../model/db.js');
const Op = db.Sequelize.Op;
const sq = db.sequelize;
const df = 'YYYY-MM-DD';
const limit = 20;

let  DeviceManagement = {};

DeviceManagement.list = async function(req,res){

  // console.log('getting api');
  let page = req.query.page;
  let uom_id = req.query.uom_id || 'all';
  let pg_id = req.query.pr_id || 'all';
  let {price_at,keyword} = req.query;
  let searchby = req.query.searchby || 'userid';
  let where = {};

  page = (!page || isNaN(page) || parseInt(page) < 0)? 0 : parseInt(page) - 1;
  let offset = page * limit;

  if(searchby == 'userid' && keyword)  where.device_id = {[Op.iLike]: `${keyword}%`};

  if(price_at){
    let p_at = price_at.split(',');

    if(p_at.length==1){
      if(!dayjs(p_at[0], df).isValid()) return res.status(422).send({errMsg: 'Date format is invalid.'});

      where.created_by = {[Op.lte]: p_at[0]};
    }else{
      if(!dayjs(p_at[0], df).isValid() || !dayjs(p_at[1], df).isValid()) return res.status(422).send({errMsg: 'Date format is invalid.'});

      where.created_by = {[Op.between]:[
        dayjs(p_at,df),
        dayjs(p_at,df)
      ]};
    }
  }

  let deviceList;
  try{
    deviceList = await db.device_management.findAndCountAll({
      attributes: [['device_id','id'], 'ime_id', 'made', 'model','year_purchased','tel_no','status','created_by'],
      where,limit,offset,
      raw : true
    })
    deviceList.limit = limit;
    deviceList.offset = offset;
  }catch(e){
    console.error(e)
    return res.status(500).send({errMsg:'Failed to get device list.'});
  }

  return res.send({status:'success', deviceList});
};

const qPriceList = `select sku_id,array_agg(json_build_object('price_group_id',price_group_id,'price',price)) as DeviceManagement from price_list pl
  where effective_at <= current_timestamp
  group by sku_id `;

DeviceManagement.AList = async function(req,res){
  let rows;

  try{
    rows = await sq.query(qPriceList,{
      type: sq.QueryTypes.SELECT,
      raw: true
    });
  }catch(e){
    console.error(e)
    return res.status(500).send({error: "Failed to get price list."})
  }

  return res.send({
    data:{
      count: rows.length,
      rows: rows
    }
  })
}


const yearQuery = `select id,value,display,is_default,active,sequence from lovs where group_id  = 'year' and active = true order by "sequence" asc`;
const madeQuery = `select id,value,display,is_default,active,sequence from lovs where group_id  = 'device_made' and active = true order by "sequence" asc`;
const modelQuery = `select id,value,display,is_default,active,sequence from lovs where group_id  = 'device_model' and active = true order by "sequence" asc`;
const statusQuery = `select id,value,display,is_default,active,sequence from lovs where group_id  = 'device_status' and active = true order by "sequence" asc`;

DeviceManagement.getDetails = async function(req,res){
    let rows;
    try{
        yearRows = await sq.query(yearQuery,{
            type: sq.QueryTypes.SELECT,
            raw: true
        });
        madeRows = await sq.query(madeQuery,{
            type: sq.QueryTypes.SELECT,
            raw: true
        });
        modelRows = await sq.query(modelQuery,{
            type: sq.QueryTypes.SELECT,
            raw: true
        });
        statusRows = await sq.query(statusQuery,{
            type: sq.QueryTypes.SELECT,
            raw: true
        });
    }catch(e){
        console.error(e)
        return res.status(500).send({error: "Failed to get device list."})
    }

    return res.send({
        data:{
            yearCount: yearRows.length,
            yearRows: yearRows,
            madeCount: madeRows.length,
            madeRows: madeRows,
            modelCount: modelRows.length,
            modelRows: modelRows,
            statusCount: statusRows.length,
            statusRows: statusRows,
        }
    });
}

DeviceManagement.create = async function(req,res){
    let {role, user_id} = req.token;  

    let uuID = req.body.uuID;
    let imeID = req.body.imeID;
    let made = req.body.made;
    let model = req.body.model;
    let telNo = req.body.telNo;
    let yearPurchased = req.body.yearPurchased;
    let status = req.body.status;

    if(!uuID) return res.status(500).send({errMsg: 'Please enter UU ID'});
    if(!imeID) return res.status(500).send({errMsg: 'Please enter IME ID'});
    if(!made) return res.status(500).send({errMsg: 'Please select made.'});
    if(!model) return res.status(500).send({errMsg: 'Please select model.'});
    if(!telNo) return res.status(500).send({errMsg: 'Please enter Tel No'});
    if(!yearPurchased) return res.status(500).send({errMsg: 'Please select year purchased'});
    if(!status) return res.status(500).send({errMsg: 'Please select status'});

    let DeviceManagement;
    try {
        let notUniqueDeviceID = await db.device_management.findOne({ where:{ device_id: {[Op.eq]: uuID} }});
        if(notUniqueDeviceID) return res.status(422).send({status:'failed', errMsg:'UU ID is already taken. Please try another UU ID.'})

        let notUniqueIMEID = await db.device_management.findOne({ where:{ ime_id: {[Op.eq]: imeID} }});
        if(notUniqueIMEID) return res.status(422).send({status:'failed', errMsg:'IME ID is already taken. Please try another IME ID.'})

        DeviceManagement = await db.device_management.create({
            device_id: uuID,
            ime_id : imeID,
            made : made,
            model : model,
            year_purchased : yearPurchased,
            tel_no : telNo,
            status : status,
            created_by: req.token.user_id,
            updated_by: req.token.user_id,
            created_at: sq.now,
            // updated_at: sq.now      
        });
    } catch(e) {
        console.error(e);
        return res.status(500).send({errMsg: 'Failed to create device.'});
    }
    return res.send({DeviceManagement});
}

DeviceManagement.update = async function(req, res){
  let id = req.body.rowId;
  let {role, user_id} = req.token;
  // console.log(req.body);return false;
  let device;
  try{
    device = await db.device_management.findOne({ 
      where: {device_id: {[Op.eq]: id}} 
    });
    if(!device) return res.status(404).send({errMsg: 'Price not found.'});

    if('uuID' in req.body){
      let uuID = req.body.uuID;
      if(uuID !== null && uuID.trim().length == 0) {
        return res.status(422).send({errMsg: 'UU ID cannot be empty.'})
      }
      device.device_id = uuID;
    }
    if('telNo' in req.body){
      let telNo = req.body.telNo;
      if(telNo !== null && telNo.trim().length == 0) {
        return res.status(422).send({errMsg: 'Tel No cannot be empty.'})
      }
      device.tel_no = telNo;
    }
    if('imeID' in req.body){
      let imeID = req.body.imeID;
      if(imeID !== null && imeID.trim().length == 0) {
        return res.status(422).send({errMsg: 'IME ID cannot be empty.'})
      }
      device.ime_id = imeID;
    }
    if('yearPurchased' in req.body){
      let yearPurchased = req.body.yearPurchased;
      if(yearPurchased !== null && yearPurchased.trim().length == 0) {
        return res.status(422).send({errMsg: 'Year purchased cannot be empty.'})
      }
      device.year_purchased = yearPurchased;
    }
    if('made' in req.body){
      let made = req.body.made;
      if(made !== null && made.trim().length == 0) {
        return res.status(422).send({errMsg: 'Made cannot be empty.'})
      }
      device.made = made;
    }
    if('status' in req.body){
      let status = req.body.status;
      if(status !== null && status.trim().length == 0) {
        return res.status(422).send({errMsg: 'Status cannot be empty.'})
      }
      device.status = status;
    }
    if('model' in req.body){
      let model = req.body.model;
      if(model !== null && model.trim().length == 0) {
        return res.status(422).send({errMsg: 'Model cannot be empty.'})
      }
      device.model = model;
    }

    device.updated_by =  user_id;
    device.updated_at =  sq.now
    await device.save();
    console.log(device);
  }catch(e){
    console.error(e);
    return res.status(500).send({errMsg: 'Failed to update user.'});
  }

  return res.send({device});
}

DeviceManagement.delete = async function(req, res){
  let id  = req.body.id;
  let {user_id,role} = req.token;

  if(!id) return res.status().send({errMsg:'Price ID not provided'});

  let price;

  try {
    price = await db.price_list.findOne({
      where: {
        id : {[Op.eq]: id}
      }
    })
    
    if(!price) return res.status(404).send({status: 'failed', errMsg: `User #${id} not found.`});
    price.active = false;
    price.save();
    // user.destroy();

  } catch(e) {
    // statements
    console.error(e);
  }

  return res.send({msg:'User deleted successfully.'})
}

DeviceManagement.read = async function(req, res){
  let id = req.params.id;

  let device;
  try{
    device = await db.device_management.findOne({
      where: {device_id: {[Op.eq]: id.toLowerCase()} },
      raw: true
    });

    yearRows = await sq.query(yearQuery,{
        type: sq.QueryTypes.SELECT,
        raw: true
    });
    madeRows = await sq.query(madeQuery,{
        type: sq.QueryTypes.SELECT,
        raw: true
    });
    modelRows = await sq.query(modelQuery,{
        type: sq.QueryTypes.SELECT,
        raw: true
    });
    statusRows = await sq.query(statusQuery,{
        type: sq.QueryTypes.SELECT,
        raw: true
    });


    if(!device) return res.status(404).send({errMsg: 'User not found.'});
  }catch(e){
    console.error(e);
    return res.status(500).send({errMsg: 'Failed to find user.'})
  }

  return res.send({
    data:{
        yearCount: yearRows.length,
        yearRows: yearRows,
        madeCount: madeRows.length,
        madeRows: madeRows,
        modelCount: modelRows.length,
        modelRows: modelRows,
        statusCount: statusRows.length,
        statusRows: statusRows,
        device : device
    }
  });
}
module.exports = DeviceManagement;