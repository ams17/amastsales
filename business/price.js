const env = process.env.NODE_ENV || 'dev';
const conf = require('../config/config.json')[env];
const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const {Parser} = require('json2csv');
const dayjs = require('dayjs');
const jwtAuth = require('../common/jwtAuth');
const Password = require('../common/password');
const db = require('../model/db.js');
const Op = db.Sequelize.Op;
const sq = db.sequelize;
const df = 'YYYY-MM-DD';
const limit = 20;

let  Prices = {};

Prices.list = async function(req,res){

  // console.log('getting api');
  let page = req.query.page;
  let uom_id = req.query.uom_id || 'all';
  let pg_id = req.query.pr_id || 'all';
  let {price_at,keyword} = req.query;
  let searchby = req.query.searchby || 'userid';
  let where = {};

  page = (!page || isNaN(page) || parseInt(page) < 0)? 0 : parseInt(page) - 1;
  let offset = page * limit;

  if(searchby == 'userid' && keyword)  where.sku_id = {[Op.iLike]: `${keyword}%`};

  if(price_at){
    let p_at = price_at.split(',');

    if(p_at.length==1){
      if(!dayjs(p_at[0], df).isValid()) return res.status(422).send({errMsg: 'Date format is invalid.'});

      where.effective_at = {[Op.lte]: p_at[0]};
    }else{
      if(!dayjs(p_at[0], df).isValid() || !dayjs(p_at[1], df).isValid()) return res.status(422).send({errMsg: 'Date format is invalid.'});

      where.effective_at = {[Op.between]:[
        dayjs(p_at,df),
        dayjs(p_at,df)
      ]};
    }
  }

  let prices;
  try{
    prices = await db.price_list.findAndCountAll({
      attributes: ['id', 'price_group_id', 'sku_id', 'uom_id', 'price', 'effective_at', 'created_by'],
      order: [['created_at','desc']],
      where,limit,offset,
      raw : true
    })
    prices.limit = limit;
    prices.offset = offset;
  }catch(e){
    console.error(e)
    return res.status(500).send({errMsg:'Failed to get price list.'});
  }

  return res.send({status:'success', prices});
};

const qPriceList = `select sku_id,array_agg(json_build_object('price_group_id',price_group_id,'price',price)) as prices from price_list pl
  where effective_at <= current_timestamp
  group by sku_id `;

Prices.AList = async function(req,res){
  let rows;

  try{
    rows = await sq.query(qPriceList,{
      type: sq.QueryTypes.SELECT,
      raw: true
    });
  }catch(e){
    console.error(e)
    return res.status(500).send({error: "Failed to get price list."})
  }

  return res.send({
    data:{
      count: rows.length,
      rows: rows
    }
  })
}


const skuQuery = `select id,"name" from skus s `;
const uomQuery = `select id,value,display,is_default,active,sequence from lovs where group_id  = 'uom' and active = true order by "sequence" asc`;
const priceGroupQuery = `select id,value,display,is_default,active,sequence from lovs where group_id  = 'price_groups' and active = true order by "sequence" asc`;

Prices.getDetails = async function(req,res){
  let rows;

  try{
    skuRows = await sq.query(skuQuery,{
      type: sq.QueryTypes.SELECT,
      raw: true
    });
    uomRows = await sq.query(uomQuery,{
      type: sq.QueryTypes.SELECT,
      raw: true
    });
    priceGroupRows = await sq.query(priceGroupQuery,{
      type: sq.QueryTypes.SELECT,
      raw: true
    });
  }catch(e){
    console.error(e)
    return res.status(500).send({error: "Failed to get price list."})
  }

  return res.send({
    data:{
      skuCount: skuRows.length,
      skuRows: skuRows,
      uomCount: uomRows.length,
      uomRows: uomRows,
      priceGroupCount: priceGroupRows.length,
      priceGroupRows: priceGroupRows,
    }
  })
}

const autoIDSql = 'SELECT MAX(id)+1 as id FROM price_list';
Prices.create = async function(req,res){
  let {role, user_id} = req.token;

  let sku = req.body.sku;
  let uom = req.body.uom;
  let price = req.body.price;
  let priceGroup = req.body.priceGroup;
  let effectiveDate = req.body.effectiveDate;

  if(!sku) return res.status(500).send({errMsg: 'Please select sku'});
  if(!uom) return res.status(500).send({errMsg: 'Please select uom'});
  if(!priceGroup) return res.status(500).send({errMsg: 'Please select priceGroup'});
  if(!price) return res.status(500).send({errMsg: 'Please enter price.'});
  if(!effectiveDate) return res.status(500).send({errMsg: 'Please select Effective Date.'});

  let Prices;
  try {

    autoID = await sq.query(autoIDSql,{
      type: sq.QueryTypes.SELECT,
      raw: true
    });
    // user = await db.users.findOne({ where:{ id: {[Op.eq]: id.toLowerCase()} }});
    // if(user) return res.status(422).send({status:'failed', errMsg:'Username is already taken. Please try another username.'})

    await db.price_list.create({
      id:autoID[0].id,
      price_group_id: priceGroup,
      sku_id: sku,
      uom_id: uom,
      price: price,
      effective_at: effectiveDate,
      created_by: req.token.user_id,
      created_at: sq.now,
      updated_at: sq.now
    });
    
  } catch(e) {
    console.error(e);
    return res.status(500).send({errMsg: 'Failed to create new price.'});
  }

  return res.send({Prices});
}

Prices.update = async function(req, res){
  let id = req.body.rowId;
  let {role, user_id} = req.token;
  // console.log(req.body);return false;
  let price;
  try{
    price = await db.price_list.findOne({ 
      where: {id: {[Op.eq]: id.toLowerCase()}} 
    });
    if(!price) return res.status(404).send({errMsg: 'Price not found.'});

    if('priceGroup' in req.body){
      let priceGroup = req.body.priceGroup;
      if(priceGroup !== null && priceGroup.trim().length == 0) {
        return res.status(422).send({errMsg: 'Price Group cannot be empty.'})
      }
      price.price_group_id = priceGroup;
    }
    if('sku' in req.body){
      let sku = req.body.sku;
      if(sku !== null && sku.trim().length == 0) {
        return res.status(422).send({errMsg: 'SKU cannot be empty.'})
      }
      price.sku_id = sku;
    }
    if('uom' in req.body){
      let uom = req.body.uom;
      if(uom !== null && uom.trim().length == 0) {
        return res.status(422).send({errMsg: 'UOM cannot be empty.'})
      }
      price.uom_id = uom;
    }
    if('price' in req.body){
      let prices = req.body.price;
      if(prices !== null && prices.trim().length == 0) {
        return res.status(422).send({errMsg: 'Price cannot be empty.'})
      }
      price.price = prices;
    }
    if('effectiveDate' in req.body){
      let effectiveDate = req.body.effectiveDate;
      if(effectiveDate !== null && effectiveDate.trim().length == 0) {
        return res.status(422).send({errMsg: 'Effective Date cannot be empty.'})
      }
      price.effective_at = effectiveDate;
    }


    price.updated_by =  user_id;
    price.updated_at =  sq.now
    await price.save();
    console.log(price);
  }catch(e){
    console.error(e);
    return res.status(500).send({errMsg: 'Failed to update user.'});
  }

  return res.send({price});
}

Prices.delete = async function(req, res){
  let id  = req.body.id;
  let {user_id,role} = req.token;

  if(!id) return res.status().send({errMsg:'Price ID not provided'});

  let price;

  try {
    price = await db.price_list.findOne({
      where: {
        id : {[Op.eq]: id}
      }
    })
    
    if(!price) return res.status(404).send({status: 'failed', errMsg: `User #${id} not found.`});
    price.active = false;
    price.save();
    // user.destroy();

  } catch(e) {
    // statements
    console.error(e);
  }

  return res.send({msg:'User deleted successfully.'})
}

Prices.read = async function(req, res){
  let id = req.params.id;

  let price;
  try{
    price = await db.price_list.findOne({
      where: {id: {[Op.eq]: id.toLowerCase()} },
      raw: true
    });

    skuRows = await sq.query(skuQuery,{
      type: sq.QueryTypes.SELECT,
      raw: true
    });
    uomRows = await sq.query(uomQuery,{
      type: sq.QueryTypes.SELECT,
      raw: true
    });
    priceGroupRows = await sq.query(priceGroupQuery,{
      type: sq.QueryTypes.SELECT,
      raw: true
    });

    if(!price) return res.status(404).send({errMsg: 'Row not found.'});
  }catch(e){
    console.error(e);
    return res.status(500).send({errMsg: 'Failed to find Row.'})
  }

  return res.send({
    // price
    data:{
      skuCount: skuRows.length,
      skuRows: skuRows,
      uomCount: uomRows.length,
      uomRows: uomRows,
      priceGroupCount: priceGroupRows.length,
      priceGroupRows: priceGroupRows,
      price:price
    }
  });
}
module.exports = Prices;