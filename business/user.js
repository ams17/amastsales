const env = process.env.NODE_ENV || 'dev';
const conf = require('../config/config.json')[env];
const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const dayjs = require('dayjs');
const {Parser} = require('json2csv');
const Constants = require('../common/constants');
const jwtAuth = require('../common/jwtAuth');
const Password = require('../common/password');
const db = require('../model/db.js');
const Op = db.Sequelize.Op;
const sq = db.sequelize;
const df = 'YYYY-MM-DD';
const limit = 20;

let User = {};

User.list = async function(req,res){
  let page = req.query.page;
  let active = req.query.active || 'all';
  let {role, keyword, lastlogin} = req.query;
  let searchby = req.query.searchby || 'userid';
  let where = {};

  page = (!page || isNaN(page) || parseInt(page) < 0)? 0 : parseInt(page) - 1;
  let offset = page * limit;

  if(searchby == 'userid' && keyword)  where.id = {[Op.iLike]: `${keyword}%`};
  if(role && role != 'all') where.role = {[Op.iLike]: `${role}`};
  if(active && active.toLowerCase() != 'all'){
    where.active = active.toLowerCase() === 'active';
  }
  if(lastlogin){
    let ll = lastlogin.split(',');

    if(ll.length == 1){
      if(!dayjs(ll[0], df).isValid()) return res.status(422).send({errMsg: 'Date format is invalid.'});
      where.last_login_at = {[Op.gte]: ll[0]};
    } else{
      if(!dayjs(ll[0], df).isValid() || !dayjs(ll[1], df).isValid()) return res.status(422).send({errMsg: 'Date format is invalid.'});
      where.last_login_at = { [Op.between]: [ 
        dayjs(ll[0]).startOf('day').toDate(), 
        dayjs(ll[1]).endOf('day').toDate() 
      ]}
    }
  }
  where.active = {[Op.eq]: true}
  let users;
  try{
    users = await db.users.findAndCountAll({
      attributes: ['id', 'name', 'role', 'active', 'retries', 'last_login_at', 'created_by', 'created_at'],
      order: [['id']],
      where, offset, limit, 
      raw: true
    });
    users.limit = limit;
    users.offset = offset;
  }catch(e){
    console.error(e);
    return res.status(500).send({errMsg: 'Failed to get users.'});
  }
  return res.send({status: 'success', users});
}

User.create = async function(req,res){
  let id = req.body.id;
  let name = req.body.name;
  let password = req.body.password;
  let role = req.body.role;
  let status = req.body.status;
  let deviceID = req.body.deviceID;
  let allowNewDevice = req.body.allowNewDevice;

  if(!id) return res.status(500).send({errMsg: 'Please enter User Id.'});
  if(!name) return res.status(500).send({errMsg: 'Please enter Name.'});
  if(!password) return res.status(500).send({errMsg: 'Please enter Password.'});

  let user;
  try {
    user = await db.users.findOne({ where:{ id: {[Op.eq]: id.toLowerCase()} }});
    if(user) return res.status(422).send({status:'failed', errMsg:'Username is already taken. Please try another username.'})

    let hash = bcrypt.hashSync(password, conf.saltRounds);
    await db.users.create({
      id: id.toLowerCase(),
      name: name,
      password: hash,
      role: role,
      active: (status==1)?(true):false,
      retries: 0,
      created_by: req.token.user_id,
      allow_new_device: (allowNewDevice==1)?(true):false,
      device_id : deviceID
    });
    
  } catch(e) {
    console.error(e);
    return res.status(500).send({errMsg: 'Failed to create new user.'});
  }

  return res.send({user});
}

User.read = async function(req, res){
  let id = req.params.id;

  let user;
  try{
    user = await db.users.findOne({
      attributes: {exclude: ['password']},
      where: {id: {[Op.eq]: id.toLowerCase()} },
      raw: true
    });
    if(!user) return res.status(404).send({errMsg: 'User not found.'});
  }catch(e){
    console.error(e);
    return res.status(500).send({errMsg: 'Failed to find user.'})
  }

  return res.send({user});
}

User.update = async function(req, res){
  let {id} = req.body;
  // console.log(req.body);return false;

  let user;
  try{
    user = await db.users.findOne({ 
      attributes: {exclude: ['password']},
      where: {id: {[Op.eq]: id.toLowerCase()}} 
    });
    if(!user) return res.status(404).send({errMsg: 'User not found.'});

    if('name' in req.body){
      let name = req.body.name;
      if(name !== null && name.trim().length == 0) {
        return res.status(422).send({errMsg: 'User name cannot be empty.'})
      }
      user.name = name;
    }

    if('role' in req.body){
      let {role} = req.body;
      if(!Constants.validRoles.includes(role)) {
        return res.status(422).send({errMsg: `User role '${role}' is not valid. Valid roles are ${Constants.validRoles}.`});
      }
      user.role = role;
    }

    if('status' in req.body){
      let {status} = req.body;
      user.active = (status == '1')?(true):false;
    }

    if('deviceID' in req.body){
      let {deviceID} = req.body;
      user.device_id = deviceID;
    }

    if('allowNewDevice' in req.body){
      let {allowNewDevice} = req.body;
      user.allow_new_device = (allowNewDevice == '1')?(true):false;
    }

    await user.save();
    console.log(user);
  }catch(e){
    console.error(e);
    return res.status(500).send({errMsg: 'Failed to update user.'});
  }

  return res.send({user});
}

User.unlock = async function(req, res){
  let {id} = req.body; 

  let user;
  try{
    user = db.users.findOne({ 
      attributes: {exclude: ['password']},
      where: {id: {[Op.eq]: id.toLowerCase()}} 
    });
    if(!user) return res.status(404).send({errMsg: 'User not found.'});

    user.retries = 0;
    user.last_login_at = dayjs().toDate();
    await user.save();
  }catch(e){
    console.error(e);
    return res.status(500).send({errMsg: 'Failed to unlock user.'});
  }

  return res.send({user});
}

User.changePassword = async function(req, res){
  let {id, password} = req.body;

  let user;
  try{
    user = db.users.findOne({ 
      attributes: {exclude: ['password']},
      where: {id: {[Op.eq]: id.toLowerCase()}} 
    });
    if(!user) return res.status(404).send({errMsg: 'User not found.'});

    if(Password.score(password) < 8) {
      return res.status(422).send({errMsg: 'Password complexity requirement not met.'});
    }

    let hash = bcrypt.hashSync(password, conf.saltRounds);
    user.password = hash;
    await user.save();
  }catch(e){
    console.error(e);
    res.status(500).send({errMsg: 'Failed to change user password'});
  }
  return res.send({user});
}

User.delete = async function(req, res){
  let id  = req.body.id;
  let {user_id,role} = req.token;

  if(!id) return res.status().send({errMsg:'User ID not provided'});

  let user;

  try {
    user = await db.users.findOne({
      where: {
        id : {[Op.eq]: id}
      }
    })
    
    if(!user) return res.status(404).send({status: 'failed', errMsg: `User #${id} not found.`});
    user.active = false;
    user.save();
    // user.destroy();

  } catch(e) {
    // statements
    console.error(e);
  }

  return res.send({msg:'User deleted successfully.'})
}


module.exports = User;