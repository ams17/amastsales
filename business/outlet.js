const env = process.env.NODE_ENV || 'dev';
const conf = require('../config/config.json')[env];
const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const moment = require('moment');
const {Parser} = require('json2csv');
const jwtAuth = require('../common/jwtAuth');
const Password = require('../common/password');
const db = require('../model/db.js');
const Op = db.Sequelize.Op;
const sq = db.sequelize;
const df = 'YYYY-MM-DD';
const limit = 20;

let Outlets = {};

Outlets.list = async function(req,res){
  let page = req.query.page;
  let active = req.query.active || 'all';
  let price_group = req.query.price_group;
  let keyword = req.query.keyword;
  let searchby = req.query.searchby || 'outletId';
  let where = {};

  active = active.toLowerCase();

  page = (!page || isNaN(page) || parseInt(page) < 0)? 0 : parseInt(page) - 1;
  let offset = page * limit;

  if (searchby == 'outletId' && keyword)  where.id = {[Op.iLike]: `${keyword}%`};
  if (price_group && price_group != 'All')        where.price_group_id = {[Op.iLike]: `${price_group}`};
  if (active && active != 'all')        where.active = {[Op.iLike]: `${active}`};

  let outlets;
  try{
    outlets = await db.outlets.findAndCountAll({
      attributes: ['id', 'name', 'active', 'price_group_id'],
      order: [['id']],
      where, offset, limit, raw: true,
      logging: console.log
    });
    outlets.limit = limit;
    outlets.offset = offset;
  }catch(e){
    console.error(e);
    return res.status(500).send({status: 'failed', errMsg: 'Failed to get outlets.'});
  }
  return res.send({status: 'success', outlets});
}

Outlets.details = async function(req, res){
  let {id} = req.params;
  let {user_id,role} = req.token;

  let outlet;
  let outlet_visit_plans;
  let outlet_credit;
  let outlet_acc_stmt;

  try{
    outlet = await db.outlet.findOne({where: {id: {[Op.eq]: id}} });
    if(!outlet) return res.status(404).send({errMsg: 'Outlet ID not found.'});

    outlet_visit_plans = await db.outlet_visit_plans.findOne({ 
      attributes: ['van_id', 'day'],
      where: {outlet_id: id} 
    });

    outlet_credit = await db.outlet_credit.findOne({ 
      attributes: ['term', 'limit', 'allow_cash', 'allow_credit', 'allow_cheque'],
      where: {outlet_id: id} 
    });

    outlet_acc_stmt = await db.outlet_acc_stmt.findOne({ 
      attributes: ['outlet_id', 'date', 'reference_id', 'prev_balance', 'debit', 'credit', 'balance'],
      where: {outlet_id: id, current: true} 
    });

  }catch(e){
    console.error(e)
    return res.status(500).send({errMsg:'Failed to get Outlet details.'})
  }

  return res.send({outlet, outlet_visit_plans, outlet_credit, outlet_acc_stmt});
}

Outlets.listAndroid = async function(req,res){
  let qOutlets = `select o.id,o."name",regno,
    json_build_object(
      'addr',o.address,
      'postcode',o.postcode,
      'city',o.city,
      'state',o.state,
      'lat',o.lat,
      'lng',o.lng
    ) as locationDetails, 
    json_build_object(
      'channelId',o.channel,
      'channel',json_build_object('id',h.channel_id,'name', (select "name" from lookup.channels c where id = h.channel_id)),
      'subChannel',json_build_object('id',h.subchannel_id,'name',(select "name" from lookup.subchannels s where id = h.subchannel_id)),
      'outletConcept',json_build_object('id',h.outlet_concept_id,'name',(select "name" from lookup.outlet_concepts oc where id = h.outlet_concept_id)),
      'outletType',json_build_object('id',h.outlet_type_id,'name',(select "name" from lookup.outlet_types ot where id = h.outlet_type_id))
    ) as "heirarchy",
    json_build_object(
      'keyAccountId',o.chain_store_type,
      'type', ka."name",
      'presence',json_build_object('id',ka.presence_id,'name',(select "name" from lookup.presence p where id = ka.presence_id))
    ) as chainStore,
    json_build_object(
      'week',json_build_object('id',o.week_frequency,'name',(select display from lovs where group_id = 'freq_week' and id = o.week_frequency)),
      'month',json_build_object('id',o.month_frequency,'name',(select display from lovs where group_id = 'freq_month' and id = o.month_frequency))
    ) as visitFrequency 
    from public.outlets o
    left join public.outlet_hierarchy h 
    on o.channel = h.id 
    left join lookup.key_accounts ka 
    on ka.id = o.chain_store_type `
  let id = req.query.id;
  let qid = 'where o.id in (:id)';

  if(id){
    id = id.split(',');
    qOutlets = qOutlets + qid;
  }

  let rows;
  try {
    rows = await sq.query(qOutlets,{
      type: sq.QueryTypes.SELECT,
      replacements : {id},
      raw: true
    })
  } catch(e) {
    // statements
    console.error(e);
    return res.status(500).send({error: 'Failed to get outlet list'});
  }

  return res.send({
    status:'success',
    data: {
      count: rows.length,
      rows: rows
      
    }
  });
  
}

module.exports = Outlets;