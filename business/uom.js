const env = process.env.NODE_ENV || 'dev';
const conf = require('../config/config.json')[env];
const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const {Parser} = require('json2csv');
const jwtAuth = require('../common/jwtAuth');
const Password = require('../common/password');
const db = require('../model/db.js');
const Op = db.Sequelize.Op;
const sq = db.sequelize;
const df = 'YYYY-MM-DD';
const limit = 20;

const Uoms = {}

Uoms.list = async function(req,res){
  let page = req.query.page;
  let keyword = req.query.keyword;
  let searchby = req.query.searchby || 'userid';
  let where = {};

  page = (!page || isNaN(page) || parseInt(page) < 0)? 0 : parseInt(page) - 1;
  let offset = page * limit;

  if(searchby == 'userid' && keyword)  where.id = {[Op.iLike]: `${keyword}%`};

  let uoms;
  try{
    uoms = await db.uoms.findAndCountAll({
      attributes: ['id', 'name','created_by'],
      order: [['id']],
      where, offset, limit, raw: true,
      logging: console.log
    });
    uoms.limit = limit;
    uoms.offset = offset;
  }catch(e){
    console.error(e);
    return res.status(500).send({status: 'failed', errMsg: 'Failed to get uoms.'});
  }
  return res.send({status: 'success', uoms});
}

Uoms.read = async function(req,res) {
  let id = req.params.id;
  if(id == null) return res.status(422).send({status: 'failed', errMsg:'SKU ID not provided.'});

  let uom;
  try{
    uom = await db.uoms.findOne({
      where: {id: {[Op.eq]: id}},
      raw:true
    });
    if(!uom) return res.status(404).send({status: 'failed', errMsg: `SKU #${id} not found.`});
  }catch(e){
    console.error(e);
    return res.status(500).send({status: 'failed', errMsg: `Failed to get uom #${id}`})
  }
  return res.send({status:'success', uom});
}

Uoms.create = async function(req,res){
  let id                = req.body.id;
  let name              = req.body.name;
  let {user_id, role}   = req.token;

  if(!id) res.status(500).send({errMsg:'Please enter Uoms id.'});
  if(!name) res.status(500).send({errMsg:'Please enter Uoms name.'});

  let uoms;

  try {
    uoms = await db.uoms.findOne({
      where: {
        id : {[Op.eq]: id}
      },
      raw: true
    });

    if(uoms) return res.status(422).send({errMsg:`Uoms id ${id} already used. Please enter a new id.`});

    await db.uoms.create({
      id: id,
      name: name,
      created_by : user_id,
      created_at : new Date()
    });

  } catch(e) {
    // statements
    console.error(e);
    return res.status(500).send({errMsg: 'Failed to create new uom.'})
  }

  return res.send({msg: `Uoms ${id} create successfully.`})
}

Uoms.update = async function(req, res){
  let id  = req.body.id;
  let {user_id,role} = req.token;

  if(!id) return res.status().send({errMsg:'Uoms ID not provided'});

  let uoms;

  try {
    uoms = await db.uoms.findOne({
      where: {
        id : {[Op.eq]: id}
      }
    })
    
    if(!uoms) return res.status(404).send({status: 'failed', errMsg: `Uoms #${id} not found.`});

    if("name" in req.body){
      if(req.body.name != null){
        if(req.body.name.toString().trim().length != 0){
          uoms.name = req.body.name
        }
      }
    }
    // if("category" in req.body){uoms.category = req.body.category}

    uoms.save();

  } catch(e) {
    // statements
    console.error(e);
  }

  return res.send({msg:'Updated uoms successfully.'})
}

Uoms.delete = async function(req, res){
  let id  = req.body.id;
  let {user_id,role} = req.token;

  if(!id) return res.status().send({errMsg:'Uoms ID not provided'});

  let uoms;

  try {
    uoms = await db.uoms.findOne({
      where: {
        id : {[Op.eq]: id}
      }
    })
    
    if(!uoms) return res.status(404).send({status: 'failed', errMsg: `Uoms #${id} not found.`});

    uoms.destroy();

  } catch(e) {
    // statements
    console.error(e);
  }

  return res.send({msg:'Uoms deleted successfully.'})
}



module.exports= Uoms;