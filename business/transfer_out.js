const env = process.env.NODE_ENV || 'dev';
const conf = require('../config/config.json')[env];
const express = require('express');
const router = express.Router();
const dayjs = require('dayjs'); 
const bcrypt = require('bcrypt');
const {Parser} = require('json2csv');
const jwtAuth = require('../common/jwtAuth');
const Constants = require('../common/constants');
const Password = require('../common/password');
const db = require('../model/db.js');
const Op = db.Sequelize.Op;
const sq = db.sequelize;
const df = 'YYYY-MM-DD';
const limit = 20;

let TrxOut = {};

const qSkus = `select json_object_agg(sku_Id, to_uom_id) as uoms from sku_uom_conversions where multiply_by = 1;`

const qNextToID = `select nextVal('seq_transfer_out') as toID; `;

const qTrxOutInsertILedger = `INSERT INTO public.inventory_ledger (id, site_id, "date", sku_id, uom_id, "condition", operation, reference_id, prev_quantity, movement, new_quantity, "current", description, created_by, created_at, updated_at)
  select uuid_generate_v4() as id, :site_id as site_id, current_date, tod.sku_id, tod.uom_id, tod."condition", 'transfer out' as operation, :toID as reference_id, coalesce(il.new_quantity, 0) as prev_quantity, -tod.quantity, coalesce(il.new_quantity, 0) - tod.quantity as new_quantity, true as current, :remark as description, :user_id as created_by, current_timestamp, current_timestamp 
  from transfer_out_details tod
  left join inventory_ledger il 
  on (tod.sku_id = il.sku_id and tod.uom_id = il.uom_id and tod."condition" = il."condition")
  and il.current = true
  and il.site_id = :site_id 
  where tod.transfer_out_id = :toID;`

const qTrxOutUpdateICurrentLedger = `update inventory_ledger il
  set "current" = false
  from transfer_out_details tod 
  where (tod.sku_id = il.sku_id and tod.uom_id = il.uom_id and tod."condition" = il."condition")
  and reference_id != tod.transfer_out_id
  and il.site_id = :site_id 
  and tod.transfer_out_id = :toID
  and current = true;`

const qTrxOutUpdateILDaily = `update inventory_ledger_daily ild set movement = ild.movement + il.movement, new_quantity = il.new_quantity 
  from inventory_ledger il
  where il.current = true
  and reference_id = :toID
  and (il.site_id = ild.site_id and il.date = ild.date and il.sku_id = ild.sku_id and il.uom_id = ild.uom_id and il.condition = ild.condition);`

const qTrxOutInsertILDaily = `INSERT INTO public.inventory_ledger_daily ( site_id, "date", sku_id, uom_id, "condition", prev_quantity, movement, new_quantity, created_at, updated_at)
  select 
    il.site_id , il.date, il.sku_id, il.uom_id, il."condition", 
    coalesce(il.prev_quantity, 0) as prev_quantity, 
    il.movement, 
    coalesce(il.prev_quantity, 0) + il.movement as new_quantity, 
    current_timestamp, current_timestamp 
  from inventory_ledger il
  where il.current = true
  and reference_id = :toID
  and (site_id, date, sku_id, uom_id, condition) not in (select site_id, date, sku_id, uom_id, condition from inventory_ledger_daily ild );`

const qTrxOutUpdateInventory = `update inventory i set quantity = il.new_quantity 
  from inventory_ledger il 
  where reference_id = :toID
  and (i.sku_id = il.sku_id and i.uom_id = il.uom_id and i."condition" = il."condition" and i.site_id = il.site_id);`;

const qInventory= `select json_object_agg(sku_id,inventory) as inventory from (
  select sku_id ,json_object_agg(condition ,quantity) as inventory from (
    with reserved_stock as (      
      select vad.sku_id, vad.uom_id, 'fresh'::text as condition, sum(vad.allotted_quantity) as quantity from van_allot va 
      inner join van_allot_details vad on vad.van_allot_id = va.id and status in ('sent for picking', 'packed')
      inner join vans v on v.id = va.van_id and v.site_id = :site_id
      group by vad.sku_id, vad.uom_id
    )
    select i.sku_id, i.uom_id, i.condition, i.quantity, coalesce(rs.quantity, 0) reserved_quantity, i.quantity - coalesce(rs.quantity, 0) available_quantity 
    from inventory i 
    left join reserved_stock rs on rs.sku_id = i.sku_id and rs.uom_id = i.sku_id and rs.condition = i.condition 
    where site_id = :site_id
    and (:inventoryConditions)
    order by sku_id, case 
      when i.condition = 'fresh' then 1 
      when i.condition = 'damaged' then 2 
      when i.condition = 'old' then 3 
      else 4 end
  ) a group by sku_id
) b`;

TrxOut.create = async function(req,res){
  let skus = req.body.skus;
  let from_site_id = req.body.from_site_id;
  let to_site_id = req.body.to_site_id;
  let remark = req.body.remark;
  let {user_id,role} = req.token;
  let dateToday = dayjs().format(df);
  let toID;

  if(!skus || skus.length == 0) return res.status(422).send({errMsg:'Please enter an sku.'});

  if(!from_site_id) return res.status(422).send({errMsg:'Please enter a source site ID.'});
  if(!to_site_id) return res.status(422).send({errMsg:'Please enter a destination site ID.'});

  let transferOut;
  let data = [];
  let inventoryConditions = [];
  let user_site;
  let inventory;
  let transaction;

  try {

    if(role != 'superadmin'){
      user_site = await db.user_sites.findOne({
        where:{
          user_id : { [Op.eq] : user_id },
          site_id : { [Op.eq] : from_site_id }
        }
      });
      if(!user_site) return res.status(404).send({errMsg:'Invalid site ID for user.'});
    }

    let uoms = await sq.query(qSkus,{type : sq.QueryTypes.SELECT, raw: true});
    uoms = uoms[0].uoms;

    toID = await sq.query(qNextToID,{type: sq.QueryTypes.SELECT, raw: true});

    toID = `OUT_${from_site_id}_${toID[0].toid}`;

    for(let e of skus){
      if(!uoms[e.sku_id] || uoms[e.sku_id] == null) {
        return res.status(404).send({errMsg:`Base UOM for ${e.sku_id} is not found.`});
      }

      for(let i = 0; i < e.quantity.length; i++){
        if(e.quantity[i] <= 0) continue;

        data.push({
          transfer_out_id : toID,
          sku_id : e.sku_id,
          uom_id : uoms[e.sku_id],
          condition : Constants.conditions[i],
          quantity : e.quantity[i]
        });

        inventoryConditions.push(`(i.sku_id = '${e.sku_id}' and i.uom_id = '${uoms[e.sku_id]}' and i.condition = '${Constants.conditions[i]}')`);
      }      
    }

    inventoryConditions = inventoryConditions.join('or');
    let qInvt = qInventory.replace(':inventoryConditions', inventoryConditions);

    transaction = await sq.transaction();

    inventory = await sq.query(qInvt,{
      type: sq.QueryTypes.SELECT,
      replacements : {site_id: from_site_id},
      raw : true,
      transaction
    });

    inventory = inventory[0].inventory;
    if(!inventory) throw 'Inventory empty.';

    for(let e of skus){
      for(let i = 0; i < e.quantity.length; i++){
        let qty = e.quantity[i];
        let sku_id = inventory[e.sku_id];
        let condition = Constants.conditions[i];

        if(qty <= 0) continue;
        if(!sku_id) throw `Insufficient stock for ${inventory[e.sku_id]}`;
        if(!sku_id[condition]) throw `Insufficient stock for ${condition} ${sku_id}`;
        if(sku_id[condition] < qty) throw `Current stock balance for ${e.sku_id} is ${sku_id[condition]}. Cannot transfer out more.`;  
      }
    }

    transferOut = await db.transfer_outs.create({
      id : toID,
      date : dateToday,
      from_site_id : from_site_id,
      to_site_id : to_site_id,
      remark : remark,
      created_by : user_id
    },{ transaction });

    await db.transfer_out_details.bulkCreate(data,{transaction});
    
    ///TODO
    await sq.query(qTrxOutInsertILedger,{
      type: sq.QueryTypes.INSERT,
      replacements: {site_id: from_site_id, toID, remark, user_id},
      transaction
    })

    await sq.query(qTrxOutUpdateICurrentLedger,{
      type: sq.QueryTypes.UPDATE,
      replacements: {site_id: from_site_id, toID},
      transaction
    });

    await sq.query(qTrxOutUpdateILDaily,{
      type: sq.QueryTypes.UPDATE,
      replacements: {toID},
      transaction
    });
    
    await sq.query(qTrxOutInsertILDaily,{
      type: sq.QueryTypes.INSERT,
      replacements: {toID},
      transaction
    });

    await sq.query(qTrxOutUpdateInventory,{
      type: sq.QueryTypes.UPDATE,
      replacements: {toID},
      transaction
    });

    await transaction.commit();

  } catch(e) {
    console.error(e);
    if(transaction) await transaction.rollback();
    return res.status(500).send({errMsg:`Failed to Transfer Out. ${e}`})
  }

  return res.send({transferOut, msg: 'Transfer out successful.'});
}

TrxOut.list = async function (req,res) {
  let page = req.query.page;
  let keyword = req.query.keyword;
  let from_site_id =  req.query.from_site_id || 'all';
  let to_site_id =  req.query.to_site_id || 'all';
  let status = req.query.status || 'all';
  let date = req.query.date;
  let where = {};

  page = (!page || isNaN(page) || parseInt(page) < 0)? 0 : parseInt(page) - 1;
  let offset = page * limit;

  let transferOut;

  if(keyword) where.id = {[Op.iLike] : `%${keyword}%`};
  if(from_site_id != 'all') where.from_site_id = {[Op.eq] : from_site_id};
  if(to_site_id != 'all') where.to_site_id = {[Op.eq] : to_site_id};
  if(status != 'all') where.status = {[Op.eq] : status};

  if(date){
    let d = date.split(',');

    if(d.length== 1){
      if(!dayjs(d[0], df).isValid()) return res.status(422).send({errMsg: 'Date format is invalid.'});
      where.date = {[Op.eq]: d[0]};
    }else{
      if(!dayjs(d[0], df).isValid() || !dayjs(d[1], df).isValid()) return res.status(422).send({errMsg: 'Date format is invalid.'});
      where.date = { [Op.between]:[ d[0].format(df), d[1].format(df) ] }
    }
  }

  try {
    transferOut = await db.transfer_outs.findAndCountAll({
      attributes: ['id', 'date', 'status', 'from_site_id', 'to_site_id', 'created_by', 'created_at'],
      order : [['created_at','DESC']],
      where,offset,limit,
      raw : true
    });

    transferOut.limit = limit;
    transferOut.offset = offset;

  } catch(e) {
    // statements
    console.error(e);
    return res.status(500).send({errMsg:'Failed to get Transfer Out list.'})
  }

  return res.send({transferOut});
}

const qTransferOutDetails = `select sku_id, uom_id, array_to_json(array[max(fresh_qty), max(damaged_qty), max(old_qty), max(recalled_qty)]) as quantity from 
(with ti as (select * from transfer_out_details tid where transfer_out_id = :toid)
select sku_id, uom_id, quantity fresh_qty, 0 damaged_qty, 0 old_qty, 0 recalled_qty from ti where condition = 'fresh'
union select sku_id, uom_id, 0 fresh_qty, quantity damaged_qty, 0 old_qty, 0 recalled_qty from ti where condition = 'damaged'
union select sku_id, uom_id, 0 fresh_qty, 0 damaged_qty, quantity old_qty, 0 recalled_qty from ti where condition = 'old'
union select sku_id, uom_id, 0 fresh_qty, 0 damaged_qty, 0 old_qty, quantity recalled_qty from ti where condition = 'recalled'
) a group by sku_id, uom_id order by sku_id`;

TrxOut.details = async function(req,res){
  let toid = req.params.toid;
  let {user_id,role} = req.token;

  let transfer_out;
  let details;

  if(!toid) return res.status(422).send({errMsg:'Please enter a Transfer Out ID.'});

  try{

    transfer_out = await db.transfer_outs.findOne({ where:{ id:{[Op.eq]: toid}}, raw: true });
    if(!transfer_out) return res.status(404).send({errmsg: 'Transfer Out ID not found.'});

    details = await sq.query(qTransferOutDetails, {
      type : sq.QueryTypes.SELECT, 
      replacements: {toid},
      raw: true
    });

  }catch(e){
    console.error(e)
    return res.status(500).send({errMsg:'Failed to get Transfer Out details.'})
  }

  return res.send({transfer_out, details});
}

module.exports = TrxOut;