const env = process.env.NODE_ENV || 'dev';
const conf = require('../config/config.json')[env];
const express = require('express');
const router = express.Router();
const dayjs = require('dayjs'); 
const bcrypt = require('bcrypt');
const {Parser} = require('json2csv');
const jwtAuth = require('../common/jwtAuth');
const Password = require('../common/password');
const db = require('../model/db.js');
const Op = db.Sequelize.Op;
const sq = db.sequelize;
const df = 'YYYY-MM-DD';
const limit = 20;

const Inventory = {};

const qCheckUserSite = `select user_id , site_id from user_sites us ,sites s 
where us.site_id  = s.id 
and user_id = :user_id 
and s.id = :site_id;`

const qInventoryStatus = `select max(updated_at) as last_updated from inventory i
  where site_id  = :site_id`

const qInventory = `select sku_id ,uom_id ,array_to_json(array[max(fresh),max("old"),max(damage),max(recall)]) as quantity 
from (
  with invt as (
  select * from inventory i
  where site_id  = :site_id 
  )
  select site_id , sku_id  ,uom_id ,quantity as fresh, 0 as "old", 0 as damage, 0 as recall from invt where "condition" = 'fresh'
  union select site_id , sku_id  ,uom_id ,0 as fresh, quantity as "old", 0 as damage, 0 as recall from invt where "condition" = 'old'
  union select site_id , sku_id  ,uom_id ,0 as fresh, 0 as "old", quantity as damage, 0 as recall from invt where "condition" = 'damage'
  union select site_id , sku_id  ,uom_id ,0 as fresh, 0 as "old", 0 as damage, quantity as recall from invt where "condition" = 'recall'
) a
group by sku_id, uom_id
order by sku_id`

Inventory.list = async function(req,res){
  let {site_id} = req.params;
  let {user_id, role} = req.token;

  let user_site;
  let inventory;
  let inventory_status;
  
  if(!site_id) return res.status(422).send({errMsg:'Please enter a site ID.'})
  try {
    
    if(role != 'superadmin'){
      user_site = await sq.query(qCheckUserSite,{
        type: sq.QueryTypes.SELECT,
        replacements: {site_id,user_id},
        raw : true
      });
      if(user_site.length == 0) return res.status(404).send({errMsg:'Site ID is invalid for this user.'});
    }

    inventory = await sq.query(qInventory,{
      type : sq.QueryTypes.SELECT,
      replacements : {site_id},
      raw : true
    });

    inventory_status = await sq.query(qInventoryStatus,{
      type : sq.QueryTypes.SELECT,
      replacements : {site_id},
      raw : true
    });

  } catch(e) {
    // statements
    console.error(e);
    res.status(500).send({errMsg:'Failed to get inventory list'});
  }

  //fresh,old,damage,recall
  res.send({inventory, inventory_status});
};

Inventory.ledgerList = async function(req,res){
  let page = req.query.page;
  let keyword = req.query.keyword;
  let site_id =  req.query.site_id || 'all';
  let uom_id = req.query.uom_id || 'all';
  let current = req.query.current || 'all';
  let condition = req.query.condition || 'all';
  let date = req.query.date;
  let where = {};

  page = (!page || isNaN(page) || parseInt(page) < 0)? 0 : parseInt(page) - 1;
  let offset = page * limit;

  let inventory_ledger;

  if(keyword) where.sku_id = {[Op.iLike] : `%${keyword}%`}
  if(site_id != 'all') where.site_id = {[Op.eq] : site_id}
  if(uom_id != 'all') where.uom_id = {[Op.eq] : uom_id}
  if(condition != 'all') where.condition = {[Op.iLike] : condition}
  if(current != 'all'){
    current = current === true 
    where.current = {[Op.eq] : current}
  }

  if(date){
    let d = date.split(',');

    if(d.length== 1){
      if(!dayjs(d[0], df).isValid()) return res.status(422).send({errMsg: 'Date format is invalid.'});
      where.date = {[Op.eq]: d[0]};
    }else{
      if(!dayjs(d[0], df).isValid() || !dayjs(d[1], df).isValid()) return res.status(422).send({errMsg: 'Date format is invalid.'});
      where.date = { [Op.between]:[ d[0].format(df), d[1].format(df) ] }
    }
  }

  try {
    inventory_ledger = await db.inventory_ledger.findAndCountAll({
      attributes: ['id', 'sku_id', 'site_id', 'date', 'uom_id','condition', 'operation', 'reference_id', 'movement', 'new_quantity', 'current', 'created_by'],
      order : [['date','desc'], ['created_at', 'desc'], 'sku_id'],
      where,offset,limit,
      raw : true
    });

    inventory_ledger.limit = limit;
    inventory_ledger.offset = offset;

  } catch(e) {
    // statements
    console.error(e);
    return res.status(500).send({errMsg:'Failed to get Inventory Ledger list.'});
  }

  return res.send({inventory_ledger});
}

Inventory.ledgerDetail = async function(req,res){
  let toid = req.params.toid;
  let where = {};

  if(!toid) return res.status(422).send({errMsg: 'Please enter an Id.'});

  let details;

  where.id = toid;

  try {
    details = await db.inventory_ledger.findOne({
      attributes: ['id', 'sku_id', 'site_id', 'date', 'uom_id','condition', 'operation', 'reference_id', 'movement', 'new_quantity', 'current', 'created_by'],
      order : [['date','desc'], ['created_at', 'desc'], 'sku_id'],
      where,
      raw : true
    });

  } catch(e) {
    // statements
    console.error(e);
    return res.status(500).send({errMsg:'Failed to get Inventory Ledger Detail.'});
  }

  return res.send({details});
}

module.exports = Inventory;