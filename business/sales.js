const env = process.env.NODE_ENV || 'dev';
const conf = require('../config/config.json')[env];
const express = require('express');
const router = express.Router();
const dayjs = require('dayjs'); 
const bcrypt = require('bcrypt');
const {Parser} = require('json2csv');
const jwtAuth = require('../common/jwtAuth');
const Constants = require('../common/constants');
const Password = require('../common/password');
const db = require('../model/db.js');
const Op = db.Sequelize.Op;
const sq = db.sequelize;
const df = 'YYYY-MM-DD';
const limit = 20;

let Sales = {};

const qSkus = `select json_object_agg(sku_Id, to_uom_id) as uoms from sku_uom_conversions where multiply_by = 1;`

const invInsertOutletAccStmt = `INSERT INTO public.outlet_acc_stmt (
    id, outlet_id , "date", reference_id, prev_balance, debit, credit, balance, "current", created_at, updated_at
  )
  select uuid_generate_v4() as id, inv.outlet_id as outlet_id , inv."date", inv.id as reference_id ,
  coalesce(oas.balance,0) as prev_balance, inv.grand_total as debit, 0 as credit, coalesce(oas.balance,0) - inv.grand_total as balance , true as "current",
  current_timestamp, current_timestamp
  from invoices inv
  left join outlet_acc_stmt oas
  on (inv.outlet_id = oas.outlet_id)
  and oas.current = true
  and oas.outlet_id = :outlet_id 
  where inv.id = :invId;`;

const receiptInsertOutletAccStmt = `INSERT INTO public.outlet_acc_stmt (
    id, outlet_id , "date", reference_id, prev_balance, debit, credit, balance, "current", created_at, updated_at
  )
  select uuid_generate_v4() as id, rec.outlet_id as outlet_id , rec."date", rec.id as reference_id ,
  coalesce(oas.balance,0) as prev_balance, 0 as debit, rec.collected_amount as credit, coalesce(oas.balance,0) + rec.collected_amount as balance , true as "current",
  current_timestamp, current_timestamp
  from receipts rec
  left join outlet_acc_stmt oas
  on (rec.outlet_id = oas.outlet_id)
  and oas.current = true
  and oas.outlet_id = :outlet_id 
  where rec.id = :recId;`;

const cnInsertOutletAccStmt = `INSERT INTO public.outlet_acc_stmt (
    id, outlet_id , "date", reference_id, prev_balance, debit, credit, balance, "current", created_at, updated_at
  )
  select uuid_generate_v4() as id, cn.outlet_id as outlet_id , cn."date", cn.id as reference_id ,
  coalesce(oas.balance,0) as prev_balance, 0 as debit, cn.grand_total as credit, coalesce(oas.balance,0) + cn.grand_total as balance , true as "current",
  current_timestamp, current_timestamp
  from credit_notes cn
  left join outlet_acc_stmt oas
  on (cn.outlet_id = oas.outlet_id)
  and oas.current = true
  and oas.outlet_id = :outlet_id 
  where cn.id = :cnId;`;

const voidInvInsertOutletAccStmt = `INSERT INTO public.outlet_acc_stmt (
    id, outlet_id , "date", reference_id, prev_balance, debit, credit, balance, "current", created_at, updated_at
  )
  select uuid_generate_v4() as id, vi.outlet_id as outlet_id , vi."date", vi.id as reference_id ,
  coalesce(oas.balance,0) as prev_balance, 0 as debit, vi.grand_total as credit, coalesce(oas.balance,0) + vi.grand_total as balance , true as "current",
  current_timestamp, current_timestamp
  from void_invoices vi
  left join outlet_acc_stmt oas
  on (vi.outlet_id = oas.outlet_id)
  and oas.current = true
  and oas.outlet_id = :outlet_id 
  where vi.id = :viId;`;

const refundInsertOutletAccStmt = `INSERT INTO public.outlet_acc_stmt (
    id, outlet_id , "date", reference_id, prev_balance, debit, credit, balance, "current", created_at, updated_at
  )
  select uuid_generate_v4() as id, r.outlet_id as outlet_id , r."date", r.id as reference_id ,
  coalesce(oas.balance,0) as prev_balance,  r.collected_amount as debit, 0 as credit, coalesce(oas.balance,0) + r.collected_amount as balance , true as "current",
  current_timestamp, current_timestamp
  from refunds r
  left join outlet_acc_stmt oas
  on (r.outlet_id = oas.outlet_id)
  and oas.current = true
  and oas.outlet_id = :outlet_id 
  where r.id = :rId`;

const dnInsertOutletAccStmt = `INSERT INTO public.outlet_acc_stmt (
    id, outlet_id , "date", reference_id, prev_balance, debit, credit, balance, "current", created_at, updated_at
  )
  select uuid_generate_v4() as id, dn.outlet_id as outlet_id , dn."date", dn.id as reference_id ,
  coalesce(oas.balance,0) as prev_balance,  dn.grand_total as debit, 0 as credit, coalesce(oas.balance,0) + dn.grand_total as balance , true as "current",
  current_timestamp, current_timestamp
  from debit_notes dn
  left join outlet_acc_stmt oas
  on (dn.outlet_id = oas.outlet_id)
  and oas.current = true
  and oas.outlet_id = :outlet_id 
  where dn.id = :dnId`;

const dnDetailsInsert = `INSERT INTO public.debit_note_details
  (id, debit_note_id, sku_id, uom_id, "condition", quantity, price, tax, line_total, created_at, updated_at)
  select uuid_generate_v4(), :debit_note_id, cnd.sku_id, cnd.uom_id, cnd."condition", cnd.quantity, cnd.price, cnd.tax, cnd.line_total, current_timestamp, current_timestamp from credit_note_details cnd
  where cnd.credit_note_id = :credit_note_id;`

const qInsertVanStockLedger = `INSERT INTO public.van_stock_ledger(id, van_id, "date", sku_id, uom_id, "condition", operation, reference, prev_quantity, movement, new_quantity, "current",description, created_by, created_at, updated_at)
  select uuid_generate_v4() as id, :van_id as van_id, current_date as "date", inv.sku_id, inv.uom_id, 'fresh', 'stock out' as operation , invoice_id as reference , coalesce(vsl.new_quantity,0) as prev_quantity , 0 - inv.quantity as movement , (0-inv.quantity)  + coalesce(vsl.new_quantity,0) as new_quantity, true as current, :desc as description,:user_id, current_timestamp, current_timestamp 
  from invoice_details inv
  left join van_stock_ledger vsl 
  on (inv.sku_id = vsl.sku_id and inv.uom_id = vsl.uom_id and vsl."condition" = 'fresh')
  and vsl."current" = true
  and vsl.van_id = :van_id 
  where inv.invoice_id = :invoice_id;`

const qUpdateVanStockLedgerCurrent = `update van_stock_ledger vsl
  set "current" = false
  from invoice_details inv
  where (inv.sku_id = vsl.sku_id and inv.uom_id = vsl.uom_id and vsl."condition" = 'fresh')
  and vsl.reference != inv.invoice_id 
  and vsl.van_id = :van_id
  and inv.invoice_id = :invoice_id 
  and current = true;`

const qUpdateVanStockLedgerDaily = `update van_stock_ledger_daily vsld set movement = vsld.movement + vsl.movement, new_quantity = vsl.new_quantity 
  from van_stock_ledger vsl
  where vsl."current" = true
  and reference = :invoice_id
  and (vsl.van_id = vsld.van_id and vsl."date" = vsld."date" and vsl.sku_id = vsld.sku_id and vsl.uom_id = vsld.uom_id and vsl."condition" = vsld."condition");`

const qInsertVanStockLedgerDaily = `INSERT INTO public.van_stock_ledger_daily ( id, van_id, "date", sku_id, uom_id, "condition", prev_quantity, movement, new_quantity, created_at, updated_at)
  select 
    uuid_generate_v4() as id, vsl.van_id , vsl.date, vsl.sku_id, vsl.uom_id, vsl."condition", 
    coalesce(vsl.prev_quantity, 0) as prev_quantity, 
    vsl.movement, 
    coalesce(vsl.prev_quantity, 0) + vsl.movement as new_quantity, 
    current_timestamp, current_timestamp 
  from van_stock_ledger vsl
  where vsl."current" = true
  and reference = :invoice_id
  and (van_id , "date", sku_id, uom_id, "condition") not in (select van_id, "date", sku_id, uom_id, "condition" from van_stock_ledger_daily ild )`;

const qRefundSettlement = `INSERT INTO public.settlement
  (id, outlet_id, invoice_id, due_date, reference, invoice_amount, prev_outstanding, settlement_amount, new_outstanding, "current", created_at, updated_at, site_id)
  select uuid_generate_v4(), s.outlet_id, s.invoice_id, s.due_date, :refund_id , s.invoice_amount, s2.new_outstanding, s.settlement_amount, s2.new_outstanding + s.settlement_amount, true, current_timestamp, current_timestamp, s.site_id from settlement s 
  left join settlement s2 
  on s.invoice_id = s2.invoice_id 
  and s2."current" = true
  where s.reference = :reference `

const qUpdateVanStck= `update van_stock vs set quantity = vsl.new_quantity 
  from van_stock_ledger vsl 
  where reference = :invoice_id
  and (vs.sku_id = vsl.sku_id and vs.uom_id = vsl.uom_id and vs."condition" = vsl."condition" and vs.van_id = vsl.van_id);`

const updateCurrentOutletAccStmt = `update outlet_acc_stmt oas
  set "current" = false
  where outlet_id = :outlet_id 
  and reference_id != :reference_id `;

const qSettlement = `select invoice_id, due_date, invoice_amount, new_outstanding from settlement s 
  where outlet_id = :outlet_id 
  and "current" = true 
  and new_outstanding != 0
  order by due_date`;

const qFilterSettlement = `select invoice_id, due_date, invoice_amount, new_outstanding from settlement s 
  where outlet_id = :outlet_id 
  and "current" = true 
  and new_outstanding != 0
  and invoice_id != :invoice_id
  order by due_date`

const qFilterSettlementRefrence = `select invoice_id from settlement s 
  where reference = :reference `

const qGetSettlementHistory = `select * from settlement s 
  where reference notnull 
  and invoice_id = :invoice_id `;

Sales.printOut = async function(req,res){
  let printout = req.body.printout || null;
  let invoice = req.body.invoice || null;
  let receipt = req.body.receipt || null;
  let credit_note = req.body.credit_note || null;
  let voidInvoice = req.body.void || null;
  let refund = req.body.refund || null;
  let debit_note = req.body.debit_note || null;

  let van_id = req.body.van_id;
  let outlet_id = req.body.outlet_id;
  let {user_id, site_id, role} = req.token;
  let dateToday = dayjs().format(df);
  
  let invSkus;
  let cnSkus;
  let dnSkus;
  let transaction
  let invoiceData = [];
  let cnData = [];
  let stmntRow = 0;

  let outlet_credit;
  let settlementList;
  let settlement;
  let settlementHistory;

  let inv;
  let rc;
  let cn;

  if(!printout.id) res.status(422).send({error:'Please enter print out id.'});
  if(!van_id) return res.status(422).send({error: 'Please enter van ID.'})
  if(!outlet_id) return res.status(422).send({error:"Please enter outlet id."});

  try{
    if(invoice && receipt && credit_note){
      invSkus = invoice.skus;
      cnSkus = credit_note.skus;

      if(!invoice.id) return res.status(422).send({error:"Please enter invoice id."});
      if(!receipt.id) return res.status(422).send({error:"Please enter receipt id."});
      if(!credit_note.id) return res.status(422).send({error:"Please enter credit note id."});
      if(invSkus.length == 0) return res.status(422).send({error:'Please enter invoice sku.'});
      if(cnSkus.length == 0) return res.status(422).send({error:'Please enter credit note sku.'})
      
      let uoms = await sq.query(qSkus,{type : sq.QueryTypes.SELECT, raw: true});
      uoms = uoms[0].uoms;

      for(let e of invSkus){
        if(!uoms[e.sku_id] || uoms[e.sku_id] == null) {
          return res.status(404).send({errMsg:`Base UOM for ${e.sku_id} is not found.`});
        }
        if(e.quantity <= 0) continue;        

        invoiceData.push({
          invoice_id : invoice.id,
          sku_id: e.sku_id,
          uom_id: uoms[e.sku_id],
          quantity: e.quantity,
          price : e.price,
          discount : e.discount,
          tax : e.tax,
          line_total : e.line_total
        });
      }

      if(invoiceData.length == 0){
        return res.status(422).send({errMsg: `SKU quantity cannot be 0.`})
      }

      transaction = await sq.transaction();

      let po = await db.printouts.findOne({ where:{ id: {[Op.eq]: printout.id} } });

      if(po) throw `Printout id #${printout.id} has been created.`

      await db.printouts.create({
        id: printout.id,
        date: dateToday,
        outlet_id: outlet_id,
        van_id: van_id,
        invoice_id: invoice.id,
        receipt_id: receipt.id,
        credit_note_id: credit_note.id,
        last_credit: printout.last_credit,
        today_credit: printout.today_credit,
        total_credit: printout.total_credit,
        created_by: user_id
      },{transaction,raw: true})

      inv = await db.invoices.findOne({ where:{id :{[Op.eq]: invoice.id} } });

      if(inv) throw `Invoice id #${invoice.id} has been created.`

      await db.invoices.create({
        id : invoice.id,
        date : dateToday,
        outlet_id : outlet_id,
        van_id : van_id,
        subtotal : invoice.subtotal,
        discount : invoice.discount,
        tax : invoice.tax,
        cent_rounding : invoice.cent_rounding,
        grand_total : invoice.grand_total,
        status : 'printed',
        created_by : user_id,
        updated_by : user_id,
        site_id : site_id
      },{transaction, raw:true});

      await db.invoice_details.bulkCreate(invoiceData,{transaction});

      await sq.query(invInsertOutletAccStmt,{
        type: sq.QueryTypes.INSERT,
        replacements :{outlet_id,invId : invoice.id},
        transaction
      });

      let credit_term;

      outlet_credit = await db.outlet_credit.findOne({
        where:{
          outlet_id: {[Op.eq]: outlet_id}
        },transaction
      });

      if(!outlet_credit) return res.status(422).send({error:`Credit term not set for ${outlet_id}.`})

      credit_term = outlet_credit.term;

      await db.settlement.create({
        outlet_id: outlet_id,
        invoice_id: invoice.id,
        due_date: dayjs().add(credit_term,'day').format(df),
        invoice_amount: invoice.grand_total,
        prev_outstanding: 0,
        settlement_amount: 0,
        new_outstanding: invoice.grand_total,
        current: true,
        site_id : site_id
      },{transaction,raw:true});

      await sq.query(updateCurrentOutletAccStmt,{
        type: sq.QueryTypes.UPDATE,
        replacements:{outlet_id,reference_id:invoice.id}
      });

      await sq.query(qInsertVanStockLedger,{
        type: sq.QueryTypes.INSERT,
        replacements :{van_id,user_id,invoice_id : invoice.id,desc: `${invoice.id} for ${outlet_id}`},
        transaction
      });

      await sq.query(qUpdateVanStockLedgerCurrent,{
        type: sq.QueryTypes.UPDATE,
        replacements :{van_id,invoice_id : invoice.id},
        transaction
      });

      await sq.query(qUpdateVanStockLedgerDaily,{
        type: sq.QueryTypes.UPDATE,
        replacements :{invoice_id : invoice.id},
        transaction
      });

      await sq.query(qInsertVanStockLedgerDaily,{
        type: sq.QueryTypes.INSERT,
        replacements :{invoice_id : invoice.id},
        transaction
      });

      await sq.query(qUpdateVanStck,{
        type: sq.QueryTypes.INSERT,
        replacements :{invoice_id : invoice.id},
        transaction
      });

      rc = await db.receipts.findOne({ where:{ id: {[Op.eq]: receipt.id} } });

      if(rc) throw `Receipt id #${receipt.id} has been created.`

      await db.receipts.create({
        id: receipt.id,
        date: dateToday,
        outlet_id : outlet_id,
        van_id: van_id,
        total_sales : receipt.total_sales,
        total_amount : receipt.total_amount,
        amount_payable : receipt.amount_payable,
        cash : receipt.cash,
        collected_amount : receipt.collected_amount,
        method : receipt.method,
        status: 'printed',
        created_by : user_id,
        site_id : site_id
      },{transaction,raw:true});

      await sq.query(receiptInsertOutletAccStmt,{
        type : sq.QueryTypes.INSERT,
        replacements:{outlet_id,recId: receipt.id},
        transaction
      });

      await sq.query(updateCurrentOutletAccStmt,{
        type: sq.QueryTypes.UPDATE,
        replacements:{outlet_id,reference_id:receipt.id},
        transaction
      });

      let collected_amount = receipt.collected_amount;

      settlementList = await sq.query(qSettlement,{
        type: sq.QueryTypes.SELECT,
        replacements:{outlet_id},
        raw:true,
        transaction
      });

      do{

        if(parseFloat(settlementList[stmntRow].new_outstanding) >= parseFloat(collected_amount)){

          await db.settlement.create({
            outlet_id: outlet_id,
            invoice_id: settlementList[stmntRow].invoice_id,
            due_date: settlementList[stmntRow].due_date,
            reference: receipt.id,
            invoice_amount:  settlementList[stmntRow].invoice_amount,
            prev_outstanding: settlementList[stmntRow].new_outstanding,
            settlement_amount: parseFloat(collected_amount).toFixed(2),
            new_outstanding: settlementList[stmntRow].new_outstanding - collected_amount,
            current: true,
            site_id : site_id
          },{transaction,raw:true});

          await db.settlement.update({
            current: false
          },{
            where:{
              invoice_id: {[Op.eq]: settlementList[stmntRow].invoice_id},
              outlet_id: {[Op.eq]: outlet_id},
              reference: {[Op.or]: [{[Op.ne]: receipt.id}, {[Op.eq]: null}] }
            },transaction
          });

          collected_amount = 0
        }else{

          await db.settlement.create({
            outlet_id: outlet_id,
            invoice_id: settlementList[stmntRow].invoice_id,
            due_date: settlementList[stmntRow].due_date,
            reference: receipt.id,
            invoice_amount:  settlementList[stmntRow].invoice_amount,
            prev_outstanding: settlementList[stmntRow].new_outstanding,
            settlement_amount: settlementList[stmntRow].new_outstanding,
            new_outstanding: 0,
            current: true,
            site_id: site_id
          },{transaction,raw:true});

          await db.settlement.update({
            current: false
          },{
            where:{
              invoice_id: {[Op.eq]: settlementList[stmntRow].invoice_id},
              outlet_id: {[Op.eq]: outlet_id},
              reference: {[Op.or]:[ {[Op.ne]: receipt.id}, {[Op.eq]: null} ]}
            },transaction
          });

          collected_amount = collected_amount - settlementList[stmntRow].new_outstanding
        }
        
        stmntRow++;

      }while(collected_amount> 0 && settlementList.length > stmntRow)
      
      await db.receipts.update({
        amount_consumed_in_settlement: receipt.collected_amount - collected_amount
      },{
        where:{
          id: receipt.id
        },transaction
      });
      
      for(let s of cnSkus){
        if(!uoms[s.sku_id] || uoms[s.sku_id] == null) {
          return res.status(404).send({errMsg:`Base UOM for ${s.sku_id} is not found.`});
        }

        for(let i = 0; i < s.quantity.length; i++){
          if(s.quantity[i] <= 0) continue;        

          cnData.push({
            credit_note_id : credit_note.id,
            sku_id: s.sku_id,
            uom_id: uoms[s.sku_id],
            condition: Constants.conditions[i],
            quantity: s.quantity[i],
            price : s.price[i],
            tax : s.tax[i],
            line_total : s.line_total[i]
          });
        }
      }

      if(cnData.length == 0){
        return res.status(422).send({errMsg: `SKU quantity cannot be 0.`})
      }

      cn = await db.credit_notes.findOne({ where:{ id: {[Op.eq]: credit_note.id} } });

      if(cn) throw `Credit note id #${credit_note.id} has been created.`;

      await db.credit_notes.create({
        id: credit_note.id,
        date : dateToday,
        outlet_id : outlet_id,
        van_id : van_id,
        subtotal : credit_note.subtotal,
        tax : credit_note.tax,
        cent_rounding : credit_note.cent_rounding,
        grand_total : credit_note.grand_total,
        status: 'printed',
        created_by : user_id,
        updated_by : user_id,
        site_id : site_id
      },{transaction,raw:true});

      await db.credit_note_details.bulkCreate(cnData,{transaction});

      await sq.query(cnInsertOutletAccStmt,{
        type: sq.QueryTypes.INSERT,
        replacements: {outlet_id, cnId: credit_note.id},
        transaction
      });

      await sq.query(updateCurrentOutletAccStmt,{
        type: sq.QueryTypes.UPDATE,
        replacements: {outlet_id,reference_id: credit_note.id},
        transaction
      });

      await transaction.commit();

    }else if(voidInvoice && refund && debit_note){

      if(!voidInvoice.id) return res.status(422).send({error:'Please enter void invoice id.'});
      if(!voidInvoice.invoice_id) return res.status(422).send({error:'Please enter invoice id'});
      if(!refund.id) return res.status(422).send({error:'Please enter refund id.'})
      if(!refund.receipt_id) return res.status(422).send({error:'Please enter receipt id.'});
      if(!debit_note.id) return  res.status(422).send({error:'Please enter debit note id.'});
      if(!debit_note.credit_note_id) return res.status(422).send({error:"Please enter credit note id."});

      
      inv = await db.invoices.findOne({
        where:{
          id:{[Op.eq]: voidInvoice.invoice_id}
        }
      });

      if(!inv) return res.status(404).send({error:`Invoice #${voidInvoice.invoice_id} not found`});
      if(inv.voided == true) res.status(404).send({error:'Invoice already voided.'});

      transaction = await sq.transaction();

      await db.printouts.create({
        id: printout.id,
        date: dateToday,
        outlet_id: outlet_id,
        van_id: van_id,
        void_invoice_id: voidInvoice.id,
        refund_id: refund.id,
        debit_note_id: debit_note.id,
        last_credit: printout.last_credit,
        today_credit: printout.today_credit,
        total_credit: printout.total_credit,
        created_by: user_id
      },{transaction,raw:true});

      settlement = await db.settlement.findOne({
        where:{
          invoice_id: inv.id,
          current: true
        },transaction
      });

      settlementHistory = await sq.query(qGetSettlementHistory,{
        type: sq.QueryTypes.SELECT,
        replacements: {invoice_id: inv.id},
        transaction
      });

      await db.settlement.create({
        outlet_id:outlet_id,
        invoice_id: inv.id,
        due_date: settlementHistory[0].due_date,
        reference: voidInvoice.id,
        invoice_amount:  settlementHistory[0].invoice_amount,
        prev_outstanding: settlementHistory[0].invoice_amount,
        settlement_amount: 0,
        new_outstanding: 0,
        current: true,
        site_id: site_id
      },{transaction,raw:true});

      await db.settlement.destroy({
        where:{
          invoice_id: {[Op.eq]: inv.id},
          outlet_id: {[Op.eq]: outlet_id},
          reference: {[Op.ne]: voidInvoice.id}
        },transaction
      });
      
      if(settlement.new_outstanding != settlement.invoice_amount){
        settlementList = await sq.query(qFilterSettlement,{
          type: sq.QueryTypes.SELECT,
          replacements: {outlet_id,invoice_id: inv.id},
          transaction
        });

        let stmt_amount;

        for(let i = 0;i < settlementHistory.length; i++){
          stmt_amount = settlementHistory[i].settlement_amount;
          stmntRow = 0;

          do{
            let num = parseFloat(settlementList[stmntRow].new_outstanding).toFixed(2)
            if(num >= parseFloat(stmt_amount).toFixed(2)){
              await db.settlement.create({
                outlet_id: outlet_id,
                invoice_id: settlementList[stmntRow].invoice_id,
                due_date: settlementList[stmntRow].due_date,
                reference: settlementHistory[i].reference,
                invoice_amount:  settlementList[stmntRow].invoice_amount,
                prev_outstanding: settlementList[stmntRow].new_outstanding,
                settlement_amount: stmt_amount,
                new_outstanding: settlementList[stmntRow].new_outstanding - stmt_amount,
                current: true,
                site_id: site_id
              },{transaction,raw:true});

              await db.settlement.update({
                current: false
              },{
                where:{
                  invoice_id: {[Op.eq]: settlementList[stmntRow].invoice_id},
                  outlet_id: {[Op.eq]: outlet_id},
                  reference: {[Op.or]:[ {[Op.ne]: settlementHistory[i].reference}, {[Op.eq]: null} ]}
                },transaction
              });
              stmt_amount = 0
            }else{
              await db.settlement.create({
                outlet_id: outlet_id,
                invoice_id: settlementList[stmntRow].invoice_id,
                due_date: settlementList[stmntRow].due_date,
                reference: settlementHistory[i].reference,
                invoice_amount:  settlementList[stmntRow].invoice_amount,
                prev_outstanding: settlementList[stmntRow].new_outstanding,
                settlement_amount: settlementList[stmntRow].new_outstanding,
                new_outstanding: 0,
                current: true,
                site_id: site_id
              },{transaction,raw:true});

              await db.settlement.update({
                current: false
              },{
                where:{
                  invoice_id: {[Op.eq]: settlementList[stmntRow].invoice_id},
                  outlet_id: {[Op.eq]: outlet_id},
                  reference: {[Op.or]:[ {[Op.ne]: settlementHistory[i].reference}, {[Op.eq]: null} ]}
                },transaction
              });

              stmt_amount = stmt_amount - settlementList[stmntRow].new_outstanding
            }

               
            stmntRow++;
          }while(stmt_amount> 0 && settlementList.length > stmntRow)
        }
      }

      let vi = await db.void_invoices.findOne({
        where:{
          id :{ [Op.eq]: voidInvoice.id}
        }
      })

      if(vi) throw `Void invoice id #${voidInvoice.id} has been created.`;

      await db.void_invoices.create({
        id:voidInvoice.id,
        date: dateToday,
        outlet_id: outlet_id,
        van_id: van_id,
        subtotal: inv.subtotal,
        discount: inv.discount,
        tax: inv.tax,
        cent_rounding: inv.cent_rounding,
        grand_total: inv.grand_total,
        status : 'printed',
        invoice_id: inv.id,
        created_by: user_id,
        site_id : site_id
      },{transaction,raw:true});

      inv.voided = true;
      await inv.save({transaction});

      await sq.query(voidInvInsertOutletAccStmt,{
        type: sq.QueryTypes.INSERT,
        replacements: {outlet_id, viId: voidInvoice.id},
        transaction
      });

      await sq.query(updateCurrentOutletAccStmt,{
        type: sq.QueryTypes.UPDATE,
        replacements: {outlet_id,reference_id: voidInvoice.id},
        transaction
      });

      let rcpt = await db.receipts.findOne({
        where:{
          id: {[Op.eq]: refund.receipt_id}
        },transaction
      });

      if(!rcpt) throw `Receipt #${refund.receipt_id} not found.`
      if(rcpt.voided != false) throw `Receipt already voided.`

      await sq.query(qRefundSettlement,{
        type: sq.QueryTypes.INSERT,
        replacements: {refund_id: refund.id, reference: rcpt.id},
        transaction
      });

      settlementHistory = await sq.query(qFilterSettlementRefrence,{
        type: sq.QueryTypes.SELECT,
        replacements: {reference: rcpt.id},
        transaction
      });

      for(let i = 0; settlementHistory.length > i;i++ ){
        await db.settlement.update({
          current: false
        },{
          where:{
            invoice_id: {[Op.eq]: settlementHistory[i].invoice_id},
            outlet_id: {[Op.eq]: outlet_id},
            reference: {[Op.or]:[ {[Op.ne]: refund.id}, {[Op.eq]: null} ]}
          },transaction
        });
      }

      let rf = await db.refunds.findOne({
        where:{
          id: {[Op.eq]: refund.id}
        }
      });

      if(rf) throw `Refund id #${refund.id} has been created`

      await db.refunds.create({
        id: refund.id,
        date: dateToday,
        outlet_id: outlet_id,
        van_id: van_id,
        collected_amount: rcpt.collected_amount,
        method: rcpt.method,
        receipt_id: rcpt.id,
        status: 'printed',
        created_by: user_id
      },{transaction,raw:true});

      rcpt.voided = true;
      await rcpt.save({transaction});

      await sq.query(refundInsertOutletAccStmt,{
        type: sq.QueryTypes.INSERT,
        replacements: {outlet_id, rId: refund.id},
        transaction
      })

      await sq.query(updateCurrentOutletAccStmt,{
        type: sq.QueryTypes.UPDATE,
        replacements: {outlet_id,reference_id: refund.id},
        transaction
      });

      cn = await db.credit_notes.findOne({
        where:{
          id: {[Op.eq]: debit_note.credit_note_id}
        },transaction
      });

      if(!cn) throw `Credit Note #${cn.id} not found.`
      if(cn.voided != false) throw `Credit note already voided.`

      let dn = await db.debit_notes.findOne({
        where: {
          id : {[Op.eq]: debit_note.id}
        }
      })

      if(dn) throw `Debit note id #${debit_note.id} has been created.`

      await db.debit_notes.create({
        id: debit_note.id,
        date: dateToday,
        outlet_id: outlet_id,
        van_id: van_id,
        subtotal: cn.subtotal,
        tax: cn.tax,
        cent_rounding: cn.cent_rounding,
        grand_total: cn.grand_total,
        credit_note_id: cn.id,
        remarks: debit_note.remarks,
        status: 'printed',
        created_by: user_id,
        site_id: site_id
      },{transaction,raw:true});

      await sq.query(dnDetailsInsert,{
        type: sq.QueryTypes.INSERT,
        replacements:{debit_note_id: debit_note.id, credit_note_id: cn.id},
        transaction
      })

      cn.voided = true;
      await cn.save({transaction});

      await sq.query(dnInsertOutletAccStmt,{
        type: sq.QueryTypes.INSERT,
        replacements: {outlet_id,dnId: debit_note.id},
        transaction
      });

      await sq.query(updateCurrentOutletAccStmt,{
        type: sq.QueryTypes.UPDATE,
        replacements: {outlet_id,reference_id: debit_note.id},
        transaction
      });

      await transaction.commit();

    }else{
      return res.status(422).send({error:'Plese enter an invoice.'})
    }
  }catch(e){
    if(transaction) transaction.rollback();
    console.error(e)
    return res.send({error:'Failed to create printout.'})
  }

  transaction = null;
  return res.send({status: 'success'});

};

module.exports = Sales;