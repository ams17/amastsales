const env = process.env.NODE_ENV || 'dev';
const conf = require('../config/config.json')[env];
const express = require('express');
const router = express.Router();
const dayjs = require('dayjs'); 
const bcrypt = require('bcrypt');
const {Parser} = require('json2csv');
const jwtAuth = require('../common/jwtAuth');
const Constants = require('../common/constants');
const Password = require('../common/password');
const db = require('../model/db.js');
const Op = db.Sequelize.Op;
const sq = db.sequelize;
const df = 'YYYY-MM-DD';
const limit = 20;

let Van_Return = {};

Van_Return.list = async function(req,res){
  let page = req.query.page;
  let keyword = req.query.keyword;
  let site_id =  req.query.site_id || 'all';
  let date = req.query.date;
  let where = {};

  page = (!page || isNaN(page) || parseInt(page) < 0)? 0 : parseInt(page) - 1;
  let offset = page * limit;

  let vanReturn;

  if(keyword) where.id = {[Op.iLike] : `%${keyword}%`};
  if(site_id != 'all') where.site_id = {[Op.eq] : site_id}

  if(date){
    let d = date.split(',');

    if(d.length== 1){
      if(!dayjs(d[0], df).isValid()) return res.status(422).send({errMsg: 'Date format is invalid.'});
      where.date = {[Op.eq]: d[0]};
    }else{
      if(!dayjs(d[0], df).isValid() || !dayjs(d[1], df).isValid()) return res.status(422).send({errMsg: 'Date format is invalid.'});
      where.date = { [Op.between]:[ d[0].format(df), d[1].format(df) ] }
    }
  }

  try {
    vanReturn = await db.returns.findAndCountAll({
      attributes: ['id', 'date', 'van_id', 'created_by', 'created_at'],
      order : [['created_at','DESC']],
      where,offset,limit,
      raw : true
    });

    vanReturn.limit = limit;
    vanReturn.offset = offset;

  } catch(e) {
    // statements
    console.error(e);
    return res.status(500).send({errMsg:'Failed to get Van Return list.'})
  }

  return res.send({vanReturn});
}

const qVanReturnDetails = `select sku_id, uom_id, array_to_json(array[max(fresh_qty), max(damaged_qty), max(old_qty), max(recalled_qty)]) as quantity from 
(with rd as (select * from return_details rid where return_id = :toid)
select sku_id, uom as uom_id, declared_quantity fresh_qty, 0 damaged_qty, 0 old_qty, 0 recalled_qty from rd where condition = 'fresh'
union select sku_id, uom as uom_id, 0 fresh_qty, declared_quantity damaged_qty, 0 old_qty, 0 recalled_qty from rd where condition = 'damaged'
union select sku_id, uom as uom_id, 0 fresh_qty, 0 damaged_qty, declared_quantity old_qty, 0 recalled_qty from rd where condition = 'old'
union select sku_id, uom as uom_id, 0 fresh_qty, 0 damaged_qty, 0 old_qty, declared_quantity recalled_qty from rd where condition = 'recalled'
) a group by sku_id, uom_id order by sku_id`;

Van_Return.details = async function(req,res){
  let toid = req.params.toid;
  let {user_id,role} = req.token;

  let returns;
  let details;

  if(!toid) return res.status(422).send({errMsg:'Please enter a Van Return ID.'});

  try{

    returns = await db.returns.findOne({ where:{ id:{[Op.eq]: toid}}, raw: true });
    if(!returns) return res.status(404).send({errmsg: 'Van Return ID not found.'});

    details = await sq.query(qVanReturnDetails, {
      type : sq.QueryTypes.SELECT, 
      replacements: {toid},
      raw: true
    });

  }catch(e){
    console.error(e)
    return res.status(500).send({errMsg:'Failed to get Van Return details.'})
  }

  return res.send({returns, details});
}

module.exports = Van_Return;