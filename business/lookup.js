const env = process.env.NODE_ENV || 'dev';
const conf = require('../config/config.json')[env];
var express = require('express');
var router = express.Router();
const db = require('../model/db.js');
const Op = db.Sequelize.Op;
const sq = db.sequelize;

const limit = 10;

let Lookup = {};

const qChannelList = `select channel_id as id, (select "name" from lookup.channels where id = channel_id) as "name" ,array_agg(json_build_object('id',subchannel_id,'name',(select "name" from lookup.subchannels s where id = subchannel_id),'outletConcepts', outletConcepts)) as subchannels from(
  select channel_id,subchannel_id, array_agg(json_build_object('id',outlet_concept_id,'name', (select "name" from lookup.outlet_concepts oc where id = outlet_concept_id), 'outletTypes', "outletTypes" )) as outletConcepts from(
    select channel_id,subchannel_id, outlet_concept_id, array_agg(json_build_object('channelId', id ,'id', outlet_type_id, 'name', (select "name" from lookup.outlet_types ot where id = outlet_type_id))) as "outletTypes" from public."outlet_hierarchy" h
    group by channel_id,subchannel_id ,outlet_concept_id
  )as c
  group by channel_id,subchannel_id 
) as c
group by channel_id`

Lookup.channelList = async function(req,res) {
  let rows;

  try {
    rows = await sq.query(qChannelList,{
      type: sq.QueryTypes.SELECT,
      raw : true
    })
  } catch(e) {
    // statements
    console.error(e);
    return res.status(500).send({error:'Failed to get channel list.'})
  }

  return res.send({
    status:'success',
    data:{
      count: rows.length,
      rows: rows
      
    }
  });
}

const qKeyAccountList = `select ka.presence_id as presence_code, (select "name" from lookup.presence where id = presence_id) as presence_name, array_agg(json_build_object('id',ka.id,'name',ka."name",'category',ka.category)) as chainStoreTypes from lookup.key_accounts ka 
group by presence_id`

Lookup.keyAccountList = async function(req,res){
  let rows;

  try {
    rows = await sq.query(qKeyAccountList,{
      type: sq.QueryTypes.SELECT,
      raw : true
    })
  } catch(e) {
    // statements
    console.error(e);
    return res.status(500).send({error:'Failed to get key account list.'})
  }

  return res.send({
    status:'success',
    data: {
      count: rows.length,
      rows: rows
      
    }
  });
}

Lookup.siteList = async function(req,res){
  
  let selected_id = req.query.selected_id;
  let is_default = req.query.is_default;
  let keywords = req.query.keywords;
  let page = req.query.page;

  let data;
  let where = {};

  page = (!page || isNaN(page) || parseInt(page) < 0)? 0 : parseInt(page) - 1;
  let offset = page * limit;

  let responseJson = {
    results: [],
    pagination: {
      more: false
    }
  }

  where.active = true;

  if (keywords != null) {
    where.id = {[Op.iLike]: `%${keywords}%`}
  }

  if (selected_id != null) {
    where.id = selected_id;
  }

  try {
    data = await db.sites.findAndCountAll({
      where: where,
      attributes: ['id', ['id', 'text']],
      order: [['id', 'asc']],
      limit: limit,
      offset: offset,
      raw: true
    });

    if (offset < data.rows.count) {
      responseJson.pagination.more = true;
    }

    responseJson.results = data.rows;
    
  } catch(e) {
    // statements
    console.error(e);
    return res.status(500).send({error:'Failed to get sites list.'});
  }
  
  return res.send(responseJson)
}

Lookup.lovList = async function(req,res){
  let group_id = req.query.group_id;
  let selected_id = req.query.selected_id;
  let is_default = req.query.is_default;
  let keywords = req.query.keywords;
  let page = req.query.page;

  let data;
  let where = {};

  if(!group_id) return res.status(422).send({error:"Please enter a group ID."})

  page = (!page || isNaN(page) || parseInt(page) < 0)? 0 : parseInt(page) - 1;
  let offset = page * limit;

  let responseJson = {
    results: [],
    pagination: {
      more: false
    }
  }

  where.group_id = group_id;
  where.active = true;

  if (keywords != null) {
    where.display = {[Op.iLike]: `%${keywords}%`}
  }

  if (selected_id != null) {
    where.id = selected_id;
  }

  if (is_default != null) {
    where.is_default = is_default;
  }

  try {
    data = await db.lovs.findAndCountAll({
      where: where,
      attributes: ['id', ['display', 'text']],
      order: [['sequence', 'asc']],
      limit: limit,
      offset: offset,
      raw: true
    });

    if (offset < data.rows.count) {
      responseJson.pagination.more = true;
    }

    responseJson.results = data.rows;
    
  } catch(e) {
    // statements
    console.error(e);
    return res.status(500).send({error:'Failed to get lovs list.'});
  }
  
  return res.send(responseJson)
}

Lookup.lovAndroidList = async function(req,res){
  let group_id = req.body.group_id;
  let data;

  if(!group_id) return res.status(422).send({error:"Please enter a group ID."})
  try {
    data = await db.lovs.findAndCountAll({
      where:{
        group_id : group_id,
        active: true
      },
      attributes: ['id', 'display', 'is_default'],
      order: [['sequence', 'asc']],
      raw: true
    });
  } catch(e) {
    // statements
    console.error(e);
    return res.status(500).send({error:'Failed to get lovs list.'});
  }

  return res.send({status:'success',data})
}

module.exports = Lookup;