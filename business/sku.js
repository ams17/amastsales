const env = process.env.NODE_ENV || 'dev';
const conf = require('../config/config.json')[env];
const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const {Parser} = require('json2csv');
const jwtAuth = require('../common/jwtAuth');
const Password = require('../common/password');
const db = require('../model/db.js');
const Op = db.Sequelize.Op;
const sq = db.sequelize;
const df = 'YYYY-MM-DD';
const limit = 20;

const SKUs = {};

SKUs.list = async function(req,res){
  let page = req.query.page;
  let keyword = req.query.keyword;
  let searchby = req.query.searchby || 'userid';
  let where = {};


  page = (!page || isNaN(page) || parseInt(page) < 0)? 0 : parseInt(page) - 1;
  let offset = page * limit;

  if(searchby == 'userid' && keyword)  where.id = {[Op.iLike]: `${keyword}%`};

  let skus;
  try{
    skus = await db.skus.findAndCountAll({
      attributes: ['id', 'name','category'],
      order: [['id']],
      where, offset, limit, raw: true,
      logging: console.log
    });
    skus.limit = limit;
    skus.offset = offset;
  }catch(e){
    console.error(e);
    return res.status(500).send({status: 'failed', errMsg: 'Failed to get skus.'});
  }
  return res.send({status: 'success', skus});
}

SKUs.create = async function(req,res){
  let id                = req.body.id;
  let name              = req.body.name;
  let category          = req.body.category;
  let {user_id, role}   = req.token;

  if(!id) res.status(500).send({errMsg:'Please enter SKUs id.'});
  if(!name) res.status(500).send({errMsg:'Please enter SKUs name.'});

  let skus;

  try {
    skus = await db.skus.findOne({
      where: {
        id : {[Op.eq]: id}
      },
      raw: true
    });

    if(skus) return res.status(422).send({errMsg:`SKUs id ${id} already used. Please enter a new id.`});

    await db.skus.create({
      id: id,
      name: name,
      category: category,
      active: true,
      created_by : user_id,
      created_at : new Date()
    });

  } catch(e) {
    // statements
    console.error(e);
    return res.status(500).send({errMsg: 'Failed to create new sku.'})
  }

  return res.send({msg: `SKUs ${id} create successfully.`})
}

SKUs.read = async function(req,res){
  let id = req.params.id;
  if(id == null) return res.status(422).send({status: 'failed', errMsg:'SKU ID not provided.'});

  let sku;
  try{
    sku = await db.skus.findOne({
      where: {id: {[Op.eq]: id}},
      raw:true
    });
    if(!sku) return res.status(404).send({status: 'failed', errMsg: `SKU #${id} not found.`});
  }catch(e){
    console.error(e);
    return res.status(500).send({status: 'failed', errMsg: `Failed to get sku #${id}`})
  }
  return res.send({status:'success', sku});
}


SKUs.update = async function(req, res){
  let id  = req.body.id;
  let {user_id,role} = req.token;

  if(!id) return res.status().send({errMsg:'SKUs ID not provided'});

  let skus;

  try {
    skus = await db.skus.findOne({
      where: {
        id : {[Op.eq]: id}
      }
    })
    
    if(!skus) return res.status(404).send({status: 'failed', errMsg: `SKUs #${id} not found.`});

    if("name" in req.body){
      if(req.body.name != null){
        if(req.body.name.toString().trim().length != 0){
          skus.name = req.body.name
        }
      }
    }
    if("category" in req.body){skus.category = req.body.category}

    skus.save();

  } catch(e) {
    // statements
    console.error(e);
  }

  return res.send({msg:'Updated skus successfully.'})
}

SKUs.delete = async function(req, res){
  let id  = req.body.id;
  let {user_id,role} = req.token;

  if(!id) return res.status().send({errMsg:'SKUs ID not provided'});

  let skus;

  try {
    skus = await db.skus.findOne({
      where: {
        id : {[Op.eq]: id}
      }
    })
    
    if(!skus) return res.status(404).send({status: 'failed', errMsg: `SKUs #${id} not found.`});

    skus.destroy();

  } catch(e) {
    // statements
    console.error(e);
  }

  return res.send({msg:'SKUs deleted successfully.'})
}
const qSkus = `select s.id,s."name",category,'data:image/jpg;base64,' || encode(image, 'base64') as image, array_agg(json_build_object('from_uom_id',suc.from_uom_id,'to_uom_id' ,suc.to_uom_id ,'multiply_by',suc.multiply_by))as "conversion", array_agg(json_build_object(su.uom_id ,json_build_object('sellable',su.sellable,'active',su.active))) as status from skus s
left join sku_uom_conversions suc 
on s.id = suc.sku_id 
left join sku_uoms su 
on s.id = su.sku_id 
and suc.from_uom_id = su.uom_id 
group by s.id, s."name" ,s.category`

SKUs.androidList = async function (req,res) {
  let rows;

  try{
    rows = await sq.query(qSkus,{
      type: sq.QueryTypes.SELECT,
      raw: true
    });

  }catch(e){
    console.error(e);
    return res.status(500).send({error:'Failed to get SKUs list.'})
  }
  
  return res.send({
    status:'success',
    data: {
      count: rows.length,
      rows: rows
      
    }
  });
}

const qTableSkus = `select sku_id, to_uom_id as uom_id from sku_uom_conversions where multiply_by = 1 order by sku_id;`
SKUs.skuTableList = async function(req, res){
  let skus; 
  try{
    skus = await sq.query(qTableSkus, {
      type : sq.QueryTypes.SELECT, 
      order:['sku_id'], 
      raw: true
    });
  }catch(e){
    console.error(e);
    return res.status(500).send({errMsg: `Failed to get sku list.`});
  }
  return res.send({skus});
}


const categoryQuery = `select id,value,display,is_default,active,sequence from lovs where group_id  = 'sku_categories' and active = true order by "sequence" asc`;
const subCategoryQuery = `select id,value,display,is_default,active,sequence from lovs where group_id  = 'sku_subcategories' and active = true order by "sequence" asc`;
const baseUomQuery = `select id,value,display,is_default,active,sequence from lovs where group_id  = 'uom' and active = true order by "sequence" asc`;

SKUs.getDetails = async function(req,res){
    let rows;
    try{
        categoryRows = await sq.query(categoryQuery,{
            type: sq.QueryTypes.SELECT,
            raw: true
        });
        subCategoryRows = await sq.query(subCategoryQuery,{
            type: sq.QueryTypes.SELECT,
            raw: true
        });
        baseUomRows = await sq.query(baseUomQuery,{
            type: sq.QueryTypes.SELECT,
            raw: true
        });
    }catch(e){
        console.error(e)
        return res.status(500).send({error: "Failed to get device list."})
    }    

    return res.send({
        data:{
            categoryCount: categoryRows.length,
            categoryRows: categoryRows,
            subCategoryCount: subCategoryRows.length,
            subCategoryRows: subCategoryRows,
            baseUomCount: baseUomRows.length,
            baseUomRows: baseUomRows,
        }
    });
}
module.exports = SKUs;