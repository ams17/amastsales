const env = process.env.NODE_ENV || 'dev';
const conf = require('../config/config.json')[env];
const express = require('express');
const router = express.Router();
const dayjs = require('dayjs'); 
const bcrypt = require('bcrypt');
const {Parser} = require('json2csv');
const jwtAuth = require('../common/jwtAuth');
const Constants = require('../common/constants');
const Password = require('../common/password');
const db = require('../model/db.js');
const Op = db.Sequelize.Op;
const sq = db.sequelize;
const df = 'YYYY-MM-DD';
const limit = 20;

let Reconciliation = {};

const qStockReconList = `select van_id as id, site_id, allotment_date, target_upload_date, status from stock_recon where site_id in (select site_id from user_sites where user_id = :user_id)`;

Reconciliation.stockList = async function(req, res){
  let date = req.query.date;
  let {role, user_id} = req.token;

  let stockRecon = {};
  try{

    let data = await sq.query(qStockReconList,{ 
      type: sq.QueryTypes.SELECT, 
      replacements: {user_id},
      raw: true
    });

    stockRecon.count = data.length;
    stockRecon.rows = data;
    stockRecon.offset = 0;
    stockRecon.limit = 20;

  }catch(e) {
    console.error(e);
    return res.status(500).send({errMsg:'Failed to get list of stock recon.'});
  }
  res.send({stockRecon});
}

const qVanStockSalesDetails = `select sku_id, uom_id, array_to_json(array[sum(alloted_qty), sum(sold_qty), sum(fresh_qty), sum(expected_qty), sum(received_qty), sum(not_received_qty)]) as quantity from 
(with  srl as (select * from stock_recon_ledger where van_id = :van_id)
select sku_id, uom_id, quantity as alloted_qty, 0 as sold_qty, 0 as fresh_qty, 0 as received_qty, 0 as expected_qty, 0 as not_received_qty from srl where operation = 'allotment'
union all select sku_id, uom_id, 0 as alloted_qty, quantity as sold_qty, 0 as fresh_qty, 0 as received_qty, 0 as expected_qty, 0 as not_received_qty from srl where operation = 'invoices'
union all select sku_id, uom_id, 0 as alloted_qty, 0 as sold_qty, 0 as fresh_qty, quantity as received_qty, 0 as expected_qty, 0 as not_received_qty from srl where operation = 'van_returns' and condition in('fresh')
union all select sku_id, uom_id, 0 as alloted_qty, 0 as sold_qty, 0 as fresh_qty, 0 as received_qty, 0 as expected_qty, quantity as not_received_qty from srl where operation = 'not_received' and condition in('fresh')
union all select sku_id, uom_id, 0 as alloted_qty, 0 as sold_qty, 0 as fresh_qty, 0 as received_qty, quantity as expected_qty, 0 as not_received_qty from srl where operation in('allotment', 'invoices', 'returns', 'not_received') and condition in('fresh')
) a group by sku_id, uom_id order by sku_id`;

const qVanStockReturnDetails = `select sku_id, uom_id, array_to_json(array[sum(old_qty), sum(damaged_qty), sum(recalled_qty), sum(expected_qty), sum(received_qty), sum(not_received_qty)]) as quantity from 
(with  srl as (select * from stock_recon_ledger where van_id = :van_id)
select sku_id, uom_id, 0 as fresh_qty, quantity as old_qty, 0 as damaged_qty, 0 as recalled_qty, 0 as received_qty, 0 as expected_qty, 0 as not_received_qty from srl where operation = 'credit_notes' and condition = 'old'
union all select sku_id, uom_id, 0 as fresh_qty, 0 as old_qty, quantity as damaged_qty, 0 as recalled_qty, 0 as received_qty, 0 as expected_qty, 0 as not_received_qty from srl where operation = 'credit_notes' and condition = 'damaged'
union all select sku_id, uom_id, 0 as fresh_qty, 0 as old_qty, 0 as damaged_qty, quantity as recalled_qty, 0 as received_qty, 0 as expected_qty, 0 as not_received_qty from srl where operation = 'credit_notes' and condition = 'recall'
union all select sku_id, uom_id, 0 as fresh_qty, 0 as old_qty, 0 as damaged_qty, 0 as recalled_qty, quantity as received_qty, 0 as expected_qty, 0 as not_received_qty from srl where operation = 'van_returns' and condition in('old', 'damaged', 'recall')
union all select sku_id, uom_id, 0 as fresh_qty, 0 as old_qty, 0 as damaged_qty, 0 as recalled_qty, 0 as received_qty, 0 as expected_qty, quantity as not_received_qty from srl where operation = 'not_received' and condition in('old', 'damaged', 'recall')
union all select sku_id, uom_id, 0 as fresh_qty, 0 as old_qty, 0 as damaged_qty, 0 as recalled_qty, 0 as received_qty, quantity as expected_qty, 0 as not_received_qty from srl where operation in('credit_notes', 'van_returns', 'not_received') and condition in('old', 'damaged', 'recall')
) a group by sku_id, uom_id order by sku_id`;

Reconciliation.details = async function(req,res){
  let van_id = req.params.van_id;
  let {user_id,role} = req.token;

  let van;
  let salesSummary, returnSummary;
  
  if(!van_id) return res.status(422).send({errMsg:'Please enter a Van ID.'});

  try{

    van = await db.vans.findOne({
      where:{
        id: {[Op.eq] : van_id}
      },
      raw : true
    });

    if(!van) return res.status(404).send({errMsg: 'Van ID not found.'});

    salesSummary = await sq.query(qVanStockSalesDetails, {
      type : sq.QueryTypes.SELECT, 
      replacements: {van_id},
      raw: true
    });

    returnSummary = await sq.query(qVanStockReturnDetails, {
      type : sq.QueryTypes.SELECT, 
      replacements: {van_id},
      raw: true
    });

  }catch(e){
    console.error(e)
    return res.status(500).send({errMsg:'Failed to get Van Stock Reconciliation details.'})
  }

  return res.send({van, salesSummary, returnSummary});
}

module.exports = Reconciliation;