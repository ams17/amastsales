const env = process.env.NODE_ENV || 'dev';
const conf = require('../config/config.json')[env];
const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const {Parser} = require('json2csv');
const jwtAuth = require('../common/jwtAuth');
const Constants = require('../common/constants');
const Password = require('../common/password');
const db = require('../model/db.js');
const Op = db.Sequelize.Op;
const sq = db.sequelize;
const df = 'YYYY-MM-DD';
const limit = 20;

const Dashboard = {};

const qSummary = `with data as (
                  select site_id from user_sites us where user_id = :user_id
                 )
                 select max(total_sales) as total_sales, max(total_returns) as total_returns, max(total_collection) as total_collections
                 from (
                   select sum(grand_total) as total_sales, 0 as total_returns, 0 as total_collection from invoices where voided = false and date >= date_trunc('month', CURRENT_DATE) and site_id in (select site_id from data)
                   union all
                   select 0 as total_sales, sum(grand_total) as total_returns, 0 as total_collection from credit_notes where voided = false and date >= date_trunc('month', CURRENT_DATE) and site_id in (select site_id from data)
                   union all
                   select 0 as total_sales, 0 as total_returns, sum(collected_amount) as total_collection from receipts where voided = false and date >= date_trunc('month', CURRENT_DATE) and site_id in (select site_id from data)
                  ) a`;

Dashboard.summaryList= async function(req,res){
  let {user_id} = req.token;
  let data;

  try{

     data = await sq.query(qSummary,{ 
      type: sq.QueryTypes.SELECT, 
      replacements: {user_id},
      raw: true
    });

  }catch(e){
    console.error(e);
    return res.status(500).send({status: 'failed', errMsg: 'Failed to get summary.'});
  }
  return res.send({status: 'success', data});
}

const qYearlySales = `with data as (
    select date, 1 as transaction_count
    from invoices
   where voided = false
     and date >= date_trunc('year', CURRENT_DATE)
     and site_id in (select site_id from user_sites where user_id = :user_id)
   )
   select to_char(date, 'Mon') as month, sum(transaction_count) as total_transaction_count from data group by to_char(date, 'MM'), to_char(date, 'Mon') order by to_char(date, 'MM') asc;`;

const qYearlyReturns = `with data as (
    select date, 1 as transaction_count
    from credit_notes
   where voided = false
     and date >= date_trunc('year', CURRENT_DATE)
     and site_id in (select site_id from user_sites where user_id = :user_id)
   )
   select to_char(date, 'Mon') as month, sum(transaction_count) as total_transaction_count from data group by to_char(date, 'MM'), to_char(date, 'Mon') order by to_char(date, 'MM') asc;`;

Dashboard.yearlySalesAndReturnsList= async function(req,res){
  let {user_id} = req.token;
  let data = {};

  try{

     let yearlySales = await sq.query(qYearlySales,{ 
      type: sq.QueryTypes.SELECT, 
      replacements: {user_id},
      raw: true
    });

     let yearlyReturns = await sq.query(qYearlyReturns,{ 
      type: sq.QueryTypes.SELECT, 
      replacements: {user_id},
      raw: true
    });

     data.yearlySales = yearlySales;
     data.yearlyReturns = yearlyReturns;

  }catch(e){
    console.error(e);
    return res.status(500).send({status: 'failed', errMsg: 'Failed to get summary.'});
  }
  return res.send({status: 'success', data});
}

const qRecentTransactionList = `select date, iv.id as reference, o.name as customer, iv.status, grand_total 
                          from invoices iv, outlets o
                          where iv.outlet_id = o.id
                          and voided = false
                          and o.site_id in (select site_id from user_sites where user_id = :user_id)
                          order by date desc
                          limit 5`;

Dashboard.recentTransactionList= async function(req,res){
  let {user_id} = req.token;
  let data;

  try{

     data = await sq.query(qRecentTransactionList,{ 
      type: sq.QueryTypes.SELECT, 
      replacements: {user_id},
      raw: true
    });

  }catch(e){
    console.error(e);
    return res.status(500).send({status: 'failed', errMsg: 'Failed to get recent transaction list.'});
  }
  return res.send({status: 'success', data});
}

const qBestSellingProductList = `with data as (
                                select sku_id, sum(quantity) as quantity
                                from invoice_details id
                                where invoice_id in(
                                select id
                                from invoices
                                where voided = false
                                and date >= date_trunc('month', CURRENT_DATE)
                                and site_id in (select site_id from user_sites where user_id = :user_id))
                                group by sku_id)
                                select sku_id, (quantity / SUM(quantity) OVER ()) AS "percentage" from data
                                limit 5`;

Dashboard.bestSellingProductList= async function(req,res){
  let {user_id} = req.token;

  let data;

  try{

     data = await sq.query(qBestSellingProductList,{ 
      type: sq.QueryTypes.SELECT, 
      replacements: {user_id},
      raw: true
    });

  }catch(e){
    console.error(e);
    return res.status(500).send({status: 'failed', errMsg: 'Failed to get best selling product.'});
  }
  return res.send({status: 'success', data});
}
module.exports = Dashboard;