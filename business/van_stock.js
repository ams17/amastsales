const env = process.env.NODE_ENV || 'dev';
const conf = require('../config/config.json')[env];
var express = require('express');
var router = express.Router();
const dayjs = require('dayjs'); 
const db = require('../model/db.js');
const Constants = require('../common/constants');
const Op = db.Sequelize.Op;
const sq = db.sequelize;
const df = 'YYYY-MM-DD';

let Van_Stock = {};

const qVanStockList = `select json_object_agg(sku_id, van_stock) as van_stock from (
  select sku_id ,json_object_agg("condition" ,quantity) as van_stock from van_stock i 
  where van_id = :van_id
  group by sku_id order by sku_id
)as s;`;

Van_Stock.details = async function(req,res){
  let van_id = req.token.van_id;
  let van_stock;

  try{
    van_stock = await sq.query(qVanStockList,{
      type: sq.QueryTypes.SELECT,
      replacements: {van_id: van_id},
      raw : true
    });

    van_stock = van_stock[0].van_stock;

  }catch(e){
    console.error(e);
    return res.status(500).send({error:'Failed to get van stock details'})
  }

  return res.send({van_stock});
}

const qAllot = `select json_object_agg(id,status) as allots from van_allot
  where van_id = :van_id
  and "date" <= current_date`;

const qNextToID = `select nextVal('seq_transfer_out') as toID; `;

const qVStckInInsertLedger = `INSERT INTO public.van_stock_ledger(id, van_id, "date", sku_id, uom_id, "condition", operation, reference, prev_quantity, movement, new_quantity, "current", description, created_by, created_at, updated_at)
  select uuid_generate_v4() as id, :van_id as van_id, current_date as "date", vas.sku_id, vas.uom_id, 'fresh', 'stock in' as operation , van_allot_id as reference , coalesce(vsl.new_quantity,0), vas.allotted_quantity, vas.allotted_quantity  + coalesce(vsl.new_quantity,0) as new_quantity, true as current, :desc as description, :user_id, current_timestamp, current_timestamp 
  from van_allot_details vas
  left join van_stock_ledger vsl 
  on (vas.sku_id = vsl.sku_id and vas.uom_id = vsl.uom_id and vsl."condition" = 'fresh')
  and vsl."current" = true
  and vsl.van_id = :van_id
  where vas.van_allot_id = :allot;`

const qVStckUpdateCurrentLedger = `update van_stock_ledger vsl
  set "current" = false
  from van_allot_details vad
  where (vad.sku_id = vsl.sku_id and vad.uom_id = vsl.uom_id and vsl."condition" = 'fresh')
  and vsl.reference != vad.van_allot_id 
  and vsl.van_id = :van_id
  and vad.van_allot_id = :allot
  and current = true;`

const qVStckUpdateLDaily = `update van_stock_ledger_daily vsld set movement = vsld.movement + vsl.movement, new_quantity = vsl.new_quantity 
  from van_stock_ledger vsl
  where vsl."current" = true
  and reference = :allot
  and (vsl.van_id = vsld.van_id and vsl."date" = vsld."date" and vsl.sku_id = vsld.sku_id and vsl.uom_id = vsld.uom_id and vsl."condition" = vsld."condition");`

const qVStckLedgerDaily = `INSERT INTO public.van_stock_ledger_daily ( id, van_id, "date", sku_id, uom_id, "condition", prev_quantity, movement, new_quantity, created_at, updated_at)
  select 
    uuid_generate_v4() as id, vsl.van_id , vsl.date, vsl.sku_id, vsl.uom_id, vsl."condition", 
    coalesce(vsl.prev_quantity, 0) as prev_quantity, 
    vsl.movement, 
    coalesce(vsl.prev_quantity, 0) + vsl.movement as new_quantity, 
    current_timestamp, current_timestamp 
  from van_stock_ledger vsl
  where vsl."current" = true
  and reference = :allot
  and (van_id , "date", sku_id, uom_id, "condition") not in (select van_id, "date", sku_id, uom_id, "condition" from van_stock_ledger_daily ild )`

const qUpdateVanStck= `update van_stock vs set quantity = vsl.new_quantity 
  from van_stock_ledger vsl 
  where reference = :allot
  and (vs.sku_id = vsl.sku_id and vs.uom_id = vsl.uom_id and vs."condition" = vsl."condition" and vs.van_id = vsl.van_id);`

const qInsertVanStck= `insert into public.van_stock(van_id, sku_id, uom_id, condition, quantity, created_at, updated_at)
select van_id, sku_id, uom_id, condition, vsl.new_quantity, current_timestamp, current_timestamp
  from van_stock_ledger vsl 
 where reference = :allot
   and (van_id , sku_id, uom_id, "condition") not in (select van_id, sku_id, uom_id, "condition" from van_stock vs);`

const qInsertTrxOutDetail = `INSERT INTO public.transfer_out_details
(id, transfer_out_id, sku_id, uom_id, "condition", quantity, created_at, updated_at)
select uuid_generate_v1(), :toID, vad.sku_id, vad.uom_id, 'fresh', sum(vad.allotted_quantity), current_timestamp, current_timestamp from van_allot_details vad 
where van_allot_id in (:allotments)
group by sku_id ,uom_id`

Van_Stock.in = async function(req,res){
  let allotments = req.body.allotments;
  let desc = req.body.desc;
  let {user_id, van_id, role} = req.token;
  let date = dayjs().format(df);
  let toID;
  let van;

  if(!allotments) return res.status(422).send({error:'Please select allotments.'});

  let allots
  try{
    van = await db.vans.findOne({
      where :{
        id: {[Op.eq]: van_id}
      }
    });

    allots = await  sq.query(qAllot,{
      type : sq.QueryTypes.SELECT,
      replacements: {van_id:van.id,date},
      raw: true
    });

    allots = allots[0].allots;

    if(!allots||allots.length == 0) return res.status(404).send({error: 'Allotment not found.'});

    transaction = await sq.transaction();

    for(let e of allotments){
      if(allots[e] != 'packed' || !allots[e]){
        throw `ID #${e} is not packed.`;
      }

      await sq.query(qVStckInInsertLedger,{
        type: sq.QueryTypes.INSERT,
        replacements : {user_id,allot: e,desc,van_id: van.id},
        transaction
      });

      await sq.query(qVStckUpdateCurrentLedger,{
        type: sq.QueryTypes.UPDATE,
        replacements : {van_id:van.id,allot: e},
        transaction
      });

      await sq.query(qVStckUpdateLDaily,{
        type: sq.QueryTypes.UPDATE,
        replacements : {allot: e},
        transaction
      });

      await sq.query(qVStckLedgerDaily,{
        type: sq.QueryTypes.UPDATE,
        replacements : {allot: e},
        transaction
      });

      await sq.query(qUpdateVanStck,{
        type: sq.QueryTypes.UPDATE,
        replacements : {allot: e},
        transaction
      });

      await sq.query(qInsertVanStck,{
        type: sq.QueryTypes.INSERT,
        replacements : {user_id,allot: e,desc,van_id: van.id},
        transaction
      });
    }

    toID = await sq.query(qNextToID,{type: sq.QueryTypes.SELECT, raw: true,transaction});

    toID = `OUT_${van.site_id}_${toID[0].toid}`;

    await db.transfer_outs.create({
      id : toID,
      date : date,
      from_site_id : van.site_id,
      to_site_id : van.id,
      remark : desc,
      created_by : user_id
    },{ transaction });

    await sq.query(qInsertTrxOutDetail,{
      type:sq.QueryTypes.INSERT,
      replacements: {allotments,toID},
      transaction
    })

    await db.van_allot.update({
      status: 'loaded'
    },{
      where:{
        id : {[Op.in]: allotments}
      },transaction
    })

    await transaction.commit();

  }catch(e){
    console.error(e);
    if(transaction) await transaction.rollback();
    return res.status(500).send({error:'Failed to create van stock stock in.'})
  }

  return res.send({status:'success'})
};

const qLoadedAllot = `select json_object_agg(id,status) as allots from van_allot
  where van_id = :van_id
  and "date" = :date
  and tag = 'do'`;

const qVanStockOutAllotInsertLedger = `INSERT INTO public.van_stock_ledger(id, van_id, "date", sku_id, uom_id, "condition", operation, reference, prev_quantity, movement, new_quantity, "current", description, created_by, created_at, updated_at)
  select uuid_generate_v4() as id, :user_id as van_id, current_date as "date", vas.sku_id, vas.uom_id, 'fresh', 'van return allot' as operation , :rID as reference , coalesce(vsl.new_quantity,0) as prev_quantity , 0 -vas.allotted_quantity as movement, coalesce(vsl.new_quantity,0) - vas.allotted_quantity as new_quantity, true as "current", :desc as description, :user_id, current_timestamp, current_timestamp 
  from van_allot_details vas
  left join van_stock_ledger vsl 
  on (vas.sku_id = vsl.sku_id and vas.uom_id = vsl.uom_id and vsl."condition" = 'fresh')
  and vsl."current" = true
  and vsl.van_id = :van_id 
  where vas.van_allot_id = :allot`;

const qNextRID = `select nextVal('seq_returns') as rID;`;

const qInsertReturn = `INSERT INTO public.return_details
  (return_id, sku_id, uom, "condition", declared_quantity, created_at, updated_at)
  select :return_id, vad.sku_id, vad.uom_id, 'fresh', sum(vad.allotted_quantity), current_timestamp, current_timestamp from van_allot_details vad 
  where vad.van_allot_id = :allot
  group by sku_id , uom_id ;`;

Van_Stock.out_delivery = async function(req,res){
  let allotments = req.body.allotments;
  let desc = req.body.desc;
  let {user_id, van_id, role} = req.token;
  let date = dayjs().format(df);

  if(!allotments) return res.status(422).send({error:'Please select allotments.'});

  let allots
  let transaction
  let van;
  let rID;

  try {
    van = await db.vans.findOne({
      where:{
        id: {[Op.eq]: van_id}
      }
    });

    allots = await sq.query(qLoadedAllot,{
      type : sq.QueryTypes.SELECT,
      replacements: {van_id: van.id,date},
      raw: true
    });

    allots = allots[0].allots;

    if(!allots||allots.length == 0) return res.status(404).send({error: 'Allotment not found.'});

    transaction = await sq.transaction();

    for(let e of allotments){
      if(allots[e] != 'loaded' || !allots[e]){
        throw `ID #${e} is not loaded.`;
      }

      rID = await sq.query(qNextRID,{type: sq.QueryTypes.SELECT, raw: true,transaction});

      rID = `R_${van.id}_${rID[0].rid}`;

      await sq.query(qVanStockOutAllotInsertLedger,{
        type: sq.QueryTypes.INSERT,
        replacements : {user_id:user_id,allot: e,desc:desc,rID,van_id: van.id},
        transaction
      });

      await sq.query(qVStckUpdateCurrentLedger,{
        type: sq.QueryTypes.UPDATE,
        replacements : {van_id: van.id,allot: e},
        transaction
      });

      await sq.query(qVStckUpdateLDaily,{
        type: sq.QueryTypes.UPDATE,
        replacements : {allot: e},
        transaction
      });

      await sq.query(qVStckLedgerDaily,{
        type: sq.QueryTypes.UPDATE,
        replacements : {allot: e},
        transaction
      });

      await sq.query(qUpdateVanStck,{
        type: sq.QueryTypes.UPDATE,
        replacements : {allot: e},
        transaction
      });

      await db.returns.create({
        id: rID,
        van_id: van.id,
		site_id: van.site_id,
        date: date,
        status: 'pending',
        package_id: e,
        created_by: user_id
      },{transaction});

      await sq.query(qInsertReturn,{
        query: sq.QueryTypes.INSERT,
        replacements:{return_id: rID,allot: e},
        transaction
      });

    }

    await transaction.commit();

  } catch(e) {
    // statements
    if(transaction) await transaction.rollback();
    console.error(e);
    return res.status(500).send({error:'Failed to create van stock out.'})
  }

  return res.send({status:'success'});
}

const qSkus = `select json_object_agg(sku_Id, to_uom_id) as uoms from sku_uom_conversions where multiply_by = 1;`

const qVanStock = `select json_object_agg(sku_id,quantity) as van_stock
  from (
    select sku_id ,array_to_json(array[max(fresh),max("old"),max(damage),max(recall)]) as quantity 
    from (
      with vs as(
        select * from van_stock vs
        where van_id  = :van_id 
      ) 
      select sku_id  ,uom_id ,quantity as fresh, 0 as "old", 0 as damage, 0 as recall from vs where "condition" = 'fresh'
      union select sku_id  ,uom_id ,0 as fresh, quantity as "old", 0 as damage, 0 as recall from vs where "condition" = 'old'
      union select sku_id  ,uom_id ,0 as fresh, 0 as "old", quantity as damage, 0 as recall from vs where "condition" = 'damage'
      union select sku_id  ,uom_id ,0 as fresh, 0 as "old", 0 as damage, quantity as recall from vs where "condition" = 'recall'
    ) a
    group  by sku_id ,uom_id 
    order by sku_id 
  )b`;

const qVanStockOutAdhocInsertLedger = `INSERT INTO public.van_stock_ledger(id, van_id, "date", sku_id, uom_id, "condition", operation, reference, prev_quantity, movement, new_quantity, "current", description, created_by, created_at, updated_at)
  select uuid_generate_v4() as id, :van_id as van_id, current_date as "date", rd.sku_id, rd.uom, rd."condition" , 'van return adhoc' as operation , :rID as reference , coalesce(vsl.new_quantity,0) as prev_quantity , 0 -rd.declared_quantity as movement, coalesce(vsl.new_quantity,0) - rd.declared_quantity as new_quantity, true as "current", :desc as description, :user_id, current_timestamp, current_timestamp 
  from return_details rd
  left join van_stock_ledger vsl 
  on (rd.sku_id = vsl.sku_id and rd.uom = vsl.uom_id and vsl."condition" = rd."condition")
  and vsl."current" = true
  and vsl.van_id = :van_id
  where rd.return_id = :rID;`;

const qVSOutAdhocUpdateCurrentLedger = `update van_stock_ledger vsl
  set "current" = false
  from return_details rd
  where (rd.sku_id = rd.sku_id and rd.uom = vsl.uom_id and vsl."condition" = rd."condition")
  and vsl.reference != rd.return_id 
  and vsl.van_id = :van_id 
  and rd.return_id = :rID
  and current = true;`;

const qVSOutAdhocUpdateLedgerDaily = `update van_stock_ledger_daily vsld set movement = vsld.movement + vsl.movement, new_quantity = vsl.new_quantity 
  from van_stock_ledger vsl
  where vsl."current" = true
  and reference = :rID
  and (vsl.van_id = vsld.van_id and vsl."date" = vsld."date" and vsl.sku_id = vsld.sku_id and vsl.uom_id = vsld.uom_id and vsl."condition" = vsld."condition");`

const qInsertVSOutLedgerDaily = `INSERT INTO public.van_stock_ledger_daily ( id, van_id, "date", sku_id, uom_id, "condition", prev_quantity, movement, new_quantity, created_at, updated_at)
  select 
    uuid_generate_v4() as id, vsl.van_id , vsl.date, vsl.sku_id, vsl.uom_id, vsl."condition", 
    coalesce(vsl.prev_quantity, 0) as prev_quantity, 
    vsl.movement, 
    coalesce(vsl.prev_quantity, 0) + vsl.movement as new_quantity, 
    current_timestamp, current_timestamp 
  from van_stock_ledger vsl
  where vsl."current" = true
  and reference = :rID
  and (van_id , "date", sku_id, uom_id, "condition") not in (select van_id, "date", sku_id, uom_id, "condition" from van_stock_ledger_daily ild )`;

  const qUpdateVanStock= `update van_stock vs set quantity = vsl.new_quantity 
  from van_stock_ledger vsl 
  where reference = :rID
  and (vs.sku_id = vsl.sku_id and vs.uom_id = vsl.uom_id and vs."condition" = vsl."condition" and vs.van_id = vsl.van_id);`

Van_Stock.out_adhoc = async function(req,res){
  let skus = req.body.skus;
  let desc = req.body.desc;
  let {user_id, van_id, role} = req.token;
  let date = dayjs().format(df);

  let van_stock;
  let transaction;
  let van;
  let rID;
  let rData =[];

  try{
    van = await db.vans.findOne({
      where:{
        id : {[Op.eq]: van_id}
      }
    });

    let uoms = await sq.query(qSkus,{type : sq.QueryTypes.SELECT, raw: true});
    uoms = uoms[0].uoms;

    van_stock = await sq.query(qVanStock,{
      type: sq.QueryTypes.SELECT,
      replacements: {van_id: van.id},
      raw: true
    });

    van_stock = van_stock[0].van_stock;

    if(!van_stock||van_stock.length == 0) return res.status(404).send({error: 'Van stock is empty.'});

    rID = await sq.query(qNextRID,{type: sq.QueryTypes.SELECT, raw: true,transaction});

    rID = `R_${van.id}_${rID[0].rid}`;

    for(let s of skus){
      for(let i = 0; i < s.quantity.length; i++){
        let qty = s.quantity[i];
        let skuid = van_stock[s.sku_id]
        let condition = i;
        
        if(qty <= 0) continue;
        if(!skuid[condition]) throw `Insufficient stock for ${condition} ${s.sku_id}`;
        if(skuid[condition] < qty) return res.status(422).send({error:`Current stock balance for ${e.sku_id} is ${skuid[condition]}. Cannot return more.`});

        rData.push({
          return_id: rID,
          sku_id: s.sku_id,
          uom: uoms[s.sku_id],
          condition: Constants.conditions[i],
          declared_quantity: qty,
		      actual_quantity: 0
        });
      }
    }

    transaction = await sq.transaction();

    await db.returns.create({
      id: rID,
      site_id: van.site_id,
      van_id: van.id,
      date: date,
      status: 'pending',
      created_by: user_id
    },{transaction});

    await db.return_details.bulkCreate(rData,{transaction});

    await sq.query(qVanStockOutAdhocInsertLedger,{
      type: sq.QueryTypes.INSERT,
      replacements: {van_id: van.id,rID,user_id,desc},
      transaction
    });

    await sq.query(qVSOutAdhocUpdateCurrentLedger,{
      type : sq.QueryTypes.UPDATE,
      replacements: {van_id: van.id,rID},
      transaction
    });

    await sq.query(qVSOutAdhocUpdateLedgerDaily,{
      type: sq.QueryTypes.UPDATE,
      replacements:{rID},
      transaction
    });

    await sq.query(qInsertVSOutLedgerDaily,{
      type: sq.QueryTypes.INSERT,
      replacements:{rID},
      transaction
    });

    await sq.query(qUpdateVanStock,{
      type: sq.QueryTypes.UPDATE,
      replacements:{rID},
      transaction
    });

    await transaction.commit();

  }catch(e){
    if(transaction) await transaction.rollback();
    console.error(e);
    return res.status(500).send({error:`Failed to create van stock return`})
  }

  return res.send({status:'success'});
}

module.exports = Van_Stock;