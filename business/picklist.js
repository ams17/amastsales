const env = process.env.NODE_ENV || 'dev';
const conf = require('../config/config.json')[env];
const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const dayjs = require('dayjs');
const {Parser} = require('json2csv');
const jwtAuth = require('../common/jwtAuth');
const Password = require('../common/password');
const db = require('../model/db.js');
const Op = db.Sequelize.Op;
const sq = db.sequelize;
const df = 'YYYY-MM-DD';
const limit = 50;

let Picklist = {};

Picklist.list = async function(req, res){
  let page = req.query.page;
  let keyword = req.query.keyword;
  let site_id =  req.query.site_id || 'all';
  let date = req.query.date;
  let where = {};

  page = (!page || isNaN(page) || parseInt(page) < 0)? 0 : parseInt(page) - 1;
  let offset = page * limit;

  let picklists;

  if(keyword) where.id = {[Op.iLike] : keyword};
  if(site_id != 'all') where.site_id = {[Op.eq] : site_id}

  if(date){
    let d = date.split(',');

    if(d.length== 1){
      if(!dayjs(d[0], df).isValid()) return res.status(422).send({errMsg: 'Date format is invalid.'});
      where.created_date = {[Op.eq]: d[0]};
    }else{
      if(!dayjs(d[0], df).isValid() || !dayjs(d[1], df).isValid()) return res.status(422).send({errMsg: 'Date format is invalid.'});
      where.created_date = { [Op.between]:[ d[0].format(df), d[1].format(df) ] }
    }
  }

  try {
    picklists = await db.picklists.findAndCountAll({
      attributes: ['id', 'site_id', 'status', 'created_by', 'created_at', 'created_date'],
      order : [['created_at','DESC']],
      where,offset,limit,
      raw : true
    });

    picklists.limit = limit;
    picklists.offset = offset;

  } catch(e) {
    // statements
    console.error(e);
    return res.status(500).send({errMsg:'Failed to get Picklists.'});
  }

  return res.send({picklists});
}

const qPicklistSum = `select sku_id, uom_id as uom_id, array_to_json(array[sum(allotted_quantity)]) as quantity
from van_allot_details vad 
inner join picklist_details pd on vad.van_allot_id = pd.van_allot_id and picklist_id = :picklist_id
group by sku_id, uom_id order by sku_id, uom_id`;

const qPicklistPacking = `select vad.van_allot_id, va.van_id, va.status, json_agg( json_build_object('sku_id', sku_id, 'uom_id', uom_id, 'quantity', array[allotted_quantity]))as allot_details
from van_allot_details vad
inner join van_allot va 
on vad.van_allot_id = va.id
inner join picklist_details pd
on pd.van_allot_id = vad.van_allot_id 
and picklist_id = :picklist_id
group by vad.van_allot_id, van_id, status 
order by va.van_id, vad.van_allot_id`;

Picklist.read = async function(req, res){
  let {id} = req.params;
  let {user_id, role} = req.token;

  let picklist, batch, packing;
  try{
    picklist = await db.picklists.findOne({where: {id: {[Op.eq]: id}}, raw: true});
    if(!picklist) return res.status(404).send({errMsg: 'Picklist ID not found.'});

    let p1 = sq.query(qPicklistSum, {
      type: sq.QueryTypes.SELECT, 
      replacements: {picklist_id: picklist.id},
      raw: true
    });

    let p2 = sq.query(qPicklistPacking, {
      type: sq.QueryTypes.SELECT, 
      replacements: {picklist_id: picklist.id},
      raw: true
    });

    [batch, packing] = await Promise.all([p1, p2]);

    // TODO
  }catch(e){
    console.error(e);
    return res.status(500).send({errMsg:'Failed to get Picklist details.'})
  }
  return res.send({picklist, batch, packing});
}

const qNextPicklist = `SELECT nextval('seq_picklist') as picklist_id`;
const qCrossSiteAllotment = `select distinct v.site_id from van_allot va left join vans v on va.van_Id = v.id where
va.id in (select van_allot_id from picklist_details where picklist_id = (select id from picklists where created_by = :user_id and status = 'opened')) or va.id in (:allotments) `;

Picklist.add = async function(req, res){
  let {allotments} = req.body;
  let {user_id, role} = req.token;

  if(!allotments) return res.status(422).send({errMsg: 'Please send a list of allotments to add to picklist.'});
  if(!Array.isArray(allotments)) return res.status(422).send({errMsg: 'Array expected for list of allotments.'});
  
  let allot;
  let picklist;
  let isNew = false;
  let transaction;

  try{

    transaction = await sq.transaction();

    let _allots = await db.van_allot.findAll({ where: {id: {[Op.in]: allotments}, status: 'allotted'}, raw: true, transaction });
    if(!_allots || _allots.length == 0){
      return res.status(422).send({errMsg: 'Could not find allotments which are not yet added to a picklist.'});
    }
    allots = _allots.map(a => a.id);

    let currentPicklist = await db.picklists.findOne({
      attributes: ['id', 'site_id'],
      where: {created_by: user_id, status: 'opened' }, 
      raw: true
    })

    let checkCrossSite = await sq.query(qCrossSiteAllotment, {
      type: sq.QueryTypes.SELECT, 
      replacements: {user_id, allotments: allots, user_id},
      raw: true, 
      transaction
    });

    let site_count = checkCrossSite.length;
    if(site_count > 1) return res.status(422).send({errMsg: `Current picklist ${currentPicklist.id} is still open for site ${currentPicklist.site_id}. Cannot add allotments from other sites.`});
    let site_id = checkCrossSite[0].site_id;

    picklist = await db.picklists.findOne({ where: {created_by: user_id, status: 'opened'}, raw: true, transaction });

    if(!picklist){
      let nv = await sq.query(qNextPicklist,{type: sq.QueryTypes.SELECT, raw: true, transaction});
      let picklist_id = 'P' + nv[0].picklist_id;
      picklist = await db.picklists.create({
        id: picklist_id, 
        site_id: site_id,
        status: 'opened',
        created_date: dayjs().format(df),
        created_by: user_id,
        created_at: sq.now, 
        updated_at: sq.now
      }, {returning: true});
      isNew = true;
    }    
    
    let data = [];
    for(let a of allots) {
      data.push({
        picklist_id: picklist.id,
        van_allot_id: a
      });
    }

    await db.picklist_details.bulkCreate(data, {transaction});
    await db.van_allot.update({
      status: 'added to picklist',
      added_to_picklist_by: user_id, 
      added_to_picklist_at: dayjs().toDate()
    },{
      where: {id: {[Op.in]: allots}}, 
      transaction
    });

    await transaction.commit();

  }catch(e){
    console.error(e);
    if(transaction) await transaction.rollback();
    return res.status(500).send({errMsg: 'Failed to add allotments to picklist.'});
  }

  return res.send({picklist, isNew});
}

const qCheckStockAvailability = `with picklist as (
  select * from picklists where id = :picklist_id and status = 'opened'
), batch as ( 
  select vad.sku_id, vad.uom_id, sum(vad.allotted_quantity) allotted_quantity
  from picklist as p 
  inner join picklist_details pd 
  on pd.picklist_id = p.id
  inner join van_allot_details vad 
  on vad.van_allot_id  = pd.van_allot_id 
  where id = :picklist_id and status = 'opened'
  group by vad.sku_id, vad.uom_id
), reserved as (      
  select vad.sku_id, vad.uom_id, 'fresh'::text as condition, sum(vad.allotted_quantity) as reserved_quantity from van_allot va 
  inner join van_allot_details vad on vad.van_allot_id = va.id and status in ('sent for picking', 'packed')
  inner join vans v on v.id = va.van_id 
  inner join picklist p on v.site_id = p.site_id
  group by vad.sku_id, vad.uom_id
)
select i.site_id, i.sku_id, i.uom_id, i.condition, 
i.quantity as available_quantity, coalesce(r.reserved_quantity, 0)::int4 as reserved_quantity, b.allotted_quantity::int4
from inventory i 
inner join picklist p on p.site_id = i.site_id
left join reserved r on r.sku_id = i.sku_id and r.uom_id = r.uom_id
inner join batch b on b.sku_id = i.sku_id and b.uom_id = i.uom_id
where i.condition = 'fresh'`;

const qUpdatePicklistSendPicking = `update picklists set status = 'sent for picking', sent_for_picking_by = :user_id, sent_for_picking_at = current_timestamp where id = :picklist_id`;
const qUpdateAllotmentsSendPicking = `update van_allot va set status = 'sent for picking', sent_for_picking_by = :user_id, sent_for_picking_at = current_timestamp where id in (select van_allot_id from picklist_details where picklist_id = :picklist_id)`;

Picklist.sendForPicking = async function(req, res){
  let {id} = req.body;
  let {role, user_id} = req.token;
  let transaction;

  if(!id) return res.status(422).send({errMsg: 'Please send picklist ID.'});

  try{
    let picklist = await db.picklists.findOne({where: {id: {[Op.eq]: id }}});
    if(!picklist) return res.status(404).send({errMsg: `Picklist ${id} not found.`});

    transaction = await sq.transaction();
    let stock = await sq.query(qCheckStockAvailability, { 
      type: sq.QueryTypes.SELECT, 
      replacements: {picklist_id: id}, 
      logging: console.log,
      raw: true, 
      transaction });
    if(!stock || stock.length == 0){
      throw `Stock matching returned zero. Data error occurred.`;
    }

    let insufficiencies = stock.filter(s => {
      return (s.available_quantity - s.reserved_quantity - s.allotted_quantity) < 0 
    });

    if(insufficiencies.length > 0){
      console.error({insufficiencies})
      if(transaction) await transaction.rollback();
      return res.status(422).send({
        insufficiencies: insufficiencies,
        errCode: 'insufficient',
        errMsg: 'Insufficient stock.'
      });
    }

    await sq.query(qUpdatePicklistSendPicking, { 
      type: sq.QueryTypes.SELECT, 
      replacements: {picklist_id: id, user_id}, 
      raw: true, transaction });
    await sq.query(qUpdateAllotmentsSendPicking, { 
      type: sq.QueryTypes.SELECT, 
      replacements: {picklist_id: id, user_id}, 
      raw: true, transaction });

    await transaction.commit();

  }catch(e){
    console.error(e);
    if(transaction) await transaction.rollback();
    return res.status(500).send({errMsg: 'Failed to send picklist for picking.'});
  }

  return res.send({id});
}

const qDelete1 = `update van_allot set status = 'allotted', added_to_picklist_by = null, added_to_picklist_at = null
  where id in (select id from van_allot va inner join picklist_details pd on pd.van_allot_id = va.id and picklist_id = :id)`;
const qDelete2 = `delete from picklist_details where picklist_id = :id`;
const qDelete3 = `delete from picklists where id = :id`;

Picklist.delete = async function(req, res){
  let {id} = req.body;
  let {role, user_id} = req.token;
  let transaction;

  if(!id) return res.status(422).send({errMsg: 'Please send picklist ID.'});

  try{
    let picklist = await db.picklists.findOne({
      where: {id: {[Op.eq]: id}}
    });
    if(!picklist) return res.status(404).send({errMsg: `Picklist ${id} not found`});
    if(picklist.created_by != user_id) return res.status(401).send({errMsg: `Unauthorized to delete. Picklist ${id} not created by you.`});

    transaction = await sq.transaction();
    await sq.query(qDelete1, { type: sq.QueryTypes.UPDATE, replacements: {id}, raw: true, transaction });
    await sq.query(qDelete2, { type: sq.QueryTypes.DELETE, replacements: {id}, raw: true, transaction });
    await sq.query(qDelete3, { type: sq.QueryTypes.DELETE, replacements: {id}, raw: true, transaction });

    await transaction.commit();
  }catch(e){
    console.error(e);
    if(transaction) await transaction.rollback();
    return res.status(500).send({errMsg: 'Failed to delete picklist.'});
  }

  return res.send({id});
}

const qRemove1 = `update van_allot set status = 'allotted', added_to_picklist_by = null, added_to_picklist_at = null where id = :allot_id`;

Picklist.remove = async function(req, res){
  let {allot_id} = req.body;
  let {role, user_id} = req.token;
  let transaction;

  if(!allot_id) return res.status(422).send({errMsg: 'Please send allotment ID.'});
  let picklist_removed = false;
  try{

    let allot = await db.van_allot.findOne({
      where: {id: {[Op.eq]: allot_id}},
      raw: true
    });
    if(!allot) return res.status(404).send({errMsg: `Allotment ${allot_id} not found.`});

    let pd = await db.picklist_details.findOne({
      where: {van_allot_id: {[Op.eq]: allot_id}}, 
      raw: true
    });

    let picklist = await db.picklists.findOne({ where: {id: {[Op.eq]: pd.picklist_id}} });
    if(!picklist) return res.status(404).send({errMsg: `Picklist ${pd.picklist_id} not found`});
    if(picklist.created_by != user_id) return res.status(401).send({errMsg: `Unauthorized to delete. Picklist ${id} not created by you.`});

    let pds = await db.picklist_details.findAll({
      where: {picklist_id: {[Op.eq]: picklist.id}}, 
      raw: true
    });

    transaction = await sq.transaction();
    await db.picklist_details.destroy({
      where: {van_allot_id: allot.id},
      transaction
    });
    await sq.query(qRemove1, { type: sq.QueryTypes.UPDATE, replacements: {allot_id}, raw: true, transaction });

    if(pds.length == 1) {
      await picklist.destroy({transaction});
      picklist_removed = true;
    }
    await transaction.commit();

  }catch(e){
    console.error(e);
    if(transaction) await transaction.rollback();
    return res.status(500).send({errMsg: 'Failed to remove allotment from picklist.'});
  }
  return res.send({allot_id, picklist_removed});
}

const qUnpacked = `select va.id, va.status from van_allot va  
inner join picklist_details pd on pd.van_allot_id = va.id and picklist_id = :picklist_id
where status = 'sent for picking';`

Picklist.pack = async function(req, res){
  let {allot_id} = req.body;
  let {role, user_id} = req.token;
  let transaction;

  if(!allot_id) return res.status(422).send({errMsg: 'Please send allotment ID.'});
  let now = dayjs().toDate();
  let all_packed = false;

  try{
    transaction = await sq.transaction();

    let allot = await db.van_allot.findOne({ where: {id: {[Op.eq]: allot_id}}, transaction });
    if(!allot) return res.status(404).send({errMsg: `Allotment ${allot_id} not found.`});
    if(allot.status != 'sent for picking') return res.status(422).send({errMsg: `Allotment ${allot_id} status (${allot.status}) is not valid for packing.`});

    allot.status = 'packed';
    allot.packed_by = user_id;
    allot.packed_at = now;
    await allot.save({transaction});

    let pd = await db.picklist_details.findOne({
      where: {van_allot_id: {[Op.eq]: allot_id}}, 
      raw: true, transaction
    });

    let picklist = await db.picklists.findOne({ 
      where: {id: {[Op.eq]: pd.picklist_id}}, 
      transaction
    });

    // check unpacked
    let unpacked = await sq.query(qUnpacked, { 
      type: sq.QueryTypes.QUERY, 
      replacements: {picklist_id: picklist.id}, 
      raw: true, transaction 
    });

    if(!picklist.started_packing_at){
      picklist.status = 'started packing';
      picklist.started_packing_at = now;
    }
    
    console.log({unpacked});
    if(unpacked[0].length == 0){
      picklist.done_packing_at = now;
      picklist.status = 'done packing';
      all_packed = true;
    }

    await picklist.save({transaction});
    await transaction.commit();

  }catch(e){
    console.error(e);
    if(transaction) await transaction.rollback();
    return res.status(500).send({errMsg: 'Failed to pack allotment.'});
  }
  return res.send({allot_id, all_packed});
}

const qPackAll = `update van_allot set status = 'packed', packed_at = current_timestamp, packed_by = :user_id
where id in ( select va.id from van_allot va 
inner join picklist_details pd on va.id = pd.van_allot_id and picklist_id = :picklist_id
where status = 'sent for picking');`

Picklist.packAll = async function(req, res){
  let picklist_id = req.body.id;
  let {role, user_id} = req.token;
  let now = dayjs().toDate();
  let transaction;

  if(!picklist_id) return res.status(422).send({errMsg: 'Please send picklist ID.'});

  let picklist;
  try{
    transaction = await sq.transaction();

    picklist = await db.picklists.findOne({ 
      where: {id: {[Op.eq]: picklist_id}}, 
      transaction
    });

    if(!['started packing', 'sent for picking'].includes(picklist.status)){
      if(transaction) await transaction.rollback();
      return res.status(422).send({errMsg: 'Current picklist status not eligible for bulk packing.'});
    }

    if(!picklist.started_packing_at){
      picklist.started_packing_at = now;
    }

    picklist.done_packing_at = now;
    picklist.status = 'done packing';
    await picklist.save({transaction});

    let pa = await sq.query(qPackAll, { 
      type: sq.QueryTypes.QUERY, 
      replacements: {picklist_id: picklist_id, user_id}, 
      raw: true, transaction 
    });

    await transaction.commit();

  }catch(e){
    console.error(e);
    if(transaction) await transaction.rollback();
    return res.status(500).send({errMsg: 'Failed to pack all allotments.'});
  }
  return res.send({picklist});
}

module.exports = Picklist;