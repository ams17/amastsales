const env = process.env.NODE_ENV || 'dev';
const conf = require('../config/config.json')[env];
const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const {Parser} = require('json2csv');
const jwtAuth = require('../common/jwtAuth');
const Password = require('../common/password');
const db = require('../model/db.js');
const Op = db.Sequelize.Op;
const sq = db.sequelize;
const df = 'YYYY-MM-DD';
const limit = 20;

const Vans = {};

Vans.list = async function(req,res){
  let page = req.query.page;
  let active = req.query.active || 'All';
  let site_id = req.query.site_id;
  let keyword = req.query.keyword;
  let searchby = req.query.searchby || 'userid';
  let where = {};

  active = active.toLowerCase();

  page = (!page || isNaN(page) || parseInt(page) < 0)? 0 : parseInt(page) - 1;
  let offset = page * limit;

  if(searchby == 'userid' && keyword)  where.id = {[Op.iLike]: `${keyword}%`};
  if (site_id && site_id != 'All')        where.site_id = {[Op.iLike]: `${site_id}`};
  if (active && active != 'all')        where.active = {[Op.iLike]: `${active}`};

  let vans;
  try{
    vans = await db.vans.findAndCountAll({
      attributes: ['id', 'name', 'active', 'site_id'],
      order: [['id']],
      where, offset, limit, raw: true,
      logging: console.log
    });
    vans.limit = limit;
    vans.offset = offset;
  }catch(e){
    console.error(e);
    return res.status(500).send({status: 'failed', errMsg: 'Failed to get vans.'});
  }
  return res.send({status: 'success', vans});
}

Vans.create = async function(req,res){
  let id = req.body.id;
  let site_id = req.body.site_id;
  let name = req.body.name;
  let territory_type = req.body.territory_type;
  let shift = req.body.shift;
  let plan_upload = req.body.plan_upload;
  let status = req.body.status;
  let {user_id, role}   = req.token;

  if(!id) return res.status(422).send({errMsg:'Please enter van ID.'});
  if(!site_id) return res.status(422).send({errMsg:'Please enter site ID'});
  if(!name) return res.status(422).send({errMsg:'Please enter van name'});
  if(!territory_type) return res.status(422).send({errMsg:'Please enter territory_type'});
  if(!shift) return res.status(422).send({errMsg:'Please enter shift'});
  if(!plan_upload) return res.status(422).send({errMsg:'Please enter plan upload'});
  if(!status) return res.status(422).send({errMsg:'Please enter status'});

  let van;

  try {
    van = await db.vans.findOne({
      where:{
        id: {[Op.eq]: id}
      }
    });

    if(van) return res.status(422).send({errMsg:`Van id ${id} already used. Please enter a new ID.`});

    await db.vans.create({
      id : id,
      site_id: site_id,
      name : name,
      site_id: site_id,
      territory_type: territory_type,
      shift: shift,
      plan_upload: plan_upload,
      status: status,
      created_by : user_id
    });

  } catch(e) {
    // statements
    console.error(e);
    return res.status(500).send({status: 'failed', errMsg: 'Failed to create new van.'});
  }

  res.send({id: id, msg: `Create van #${id} successfully`})
}

Vans.read = async function (req,res) {
  let id = req.params.id;
  if(id == null) return res.status(422).send({status: 'failed', errMsg:'Van ID not provided.'});

  let van;
  try{
    van = await db.vans.findOne({
      where: {id: {[Op.eq]: id}},
      raw:true
    });

    if(!van) return res.status(404).send({status: 'failed', errMsg: `Van #${id} not found.`});
  }catch(e){
    console.error(e);
    return res.status(500).send({status: 'failed', errMsg: `Failed to get van #${id}`})
  }
  return res.send({status:'success', van});
}

Vans.update = async function(req,res){
  let id  = req.body.id;
  let {user_id,role} = req.token;

  if(!id) return res.status().send({errMsg:'Van ID not provided'});

  let van;

  try {
    van = await db.vans.findOne({
      where: {
        id : {[Op.eq]: id}
      }
    })
    
    if(!van) return res.status(404).send({status: 'failed', errMsg: `Van #${id} not found.`});

    if('name' in req.body){
      if(req.body.name != null){
        if(req.body.name.toString().trim().length != 0){
          van.name = req.body.name
        }
      }
    }

    if('territory_type' in req.body){
      if(req.body.territory_type != null){
        if(req.body.territory_type.toString().trim().length != 0){
          van.territory_type = req.body.territory_type
        }
      }
    }

    if('shift' in req.body){
      if(req.body.shift != null){
        if(req.body.shift.toString().trim().length != 0){
          van.shift = req.body.shift
        }
      }
    }

    if('plan_upload' in req.body){
      if(req.body.plan_upload != null){
        if(req.body.plan_upload.toString().trim().length != 0){
          van.plan_upload = req.body.plan_upload
        }
      }
    }

    if('status' in req.body){
      console.log(req.body.status);
      if(req.body.status != null){
        if(req.body.status.toString().trim().length != 0){
          van.active = req.body.status
        }
      }
    }

    van.save();

  } catch(e) {
    // statements
    console.error(e);
  }

  return res.send({msg:'Updated van successfully.'})
}

Vans.delete = async function(req, res){
  let id  = req.body.id;
  let {user_id,role} = req.token;

  if(!id) return res.status().send({errMsg:'Van ID not provided'});

  let van;

  try {
    van = await db.vans.findOne({
      where: {
        id : {[Op.eq]: id}
      }
    })
    
    if(!van) return res.status(404).send({status: 'failed', errMsg: `Van #${id} not found.`});

    van.active = false;
    van.save();

  } catch(e) {
    // statements
    console.error(e);
  }

  return res.send({msg:'Van deleted successfully.'})
}

const qVisitPlan = `select van_id, day, visit_list 
  from ( 
  with plans as (
    select van_id, day, outlet_id 
      from outlet_visit_plans vp
     where van_id = :van_id
    order by day
  )
  select van_id, 'MON' as day, array_agg(outlet_id) as visit_list from plans where day = 'MON' group by van_id
  union select van_id, 'TUE' as day, array_agg(outlet_id) as visit_list from plans where day = 'TUE' group by van_id
  union select van_id, 'WED' as day, array_agg(outlet_id) as visit_list from plans where day = 'WED' group by van_id
  union select van_id, 'THU' as day, array_agg(outlet_id) as visit_list from plans where day = 'THU' group by van_id
  union select van_id, 'FRI' as day, array_agg(outlet_id) as visit_list from plans where day = 'FRI' group by van_id
  union select van_id, 'SAT' as day, array_agg(outlet_id) as visit_list from plans where day = 'SAT' group by van_id
  union select van_id, 'SUN' as day, array_agg(outlet_id) as visit_list from plans where day = 'SUN' group by van_id
  ) a`;

Vans.visitPlanList = async function(req,res){
  let {van_id} = req.params;
  let {user_id, role} = req.token; 
  if(!van_id) return res.status(422).send({status: 'failed', errMsg:'Please enter Van ID.'});

  let plan;
  try {
    let van = await db.vans.findOne({
      where: {id: {[Op.eq]: van_id}},
      raw: true
    });
    if(!van) return res.status(404).send({errMsg: 'Van ID not found.'});

    plan = await sq.query(qVisitPlan,{
      type: sq.QueryTypes.SELECT,
      replacements: {van_id},
      raw: true
    });
  } catch(e) {
    console.error(e);
    return res.status(500).send({errMsg:'Failed to get Van Plan list.'});
  }
  res.send({van_id, plan, desc: 'planned visit arrayed from Mon to Sun.'});
}

Vans.visitPlanUpdate = async function(req,res){
  let van_id  = req.body.van_id;
  let plan  = req.body.plan;
  let {user_id,role} = req.token;

  if(!van_id) return res.status(422).send({errMsg:'Please enter Van ID.'});
  if(!plan) return res.status(422).send({errMsg:'Please submit Visit Plan.'});

  let vanVisitPlan = [];
  let transaction;

  try {
    van = await db.vans.findOne({
      where:{id: {[Op.eq]: van_id}},
      raw: true
    });
    if(!van) return res.status(404).send({errMsg:'Van ID not found.'});

    for(let p of plan){
      for (let visit of p.visit_list){
         vanVisitPlan.push({
            van_id: van_id,
            day: p.day, 
            outlet_id: visit,
            updated_by: user_id
          });
      }     
    }

    transaction = await sq.transaction();
    await db.visit_plan.destroy({
      where: {van_id: {[Op.eq]: van_id}},
      transaction
    });
    await db.visit_plan.bulkCreate(vanVisitPlan, {transaction, logging: console.log});
    await transaction.commit();
  } catch(e) {
    if(transaction) await transaction.rollback();
    console.error(e);
    return res.status(500).send({errMsg:'Failed to update visit plan.'});
  }

  return res.send({msg:'Updated visit plan successfully.'})
}

module.exports = Vans;