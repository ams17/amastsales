const env = process.env.NODE_ENV || 'dev';
const conf = require('../config/config.json')[env];
const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const dayjs = require('dayjs');
const {Parser} = require('json2csv');
const jwtAuth = require('../common/jwtAuth');
const Password = require('../common/password');
const db = require('../model/db.js');
const Op = db.Sequelize.Op;
const sq = db.sequelize;
const df = 'YYYY-MM-DD';
const limit = 50;

let OutletAccStmt = {};

const qOutletWithOutstandingInvoice = `select x.site_id, a.outlet_id as id, x.outlet_name, sum(new_outstanding) as total_outstanding, 
sum(DATE_PART('day', now() - a.due_date)) as total_aging
from settlement a, invoices i, 
(select o.site_id, o.id outlet_id, o.name outlet_name from outlets o) x 
where "current" = true
and a.invoice_id = i.id 
and i.outlet_id = x.outlet_id 
and new_outstanding > 0
and (:outlet_id is null or a.outlet_id LIKE ('%' || :outlet_id || '%'))
and (:site_id is null or x.site_id LIKE ('%' || :site_id || '%'))
group by x.site_id, a.outlet_id, x.outlet_name
LIMIT :limit 
OFFSET :offset`;

const searchByOpts = ['site_id', 'outlet_id'];
OutletAccStmt.list = async function(req, res){
  let page = req.query.page;
  let {keyword, site_id, date}  = req.query;
  let searchby = req.query.searchby;
  let where = {};

  page = (!page || isNaN(page) || parseInt(page) < 0)? 0 : parseInt(page) - 1;
  let offset = page * limit;

  if (!keyword){
    keyword = null;
  }

  if (searchby == 'site_id' && keyword){
    site_id = keyword;
    outlet_id = null;
    keyword = null;
  } else if (searchby == 'outlet_id' && keyword){
    outlet_id = keyword;
    site_id = null;
    keyword = null;
  } else{
    outlet_id = null;
    site_id = null;
    keyword = null;
  }
  
  let outletAccStmt = {};
  try{

    let data = await sq.query(qOutletWithOutstandingInvoice,{
      type : sq.QueryTypes.SELECT,
      replacements: {limit, offset, keyword, site_id, outlet_id},
      raw : true
    });

    outletAccStmt.count = data.length;
    outletAccStmt.limit = limit;
    outletAccStmt.offset = offset;
    outletAccStmt.rows = data;

  }catch(e){
    console.error(e);
    return res.status(500).send({status: 'failed', errMsg: 'Failed to get Outlet Acc Stmts.'});
  }
  return res.send({status: 'success', outletAccStmt});
}

const qOutletWithOutstandingInvoiceDetails = `select x.site_id, a.outlet_id, x.outlet_name, invoice_id, i."date" invoice_date, a.due_date, sum(new_outstanding) as outstanding_amount, 
sum(DATE_PART('day', now() - a.due_date)) as aging
from settlement a, invoices i, 
(select o.site_id, o.id outlet_id, o.name outlet_name from outlets o where o.id = :outlet_id) x 
where "current" = true
and a.invoice_id = i.id 
and i.outlet_id = x.outlet_id 
and new_outstanding > 0
group by x.site_id, a.outlet_id , x.outlet_name , invoice_id , i."date", a.due_date`;

OutletAccStmt.read = async function(req,res){
  let outlet_id = req.params.outlet_id;
  let invoice_id = req.params.invoice_id;
  if(outlet_id == null) return res.status(422).send({status: 'failed', errMsg:'Outlet ID not provided.'});

  let header = {};
  try{

    let data = await sq.query(qOutletWithOutstandingInvoiceDetails,{
      type : sq.QueryTypes.SELECT,
      replacements: {outlet_id},
      raw : true
    });
    
    if(!data){
      return res.status(404).send({status: 'failed', errMsg: `Statement of Account for #${outlet_id} not found.`});
    }

    header = data[0];
    details = data;

  }catch(e){
    console.error(e);
    return res.status(500).send({status: 'failed', errMsg: `Failed to get Statement of Account for outlet #${outlet_id}`})
  }

  return res.send({status:'success', header, details});
}


module.exports = OutletAccStmt;