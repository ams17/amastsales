const env = process.env.NODE_ENV || 'dev';
const conf = require('../config/config.json')[env];
const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const moment = require('moment');
const dayjs = require('dayjs');
const {Parser} = require('json2csv');
const jwtAuth = require('../common/jwtAuth');
const Password = require('../common/password');
const db = require('../model/db.js');
const Op = db.Sequelize.Op;
const sq = db.sequelize;
const df = 'YYYY-MM-DD';
const limit = 20;

let VanVisits = {};

VanVisits.list = async function(req,res){
  let {van_id, token} = req.token;
  let dow = dayjs().format('ddd');
  let visit_plan;

  try{
    visit_plan = await db.outlet_visit_plan.findAndCountAll({
      where:{
          day: dow.toUpperCase(),
          van_id : {[Op.eq]: van_id}
      }
    });

  }catch(e){
    console.error(e);
    return res.status.send({error: 'Failed to get visit list'});
  }

  return res.send({status:'success',visit_plan});
}

VanVisits.checkIn = async function(req,res){
  let outlet_id = req.body.outlet_id;
  let today = dayjs().format(df);
  let current = dayjs().format('YYYY-MM-DD hh:mm:ss');
  let {user_id,van_id,role} = req.token;
  let van_visit;
  let outlet;

  if(!outlet_id) return res.status(422).send({error:"Please enter a outlet ID."})
  try {
    outlet = await db.outlets.findOne({
      where:{
        id: {[Op.eq]: outlet_id}
      }
    })

    if(!outlet) return res.status(404).send({error:'Outlet ID not found.'});

    van_visit= await db.van_visits.findOne({
      where:{
        date: today,
        outlet_id: {[Op.eq]:outlet_id},
        check_out_at: {[Op.is]: null}
      }
    });

    if(van_visit) return res.status(422).send({error:'This outlet has been Check-In.'});

    await db.van_visits.update({
      check_out_at : current
    },{
      where:{
        created_by : {[Op.eq] : user_id},
        check_out_at : {[Op.is]: null} 
      }
    });

    await db.van_visits.create({
      date: today,
      van_id: van_id,
      outlet_id: outlet_id,
      check_in_at: current,
      created_by: user_id
    });

    return res.send({status:'success'});
  } catch(e) {
    // statements
    console.error(e);
    return res.status(500).send({error:'Check-In outlet failed.'})
  }

}

VanVisits.checkOut = async function(req,res){
  let outlet_id = req.body.outlet_id;
  let today = dayjs().format(df);
  let current = dayjs().format('YYYY-MM-DD hh:mm:ss');
  let {user_id,role} = req.token;
  let van_visit;
  let outlet;

  if(!outlet_id) return res.status(422).send({error:"Please enter a outlet ID."})

  try {
    outlet = await db.outlets.findOne({
      where:{
        id: {[Op.eq]: outlet_id}
      }
    })

    if(!outlet) return res.status(404).send({error:'Outlet ID not found.'});

    van_visit= await db.van_visits.findOne({
      where:{
        date: today,
        outlet_id: {[Op.eq]:outlet_id},
        check_out_at: {[Op.is]: null}
      }
    });

    if(!van_visit) return res.status(422).send({error:"This outlet hasn't Check-In."});

    van_visit.check_out_at = current;
    van_visit.save();

    return res.send({status:'success'})
  } catch(e) {
    // statements
    console.error(e);
    res.status(500).send({error:'Check-Out outlet failed.'})
  }
}

module.exports = VanVisits;