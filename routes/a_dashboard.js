const env = process.env.NODE_ENV || 'dev';
const conf = require('../config/config.json')[env];
const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const moment = require('moment');
const {Parser} = require('json2csv');
const jwtAuth = require('../common/jwtAuth');
const Password = require('../common/password');
const db = require('../model/db.js');
const biz = require('../business/business.js')
const Op = db.Sequelize.Op;
const sq = db.sequelize;
const df = 'YYYY-MM-DD';
const limit = 20;

router.get('/summary/list', jwtAuth.checkTokenForApi, async (req, res)=>{
  biz.dashboard.summaryList(req,res);
});

router.get('/yearlySalesAndReturns/list', jwtAuth.checkTokenForApi, async (req, res)=>{
  biz.dashboard.yearlySalesAndReturnsList(req,res);
});

router.get('/recent_transaction/list', jwtAuth.checkTokenForApi, async (req, res)=>{
  biz.dashboard.recentTransactionList(req,res);
});

router.get('/best_selling_product/list', jwtAuth.checkTokenForApi, async (req, res)=>{
  biz.dashboard.bestSellingProductList(req,res);
});

module.exports = router;
