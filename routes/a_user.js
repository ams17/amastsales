const env = process.env.NODE_ENV || 'dev';
const conf = require('../config/config.json')[env];
const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const moment = require('moment');
const {Parser} = require('json2csv');
const jwtAuth = require('../common/jwtAuth');
const Password = require('../common/password');
const db = require('../model/db.js');
const biz = require('../business/business.js')
const Op = db.Sequelize.Op;
const sq = db.sequelize;
const df = 'YYYY-MM-DD';
const limit = 20;

function send(res, code, msg){
  let data;
  if(code >= 300){
    if (msg == null) {
      switch(code){
        case 422 : msg = "Unprocessable"; break;
        case 403 : msg = "Forbidden"; break;
        case 404 : msg = "Not Found"; break;
        case 500 : msg = "Internal Server Error"; break;
      }
    }
    data = { success: false, errorMsg: msg };
    res.status(code);
  } else if(code >= 200){
    data = {success: true};
    res.status(code);
  }else if (code){
    res.status(code);
  }
  res.send(data);
}

router.get('/profile', jwtAuth.checkTokenForApi, async (req, res, next)=>{
  let {user_id} = req.token;
  let user;

  try{
    user = await db.users.findOne({where: {id: {[Op.eq]: user_id}}});
    if(!user) return send(res, 400);
  }catch(e){
    console.error(e);
    return send(res, 500);
  }

  return res.send({
    user_id: user.id, 
    role: user.role, 
    name: user.name
  });
});

router.get('/list', jwtAuth.checkTokenForApi, async (req, res)=>{
  biz.users.list(req,res);
});

router.post('/create', jwtAuth.checkTokenForApi, async function(req,res){
  biz.users.create(req,res);
});

router.get('/o/:id', jwtAuth.checkTokenForApi, async function(req,res){
  biz.users.read(req,res);
});

router.get('/self', jwtAuth.checkTokenForApi, async function(req,res){
  req.params.id = req.token.user_id;
  biz.users.read(req,res);
});

router.post('/update', jwtAuth.checkTokenForApi, async function(req,res){
  biz.users.update(req,res);
});

router.get('/init/superadmin/:key', async (req, res, next)=>{
  let key = req.params.key;
  if(key != conf.key){
    console.error(key);
    return res.status(403).end('Forbidden. Incorrect key.');
  }

  let user;

  try{
    user = await db.users.findOne({where: {id: 'superadmin'}});
    if(user) return res.end('Superuser already initialized.');

    let hash = bcrypt.hashSync(conf.defaultPassword, conf.saltRounds);

    user = await db.users.create({
      id: 'superadmin',
      password: hash,
      role: 'superadmin',
      name: 'superadmin',
      status: 'active',
      created_by: 'system'
    });

  } catch(e){
    console.error(e);
    return res.status(500).end('Failed to initialize superadmin.');
  }

  return res.status(200).end('Superadmin initialized successfully.');
});

router.all('/*', (req, res, next) => {
  send(res, 404, 'API Not Found');
})

module.exports = router;
