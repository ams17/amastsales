const env = process.env.NODE_ENV || 'dev';
const conf = require('../config/config.json')[env];
const express = require('express');
const router = express.Router();
const moment = require('moment');
const {Parser} = require('json2csv');
const jwtAuth = require('../common/jwtAuth');
const Password = require('../common/password');
const helper = require('../helper/helper.js');
const db = require('../model/db.js');
const business = require('../business/business.js')
const Op = db.Sequelize.Op;
const sq = db.sequelize;
const df = 'YYYY-MM-DD';
const limit = 20;

router.get('/invoice', jwtAuth.checkTokenForApi, async (req, res)=>{
  business.report.invoice(req, res);
});

router.get('/credit_note', jwtAuth.checkTokenForApi, async (req, res)=>{
  business.report.credit_note(req, res);
});

router.get('/receipt', jwtAuth.checkTokenForApi, async (req, res)=>{
  business.report.receipt(req, res);
});

router.get('/void_invoice', jwtAuth.checkTokenForApi, async (req, res)=>{
  business.report.void_invoice(req, res);
});

router.get('/debit_note', jwtAuth.checkTokenForApi, async (req, res)=>{
  business.report.debit_note(req, res);
});

router.get('/refund', jwtAuth.checkTokenForApi, async (req, res)=>{
  business.report.refund(req, res);
});

router.get('/transfer_in', jwtAuth.checkTokenForApi, async (req, res)=>{
  business.report.transfer_in(req, res);
});

router.get('/transfer_out', jwtAuth.checkTokenForApi, async (req, res)=>{
  business.report.transfer_out(req, res);
});

router.get('/write_off', jwtAuth.checkTokenForApi, async (req, res)=>{
  business.report.write_off(req, res);
});

router.get('/outlet_acc_stmt', jwtAuth.checkTokenForApi, async (req, res)=>{
  business.report.outlet_acc_stmt(req, res);
});

module.exports = router;