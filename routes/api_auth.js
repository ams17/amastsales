const env = process.env.NODE_ENV || 'dev';
const conf = require('../config/config.json')[env];
var express = require('express');
var router = express.Router();
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { v4: uuidv4 } = require('uuid');
const db = require('../model/db.js');
const Op = db.Sequelize.Op;
const sq = db.sequelize;

const msgMissingParam = 'Please enter username, password and device id';
const msgNoVanAssigned = 'No van id assigned';
const msgLoginFailed = 'Invalid username or password';
const msgSignFailed = 'Failed to sign token';

router.post('/login', async (req, res) => {
  let token;
  let {username, password, device_id} = req.body;
  if(!username || ! password || !device_id) {
    console.error(msgMissingParam);
    return res.status(422).send({errMsg: msgMissingParam});
  }

  let user = await db.users.findOne({
    where: { id: {[Op.eq]: username.toLowerCase()}}
  });

  if(!user){
    return res.status(422).send({errMsg: msgLoginFailed});
  }

  if(user.retries > 5 || !user.active){
    user.retries++; 
    await user.save();
    return res.status(422).send({errMsg: msgLoginFailed});
  }

  if(user.device_id != device_id && !user.allow_new_device){
    user.retries++; 
    await user.save();
    return res.status(422).send({errMsg: msgLoginFailed});
  }

  if(user.device_id == device_id && user.allow_new_device == true){
    return res.status(422).send({errMsg: msgLoginFailed});
  }

  if(user.allow_new_device == true){
    user.device_id = device_id;
    user.allow_new_device = false;
  }

  let valid = await bcrypt.compare(password, user.password);
  if(!valid) {
    user.retries++;
    await user.save();
    console.error('Wrong password.');
    return res.status(422).send({errMsg: msgLoginFailed});
  }

  let van = await db.vans.findOne({
    where: { user_id: {[Op.eq]: user.id}}
  });

  if(!van){
    return res.status(422).send({errMsg: msgNoVanAssigned});
  }

  user.retries = 0;
  user.last_login_at = new Date();
  user.session_id = uuidv4();
  await user.save();
  
  let _tkn = {
    user_id: user.id, 
    van_id: van.id,
    site_id: van.site_id,
    role: user.role,
    session_id: user.session_id
  };

  console.log(_tkn);

  try{
    token = jwt.sign(_tkn, conf.cookie.secret,{expiresIn: '24h'});
  } catch(err){
    console.error(msgSignFailed);
    return res.status(422).send({errMsg: msgSignFailed});
  }
  
  return res.json({token});
});

router.all('/*', (req, res, next) => {
  res.status(404).send('API not found');  
})

module.exports = router;
