const env = process.env.NODE_ENV || 'dev';
const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const { v4: uuidv4 } = require('uuid');
const db = require('../model/db.js');
const jwt = require('jsonwebtoken');
const jwtAuth = require('../common/jwtAuth');
const conf = require('../config/config.json')[env];
const Op = db.Sequelize.Op;
const sq = db.sequelize;

const msgMissingParam = 'Please enter username and password';
const msgLoginFailed = 'Invalid username or password';
const msgSignFailed = 'Failed to sign token';

router.get('/', jwtAuth.checkTokenForLogin, (req, res) => {
  res.render('login');
});

router.post('/', async (req, res)=> {
  let {username, password} = req.body;
  if(!username || ! password) {
    console.error(msgMissingParam);
    return res.render('login', {error: msgMissingParam});
  }

  let user = await db.users.findOne({
    where: { id: {[Op.eq]: username.toLowerCase()}}
  });

  if(!user){
    return res.render('login', {error: msgLoginFailed});
  }

  if(user.retries > 5 || !user.active){
    user.retries++; 
    await user.save();
    return res.render('login', {error: msgLoginFailed});
  }

  let valid = await bcrypt.compare(password, user.password);
  if(!valid) {
    user.retries++;
    await user.save();
    console.error('Wrong password.');
    return res.render('login', {error: msgLoginFailed});
  }

  user.retries = 0;
  user.last_login_at = new Date();
  user.session_id = uuidv4();
  await user.save();
  
  let tkn = {
    user_id: user.id, 
    role: user.role,
    session_id: user.session_id
  };

  console.log(tkn);

  try{
    var token = jwt.sign(tkn, conf.cookie.secret,{expiresIn: '12h'});
    // cookie secure attribute prevents cookie from being used in http.
    res.cookie(conf.cookie.tokenName, token, {HttpOnly: true, secure: (env==='local' || env==='dev')? false : true});
  } catch(err){
    console.error(msgSignFailed);
    //console.error(err);
    return res.render('login', {subdomain, error: msgSignFailed});
  }
  
  return res.redirect('/home');
});

router.all('*', (req, res)=>{
  res.redirect('/login');
})


module.exports = router;
