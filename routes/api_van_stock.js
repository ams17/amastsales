const env = process.env.NODE_ENV || 'dev';
const conf = require('../config/config.json')[env];
const express = require('express');
const router = express.Router();
const dayjs = require('dayjs'); 
const bcrypt = require('bcrypt');
const {Parser} = require('json2csv');
const jwtAuth = require('../common/jwtAuth');
const apiAuth = require('../common/apiAuth');
const db = require('../model/db.js');
const biz = require('../business/business.js')
const Op = db.Sequelize.Op;
const sq = db.sequelize;

router.post('/add',apiAuth.checkToken,async function(req,res){
  biz.van_stock.in(req,res);
});

router.get('/detail',apiAuth.checkToken,async function(req,res){
  biz.van_stock.details(req,res);
});

router.post('/reduce/allot',apiAuth.checkToken,async function(req,res){
  biz.van_stock.out_delivery(req,res);
});

router.post('/reduce/adhoc',apiAuth.checkToken,async function(req,res){
  biz.van_stock.out_adhoc(req,res);
});


module.exports = router;