const env = process.env.NODE_ENV || 'dev';
const conf = require('../config/config.json')[env];
const express = require('express');
const router = express.Router();
const dayjs = require('dayjs'); 
const bcrypt = require('bcrypt');
const {Parser} = require('json2csv');
const jwtAuth = require('../common/jwtAuth');
const apiAuth = require('../common/apiAuth');
const db = require('../model/db.js');
const biz = require('../business/business.js')
const Op = db.Sequelize.Op;
const sq = db.sequelize;

router.get('/outlet/list',apiAuth.checkToken,async function(req,res){
  biz.stockCount.list(req,res);
});

router.post('/outlet/save',apiAuth.checkToken,async function(req,res){
  biz.stockCount.save(req,res);
});

router.post('/add',apiAuth.checkToken,async function(req,res){
  biz.van_stock.in(req,res);
});

module.exports= router;