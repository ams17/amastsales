const env = process.env.NODE_ENV || 'dev';
var express = require('express');
var router = express.Router();
const db = require('../model/db.js');
const jwtAuth = require('../common/jwtAuth');
const Op = db.Sequelize.Op;
const sq = db.sequelize;
const conf = require('../config/config.json')[env];
const moment = require('moment');
const bcrypt = require('bcrypt');
const passwordRegex = /(?=(.*[0-9]))(?=.*[\!@#$%^&*()\\[\]{}\-_+=~`|:;"'<>,./?])(?=.*[a-z])(?=(.*[A-Z]))(?=(.*)).{8,}/;

router.get('/details', jwtAuth.checkTokenForApi, async (req, res)=>{
  let user_id = req.token.user_id;
  let details;
  try{
    details = await db.users.findOne({
      attributes: ['id', 'role', 'name', 'status', 'retries', 'last_login_at', 'created_by', 'created_at'],
      where: {id: {[Op.eq]: user_id}},
      raw: true
    });

    if(!details) return res.status(500).send({errMsg: 'Failed to get account details.'})
  }catch(e){
    console.error(e);
    return res.status(500).send({errMsg: 'Failed to read account details.'})
  }

  return res.send({details});
});

router.post('/name', jwtAuth.checkTokenForApi, async (req, res)=>{
  let user_id = req.token.user_id;
  let name =  req.body.name;

  if(!name) return res.status(422).send({errMsg: 'Please enter name'});

  try{
    let user = await db.users.findOne({
      where: {id: {[Op.eq]: user_id}}
    });

    if(!name) {
      console.error('User not found. Failed to save name.');
      return res.status(500).send({errMsg: 'Failed to save name.'});
    }

    user.name = name;
    await user.save();

  }catch(e){
    console.error(e);
    return res.status(500).send({errMsg: 'Failed to save name.'})
  }
  return res.send({status:'success'});
});

router.post('/password', jwtAuth.checkTokenForApi, async function(req, res){
  let {currPassword, newPassword} = req.body;
  let userID = req.token.user_id.toLowerCase();
  
  if(!passwordRegex.test(newPassword)){
    let errMsg = 'Password does not meet complexity requirement';
    console.error(errMsg);
    return res.status(422).send({status: 'failed', errMsg});
  }

  try{
    let user = await db.users.findOne({ where: { id: {[Op.eq]: userID}} });

    if(!user){
      console.error('User not found. Failed to save new password.');
      return res.status(404).send({status: 'failed', errMsg: 'Failed to save new password.'});
    }

    if(!bcrypt.compareSync(currPassword, user.password)){
      return res.status(422).send({status: 'failed', errMsg: 'Current password incorrect.'});
    }

    let newHash = bcrypt.hashSync(newPassword, conf.saltRounds);
    user.password = newHash;
    await user.save();
    
  }catch(e){
    console.error(e);
    return res.status(500).send({status: 'failed', errMsg: 'Failed to save new password.'})
  };

  return res.send({status: 'success'});
});

router.all('/*', (req, res, next) => {
  res.status(404).send('API not found');
});

module.exports = router;
