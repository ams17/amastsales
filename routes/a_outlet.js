const env = process.env.NODE_ENV || 'dev';
const conf = require('../config/config.json')[env];
const express = require('express');
const router = express.Router();
const moment = require('moment');
const {Parser} = require('json2csv');
const jwtAuth = require('../common/jwtAuth');
const Password = require('../common/password');
const helper = require('../helper/helper.js');
const db = require('../model/db.js');
const business = require('../business/business.js')
const Op = db.Sequelize.Op;
const sq = db.sequelize;
const df = 'YYYY-MM-DD';
const limit = 20;

router.get('/list', jwtAuth.checkTokenForApi, async (req, res)=>{
  business.outlets.list(req, res);
});

router.get('/o/:id', jwtAuth.checkTokenForApi, async (req, res)=>{
  business.outlets.details(req, res);
});

module.exports = router;