const env = process.env.NODE_ENV || 'dev';
const conf = require('../config/config.json')[env];
const express = require('express');
const router = express.Router();
const moment = require('moment');
const {Parser} = require('json2csv');
const jwtAuth = require('../common/jwtAuth');
const Password = require('../common/password');
const helper = require('../helper/helper.js');
const db = require('../model/db.js');
const biz = require('../business/business.js')
const Op = db.Sequelize.Op;
const sq = db.sequelize;
const df = 'YYYY-MM-DD';
const limit = 20;

router.post('/transfer_in', jwtAuth.checkTokenForApi,async (req,res)=>{
  biz.trxIn.create(req,res);
});

router.post('/transfer_out', jwtAuth.checkTokenForApi,async (req,res)=>{
  biz.trxOut.create(req,res);
});

router.post('/write_off',jwtAuth.checkTokenForApi,async(req,res)=>{
  biz.writeOff.create(req,res);
});

router.get('/transfer_in/list', jwtAuth.checkTokenForApi,async (req,res)=>{
  biz.trxIn.list(req,res);
});

router.get('/transfer_in/o/:tiid', jwtAuth.checkTokenForApi,async (req,res)=>{
  biz.trxIn.details(req,res);
});

router.get('/transfer_out/list', jwtAuth.checkTokenForApi,async (req,res)=>{
  biz.trxOut.list(req,res);
});

router.get('/transfer_out/o/:toid', jwtAuth.checkTokenForApi,async (req,res)=>{
  biz.trxOut.details(req,res);
});

router.get('/write_off/list',jwtAuth.checkTokenForApi,async(req,res)=>{
  biz.writeOff.list(req,res);
});

router.get('/write_off/o/:woid', jwtAuth.checkTokenForApi,async (req,res)=>{
  biz.writeOff.details(req,res);
});

router.get('/list/:site_id', jwtAuth.checkTokenForApi,async (req,res)=>{
  biz.inventories.list(req,res);
});

router.get('/ledger/list', jwtAuth.checkTokenForApi,async (req,res)=>{
  biz.inventories.ledgerList(req,res);
});

router.get('/ledger/o/:toid', jwtAuth.checkTokenForApi,async (req,res)=>{
  biz.inventories.ledgerDetail(req,res);
});

router.get('/van_return/list', jwtAuth.checkTokenForApi,async (req,res)=>{
  biz.van_return.list(req,res);
});

router.get('/van_return/o/:toid', jwtAuth.checkTokenForApi,async (req,res)=>{
  biz.van_return.details(req,res);
});

module.exports = router;