const env = process.env.NODE_ENV || 'dev';
const conf = require('../config/config.json')[env];
const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const {Parser} = require('json2csv');
const jwtAuth = require('../common/jwtAuth');
const send = require('../common/sender')['send'];
const Password = require('../common/password');
const db = require('../model/db.js');
const biz = require('../business/business.js')
const Op = db.Sequelize.Op;
const sq = db.sequelize;
const df = 'YYYY-MM-DD';
const limit = 20;

router.get('/list', jwtAuth.checkTokenForApi, async (req, res)=>{
  biz.sites.list(req,res);
});

router.get('/addSite', jwtAuth.checkTokenForApi, async (req, res)=>{
  biz.sites.addSite(req,res)
});
router.post('/create', jwtAuth.checkTokenForApi, async function(req,res) {
  biz.sites.create(req,res);
})

router.get('/o/:id', jwtAuth.checkTokenForApi, async (req, res)=>{
  biz.sites.read(req,res);
});

router.post('/update', jwtAuth.checkTokenForApi, async function(req,res){
  biz.sites.update(req,res);
});

router.post('/delete', jwtAuth.checkTokenForApi, async function(req,res){
  biz.sites.delete(req,res);
});

router.get('/list/min', jwtAuth.checkTokenForApi, async function(req,res){
  biz.sites.listmin(req,res);
});
router.get('/getDetails', jwtAuth.checkTokenForApi, async (req, res)=>{
  biz.sites.getDetails(req,res);
});
router.get('/getCities/:id', jwtAuth.checkTokenForApi, async (req, res)=>{
  biz.sites.getCities(req,res);
});

router.all('/*', (req, res, next) => {
  send(res, 404, 'API Not Found');
})

module.exports = router;
