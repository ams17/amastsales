const env = process.env.NODE_ENV || 'dev';
const conf = require('../config/config.json')[env];
const express = require('express');
const router = express.Router();
const moment = require('moment');
const {Parser} = require('json2csv');
const jwtAuth = require('../common/jwtAuth');
const Password = require('../common/password');
const helper = require('../helper/helper.js');
const db = require('../model/db.js');
const biz = require('../business/business.js')
const Op = db.Sequelize.Op;
const sq = db.sequelize;
const df = 'YYYY-MM-DD';
const limit = 20;

router.get('/list', jwtAuth.checkTokenForApi, async (req, res)=>{
  biz.skus.list(req,res)
});

router.get('/o/:id',jwtAuth.checkTokenForApi,async function(req,res) {
  biz.skus.read(req,res)
})

router.get('/tablelist',jwtAuth.checkTokenForApi,async function(req,res) {
  biz.skus.skuTableList(req,res)
})

router.post('/create', jwtAuth.checkTokenForApi, async function(req,res) {
  biz.skus.create(req,res);
})

router.post('/update', jwtAuth.checkTokenForApi, async function(req,res){
  biz.skus.update(req,res);
});

router.post('/delete', jwtAuth.checkTokenForApi, async function(req,res){
  biz.skus.delete(req,res);
});

router.get('/getDetails', jwtAuth.checkTokenForApi, async (req, res)=>{
  biz.skus.getDetails(req,res);
});
module.exports = router;