const env = process.env.NODE_ENV || 'dev';
const conf = require('../config/config.json')[env];
const express = require('express');
const router = express.Router();
const moment = require('moment');
const {Parser} = require('json2csv');
const jwtAuth = require('../common/jwtAuth');
const Password = require('../common/password');
const helper = require('../helper/helper.js');
const db = require('../model/db.js');
const biz = require('../business/business.js')
const Op = db.Sequelize.Op;
const sq = db.sequelize;
const df = 'YYYY-MM-DD';
const limit = 20;


// list
router.get('/invoice/list', jwtAuth.checkTokenForApi,async (req,res)=>{
  biz.invoice.list(req,res);
});

router.get('/receipt/list', jwtAuth.checkTokenForApi,async (req,res)=>{
  biz.receipt.list(req,res);
});

router.get('/credit_note/list', jwtAuth.checkTokenForApi,async (req,res)=>{
  biz.credit_note.list(req,res);
});

router.get('/void/list', jwtAuth.checkTokenForApi,async (req,res)=>{
  biz.void.list(req,res);
});

router.get('/refund/list', jwtAuth.checkTokenForApi,async (req,res)=>{
  biz.refund.list(req,res);
});

router.get('/debit_note/list', jwtAuth.checkTokenForApi,async (req,res)=>{
  biz.debit_note.list(req,res);
});

router.get('/outlet_acc_stmt/list', jwtAuth.checkTokenForApi,async (req,res)=>{
  biz.outlet_acc_stmt.list(req,res);
});

// read
router.get('/invoice/o/:id', jwtAuth.checkTokenForApi,async (req,res)=>{
  biz.invoice.read(req,res);
});

router.get('/receipt/o/:id', jwtAuth.checkTokenForApi,async (req,res)=>{
  biz.receipt.read(req,res);
});

router.get('/credit_note/o/:id', jwtAuth.checkTokenForApi,async (req,res)=>{
  biz.credit_note.read(req,res);
});

router.get('/void/o/:id', jwtAuth.checkTokenForApi,async (req,res)=>{
  biz.void.read(req,res);
});

router.get('/refund/o/:id', jwtAuth.checkTokenForApi,async (req,res)=>{
  biz.refund.read(req,res);
});

router.get('/debit_note/o/:id', jwtAuth.checkTokenForApi,async (req,res)=>{
  biz.debit_note.read(req,res);
});

router.get('/outlet_acc_stmt/o/:outlet_id', jwtAuth.checkTokenForApi,async (req,res)=>{
  biz.outlet_acc_stmt.read(req,res);
});

module.exports = router;