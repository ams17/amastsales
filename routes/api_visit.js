const env = process.env.NODE_ENV || 'dev';
const conf = require('../config/config.json')[env];
const express = require('express');
const router = express.Router();
const dayjs = require('dayjs'); 
const bcrypt = require('bcrypt');
const {Parser} = require('json2csv');
const jwtAuth = require('../common/jwtAuth');
const apiAuth = require('../common/apiAuth');
const db = require('../model/db.js');
const biz = require('../business/business.js')
const Op = db.Sequelize.Op;
const sq = db.sequelize;
const df = 'YYYY-MM-DD';

router.post('/checkIn',apiAuth.checkToken,async function(req,res){
  biz.van_visits.checkIn(req,res);
});

router.post('/checkOut',apiAuth.checkToken,async function(req,res){
  biz.van_visits.checkOut(req,res);
});

router.get('/list',apiAuth.checkToken,async function(req,res){
  biz.van_visits.list(req,res);
});

module.exports = router