const env = process.env.NODE_ENV || 'dev';
const conf = require('../config/config.json')[env];
var express = require('express');
var router = express.Router();
const jwtAuth = require('../common/jwtAuth');
const db = require('../model/db.js');
const Op = db.Sequelize.Op;
const sq = db.sequelize;

router.get('/', (req, res, next) => {
  res.redirect('/home');
});

router.get('/home', jwtAuth.checkToken, (req, res, next)=>{
  let {user_id, role} = req.token;
  res.render('home', {user_id, role});
});

router.get('/account_settings', jwtAuth.checkToken, (req, res, next)=>{
  let {user_id, role} = req.token;
  res.render('account_settings', {user_id, role});
});

// master

router.get('/users', jwtAuth.checkToken, (req, res, next)=>{
  let {user_id, role} = req.token;
  res.render('users', {user_id, role});
});

router.get('/outlets', jwtAuth.checkToken, (req, res, next)=>{
  let {user_id, role} = req.token;
  res.render('outlets', {user_id, role});
});

router.get('/vans', jwtAuth.checkToken, (req, res, next)=>{
  let {user_id, role} = req.token;
  res.render('vans', {user_id, role});
});

router.get('/uoms', jwtAuth.checkToken, (req, res, next)=>{
  let {user_id, role} = req.token;
  res.render('uoms', {user_id, role});
});

router.get('/skus', jwtAuth.checkToken, (req, res, next)=>{
  let {user_id, role} = req.token;
  res.render('skus', {user_id, role});
});

router.get('/sites', jwtAuth.checkToken, (req, res, next)=>{
  let {user_id, role} = req.token;
  res.render('sites', {user_id, role});
});

router.get('/price', jwtAuth.checkToken, (req, res, next)=>{
  let {user_id, role} = req.token;
  res.render('price', {user_id, role});
});

router.get('/device_management', jwtAuth.checkToken, (req, res, next)=>{
  let {user_id, role} = req.token;
  res.render('device_management', {user_id, role});
});

//system settings
router.get('/lov', jwtAuth.checkToken, (req, res, next)=>{
  let {user_id, role} = req.token;
  res.render('lov', {user_id, role});
});

// operation
router.get('/van_allotment_plan', jwtAuth.checkToken, (req, res, next)=>{
  let {user_id, role} = req.token;
  res.render('van_allotment_plan', {user_id, role});
});

router.get('/van_allotment', jwtAuth.checkToken, (req, res, next)=>{
  let {user_id, role} = req.token;
  res.render('van_allotment', {user_id, role});
});

router.get('/picklists', jwtAuth.checkToken, (req, res, next)=>{
  let {user_id, role} = req.token;
  res.render('picklists', {user_id, role});
});

router.get('/recon', jwtAuth.checkToken, (req, res, next)=>{
  let {user_id, role} = req.token;
  res.render('recon', {user_id, role});
});

// inventory

router.get('/inventory', jwtAuth.checkToken, (req, res, next)=>{
  let {user_id, role} = req.token;
  res.render('inventory', {user_id, role});
});

router.get('/transfer_in', jwtAuth.checkToken, (req, res, next)=>{
  let {user_id, role} = req.token;
  res.render('transfer_in', {user_id, role});
});

router.get('/transfer_out', jwtAuth.checkToken, (req, res, next)=>{
  let {user_id, role} = req.token;
  res.render('transfer_out', {user_id, role});
});

router.get('/write_off', jwtAuth.checkToken, (req, res, next)=>{
  let {user_id, role} = req.token;
  res.render('write_off', {user_id, role});
});

// finance

router.get('/invoice', jwtAuth.checkToken, (req, res, next)=>{
  let {user_id, role} = req.token;
  res.render('invoice', {user_id, role});
});

router.get('/receipt', jwtAuth.checkToken, (req, res, next)=>{
  let {user_id, role} = req.token;
  res.render('receipt', {user_id, role});
});

router.get('/credit_note', jwtAuth.checkToken, (req, res, next)=>{
  let {user_id, role} = req.token;
  res.render('credit_note', {user_id, role});
});

router.get('/void', jwtAuth.checkToken, (req, res, next)=>{
  let {user_id, role} = req.token;
  res.render('void', {user_id, role});
});

router.get('/refund', jwtAuth.checkToken, (req, res, next)=>{
  let {user_id, role} = req.token;
  res.render('refund', {user_id, role});
});

router.get('/debit_note', jwtAuth.checkToken, (req, res, next)=>{
  let {user_id, role} = req.token;
  res.render('debit_note', {user_id, role});
});

router.get('/outlet_acc_stmt', jwtAuth.checkToken, (req, res, next)=>{
  let {user_id, role} = req.token;
  res.render('outlet_acc_stmt', {user_id, role});
});

// logout

router.get('/logout', jwtAuth.checkToken, (req, res, next)=>{
  console.log('log out now');
  res.clearCookie(conf.cookie.tokenName);
  res.redirect('/');
});

module.exports = router;
