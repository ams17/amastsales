const env = process.env.NODE_ENV || 'dev';
const conf = require('../config/config.json')[env];
const express = require('express');
const router = express.Router();
const moment = require('moment');
const {Parser} = require('json2csv');
const jwtAuth = require('../common/jwtAuth');
const Password = require('../common/password');
const helper = require('../helper/helper.js');
const db = require('../model/db.js');
const biz = require('../business/business.js')
const Op = db.Sequelize.Op;
const sq = db.sequelize;
const df = 'YYYY-MM-DD';
const limit = 20;

router.get('/list', jwtAuth.checkTokenForApi, async (req, res)=>{
  biz.vans.list(req,res)
});

router.post('/create', jwtAuth.checkTokenForApi, async function(req,res) {
  biz.vans.create(req,res);
})

router.get('/o/:id', jwtAuth.checkTokenForApi, async (req, res)=>{
  biz.vans.read(req,res);
});

router.post('/update', jwtAuth.checkTokenForApi, async function(req,res){
  biz.vans.update(req,res);
});

router.post('/delete', jwtAuth.checkTokenForApi, async function(req,res){
  biz.vans.delete(req,res);
});

router.get('/visit_plan/list/:van_id', jwtAuth.checkTokenForApi,async function(req,res){
  biz.vans.visitPlanList(req,res);
});

router.post('/visit_plan/update', jwtAuth.checkTokenForApi,async function(req,res){
  biz.vans.visitPlanUpdate(req,res);
});

router.get('/allot_plan/list/:van_id', jwtAuth.checkTokenForApi,async function(req,res){
  biz.allots.planList(req,res);
});

router.post('/allot_plan/save', jwtAuth.checkTokenForApi,async function(req,res){
  biz.allots.planSave(req,res);
});

router.get('/allot/list', jwtAuth.checkTokenForApi,async function(req,res){
  biz.allots.list(req,res);
});

router.get('/allot/create/skulist', jwtAuth.checkTokenForApi,async function(req,res){
  biz.allots.createSkuList(req,res);
});

router.get('/allot/create/skulist/planned/:date/:van_id', jwtAuth.checkTokenForApi,async function(req,res){
  biz.allots.createSkuListPlanned(req,res);
});

router.get('/allot/list/vanStatus/:site_id/:date', jwtAuth.checkTokenForApi,async function(req,res){
  biz.allots.listVanStatus(req,res);
});

router.post('/allot/create', jwtAuth.checkTokenForApi,async function(req,res){
  biz.allots.create(req,res);
});

router.post('/allot/bulkCreate', jwtAuth.checkTokenForApi,async function(req,res){
  biz.allots.bulkCreate(req,res);
});

router.get('/allot/o/:id', jwtAuth.checkTokenForApi,async function(req,res){
  biz.allots.read(req,res);
});

router.post('/allot/picking', jwtAuth.checkTokenForApi,async function(req,res){
  biz.picklist.add(req,res);
});

module.exports = router;