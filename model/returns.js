module.exports = (sequelize, Sequelize) => {
  var Returns = sequelize.define('returns', {
    id:{
      type: Sequelize.TEXT,
      primaryKey: true
    },
    van_id:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    site_id:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    date:{
      type: Sequelize.DATEONLY,
      defaultValue: Sequelize.NOW,
      allowNull: false
    },
    status:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    // tag:{
    //   type: Sequelize.TEXT,
    //   allowNull: false
    // },
    package_id:{
      type: Sequelize.TEXT
    },
    created_by: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    confirmed_by: {
      type: Sequelize.TEXT
    },
    confirmed_at:{
      type: Sequelize.DATE
    },
    confirmed_date:{
      type: Sequelize.DATEONLY,
      allowNull: true
    }
  },{
    timestamps: true,
    underscored: true,
    freezeTableName: true
  });
  return Returns;
}
