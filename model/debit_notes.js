module.exports = (sequelize, Sequelize) => {
  var DebitNote = sequelize.define('debit_notes', {
    id: {
      type: Sequelize.TEXT,
      primaryKey: true
    },
    date: {
      type: Sequelize.DATEONLY,
      defaultValue: Sequelize.NOW,
      allowNull: false
    },
    site_id: {
      type: Sequelize.TEXT,
      allowNull: false,
      unqiue: true
    },
    outlet_id: {
      type: Sequelize.TEXT,
      allowNull: false,
      unqiue: true
    },
    van_id: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    subtotal: {
      type: Sequelize.DECIMAL,
      allowNull: false
    },
    tax: {
      type: Sequelize.DECIMAL,
      allowNull: false,
      defaultValue: 0
    },
    cent_rounding: {
      type: Sequelize.DECIMAL,
      allowNull: false,
      defaultValue: 0
    },
    grand_total: {
      type: Sequelize.DECIMAL,
      allowNull: false
    },
    credit_note_id:{
      type: Sequelize.TEXT
    },
    remarks:{
      type: Sequelize.TEXT
    },
    status:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    created_by: {
      type: Sequelize.TEXT,
      allowNull: false
    }
  },{
    timestamps: true,
    underscored: true,
    freezeTableName: true
  });
  return DebitNote;
}
