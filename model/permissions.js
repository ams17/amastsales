module.exports = (sequelize, Sequelize) => {
  var Permissions = sequelize.define('permissions', {
    role:{
      type: Sequelize.TEXT,
      primaryKey: true
    },
    func_id:{
      type: Sequelize.TEXT,
      allowNull: false
    }
  },{
    timestamps: true,
    underscored: true,
    freezeTableName: true
  });
  return Permissions;
}