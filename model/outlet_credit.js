module.exports = (sequelize, Sequelize) => {
  var OutletCredit = sequelize.define('outlet_credit', {
    outlet_id:{
      type: Sequelize.TEXT,
      allowNull: false,
      primaryKey: true,
      unqiue: true
    },
    term:{
      type: Sequelize.INTEGER
    },
    limit:{
      type:Sequelize.DECIMAL
    },
    allow_cash:{
      type: Sequelize.BOOLEAN,
      allowNull: false
    },
    allow_credit:{
      type: Sequelize.BOOLEAN,
      allowNull: false
    },
    allow_cheque:{
      type: Sequelize.BOOLEAN,
      allowNull: false
    },
    allow_credit_card:{
      type: Sequelize.BOOLEAN,
      allowNull: false
    },
    created_by: {
      type: Sequelize.TEXT,
      allowNull: false
    }
  },{
    timestamps: true,
    underscored: true,
    freezeTableName: true
  });
  return OutletCredit;
}
