module.exports = (sequelize, Sequelize) => {
  var SkuUoms = sequelize.define('sku_uoms', {
    sku_id:{
      type: Sequelize.TEXT,
      primaryKey: true
    },
    uom_id:{
      type: Sequelize.TEXT,
      primaryKey: true
    },
    sellable:{
      type: Sequelize.BOOLEAN,
      defaultValue: true
    },
    active:{
      type: Sequelize.BOOLEAN,
      defaultValue: true
    },
    created_by: {
      type: Sequelize.TEXT
    }
  },{
    timestamps: true,
    underscored: true,
    freezeTableName: true
  });
  return SkuUoms;
}
