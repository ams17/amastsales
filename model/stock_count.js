module.exports = (sequelize, Sequelize) => {
  var StockCount = sequelize.define('stock_count', {
    id:{
      type: Sequelize.UUID,
      allowNull: false,
      primaryKey: true
    },
    date:{
      type: Sequelize.DATEONLY,
      allowNull: false,
      primaryKey: true
    },
    outlet_id:{
      type: Sequelize.TEXT,
      allowNull: false,
      primaryKey: true
    },
    sku_id:{
      type: Sequelize.TEXT,
      allowNull: false,
      primaryKey: true
    },
    quantity:{
      type: Sequelize.INTEGER,
      allowNull: false
    },
    created_by: {
      type: Sequelize.TEXT,
      allowNull: false
    }
  },{
    timestamps: true,
    underscored: true,
    freezeTableName: true
  });
  return StockCount;
}
