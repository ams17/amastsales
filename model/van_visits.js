module.exports = (sequelize,Sequelize) =>{
  var VanVisits = sequelize.define('van_visits',{
    id: {
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV4,
      allowNull: false,
      primaryKey: true
    },
    date:{
      type: Sequelize.DATEONLY,
      allowNull:false
    },
    van_id:{
      type: Sequelize.TEXT,
      allowNull:false
    },
    outlet_id:{
      type: Sequelize.TEXT,
      allowNull:false
    },
    check_in_at:{
      type: Sequelize.DATE,
      allowNull: false
    },
    check_out_at:{
      type: Sequelize.DATE
    },
    created_by:{
      type: Sequelize.TEXT,
      allowNull: false
    }
  })
  return VanVisits
}