module.exports = (sequelize, Sequelize) => {
  var PriceList = sequelize.define('devices', {
    device_id:{
      type: Sequelize.TEXT,
      primaryKey: true
    },
    ime_id:{
      type: Sequelize.INTEGER,
      allowNull: false
    },
    made:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    model:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    year_purchased:{
      type: Sequelize.INTEGER,
      allowNull: false
    },
    tel_no:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    status:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    created_by: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    updated_by: {
      type: Sequelize.TEXT,
      allowNull: false
    }
  },{
    timestamps: true,
    underscored: true,
    freezeTableName: true
  });
  return PriceList;
}
