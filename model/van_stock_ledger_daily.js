module.exports = (sequelize, Sequelize) => {
  var VanStockLedgerDaily = sequelize.define('van_stock_ledger_daily', {
    id:{
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV4,
      allowNull: false,
      primaryKey: true
    },
    van_id:{
      type: Sequelize.TEXT,
      primaryKey: true,
      allowNull: false
    },
    date:{
      type: Sequelize.DATEONLY,
      primaryKey: true,
      allowNull: false
    },
    sku_id:{
      type: Sequelize.TEXT,
      primaryKey: true
    },
    uom_id:{
      type: Sequelize.TEXT,
      primaryKey: true
    },
    condition:{
      type: Sequelize.TEXT,
      defaultValue: 'fresh',
      allowNull: false,
      primaryKey: true
    },
    prev_quantity:{
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    movement:{
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    new_quantity:{
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: 0
    }
  },{
    timestamps: true,
    underscored: true,
    freezeTableName: true
  });
  return VanStockLedgerDaily;
}