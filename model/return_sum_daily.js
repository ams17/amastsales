module.exports = (sequelize, Sequelize) => {
  var ReturnSumDaily = sequelize.define('return_sum_daily', {
    date:{
      type: Sequelize.DATEONLY,
      defaultValue: Sequelize.NOW,
      primaryKey: true,
      allowNull: false
    },
    van_id:{
      type: Sequelize.TEXT,
      primaryKey: true,
      allowNull: false
    },
    sku_id:{
      type: Sequelize.TEXT,
      primaryKey: true,
      allowNull: false
    },
    uom:{
      type: Sequelize.TEXT,
      primaryKey: true,
      allowNull: false
    },
    condition:{
      type: Sequelize.TEXT,
      primaryKey: true,
      allowNull: false
    },
    declared_quantity:{
      type: Sequelize.INTEGER,
      allowNull: false, 
      defaultValue: 0
    },
    actual_quantity:{
      type: Sequelize.INTEGER,
      allowNull: false, 
      defaultValue: 0
    }
  },{
    timestamps: true,
    underscored: true,
    freezeTableName: true
  });
  return ReturnSumDaily;
}
