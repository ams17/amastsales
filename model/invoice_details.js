module.exports = (sequelize, Sequelize) => {
  var InvoiceDetails = sequelize.define('invoice_details', {
    id: {
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV4,
      primaryKey: true
    },
    invoice_id: {
      type: Sequelize.TEXT,
    },
    sku_id:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    uom_id:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    quantity:{
      type: Sequelize.INTEGER,
      allowNull: false
    },
    price:{
      type: Sequelize.DECIMAL,
      allowNull: false
    },
    discount:{
      type: Sequelize.DECIMAL,
      allowNull: false
    },
    tax:{
      type: Sequelize.DECIMAL,
      allowNull: false
    },
    line_total:{
      type: Sequelize.DECIMAL,
      allowNull: false
    }
  },{
    timestamps: true,
    underscored: true,
    freezeTableName: true
  });
  return InvoiceDetails;
}
