module.exports = (sequelize, Sequelize) => {
  var VoidInvoices = sequelize.define('void_invoices', {
    id: {
      type: Sequelize.TEXT,
      primaryKey: true
    },
    date: {
      type: Sequelize.DATEONLY,
      defaultValue: Sequelize.NOW,
      allowNull: false
    },
    site_id: {
      type: Sequelize.TEXT,
      allowNull: false,
      unqiue: true
    },
    outlet_id: {
      type: Sequelize.TEXT,
      allowNull: false,
      unqiue: true
    },
    van_id: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    subtotal: {
      type: Sequelize.DECIMAL,
      allowNull: false
    },
    discount: {
      type: Sequelize.DECIMAL,
      allowNull: false,
      defaultValue: 0
    },
    tax: {
      type: Sequelize.DECIMAL,
      allowNull: false,
      defaultValue: 0
    },
    cent_rounding: {
      type: Sequelize.DECIMAL,
      allowNull: false,
      defaultValue: 0
    },
    grand_total: {
      type: Sequelize.DECIMAL,
      allowNull: false
    },
    status: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    invoice_id:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    created_by: {
      type: Sequelize.TEXT,
      allowNull: false
    }
  },{
    timestamps: true,
    underscored: true,
    freezeTableName: true
  });
  return VoidInvoices;
}
