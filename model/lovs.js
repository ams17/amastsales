module.exports = (sequelize, Sequelize) => {
  var LOVs = sequelize.define('lovs', {
    group_id:{
      type: Sequelize.TEXT,
      allowNull: false,
      primaryKey: true
    },
    id:{
      type: Sequelize.TEXT,
      primaryKey: true
    },
    value:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    display:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    sequence:{
      type: Sequelize.INTEGER,
      allowNull: false
    },
    is_default:{
      type: Sequelize.BOOLEAN,
      defaultValue: false,
      allowNull: false
    },
    active:{
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: true
    },
    created_by:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    updated_by:{
      type: Sequelize.TEXT,
      allowNull: false
    }
  },{
    timestamps: true,
    underscored: true,
    freezeTableName: true
  });
  return LOVs;
}