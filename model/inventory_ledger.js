module.exports = (sequelize, Sequelize) => {
  var InventoryLedger = sequelize.define('inventory_ledger', {
    id:{
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV4,
      allowNull: false,
      primaryKey: true
    },
    site_id:{
      type: Sequelize.TEXT,
      allowNull: false,
      primaryKey: true
    },
    date:{
      type: Sequelize.DATEONLY,
      allowNull: false
    },
    sku_id:{
      type: Sequelize.TEXT,
      primaryKey: true
    },
    uom_id:{
      type: Sequelize.TEXT,
      primaryKey: true
    },
    condition:{
      type: Sequelize.TEXT,
      defaultValue: 'fresh',
      allowNull: false,
      primaryKey: true
    },
    operation:{
      type: Sequelize.TEXT
    },
    reference_id:{
      type: Sequelize.TEXT
    },
    prev_quantity:{
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    movement:{
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    new_quantity:{
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    current:{
      type: Sequelize.BOOLEAN,
      allowNull: false
    },
    description:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    created_by: {
      type: Sequelize.TEXT,
      allowNull: false
    }
  },{
    timestamps: true,
    underscored: true,
    freezeTableName: true
  });
  return InventoryLedger;
}