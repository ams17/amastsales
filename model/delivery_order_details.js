module.exports = (sequelize, Sequelize) => {
  var DeliveryOrderDetails = sequelize.define('delivery_order_details', {
    delivery_order_id:{
      type: Sequelize.TEXT,
      primaryKey: true
    },
    sku_id:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    uom:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    quantity:{
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: 0
    }
  },{
    timestamps: true,
    underscored: true,
    freezeTableName: true
  });
  return DeliveryOrderDetails;
}
