module.exports = (sequelize, Sequelize) => {
  var VanAllotDetails = sequelize.define('van_allot_details', {
    van_allot_id:{
      type: Sequelize.TEXT,
      primaryKey: true
    },
    sku_id:{
      type: Sequelize.TEXT,
      allowNull: false,
      primaryKey: true
    },
    uom_id:{
      type: Sequelize.TEXT,
      allowNull: false,
      primaryKey: true
    },
    requested_quantity:{
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    allotted_quantity:{
      type: Sequelize.INTEGER,
      allowNull: false, 
      defaultValue: 0
    }
  },{
    timestamps: true,
    underscored: true,
    freezeTableName: true
  });
  return VanAllotDetails;
}
