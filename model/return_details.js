module.exports = (sequelize, Sequelize) => {
  var ReturnDetails = sequelize.define('return_details', {
    return_id:{
      type: Sequelize.TEXT,
      primaryKey: true
    },
    sku_id:{
      type: Sequelize.TEXT,
      primaryKey: true,
      allowNull: false
    },
    uom:{
      type: Sequelize.TEXT,
      primaryKey: true,
      allowNull: false
    },
    condition:{
      type: Sequelize.TEXT,
      primaryKey: true,
      allowNull: false
    },
    declared_quantity:{
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    actual_quantity:{
      type: Sequelize.INTEGER,
      allowNull: false, 
      defaultValue: 0
    }
  },{
    timestamps: true,
    underscored: true,
    freezeTableName: true
  });
  return ReturnDetails;
}
