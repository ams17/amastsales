module.exports = (sequelize, Sequelize) => {
  var VanAllotPlan = sequelize.define('van_allot_plan', {
    van_id:{
      type: Sequelize.TEXT,
      primaryKey: true,
      allowNull: false
    },
    day:{
      type: Sequelize.TEXT,
      primaryKey: true,
      allowNull: false
    },
    sku_id:{
      type: Sequelize.TEXT,
      primaryKey: true,
      allowNull: false
    },
    uom_id:{
      type: Sequelize.TEXT,
      primaryKey: true,
      allowNull: false
    },
    quantity:{
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    created_by: {
      type: Sequelize.TEXT
    }
  },{
    timestamps: true,
    underscored: true,
    freezeTableName: true
  });
  return VanAllotPlan;
}
