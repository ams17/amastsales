module.exports = (sequelize, Sequelize) => {
  var ReturnOrderDetails = sequelize.define('return_order_details', {
    return_order_id:{
      type: Sequelize.TEXT,
      primaryKey: true
    },
    sku_id:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    uom:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    condition:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    declared_quantity:{
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    actual_quantity:{
      type: Sequelize.INTEGER,
      allowNull: false, 
      defaultValue: 0
    }
  },{
    timestamps: true,
    underscored: true,
    freezeTableName: true
  });
  return ReturnOrderDetails;
}
