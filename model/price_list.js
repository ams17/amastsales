module.exports = (sequelize, Sequelize) => {
  var PriceList = sequelize.define('price_list', {
    id:{
      type: Sequelize.TEXT,
      primaryKey: true,
      allowNull: true
    },
    price_group_id:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    sku_id:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    uom_id:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    price:{
      type: Sequelize.DECIMAL,
      allowNull: false,
      defaultValue: 0
    },
    effective_at:{
      type: Sequelize.DATEONLY,
      allowNull: false
    },
    created_by: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    updated_by: {
      type: Sequelize.TEXT,
      allowNull: false
    }
  },{
    timestamps: true,
    underscored: true,
    freezeTableName: true
  });
  return PriceList;
}
