module.exports = (sequelize, Sequelize) => {
  var VanStock = sequelize.define('van_stock', {
    van_id:{
      type: Sequelize.TEXT,
      primaryKey: true
    },
    sku_id:{
      type: Sequelize.TEXT,
      primaryKey: true
    },
    uom_id:{
      type: Sequelize.TEXT,
      primaryKey: true
    },
    condition:{
      type: Sequelize.TEXT,
      defaultValue: 'fresh', //damaged old recalled
      allowNull: false,
      primaryKey: true
    },
    quantity:{
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: 0
    }
  },{
    timestamps: true,
    underscored: true,
    freezeTableName: true
  });
  return VanStock;
}