module.exports = (sequelize, Sequelize) => {
  var Refunds = sequelize.define('refunds', {
    id: {
      type: Sequelize.TEXT,
      primaryKey: true
    },
    date: {
      type: Sequelize.DATEONLY,
      defaultValue: Sequelize.NOW,
      allowNull: false
    },
    outlet_id: {
      type: Sequelize.TEXT,
      allowNull: false,
      unqiue: true
    },
    van_id: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    collected_amount:{
      type: Sequelize.DECIMAL,
      allowNull: false
    },
    method:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    status: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    receipt_id: { 
      type: Sequelize.TEXT,
      allowNull: false
    },
    created_by: {
      type: Sequelize.TEXT,
      allowNull: false
    }
  },{
    timestamps: true,
    underscored: true,
    freezeTableName: true
  });
  return Refunds;
}
