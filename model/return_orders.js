module.exports = (sequelize, Sequelize) => {
  var ReturnOrders = sequelize.define('return_orders', {
    id:{
      type: Sequelize.TEXT,
      primaryKey: true
    },
    outlet_id:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    van_id:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    status:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    collected_by: {
      type: Sequelize.TEXT
    },
    collected_date: {
      type: Sequelize.DATEONLY
    },
    collected_at: {
      type: Sequelize.DATE
    },
    last_attempted_by:{
      type: Sequelize.TEXT
    },
    last_attempted_date:{
      type: Sequelize.DATEONLY
    },
    last_attempted_at:{
      type: Sequelize.DATE
    },
    created_by:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    created_date:{
      type: Sequelize.DATEONLY,
      defaultValue: Sequelize.NOW,
      allowNull: false
    },
  },{
    timestamps: true,
    underscored: true,
    freezeTableName: true
  });
  return ReturnOrders;
}
