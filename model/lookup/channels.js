module.exports = (sequelize, Sequelize) => {
  var Channels = sequelize.define('channels', {
    id:{
      type: Sequelize.TEXT,
      primaryKey: true
    },
    name:{
      type: Sequelize.TEXT,
      allowNull: false
    },    
    active:{
      type: Sequelize.BOOLEAN,
      allowNull: false
    },
    created_by:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    updated_by:{
      type: Sequelize.TEXT
    }
  },{
    timestamps: true,
    underscored: true,
    freezeTableName: true
  });
  
  return Channels;
}
