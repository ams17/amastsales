module.exports = (sequelize, Sequelize) => {
  var KeyAccounts = sequelize.define('key_accounts', {
    id:{
      type: Sequelize.TEXT,
      primaryKey: true
    },
    presence_id:{
      type: Sequelize.TEXT,
      primaryKey: true
    },
    name:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    category:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    active:{
      type: Sequelize.BOOLEAN,
      allowNull: false
    },
    created_by:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    updated_by:{
      type: Sequelize.TEXT
    }
  },{
    timestamps: true,
    underscored: true,
    freezeTableName: true
  });
  return KeyAccounts;
}
