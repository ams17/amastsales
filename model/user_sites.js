module.exports = (sequelize, Sequelize) => {
  var UserSites = sequelize.define('user_sites', {
    user_id:{
      type: Sequelize.TEXT,
      primaryKey: true
    },
    site_id:{
      type: Sequelize.TEXT,
      primaryKey: true
    },
    created_by: {
      type: Sequelize.TEXT
    }
  },{
    timestamps: true,
    underscored: true,
    freezeTableName: true
  });
  return UserSites;
}
