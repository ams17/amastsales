'use strict'

const env = process.env.NODE_ENV || 'dev';
const conf = require('../config/config.json')[env];
const Sequelize = require('sequelize');

const sequelize = new Sequelize(conf.db.name, conf.db.user, conf.db.pass, {
  dialect: 'postgres',
  host: conf.db.host,
  port: conf.db.port,
  timezone: 'Asia/Kuala_Lumpur',
  dialectOptions: {
    useUTC: true
  },
  pool: {
    max: 20,
    min: 10,
    idle: 600000
  },
  logging: false,
  alter: false
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

// public schema
db.credit_note_details = require('./credit_note_details.js')(sequelize, Sequelize);
db.credit_notes = require('./credit_notes.js')(sequelize, Sequelize);
db.debit_notes = require('./debit_notes.js')(sequelize, Sequelize);
db.delivery_order_details = require('./delivery_order_details.js')(sequelize, Sequelize);
db.delivery_orders = require('./delivery_orders.js')(sequelize, Sequelize);
db.inventory = require('./inventory.js')(sequelize, Sequelize);
db.inventory_ledger = require('./inventory_ledger.js')(sequelize, Sequelize);
db.inventory_ledger_daily = require('./inventory_ledger_daily.js')(sequelize, Sequelize);
db.invoice_details = require('./invoice_details.js')(sequelize, Sequelize);
db.invoices = require('./invoices.js')(sequelize, Sequelize);
db.outlet_hierarchy = require('./outlet_hierarchy.js')(sequelize, Sequelize);
db.lovs = require('./lovs.js')(sequelize, Sequelize);
db.outlet_acc_stmt = require('./outlet_acc_stmt.js')(sequelize, Sequelize);
db.outlet_credit = require('./outlet_credit.js')(sequelize, Sequelize);
db.outlets = require('./outlets.js')(sequelize, Sequelize);
db.permissions = require('./permissions.js')(sequelize, Sequelize);
db.picklist_details = require('./picklist_details.js')(sequelize, Sequelize);
db.picklists = require('./picklists.js')(sequelize, Sequelize);
db.price_list = require('./price_list.js')(sequelize, Sequelize);
db.printouts = require('./printouts.js')(sequelize, Sequelize);
db.receipts = require('./receipts.js')(sequelize, Sequelize);
db.refunds = require('./refunds.js')(sequelize, Sequelize);
db.return_details = require('./return_details.js')(sequelize, Sequelize);
db.return_order_details = require('./return_order_details.js')(sequelize, Sequelize);
db.return_orders = require('./return_orders.js')(sequelize, Sequelize);
db.return_sum_daily = require('./return_sum_daily.js')(sequelize, Sequelize);
db.returns = require('./returns.js')(sequelize, Sequelize);
db.settlement = require('./settlement.js')(sequelize, Sequelize);
db.sites = require('./sites.js')(sequelize, Sequelize);
db.sku_uom_conversions = require('./sku_uom_conversions.js')(sequelize, Sequelize);
db.sku_uoms = require('./sku_uoms.js')(sequelize, Sequelize);
db.skus = require('./skus.js')(sequelize, Sequelize);
db.stock_count = require('./stock_count.js')(sequelize, Sequelize);
db.transfer_in_details = require('./transfer_in_details.js')(sequelize, Sequelize);
db.transfer_ins = require('./transfer_ins.js')(sequelize, Sequelize);
db.transfer_out_details = require('./transfer_out_details.js')(sequelize, Sequelize);
db.transfer_outs = require('./transfer_outs.js')(sequelize, Sequelize);
db.user_sites = require('./user_sites.js')(sequelize, Sequelize);
db.users = require('./users.js')(sequelize, Sequelize);
db.van_allot = require('./van_allot.js')(sequelize, Sequelize);
db.van_allot_details = require('./van_allot_details.js')(sequelize, Sequelize);
db.van_allot_plan = require('./van_allot_plan.js')(sequelize, Sequelize);
db.van_allot_sum_daily = require('./van_allot_sum_daily.js')(sequelize, Sequelize);
db.van_stock = require('./van_stock.js')(sequelize, Sequelize);
db.van_stock_ledger = require('./van_stock_ledger.js')(sequelize, Sequelize);
db.van_stock_ledger_daily = require('./van_stock_ledger_daily.js')(sequelize, Sequelize);
db.vans = require('./vans.js')(sequelize, Sequelize);
db.outlet_visit_plan = require('./outlet_visit_plan.js')(sequelize,Sequelize);
db.van_visits = require('./van_visits.js')(sequelize,Sequelize);
db.void_invoices = require('./void_invoices.js')(sequelize, Sequelize);
db.write_off_details = require('./write_off_details.js')(sequelize, Sequelize);
db.write_offs = require('./write_offs.js')(sequelize, Sequelize);
db.device_management = require('./device_management.js')(sequelize, Sequelize);

sequelize.sync();

module.exports = db;