module.exports = (sequelize, Sequelize) => {
  var Receipts = sequelize.define('receipts', {
    id: {
      type: Sequelize.TEXT,
      primaryKey: true
    },
    date: {
      type: Sequelize.DATEONLY,
      defaultValue: Sequelize.NOW,
      allowNull: false
    },
    site_id: {
      type: Sequelize.TEXT,
      allowNull: false,
      unqiue: true
    },
    outlet_id: {
      type: Sequelize.TEXT,
      allowNull: false,
      unqiue: true
    },
    van_id: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    total_sales:{
      type: Sequelize.DECIMAL,
      allowNull: false
    },
    total_amount:{
      type: Sequelize.DECIMAL,
      allowNull: false
    },
    amount_payable:{
      type: Sequelize.DECIMAL,
      allowNull: false
    },
    cash:{
      type: Sequelize.DECIMAL,
      allowNull: false
    },
    collected_amount:{
      type: Sequelize.DECIMAL,
      allowNull: false
    },
    method:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    amount_consumed_in_settlement:{
      type: Sequelize.DECIMAL,
      allowNull: false,
      defaultValue: 0
    },
    status: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    voided:{
      type: Sequelize.BOOLEAN,
      defaultValue: false,
      allowNull: false
    },
    created_by: {
      type: Sequelize.TEXT,
      allowNull: false
    }
  },{
    timestamps: true,
    underscored: true,
    freezeTableName: true
  });
  return Receipts;
}
