module.exports = (sequelize, Sequelize) => {
  var TransferInDetails = sequelize.define('transfer_in_details', {
    id: {
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV4,
      allowNull: false,
      primaryKey: true
    },
    transfer_in_id: {
      type: Sequelize.TEXT,
    },
    receiving:{
      type: Sequelize.BOOLEAN,
      defaultValue: true,
      allowNull: false
    },
    sku_id:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    uom_id:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    condition:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    quantity:{
      type: Sequelize.INTEGER,
      allowNull: false
    },
  },{
    timestamps: true,
    underscored: true,
    freezeTableName: true,
    indexes: [
      {
        name: 'transfer_in_details_unique',
        unique: true,
        fields: ['transfer_in_id', 'receiving', 'sku_id', 'uom_id', 'condition']
      }
    ]
  });
  return TransferInDetails;
}
