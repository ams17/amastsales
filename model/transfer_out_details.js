module.exports = (sequelize, Sequelize) => {
  var TransferOutDetails = sequelize.define('transfer_out_details', {
    id: {
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV4,
      allowNull: false,
      primaryKey: true
    },
    transfer_out_id: {
      type: Sequelize.TEXT,
    },
    sku_id:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    uom_id:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    condition:{
      type: Sequelize.TEXT,
      allowNull: false
    }, 
    quantity:{
      type: Sequelize.INTEGER,
      allowNull: false
    },
  },{
    timestamps: true,
    underscored: true,
    freezeTableName: true,
    indexes: [
      {
        unique: true,
        fields: ['transfer_out_id', 'sku_id', 'uom_id', 'condition']
      }
    ]
  });
  return TransferOutDetails;
}
