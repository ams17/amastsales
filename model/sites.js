module.exports = (sequelize, Sequelize) => {
  var Sites = sequelize.define('sites', {
    id:{
      type: Sequelize.TEXT,
      primaryKey: true
    },
    name:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    address:{
      type: Sequelize.TEXT,
    },
    postcode:{
      type: Sequelize.TEXT,
    },
    city:{
      type: Sequelize.TEXT,
    },
    state:{
      type: Sequelize.TEXT,
    },
    active:{
      type: Sequelize.BOOLEAN,
      defaultValue: true
    },
    created_by: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    logo: {
      type: Sequelize.BLOB,
      allowNull: true
    }
  },{
    timestamps: true,
    underscored: true,
    freezeTableName: true
  });
  return Sites;
}
