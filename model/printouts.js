module.exports = (sequelize, Sequelize) => {
  var Printouts = sequelize.define('printouts', {
    id: {
      type: Sequelize.TEXT,
      primaryKey: true
    },
    date: {
      type: Sequelize.DATEONLY,
      defaultValue: Sequelize.NOW,
      allowNull: false
    },
    outlet_id: {
      type: Sequelize.TEXT,
      allowNull: false,
      unqiue: true
    },
    van_id: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    invoice_id:{
      type:Sequelize.TEXT
    },
    receipt_id:{
      type:Sequelize.TEXT
    },
    credit_note_id:{
      type:Sequelize.TEXT
    },
    void_invoice_id:{
      type:Sequelize.TEXT
    },
    refund_id: {
      type:Sequelize.TEXT
    },
    debit_note_id:{
      type:Sequelize.TEXT
    },
    last_credit:{
      type: Sequelize.DECIMAL,
      allowNull: false
    },
    today_credit:{
      type: Sequelize.DECIMAL,
      allowNull: false
    },
    total_credit:{
      type: Sequelize.DECIMAL,
      allowNull: false
    },
    created_by: {
      type: Sequelize.TEXT,
      allowNull: false
    }
  },{
    timestamps: true,
    underscored: true,
    freezeTableName: true
  });
  return Printouts;
}
