module.exports = (sequelize, Sequelize) => {
  var TransferOuts = sequelize.define('transfer_outs', {
    id: {
      type: Sequelize.TEXT,
      primaryKey: true
    },
    date:{
      type: Sequelize.DATEONLY,
      allowNull: false
    },
    from_site_id:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    to_site_id:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    status:{
      type: Sequelize.TEXT,
      defaultValue: 'in transit',
      allowNull: false
    },
    remark: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    created_by:{
      type: Sequelize.TEXT,
      allowNull: false
    }
  },{
    timestamps: true,
    underscored: true,
    freezeTableName: true
  });
  return TransferOuts;
}
