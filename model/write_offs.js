module.exports = (sequelize, Sequelize) => {
  var WriteOffs = sequelize.define('write_offs', {
    id: {
      type: Sequelize.TEXT,
      primaryKey: true
    },
    date:{
      type: Sequelize.DATEONLY,
      allowNull: false
    },
    site_id:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    remark: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    created_by:{
      type: Sequelize.TEXT,
      allowNull: false
    }
  },{
    timestamps: true,
    underscored: true,
    freezeTableName: true
  });
  return WriteOffs;
}
