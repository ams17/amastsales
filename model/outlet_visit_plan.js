module.exports = (sequelize,Sequelize) =>{
  var OutletVisitPlan = sequelize.define('outlet_visit_plans',{
    van_id:{
      type: Sequelize.TEXT,
      allowNull: false,
      primaryKey: true
    },
    day:{
      type: Sequelize.TEXT,
      allowNull:false,
      primaryKey: true
    },
    outlet_id:{
      type: Sequelize.TEXT,
      allowNull:false,
      primaryKey: true
    },
    updated_by:{
      type: Sequelize.TEXT
    }
  },{
    timestamps: true,
    underscored: true,
    freezeTableName: true
  })
  return OutletVisitPlan
}