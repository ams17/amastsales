module.exports = (sequelize, Sequelize) => {
  var Invoices = sequelize.define('invoices', {
    id: {
      type: Sequelize.TEXT,
      primaryKey: true
    },
    site_id: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    date: {
      type: Sequelize.DATEONLY,
      defaultValue: Sequelize.NOW,
      allowNull: false
    },
    outlet_id: {
      type: Sequelize.TEXT,
      allowNull: false,
      unqiue: true
    },
    van_id: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    subtotal: {
      type: Sequelize.DECIMAL,
      allowNull: false
    },
    discount: {
      type: Sequelize.DECIMAL,
      allowNull: false,
      defaultValue: 0
    },
    tax: {
      type: Sequelize.DECIMAL,
      allowNull: false,
      defaultValue: 0
    },
    cent_rounding: {
      type: Sequelize.DECIMAL,
      allowNull: false,
      defaultValue: 0
    },
    grand_total: {
      type: Sequelize.DECIMAL,
      allowNull: false
    },
    status: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    voided:{
      type: Sequelize.BOOLEAN,
      defaultValue: false,
      allowNull: false
    },
    created_by: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    updated_by: {
      type: Sequelize.TEXT,
      allowNull: false
    }
  },{
    timestamps: true,
    underscored: true,
    freezeTableName: true
  });
  return Invoices;
}
