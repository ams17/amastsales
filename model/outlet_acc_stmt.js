module.exports = (sequelize, Sequelize) => {
  var OutletAccStmt = sequelize.define('outlet_acc_stmt', {
    id:{
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV4,
      allowNull: false,
      primaryKey: true
    },
    outlet_id:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    date:{
      type: Sequelize.DATEONLY,
      defaultValue: Sequelize.NOW,
      allowNull: false
    },
    reference_id:{
      type: Sequelize.DECIMAL,
      allowNull: false,
      defaultValue: 0
    },
    prev_balance:{
      type: Sequelize.DECIMAL,
      allowNull: false,
      defaultValue: 0
    },
    debit:{
      type: Sequelize.DECIMAL
    },
    credit:{
      type: Sequelize.DECIMAL
    },
    balance:{
      type: Sequelize.DECIMAL,
      allowNull: false,
      defaultValue: 0
    },
    current:{
      type: Sequelize.BOOLEAN,
      allowNull: false
    }
  },{
    timestamps: true,
    underscored: true,
    freezeTableName: true
  });
  return OutletAccStmt;
}