module.exports = (sequelize, Sequelize) => {
  var SkuUomConversions = sequelize.define('sku_uom_conversions', {
    sku_id:{
      type: Sequelize.TEXT,
      primaryKey: true
    },
    from_uom_id:{
      type: Sequelize.TEXT,
      primaryKey: true
    },
    to_uom_id:{
      type: Sequelize.TEXT,
      primaryKey: true
    },
    multiply_by:{
      type: Sequelize.INTEGER,
      allowNull: false
    },
    created_by:{
      type: Sequelize.TEXT
    }
  },{
    timestamps: true,
    underscored: true,
    freezeTableName: true
  });
  return SkuUomConversions;
}
