module.exports = (sequelize, Sequelize) => {
  var PicklistDetails = sequelize.define('picklist_details', {
    picklist_id:{
      type: Sequelize.TEXT,
      primaryKey: true
    },
    van_allot_id:{
      type: Sequelize.TEXT,
      primaryKey: true
    }
  },{
    timestamps: true,
    underscored: true,
    freezeTableName: true
  });
  return PicklistDetails;
}
