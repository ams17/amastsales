module.exports = (sequelize, Sequelize) => {
  var TransferIns = sequelize.define('transfer_ins', {
    id: {
      type: Sequelize.TEXT,
      primaryKey: true
    },
    date:{
      type: Sequelize.DATEONLY,
      allowNull: false
    },
    site_id:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    type:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    ref_id:{
      type: Sequelize.TEXT,
      allowNull: false
    },    
    remark: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    created_by:{
      type: Sequelize.TEXT,
      allowNull: false
    }
  },{
    timestamps: true,
    underscored: true,
    freezeTableName: true
  });
  return TransferIns;
}
