module.exports = (sequelize, Sequelize) => {
  var DebitNoteDetails = sequelize.define('debit_note_details', {
    id: {
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV4,
      allowNull: false,
      primaryKey: true
    },
    debit_note_id: {
      type: Sequelize.TEXT,
    },
    sku_id:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    uom_id:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    condition:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    quantity:{
      type: Sequelize.INTEGER,
      allowNull: false
    },
    price:{
      type: Sequelize.DECIMAL,
      allowNull: false
    },
    tax:{
      type: Sequelize.DECIMAL,
      allowNull: false
    },
    line_total:{
      type: Sequelize.DECIMAL,
      allowNull: false
    }
  },{
    timestamps: true,
    underscored: true,
    freezeTableName: true
  });
  return DebitNoteDetails;
}
