module.exports = (sequelize, Sequelize) => {
  var OutletHierarchy = sequelize.define('outlet_hierarchy', {
    id:{
      type: Sequelize.TEXT,
      primaryKey: true
    },
    channel_id: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    subchannel_id: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    outlet_concept_id: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    outlet_type_id: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    active:{
      type: Sequelize.BOOLEAN,
      allowNull: false
    },
    created_by:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    updated_by:{
      type: Sequelize.TEXT
    }
  },{
    timestamps: true,
    underscored: true,
    freezeTableName: true
  });
  
  return OutletHierarchy;
}
