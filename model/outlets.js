module.exports = (sequelize, Sequelize) => {
  var Outlets = sequelize.define('outlets', {
    id:{
      type: Sequelize.TEXT,
      primaryKey: true
    },
    name:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    regno:{
      type: Sequelize.TEXT
    },
    address:{
      type: Sequelize.TEXT
    },
    postcode:{
      type: Sequelize.TEXT
    },
    city:{
      type: Sequelize.TEXT
    },
    state:{
      type: Sequelize.TEXT
    },
    price_group:{
      type: Sequelize.TEXT
    },
    channel:{
      type: Sequelize.TEXT
    },
    subchannel:{
      type: Sequelize.TEXT
    },
    outlet_concept:{
      type: Sequelize.TEXT
    },
    outlet_type:{
      type: Sequelize.TEXT
    },
    category:{
      type: Sequelize.TEXT
    },
    sub_category:{
      type: Sequelize.TEXT
    },
    chain_store_type: {
      type: Sequelize.TEXT
    },
    payment_type: {
      type: Sequelize.TEXT
    },
    payment_term: {
      type: Sequelize.TEXT
    },
    month_frequency:{
      type: Sequelize.TEXT,
      defaultValue: 'monthly',
      allowNull: false
    },
    week_frequency:{
      type: Sequelize.TEXT,
      defaultValue: 'weekly',
      allowNul: false
    },
    status:{
      type: Sequelize.TEXT,
      defaultValue: 'active',
      allowNull: false
    },
    lat:{
      type: Sequelize.DECIMAL,
      allowNull: true
    },
    lng:{
      type: Sequelize.DECIMAL,
      allowNull: true
    },
    created_by: {
      type: Sequelize.TEXT,
      allowNull: false
    }
  },{
    timestamps: true,
    underscored: true,
    freezeTableName: true
  });
  return Outlets;
}