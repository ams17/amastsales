module.exports = (sequelize, Sequelize) => {
  var Skus = sequelize.define('skus', {
    id:{
      type: Sequelize.TEXT,
      primaryKey: true
    },
    name:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    category:{
      type: Sequelize.TEXT
    },
    created_by: {
      type: Sequelize.TEXT,
      allowNull: false
    }
  },{
    timestamps: true,
    underscored: true,
    freezeTableName: true
  });
  return Skus;
}
