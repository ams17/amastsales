module.exports = (sequelize, Sequelize) => {
  var VanAllot = sequelize.define('van_allot', {
    id:{
      type: Sequelize.TEXT,
      primaryKey: true
    },
    van_id:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    date:{
      type: Sequelize.DATEONLY,
      defaultValue: Sequelize.NOW,
      allowNull: false
    },
    status:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    tag: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    delivery_order_id:{
      type: Sequelize.TEXT
    },
    requested_by: {
      type: Sequelize.TEXT
    },
    requested_at: {
      type: Sequelize.DATE
    },
    approved_by:{
      type: Sequelize.TEXT
    },
    approved_at:{
      type: Sequelize.DATE
    },
    rejected_by:{
      type: Sequelize.TEXT
    },
    rejected_at:{
      type: Sequelize.DATE
    },
    added_to_picklist_by:{
      type: Sequelize.TEXT
    },
    added_to_picklist_at:{
      type: Sequelize.DATE
    },
    sent_for_picking_by:{
      type: Sequelize.TEXT
    },
    sent_for_picking_at:{
      type: Sequelize.DATE
    },
    packed_by:{
      type: Sequelize.TEXT
    },
    packed_at:{
      type: Sequelize.DATE
    },
    loaded_by:{
      type: Sequelize.TEXT
    },
    loaded_at:{
      type: Sequelize.DATE
    },
    created_by: {
      type: Sequelize.TEXT
    }
  },{
    timestamps: true,
    underscored: true,
    freezeTableName: true
  });
  return VanAllot;
}
