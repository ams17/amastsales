module.exports = (sequelize, Sequelize) => {
  var Users = sequelize.define('users', {
    id:{
      type: Sequelize.TEXT,
      primaryKey: true
    },
    password:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    role:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    name:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    active:{
      type: Sequelize.BOOLEAN,
      defaultValue: true
    },
    last_login_at:{
      type: Sequelize.DATE
    },
    retries: {
      type: Sequelize.INTEGER,
      defaultValue: 0
    },
    session_id:{
      type: Sequelize.TEXT
    },
    device_id:{
      type: Sequelize.TEXT
    },
    allow_new_device:{
      type: Sequelize.BOOLEAN,
      defaultValue: false,
      allowNull: false
    },
    created_by: {
      type: Sequelize.TEXT
    }
  },{
    timestamps: true,
    underscored: true,
    freezeTableName: true
  });
  return Users;
}
