module.exports = (sequelize, Sequelize) => {
  var WriteOffDetails = sequelize.define('write_off_details', {
    id: {
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV4,
      allowNull: false,
      primaryKey: true
    },
    write_off_id: {
      type: Sequelize.TEXT,
    },
    sku_id:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    uom_id:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    condition:{
      type: Sequelize.TEXT,
      allowNull: false
    }, 
    quantity:{
      type: Sequelize.INTEGER,
      allowNull: false
    },
  },{
    timestamps: true,
    underscored: true,
    freezeTableName: true,
    indexes: [
      {
        unique: true,
        fields: ['write_off_id', 'sku_id', 'uom_id', 'condition']
      }
    ]
  });
  return WriteOffDetails;
}
