module.exports = (sequelize, Sequelize) => {
  var CreditNote = sequelize.define('credit_notes', {
    id: {
      type: Sequelize.TEXT,
      primaryKey: true
    },
    date: {
      type: Sequelize.DATEONLY,
      defaultValue: Sequelize.NOW,
      allowNull: false
    },
    site_id: {
      type: Sequelize.TEXT,
      allowNull: false,
      unqiue: true
    },
    outlet_id: {
      type: Sequelize.TEXT,
      allowNull: false,
      unqiue: true
    },
    van_id: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    subtotal: {
      type: Sequelize.DECIMAL,
      allowNull: false
    },
    tax: {
      type: Sequelize.DECIMAL,
      allowNull: false,
      defaultValue: 0
    },
    cent_rounding: {
      type: Sequelize.DECIMAL,
      allowNull: false,
      defaultValue: 0
    },
    discount: {
      type: Sequelize.DECIMAL,
      allowNull: false,
      defaultValue: 0
    },
    grand_total: {
      type: Sequelize.DECIMAL,
      allowNull: false
    },
    created_by: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    updated_by: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    status: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    voided:{
      type: Sequelize.BOOLEAN,
      defaultValue: false,
      allowNull: false
    }
  },{
    timestamps: true,
    underscored: true,
    freezeTableName: true
  });
  return CreditNote;
}
