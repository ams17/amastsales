module.exports = (sequelize, Sequelize) => {
  var DeliveryOrders = sequelize.define('delivery_orders', {
    id:{
      type: Sequelize.TEXT,
      primaryKey: true
    },
    outlet_id:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    van_id:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    status:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    delivery_start_from_date:{
      type: Sequelize.DATEONLY,
      allowNull: false
    },
    delivered_by: {
      type: Sequelize.TEXT
    },
    delivered_at: {
      type: Sequelize.DATE
    },
    signature_64:{
      type: Sequelize.TEXT
    },
    cancelled_by:{
      type: Sequelize.TEXT
    },
    cancelled_at:{
      type: Sequelize.DATE
    },
    created_date:{
      type: Sequelize.DATEONLY,
      defaultValue: Sequelize.NOW,
      allowNull: false
    }
  },{
    timestamps: true,
    underscored: true,
    freezeTableName: true
  });
  return DeliveryOrders;
}
