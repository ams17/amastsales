module.exports = (sequelize, Sequelize) => {
  var VanAllotSumDaily = sequelize.define('van_allot_sum_daily', {
    date:{
      type: Sequelize.DATEONLY,
      defaultValue: Sequelize.NOW,
      primaryKey: true,
      allowNull: false
    },
    van_id:{
      type: Sequelize.TEXT,
      primaryKey: true,
      allowNull: false
    },
    sku_id:{
      type: Sequelize.TEXT,
      primaryKey: true,
      allowNull: false
    },
    uom:{
      type: Sequelize.TEXT,
      primaryKey: true,
      allowNull: false
    },
    planned_quantity:{
      type: Sequelize.INTEGER,
      allowNull: false, 
      defaultValue: 0
    },
    additional_quantity:{
      type: Sequelize.INTEGER,
      allowNull: false, 
      defaultValue: 0
    },
    delivery_quantity:{
      type: Sequelize.INTEGER,
      allowNull: false, 
      defaultValue: 0
    },
    total_quantity:{
      type: Sequelize.INTEGER,
      allowNull: false, 
      defaultValue: 0
    }
  },{
    timestamps: true,
    underscored: true,
    freezeTableName: true
  });
  return VanAllotSumDaily;
}
