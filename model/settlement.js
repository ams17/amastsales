module.exports = (sequelize, Sequelize) => {
  var Settlement = sequelize.define('settlement', {
    id: {
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV4,
      allowNull: false,
      primaryKey: true
    },
    site_id: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    outlet_id: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    invoice_id: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    due_date: {
      type: Sequelize.DATEONLY,
      allowNull: false
    },
    reference: {
      type: Sequelize.TEXT,
      allowNull: true
    },
    invoice_amount: {
      type: Sequelize.DECIMAL,
      allowNull: false
    },
    prev_outstanding: {
      type: Sequelize.DECIMAL,
      allowNull: false
    },
    settlement_amount: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    new_outstanding: {
      type: Sequelize.DECIMAL,
      allowNull: false
    },
    current: {
      type: Sequelize.BOOLEAN,
      allowNull: false
    }
  },{
    timestamps: true,
    underscored: true,
    freezeTableName: true
  });
  return Settlement;
}
