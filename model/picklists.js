module.exports = (sequelize, Sequelize) => {
  var Picklist = sequelize.define('picklists', {
    id:{
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER
    },
    site_id:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    status:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    sent_for_picking_by: {
      type: Sequelize.TEXT
    },
    sent_for_picking_at: {
      type: Sequelize.DATE
    },
    started_packing_at:{
      type: Sequelize.DATE
    },
    done_packing_at:{
      type: Sequelize.DATE
    },
    created_date:{
      type: Sequelize.DATEONLY,
      allowNull: false
    },
    created_by: {
      type: Sequelize.TEXT,
      allowNull: false
    }
  },{
    timestamps: true,
    underscored: true,
    freezeTableName: true
  });
  return Picklist;
}
