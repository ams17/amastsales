module.exports = (sequelize, Sequelize) => {
  var InventoryLedgerDaily = sequelize.define('inventory_ledger_daily', {
    site_id:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    date:{
      type: Sequelize.DATEONLY,
      allowNull: false
    },
    sku_id:{
      type: Sequelize.TEXT,
      primaryKey: true
    },
    uom_id:{
      type: Sequelize.TEXT,
      primaryKey: true
    },
    condition:{
      type: Sequelize.TEXT,
      defaultValue: 'fresh',
      allowNull: false,
      primaryKey: true
    },
    prev_quantity:{
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    movement:{
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    new_quantity:{
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: 0
    }
  },{
    timestamps: true,
    underscored: true,
    freezeTableName: true
  });
  return InventoryLedgerDaily;
}