module.exports = (sequelize, Sequelize) => {
  var Vans = sequelize.define('vans', {
    id:{
      type: Sequelize.TEXT,
      primaryKey: true
    },
    site_id:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    user_id:{
      type: Sequelize.TEXT,
      allowNull: true
    },
    name:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    territory_type:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    plan_upload:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    shift:{
      type: Sequelize.TEXT,
      allowNull: false
    },
    active:{
      type: Sequelize.BOOLEAN,
      defaultValue: true
    },
    created_by: {
      type: Sequelize.TEXT
    }
  },{
    timestamps: true,
    underscored: true,
    freezeTableName: true
  });
  return Vans;
}