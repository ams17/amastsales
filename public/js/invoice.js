$(()=>{
  let sites = [];
  let xhr;
  let docId;

  let listScreen = $('.content section');
  let viewScreen = new Screen({
    id: 'viewScreen',
    h1: 'Invoice Details', 
    prevScreen: listScreen,
    hasFooter: true
  });

  docHeader = new PivotTable({
    parent: viewScreen.content,
    columns: [
      ['id', 'Invoice ID'],
      ['createdAt', 'Created At'],
      ['outlet_id', 'Outlet ID'],
      ['van_id', 'Van ID'],
      ['status', 'Status'],
      ['voided', 'Voided']
    ],
    format:{
      createdAt: (v) => { return (v)? dayjs(v).format(ntf) : '-'} 
    }
  });
  let val_Voided = $('#val_Voided');

  docDetails = new Table({
      parent: viewScreen.content,
      id: 'doc_details',
      title: 'Invoice',
      columns: [ 
        ['condition', 'Description'],
        ['sku_id', 'SKU'],
        ['uom_id', 'UOM'],
        'price',
        'quantity',
        'tax',
        ['line_total', 'Subtotal']
      ],
      format:{
        condition: (v) => 'SALES',
        price: (v) => Decimal(v).toFixed(2),
        tax: (v) => Decimal(v).toFixed(2),
        line_total: (v) => Decimal(v).toFixed(2),
        createdAt: (v) => { return (v)? dayjs(v).format(ntf) : '-'} 
      },
      selectable: false,
      rowId: 'id',
    });
  
  docDetails.tfoot = $(`<tfoot></tfoot>`).appendTo(docDetails.table);
  docDetails.clear = function(){
    this.tbody.empty();
    this.tfoot.empty();
    return this;
  }

  let result_link = function(doc_id){
    docId = doc_id;
    viewScreen.title.text(`${viewScreen.h1} - ${docId}`);
    viewScreen.show();
    docHeader.clear();
    docDetails.clear();

    if(xhr) xhr.abort();
    xhr = $.get(`a/finance/invoice/o/${docId}`).done(function(d){
      if(!d || !d.invoice) {
        return alert('Invoice ID not found.');
      }
      
      let {invoice, details} = d;
      console.log({invoice, details});
      docHeader.update(invoice);
      docDetails.list(details);
      let cs = 6;
      
      docDetails.tfoot.append(`<tr><td colspan='${cs}'>Sub Total</td><td class='subtotal'>${invoice.subtotal}</td></tr>`);
      docDetails.tfoot.append(`<tr><td colspan='${cs}'>Tax</td><td class='tax'>${invoice.tax}</td></tr>`);
      if(invoice.discount && parseFloat(invoice.discount) > 0){
        docDetails.tfoot.append(`<tr><td colspan='${cs}'>Discount</td><td class='discount'>${invoice.discount}</td></tr>`);  
      }
      docDetails.tfoot.append(`<tr><td colspan='${cs}'>Rounding</td><td class='rounding'>${invoice.cent_rounding}</td></tr>`);
      docDetails.tfoot.append(`<tr><td colspan='${cs}'>Grand Total</td class='grand_total'><td>${invoice.grand_total}</td></tr>`);
      
      (invoice.voided === true)? val_Voided.addClass('voided') : val_Voided.removeClass('voided');

    }).fail(function(x,s,e){
      error(x,s,e);
      alert('Invoice ID not found.')
    });
  }

  function drawDataTable(){
    let dt = new DataTable({
      api: '/a/finance/invoice/list',
      columns: ['id', 'date', 'outlet_id', 'van_id', 'grand_total', 'status', 'voided'],
      format:{
        grand_total: (v) => Decimal(v).toFixed(2),
      },
      filters: {
        searchby: {name: 'Search by', type: 'option', options: [['id','Invoice ID'], ['outlet_id', 'Outlet ID'], ['van_id','Van ID']]},
        date: {name: 'Date', type: 'date', dateOptions: [
            ['anytime', 'Any time'],
            ['today', 'Today'],
            ['weektodate', 'Week-to-date'],
            ['monthtodate', 'Month-to-date'],
            ['yeartodate', 'Year-to-date'],
            ['customdate', 'Custom date'],
            ['customrange', 'Custom range']
          ]}        
      },
      data: 'invoices',
      result_link: result_link
    });

    dt.search(getJsonQueries());
    window.onpopstate = function(event) {
      dt.search(getJsonQueries(), true);
    }
  }

  $.get('/a/site/list').done(function(data){    
    if(data.sites && data.sites.rows){
      sites = data.sites.rows.map(e => e.id);
    }
    drawDataTable();
  });

  let btnPrint = $(`<button type='button' id='printer' class='primary' class='btnPrint'>Print</button>`).appendTo(viewScreen.navRight);
  btnPrint.click(function(){
    let api = `/a/report/invoice?report_id=${docId}`;

    printJS({
      printable:  api,
      type: 'pdf',
          onError: function  (error) {
            alert('Print PDF failed');
          }
      })
  });
})