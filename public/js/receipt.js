$(()=>{
  let sites = [];
  let xhr;
  let docId;

  let listScreen = $('.content section');
  let viewScreen = new Screen({
    id: 'viewScreen',
    h1: 'Receipt Details', 
    prevScreen: listScreen,
    hasFooter: true
  });

  docHeader = new PivotTable({
    parent: viewScreen.content,
    columns: [
      ['id', 'Receipt ID'],
      ['createdAt', 'Created At'],
      ['outlet_id', 'Outlet ID'],
      ['van_id', 'Van ID'],
      ['status', 'Status'],
      ['voided', 'Voided'],
      ['method', 'Method'],
      ['collected_amount', 'Collected Amount'],
      ['amount_consumed_in_settlement', 'Amount Consumed in Settlement']
    ],
    format:{
      collected_amount: (v) => Decimal(v).toFixed(2),
      amount_consumed_in_settlement: (v) =>  Decimal(v).toFixed(2),
      createdAt: (v) => { return (v)? dayjs(v).format(ntf) : '-'} 
    }
  });

  let result_link = function(doc_id){
    docId = doc_id;
    viewScreen.title.text(`${viewScreen.h1} - ${docId}`);
    viewScreen.show();
    docHeader.clear();

    if(xhr) xhr.abort();
    xhr = $.get(`a/finance/receipt/o/${docId}`).done(function(d){
      if(!d || !d.receipt) {
        return alert('Receipt ID not found.');
      }
      
      docHeader.update(d.receipt);

    }).fail(function(x,s,e){
      error(x,s,e);
      alert('Receipt ID not found.')
    });
  }

  function drawDataTable(){
    let dt = new DataTable({
      api: '/a/finance/receipt/list',
      columns: ['id', 'date', 'outlet_id', 'van_id', 'status', 'voided', 'method', 'collected_amount', ['amount_consumed_in_settlement', 'consumed']],
      format:{
        collected_amount: (v) => Decimal(v).toFixed(2),
        amount_consumed_in_settlement: (v) =>  Decimal(v).toFixed(2),
        createdAt: (v) => { return (v)? dayjs(v).format(ntf) : '-'} 
      },
      filters: {
        searchby: {name: 'Search by', type: 'option', options: [['id','Receipt ID'], ['outlet_id', 'Outlet ID'], ['van_id','Van ID']]},
        date: {name: 'Date', type: 'date', dateOptions: [
            ['anytime', 'Any time'],
            ['today', 'Today'],
            ['weektodate', 'Week-to-date'],
            ['monthtodate', 'Month-to-date'],
            ['yeartodate', 'Year-to-date'],
            ['customdate', 'Custom date'],
            ['customrange', 'Custom range']
          ]}        
      },
      data: 'receipts',
      result_link: result_link
    });

    dt.search(getJsonQueries());
    window.onpopstate = function(event) {
      dt.search(getJsonQueries(), true);
    }
  }

  $.get('/a/site/list').done(function(data){    
    if(data.sites && data.sites.rows){
      sites = data.sites.rows.map(e => e.id);
    }
    drawDataTable();
  });

  let btnPrint = $(`<button type='button' id='printer' class='primary' class='btnPrint'>Print</button>`).appendTo(viewScreen.navRight);
  btnPrint.click(function(){
    let api = `/a/report/receipt?report_id=${docId}`;

    printJS({
      printable:  api,
      type: 'pdf',
          onError: function  (error) {
            alert('Print PDF failed');
          }
      })
  });

})