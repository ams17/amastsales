$(()=>{

  let dt;
  let xhr;
  let picklistData;
  let selectedPicklist;

  let listScreen = $('.content section');
  let viewPickingScreen;
  let viewDetails;
  let viewStatus;
  let viewBatch;
  let viewAllotments;
  
  let btnSendForPicking;
  let viewPackingScreen;
  let btnPackAll;
  let scanMode = false;

  function genViewPackingScreen(){
    viewPackingScreen = new Screen({
      id: 'viewPackingScreen',
      h1: 'View Packing List',
      prevScreen: viewPickingScreen
    });
  }

  function genViewPickingScreen(){
    viewPickingScreen = new Screen({
      id: 'viewPickingScreen',
      h1: 'View Picklist', 
      prevScreen: listScreen
    });



    let div1 = $(`<div id='div1'></div>`).appendTo(viewPickingScreen.content);
    let div2 = $(`<div id='div2'></div>`).appendTo(viewPickingScreen.content);
    let div3 = $(`<div id='div3'></div>`).appendTo(viewPickingScreen.content);

    viewDetails = new PivotTable({
      parent: div1,
      columns: [
        ['id', 'Picklist ID'], 
        ['site_id', 'Site ID'],
        ['created_date', 'Created Date']
      ]
    });

    viewStatus = new EventHistory({
      parent: div1,
      id: 'picklistStatus',
      title: 'Status History',
      format: `DD MMM 'YY, hh:mmA`
    });

    viewBatch = new SkuTable({
      parent: div2,
      id: 'batch',
      title: 'Batch Picklist',
      columns: [['sku_id', 'SKU ID'], ['uom_id', 'UOM']],
      quantityColumns: ['Allotted Qty'],
      viewOnly: true
    });

    function allot_result_link(id){
      let d = new Dialog();
      d.overlay.click(function(){ $('body').css('overflow', 'auto') });
      d.modalCont.addClass('allot');
      d.modal.empty();
      d.title = $(`<h1>Allotment ${id}</h1>`).appendTo(d.modal);
      d.content = $(`<div class='content'></div>`).appendTo(d.modal);
      d.details = new PivotTable({
        parent: d.content,
        columns: [
          ['id', 'Allotment ID'], 
          ['van_id', 'Van ID'],
          ['date', 'Date'],
          ['tag', 'Tag'],
          ['status', 'Status'],
          ['delivery_order_id', 'Delivery Order ID']
        ]
      });

      d.skuTable = new SkuTable({
        parent: d.content,
        title: 'Allotment',
        columns: [['sku_id', 'SKU ID'], ['uom_id', 'UOM']],
        quantityColumns: ['quantity'],
        viewOnly: true
      });
      d.btns = $(`<div class='btns'></div>`).appendTo(d.modal);

      if(xhr) xhr.abort();
      xhr = $.get(`/a/van/allot/o/${id}`).done(function(data){
        d.details.update(data.allotment);

        for(let i = 0; i < data.details.length; i++){
          // take only allotted qty
          data.details[i].quantity = [data.details[i].quantity[1]];
        }
        d.skuTable.list(data.details);

        if(data.allotment.status == 'added to picklist'){
          let btnRemoveFromPicklist = $(`<button id='btnRemoveFromPicklist' class='secondary warn'>Remove From Picklist</button>`).appendTo(d.btns);

          btnRemoveFromPicklist.click(function(){
            confirmRemove = confirm(`Are you sure you want remove this allotment from the picklist?`);

            if(!confirmRemove) return;
            btnRemoveFromPicklist.prop('disabled', true);
            $.post(`/a/picklist/remove`, {allot_id: id}).done(function(data){
              alert(`${id} removed from picklist successfully.`);
              d.overlay.trigger('click');

              if(data.picklist_removed){
                alert('Picklist deleted.');
                viewPickingScreen.btnBack.trigger('click');
                dt.search(getJsonQueries());  
                return;
              }

              result_link(selectedPicklist);

            }).fail(function(x,s,e){
              error(x,s,e);
            }).always(function(){
              btnRemoveFromPicklist.prop('disabled', false);
            });
          });
        }

        if(data.allotment.status == 'sent for picking'){
          let btnPack = $(`<button id='btnPack' class='primary'>Pack</button>`).appendTo(d.btns);
          btnPack.click(function(){
            btnPack.prop('disabled', true);
            $.post(`/a/picklist/pack`, {allot_id: id}).done(function(data){
              alert(`${id} packed.`);
              d.overlay.trigger('click');
              result_link(selectedPicklist);
              dt.search(getJsonQueries());  
            }).fail(function(x,s,e){
              error(x,s,e);
            }).always(function(){
              btnPack.prop('disabled', false);
            });
          });
        }

      }).fail(function(x,s,e){
        error(x,s,e);
      });
    }

    viewAllotments = new Table({
      parent: div3,
      id: 'allots',
      title: 'Packing List',
      columns: [['van_allot_id', 'Allotment ID'], ['van_id', 'Van ID'], 'status'],
      selectable: true,
      rowId: 'van_allot_id',
      result_link: allot_result_link
    });

    btnPackAll = $(`<button type='button' id='btnPackAll' class='primary' class='btnPackAll'>Pack All</button>`).appendTo(viewPickingScreen.navRight);
    btnPackAll.hide();
    btnPackAll.click(function(){
      let confirmPackAll = confirm('Are you sure you want to pack all remaining allotments?');
      if(!confirmPackAll) return;

      btnPackAll.prop('disabled', true);
      if(xhr) xhr.abort();
      xhr = $.post('/a/picklist/packAll', {id:  selectedPicklist}).done(function(data){
        alert(`Picklist ${selectedPicklist} is fully packed successfully.`);
        viewPickingScreen.btnBack.trigger('click');
        dt.search({keyword: selectedPicklist}, true);
      }).fail(function(x,s,e){
        error(x,s,e);
      }).always(function(){
        btnPackAll.prop('disabled', false);
      });
    });

    btnViewPackingList = $(`<button type='button' id='btnViewPackingList' class='secondary'>View Packing List</button>`).appendTo(viewPickingScreen.navRight);
    btnViewPackingList.hide();
    btnViewPackingList.click(function(){
      if(!viewPackingScreen) genViewPackingScreen();
      viewPackingScreen.show();
    });

    btnDeletePicklist = $(`<button type='button' id='btnDeletePicklist' class='secondary'>Delete Picklist</button>`).appendTo(viewPickingScreen.navRight);
    btnDeletePicklist.hide();
    btnDeletePicklist.click(function(){
      let confirmDelete = confirm(`Are you sure you want to delete picklist ${selectedPicklist}?\nThis cannot be undone.`);
      if(!confirmDelete) return;

      btnDeletePicklist.prop('disabled', true);
      if(xhr) xhr.abort();
      xhr = $.post('/a/picklist/delete', {id: selectedPicklist}).done(function(data){
        alert(`Picklist ${selectedPicklist} deleted successfully.`);
        viewPickingScreen.btnBack.trigger('click');
        dt.search(getJsonQueries());
      }).fail(function(x,s,e){
        error(x,s,e);
      }).always(function(){
        btnDeletePicklist.prop('disabled', false);
      });
    });

    btnSendForPicking = $(`<button type='button' id='btnSendForPicking'>Send for Picking</button>`).appendTo(viewPickingScreen.navRight);
    btnSendForPicking.hide();
    btnSendForPicking.click(function(){
      let confirmSend = confirm(`Are you sure you want to send picklist ${selectedPicklist} for picking?`);
      if(!confirmSend) return;

      btnSendForPicking.prop('disabled', true);
      if(xhr) xhr.abort();
      xhr = $.post('/a/picklist/sendForPicking', {id: selectedPicklist}).done(function(data){
        alert(`Picklist ${selectedPicklist} sent for picking.`);
        viewPickingScreen.btnBack.trigger('click');
        dt.search({keyword: selectedPicklist}, true);
      }).fail(function(x,s,e){
        let res = JSON.parse(x.responseText);
        let errCode = res.errCode || '';
        if(errCode != 'insufficient') return error(x,s,e);

        let errTxt = res.errMsg + res.insufficiencies.map(i => {
          let txt = `\n${i.sku_id} ${i.uom_id} - stock: ${i.available_quantity}`;
          if(i.reserved_quantity > 0) txt += `, reserved: ${i.reserved_quantity}`;
          return txt + '.';
        }).join('');

        alert(errTxt);

      }).always(function(){
        btnSendForPicking.prop('disabled', false);
      });
    });
  }

  function result_link(id){
    selectedPicklist = id;
    if(!viewPickingScreen) genViewPickingScreen();
    viewDetails.clear();
    viewStatus.clear();
    viewBatch.clear();
    btnDeletePicklist.hide();
    btnSendForPicking.hide();
    viewPickingScreen.show();

    if(xhr) xhr.abort();
    $.get(`/a/picklist/o/${id}`).done(function(data){
      picklistData = data;
      viewDetails.update(data.picklist);
      let d = data.picklist;
      let history = [
        {status: 'Cancelled', by: d.cancelled_by, at: d.cancelled_at},
        {status: 'Done packing', by: null, at: d.done_packing_at},
        {status: 'Started packing', by: null, at: d.started_packing_at},
        {status: 'Sent for picking', by: d.sent_for_picking_by, at: d.sent_for_picking_at},
        {status: 'Opened', by: d.created_by, at: d.createdAt}
      ];
      viewStatus.list(history);
      viewBatch.list(data.batch);
      viewAllotments.list(data.packing);

      if(data.picklist.status == 'opened'){
        btnDeletePicklist.show();
        btnSendForPicking.show();
      }

      if(['sent for picking', 'started packing'].includes(data.picklist.status)){
        btnPackAll.show();

        viewAllotments.sideBtns = $(`<div class='sideBtns'></div>`).appendTo(viewAllotments.title);
        let btnScan = $(`<button id='btnScan' class='sideBtn secondary'><img src='/img/scan.svg'></button>`).appendTo(viewAllotments.sideBtns);
        btnScan.click(function(){
          scanMode = true;
          // scan dialog
          let sd = new Dialog('Scan Allotment');
          sd.overlay.click(function(){
            $('body').css('overflow', 'auto');
            scanMode = false;
          });
        });
      }
      
     
    }).fail(function(x,s,e){
      error(x,s,e);
    })
  }

  function drawDataTable(){
    dt = new DataTable({
      api: 'a/picklist/list',
      columns: [['created_date','Created Date'], 'site_id', 'id', 'status', 'created_by', ['created_at', 'Created At']],
      filters: {
        site_id: {name: 'Site ID', type: 'option', options:['All', ...sites]},
        date: {name: 'Date', type: 'date', dateOptions: [
            ['anytime', 'Any time'],
            ['today', 'Today'],
            ['weektodate', 'Week-to-date'],
            ['monthtodate', 'Month-to-date'],
            ['yeartodate', 'Year-to-date'],
            ['customdate', 'Custom date'],
            ['customrange', 'Custom range']
          ]}
      },
      format:{
        created_at: (v) => { return (v)? dayjs(v).format(df) : '-'} 
      },
      data: 'picklists',
      result_link: result_link
    });

    dt.search(getJsonQueries());
    window.onpopstate = function(event) {
      dt.search(getJsonQueries(), true);
    }
  }

  $.get('/a/site/list').done(function(data){    
    if(data.sites && data.sites.rows){
      sites = data.sites.rows.map(e => e.id);
    }
    drawDataTable();
  });



})