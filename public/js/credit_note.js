$(()=>{
  let sites = [];
  let xhr;

  let listScreen = $('.content section');
  let viewScreen = new Screen({
    id: 'viewScreen',
    h1: 'Credit Note Details', 
    prevScreen: listScreen,
    hasFooter: true
  });

  docHeader = new PivotTable({
    parent: viewScreen.content,
    columns: [
      ['id', 'Credit Note ID'],
      ['createdAt', 'Created At'],
      ['outlet_id', 'Outlet ID'],
      ['van_id', 'Van ID'],
      ['status', 'Status'],
      ['voided', 'Voided']
    ],
    format:{
      createdAt: (v) => { return (v)? dayjs(v).format(ntf) : '-'} 
    }
  });

  docDetails = new Table({
      parent: viewScreen.content,
      id: 'doc_details',
      title: 'Credit Note',
      columns: [ 
        ['condition', 'Description'],
        ['sku_id', 'SKU'],
        ['uom_id', 'UOM'],
        'price',
        'quantity',
        'tax',
        ['line_total', 'Subtotal']
      ],
      format:{
        condition: (v) => (v).toUpperCase(),
        price: (v) => Decimal(v).toFixed(2),
        tax: (v) => Decimal(v).toFixed(2),
        line_total: (v) => Decimal(v).toFixed(2),
        createdAt: (v) => { return (v)? dayjs(v).format(ntf) : '-'} 
      },
      selectable: false,
      rowId: 'id',
    });
  
  docDetails.tfoot = $(`<tfoot></tfoot>`).appendTo(docDetails.table);
  docDetails.clear = function(){
    this.tbody.empty();
    this.tfoot.empty();
    return this;
  }

  let result_link = function(doc_id){
    docId = doc_id;
    viewScreen.title.text(`${viewScreen.h1} - ${docId}`);
    viewScreen.show();
    docHeader.clear();
    docDetails.clear();

    if(xhr) xhr.abort();
    xhr = $.get(`a/finance/credit_note/o/${docId}`).done(function(d){
      if(!d || !d.credit_note) {
        return alert('Credit Note ID not found.');
      }
      
      let {credit_note, details} = d;
      console.log({credit_note, details});
      docHeader.update(credit_note);
      docDetails.list(details);
      let cs = 6;
      
      docDetails.tfoot.append(`<tr><td colspan='${cs}'>Sub Total</td><td class='subtotal'>${credit_note.subtotal}</td></tr>`);
      docDetails.tfoot.append(`<tr><td colspan='${cs}'>Tax</td><td class='tax'>${credit_note.tax}</td></tr>`);
      if(credit_note.discount && parseFloat(credit_note.discount) > 0){
        docDetails.tfoot.append(`<tr><td colspan='${cs}'>Discount</td><td class='discount'>${credit_note.discount}</td></tr>`);  
      }
      docDetails.tfoot.append(`<tr><td colspan='${cs}'>Rounding</td><td class='rounding'>${credit_note.cent_rounding}</td></tr>`);
      docDetails.tfoot.append(`<tr><td colspan='${cs}'>Grand Total</td class='grand_total'><td>${credit_note.grand_total}</td></tr>`);
      

    }).fail(function(x,s,e){
      error(x,s,e);
      alert('Credit Note ID not found.')
    });
  }

  function drawDataTable(){
    let dt = new DataTable({
      api: '/a/finance/credit_note/list',
      columns: ['id', 'date', 'outlet_id', 'van_id', 'grand_total', 'status', 'voided'],
      filters: {
        searchby: {name: 'Search by', type: 'option', options: [['id','Credit Note ID'], ['outlet_id', 'Outlet ID'], ['van_id','Van ID']]},
        date: {name: 'Date', type: 'date', dateOptions: [
            ['anytime', 'Any time'],
            ['today', 'Today'],
            ['weektodate', 'Week-to-date'],
            ['monthtodate', 'Month-to-date'],
            ['yeartodate', 'Year-to-date'],
            ['customdate', 'Custom date'],
            ['customrange', 'Custom range']
          ]}        
      },
      data: 'credit_notes',
      result_link: result_link
    });

    dt.search(getJsonQueries());
    window.onpopstate = function(event) {
      dt.search(getJsonQueries(), true);
    }
  }

  $.get('/a/site/list').done(function(data){    
    if(data.sites && data.sites.rows){
      sites = data.sites.rows.map(e => e.id);
    }
    drawDataTable();
  });

  let btnPrint = $(`<button type='button' id='printer' class='primary' class='btnPrint'>Print</button>`).appendTo(viewScreen.navRight);
  btnPrint.click(function(){
    let api = `/a/report/credit_note?report_id=${docId}`;

    printJS({
      printable:  api,
      type: 'pdf',
          onError: function  (error) {
            alert('Print PDF failed');
          }
      })
  });

})