$(()=>{
  let inputRole = $('#role');
  const role = inputRole.val();
  inputRole.remove();

  const inputUserid = $('#userid');
  const statusTags = $('.status .tags');
  const roleTags = $('.role .tags');
  const divCardHeader = $('.card.header');
  const userlist = $('.userlist');
  const tbUsers = $('#tbUsers');

  const grpctrl = $('.grpctrl');
  const btnDownload = $('#btnDownload');
  const pager = $('.pager');
  const counterLower = $('.counter .lower');
  const counterUpper = $('.counter .upper');
  const counterTotal = $('.counter .total');
  const stepPrev = $('.stepper .prev');
  const stepNext = $('.stepper .next');

  const divCardDetails = $('.card.details');
  const btnBack = $('#btnBack');

  const tbUser = $('#tbUser', divCardDetails);
  const odId = $('h1', divCardDetails);

  const udId = $('.id .value', tbUser);
  const udName = $('.name .value', tbUser);
  const udRole = $('.role .value', tbUser);
  const udStatus = $('.status .value', tbUser);
  const udLastLoginAt = $('.lastLoginAt .value', tbUser);
  const udRetries = $('.retries .value', tbUser);
  const udCreatedBy = $('.createdBy .value', tbUser);
  const udCreatedAt = $('.createdAt .value', tbUser);

  let openedUser;

  const df = 'YYYY-MM-DD h:mmA';
  const nousers = $(`<div class='nousers'><p>No users found. Try a different search criteria.</p></div>`);

  let page = 1;
  let searchby = 'userid';

  $('input[type="radio"]', statusTags).click(function(e){
    page = 1;
    let stat = $('input:checked', statusTags).val();
    let role = $('input:checked', roleTags).val();

    getUsers();

    btnDownload.prop('href', `/a/user/download/csv/${stat}/${role}`);
  });

  $('input[type="radio"]', roleTags).click(function(e){
    page = 1;
    let stat = $('input:checked', statusTags).val();
    let role = $('input:checked', roleTags).val();
    getUsers();

    btnDownload.prop('href', `/a/user/download/csv/${stat}/${role}`);
  });

  btnBack.click(function(e){
    divCardHeader.show();
    divCardDetails.hide();
    getUsers();
    $(window).scrollTop(lastScrollPos);
    openedUser = null;
  });

  inputUserid.on('keypress', function(e){

    if(e.which == 13) {
      page = 1;
      getUsers();
    }

    let stat = $('input:checked', statusTags).val();
    let role = $('input:checked', roleTags).val();

    btnDownload.prop('href', `/a/user/download/csv/${stat}/${role}/userid/${inputUserid.val()}`);

  });

  btnBack.click(function(e){
    divCardHeader.show();
    divCardDetails.hide();
    getUsers();

    $(window).scrollTop(lastScrollPos);

    openedUser = null;
    
  });

  stepPrev.click(function(e){
    stepPrev.prop('disabled', true);
    stepNext.prop('disabled', true);
    page--;
    getUsers(function(){
      stepPrev.prop('disabled', false);
      stepNext.prop('disabled', false);
    });
  });

  stepNext.click(function(e){
    stepPrev.prop('disabled', true);
    stepNext.prop('disabled', true);
    page++;
    getUsers(function(){
      stepPrev.prop('disabled', false);
      stepNext.prop('disabled', false);
    });
  });

  function showNoUsers(){
    tbUsers.hide();
    divCardHeader.append(nousers);
    grpctrl.hide();
    updatePager();
  }

  function updatePager(orders){
    let cLower = (!orders || orders.offset == null )? 0: orders.offset + 1;
    let cUpper = (!orders || orders.offset == null && !orders.rows)? 0: orders.offset + orders.rows.length;
    let cTotal = (!orders || orders.count  == null)? 0: orders.count;

    counterLower.text(cLower);
    counterUpper.text(cUpper);
    counterTotal.text(cTotal);

    if(cLower <= 1) {
      stepPrev.prop('disabled', true);
    } else {
      stepPrev.prop('disabled', false);
    }
    if(cUpper >= cTotal) {
      stepNext.prop('disabled', true);
    } else {
      stepNext.prop('disabled', false);
    }

    if(cTotal <= 0){
      pager.hide();
    } else {
      pager.show();
    }
  }

  function openDetails(oid){
    if(!oid || oid == 'undefined' || oid == null) {
      console.error('ID for details not provided.');
      let dialog = new Dialog('Failed to get user details.', 'An error occurred when getting user details.');
      dialog.addCancelButton().text('OK');
      return;
    }
    //clearOrderDetails();
    divCardHeader.hide();
    divCardDetails.show();

    $.get(`/a/user/o/${oid}`).done(function(r){
      let u = r.user;

      odId.text(u.id.toUpperCase());

      openedUser = u.id;

      udId.text(u.id);
      udName.text(u.name);
      udRole.text(u.role);
      udStatus.text(u.status)
      udLastLoginAt.text((u.last_login_at == null)? '' : moment(u.last_login_at).format(df));
      udRetries.text(u.retries);
      udCreatedBy.text(u.created_by);
      udCreatedAt.text(moment(u.createdAt).format(df));
  
      $(window).scrollTop(0);
      
    }).fail(function(e){
      console.error(e);
    });
  }

  function drawUsers(users){
    
    let html = '';
    for(let o of users){
      html += `<tr class='user' data-id='${o.id}'>`
      
      html += `<td class='id'>${o.id}</td>
               <td class='name'>${o.name}</td>              
               <td class='role'>${o.role}</td>
               <td class='status'>${o.status}</td>
               <td class='ts'>${o.last_login_at == null ? '' : moment(o.last_login_at).format(df)}</td>
               <td class='by'>${o.created_by}</td>
               <td class='ts'>${moment(o.created_at).format(df)}</td>
            <tr>`;
    }

    userlist.empty().append(html);

    // craete listeners
    if(users.length > 0){      
      
      $('.user td:not(.chk)').click(function(e){
        lastScrollPos = $(window).scrollTop();
        openDetails($(this).parent().data('id'));
      });
    }
  }

  function getUsers(next){

    let api = '/a/user/list';
    queries = [];

    let stat = $('input:checked', statusTags).val();
    if(stat) queries.push(`status=${stat}`);

    let role = $('input:checked', roleTags).val();
    if(role) queries.push(`role=${role}`);

    if(searchby == 'userid') {
      queries.push(`searchby=userid&keywords=${inputUserid.val()}`);
    }
    
    if(page > 1) queries.push(`page=${page}`);
    if(queries.length > 0) api+=`?${queries.join('&')}`;

    $('.nousers', divCardHeader).remove();
    $.get(api).done(function(r){
      if(!r || !r.users || !r.users.rows){        
        new Dialog('Server not returning users').addCancelButton().text('OK');
        return console.error('Server not returning users');
      }

      let users = r.users;
      if(users.rows.length == 0) return showNoUsers();

      grpctrl.show();
      pager.show();
      tbUsers.show();
      
      drawUsers(users.rows);
      updatePager(users);

    }).fail(function(err){
      if(err.statusText != 'abort'){
        console.error(err);
        let dialog = new Dialog('Failed to get users.', 'An error occurred when getting users.');
        dialog.addCancelButton().text('OK');
      }
      showNoUsers();
    });

    if(next) next();
  }

  getUsers();

})