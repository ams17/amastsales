let salesSummarySkus, returnSummarySkus;
$(()=>{

  let xhr;
  let icont = $('article .icont');
  let pageTitle = $('article .top h1');
  let sitelist = getSitelist();
  let searchBar;
  let selectedSite, selectedSku, selectedDate;
  let invtTable;
  let sites;
  const today = dayjs().format(df);
  const ledgerApi = '/a/inventory/ledger/list';

  let mainSection = $('.content section');
  let ledgerScreen;
  let stockReconDataTable;
  let fQuantity;

  let expectedQty;
  let expectedQtyMap = new Map();

  let fType = new Field({ parent: icont, id: 'fType', title: 'Select action'});
  typeCards = new Cards({
    parent: fType.label,
    id: 'typeCards'
  }, [
    {id: 'sr', display: 'Stock Recon'}, 
    {id: 'cr', display: 'Cash Recon'}
  ]);
  fType.show();

  $.get('/a/site/list').done(function(data){    
    if(data.sites && data.sites.rows){
      sites = data.sites.rows.map(e => e.id);
    }
  });

  typeCards.cards.on('selected', function(ev, id){
    let type_id = $('.card.selected', typeCards.cards).eq(0).data('id');
    if(type_id == 'sr'){
      pageTitle.text('Stock Recon');
      if (!stockReconDataTable) createStockReconScreen();
      searchBar.show();
      typeCards.hide();
    } 
  });

  typeCards.cards.on('deselected', function(){
    fType.title.show();
    typeCards.deselect();
    searchBar.hide();
    salesSummarySkus.hide();
    returnSummarySkus.hide();
    fQuantity.hide();
  });

  function createStockReconScreen(){
    fType.title.hide();

    searchBar = $(`<article>
      <div class='top'>
      </div>
      <div class='searchCont'></div>
      <div class='actPager'>
        <div class='act'></div>
        <div class='pager'></div>
      </div>
    </article>
    <div class='results'></div>`).appendTo(icont);
    
    stockReconDataTable = new DataTable({
      api: 'a/reconciliation/stock/list',
      columns: ['site_id', ['id', 'van_id'], 'allotment_date', 'target_upload_date', 'status'],
      filters: {
        site_id: {name: 'Site ID', type: 'option', options:['All', ...sites]},
        date: {name: 'Date', type: 'date', dateOptions: [
            ['anytime', 'Any time'],
            ['today', 'Today'],
            ['weektodate', 'Week-to-date'],
            ['monthtodate', 'Month-to-date'],
            ['yeartodate', 'Year-to-date'],
            ['customdate', 'Custom date'],
            ['customrange', 'Custom range']
          ]}        
      },
      format:{
        created_at: (v) => { return (v)? dayjs(v).format(df) : '-'} 
      },
      data: 'stockRecon',
      result_link: stockReconDetails
    });

    stockReconDataTable.search(getJsonQueries());
    window.onpopstate = function(event) {
      stockReconDataTable.search(getJsonQueries(), true);
    }

    fQuantity = new Field({ parent: icont, id: 'fQuantity', title: 'SKU quantity'});
    fQuantity.d1 = $(`<div class='d1'></div>`).appendTo(fQuantity.field);
    fQuantity.hide();

    salesSummarySkus = new SkuTable({
      parent: fQuantity.d1,
      id: 'salesSkus',
      title: 'Sales Summary',
      columns: [['sku_id', 'SKU ID'], ['uom_id', 'UOM']],
      quantityColumns: ['Alloted', 'Sales', 'Fresh', 'Expected', 'Received', 'Not Received'],
      viewOnly: true
    });
    salesSummarySkus.hide();

    returnSummarySkus = new SkuTable({
      parent: fQuantity.d1,
      id: 'returnSkus',
      title: 'Return Summary',
      columns: [['sku_id', 'SKU ID'], ['uom_id', 'UOM']],
      quantityColumns: ['Old', 'Damaged', 'Recalled', 'Expected', 'Received', 'Not Received'],
      viewOnly: true
    });
    returnSummarySkus.hide();

  }

  function stockReconDetails(vanId) {
    pageTitle.text('Stock Recon - ' + vanId);
    searchBar.hide();
    salesSummarySkus.show();
    salesSummarySkus.clear(); 

    returnSummarySkus.show();
    returnSummarySkus.clear();

    url = `/a/reconciliation/o/stock/${vanId}`;
    $.ajax({ type: 'GET', async: false, url: url, 
      error: function(x,s,e){ err = {x,s,e} },
      success: function(data) { 
        if(data.salesSummary) {
          salesSummarySkus.list(data.salesSummary);
          salesSummarySkus.skutable.show();
        }

        if(data.returnSummary) {
          returnSummarySkus.list(data.returnSummary);
          returnSummarySkus.skutable.show();
        }
      } 
    });

    fQuantity.show();

    let btnConfirm = $(`<button class='confirm primary'>Confirm</button>`).appendTo(icont);
  }
});

  