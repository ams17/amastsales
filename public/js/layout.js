$(()=>{
  dayjs.extend(dayjs_plugin_isoWeek);
  
  let user = $('#user');
  let user_profile = $('#user_profile');
  let notification = $('#notification');
  let logout = $('#logout');
  let printer = $('button.printer');

  logout.click(function(){
    let dialog = new Dialog('Log Out Your Account?');
    dialog.addCancelButton();
    dialog.addButton('primary','Yes').click(function(){
      window.location.href = '/logout';
    });
  });

  $('.main-menu > ul > li > a').on('click', function (e) {
    if ($(this).hasClass('panel-collapsed')) {
      // collapse other panels
      $(this).parents('ul').find('.collapsein').slideUp();
      $(this).parents('ul').find('a').addClass('panel-collapsed');

      // expand the panel
      $(this).parents('li').find('.collapsein').slideDown();
      $(this).removeClass('panel-collapsed');
    }
    else {
      // collapse the panel
      $(this).parents('li').find('.collapsein').slideUp();
      $(this).addClass('panel-collapsed');
    }
  });

  printer.click(function(e){
    window.print();
  });

  (function getUserProfile(){
    $.get('/a/user/profile').done(function(r){
      $('.name', user_profile).text(r.name.toUpperCase());
    }).fail(function(e){
      console.error(e);
      alert('Failed to get user profile.');
    });
  })();

  (function getNotificationCount(){
    $.get('/a/notification/count').done(function(r){
      if (r.notification != null) {

        $('.count', notification).show();

        if (r.notification.count > 99) {
          $('.count', notification).text('99+');
        } else {
          $('.count', notification).text(r.notification.count);
        }
      }
    }).fail(function(e){
      console.error(e);
      alert('Failed to get notification count.');
    });
  })();

  user.click(function(){
    user_profile.show();
  });

  notification.click(function(){
    let d = new Dialog();
    d.overlay.click(function(){ $('body').css('overflow', 'auto') });
    d.modalCont.addClass('notificationDetails');
    d.modal.empty();
    d.title = $(`<h1>Notification</h1>`).appendTo(d.modal);
    $(`<article>
      <div class='top'>
      </div>
      <div class='searchCont'></div>
      <div class='actPager'>
        <div class='act'></div>
        <div class='pager'></div>
      </div>
    </article>
    <div class='results'></div>`).appendTo(d.modal);

    dt = new DataTable({
      api: '/a/notification/list',
      columns: ['date', ['id','Reference'], 'sender', 'status'],
      data: 'notifications',
      parent: d.modal,
      filters: {},
      format:{
        status: (v) => v.toUpperCase()
      },
      result_link: result_link
    });

    dt.search(null, true);
  });

  function result_link(toid){
    window.location.href = `/transfer_in?action=create&id=${toid}`;
  };

  $('.overlay', user_profile).click(function(){
    user_profile.hide();
  });

});