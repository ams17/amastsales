$(()=>{

    getlist();

    const btnCreateScreen   = $('#btnCreateScreen');
    const btnSave           = $('.save');
    const btnUpdate         = $('.update');
    const btnCancel         = $('.cancel');
    const listDiv           = $('.list');
    const createDiv         = $('.create');
    const formLabel         = $('#formLabel');

    //Input Fields
    const rowIdField            = $('#rowId');
    const skuIdField            = $('#skuId');
    const categoryField         = $('#category');
    const skuNameField          = $('#skuName');
    const subCategoryField      = $('#subCategory');
    const activeField           = $('#active');
    const baseUomField          = $('#baseUom');

    // btnCreateScreen.click(function(){
    //     listDiv.hide();
    //     createDiv.show();
    //     formLabel.text('Main Information');
    //     btnUpdate.hide();
    //     btnSave.show();

    //     idField.val('');
    //     nameField.val('');
    //     categoryField.val('');
    // });

    btnCreateScreen.click(function(event){
        event.preventDefault();
        let loadingDialog = new Dialog('Loading... ');
        loadingDialog.overlay.off();    

        $.get('/a/sku/getDetails').done(function(r){
            skuIdField.val('');
            categoryField.val('');
            skuNameField.val('');
            subCategoryField.val('');
            activeField.val('');
            baseUomField.val('');
            // console.log(r);
            listDiv.hide();
            createDiv.show();
            formLabel.text('Main Information');
            btnUpdate.hide();
            btnSave.show();

            let data        = r.data;
            let categoryRows    = data.categoryRows;
            let categoryCount   = data.categoryCount;

            let subCategoryRows   = data.subCategoryRows;
            let subCategoryCount  = data.subCategoryCount;

            let baseUomRows    = data.baseUomRows;
            let baseUomCount   = data.baseUomCount;

            var categoryOptions = [];
            for (var i = 0; i < categoryCount; i++) {
                if(categoryRows[i].is_default === true){
                    categoryOptions.push('<option value="',
                      categoryRows[i].id,'" selected="selected">',
                      categoryRows[i].display, '</option>');
                }else{
                    categoryOptions.push('<option value="',
                      categoryRows[i].id, '">',
                      categoryRows[i].display, '</option>');
                }
            }
            $("#category").html(categoryOptions.join('')).select2({
                width: '54%',
            });

            var subCategoryOptions = [];
            for (var i = 0; i < subCategoryCount; i++) {
                if(subCategoryRows[i].is_default === true){
                    subCategoryOptions.push('<option value="',
                      subCategoryRows[i].id,'" selected="selected">',
                      subCategoryRows[i].display, '</option>');
                }else{
                    subCategoryOptions.push('<option value="',
                      subCategoryRows[i].id, '">',
                      subCategoryRows[i].display, '</option>');
                }
            }
            $("#subCategory").html(subCategoryOptions.join('')).select2({
                width: '54%',
            });

            var baseUomOptions = [];
            for (var i = 0; i < baseUomCount; i++) {
                if(baseUomRows[i].is_default === true){
                    baseUomOptions.push('<option value="',
                      baseUomRows[i].id, '" selected="selected">',
                      baseUomRows[i].display, '</option>');
                }else{
                    baseUomOptions.push('<option value="',
                      baseUomRows[i].id, '">',
                      baseUomRows[i].display, '</option>');
                }
            }
            $("#baseUom").html(baseUomOptions.join('')).select2({
                width: '54%',
            });

        }).fail(function(e){
        console.error(e);
            errMsg = JSON.parse(e.responseText).errMsg;
            new Dialog(errMsg || 'An error occured.').addCancelButton().text('OK');
        }).always(function(){
            loadingDialog.remove();
            btnSave.prop('disabled', false);
        });
    });

    btnCancel.click(function(){
        createDiv.hide();
        listDiv.show();
    });
    $('#from').on('dblclick','option', function(){
        let toOptions =  [];
        $(this).off('click');
        $(this).off('dblclick');
        toOptions.push('<option value="',
                  $(this).val(),'" selected="selected">',
                  $(this).html(), '</option>');
        $("#to").append(toOptions.join(''));
        $(this).remove();
    });


    $('#to').on('dblclick','option', function(){
        let fromOptions =  [];
        $(this).off('click');
        $(this).off('dblclick');
        fromOptions.push('<option value="',
                  $(this).val(),'">',
                  $(this).html(), '</option>');
        $("#from").append(fromOptions.join(''));
        $(this).remove();
    });

    $('#moveRight').on('click', function(){
        var options = $("#from option:selected");
        let toOptions =  [];
        var values = $.map(options ,function(option) {
            toOptions.push('<option value="',
                  option.value,'" selected="selected">',
                  option.text, '</option>');
        });
        $("#to").append(toOptions.join(''));
        options.remove();
    });

    $('#moveleft').on('click', function(){
        var options = $("#to option:selected");
        let fromOptions =  [];
        var values = $.map(options ,function(option) {
            fromOptions.push('<option value="',
                  option.value,'">',
                  option.text, '</option>');
        });
        $("#from").append(fromOptions.join(''));
        options.remove();
    });

  btnSave.click(function(){

      // Input fields
      let id        = idField.val();
      let name      = nameField.val();
      let category  = categoryField.val();

      if(!id) return new Dialog('Please enter skus id').addCancelButton().text('OK');
      if(!name) return new Dialog('Please enter skus name').addCancelButton().text('OK');

      btnSave.prop('disabled', true);
      // vanName.prop('disabled', true);
      let loadingDialog = new Dialog('Saving... ');
      loadingDialog.overlay.off();
      var postData = {
        id,
        name,
        category
      };
      // console.log(postData); return false;
      $.post('/a/sku/create', postData).done(function(r){
        let dialog = new Dialog('SKUs saved successfully')
        dialog.addButton('primary', 'OK').click(function(){
          dialog.remove();
          btnCancel.trigger('click');
          location.reload();
        });
        // getlist();
      }).fail(function(e){
        console.error(e);
        errMsg = JSON.parse(e.responseText).errMsg;
        new Dialog(errMsg || 'An error occured.').addCancelButton().text('OK');
      }).always(function(){
        loadingDialog.remove();
        btnSave.prop('disabled', false);
      });
  });

  btnUpdate.click(function(){

      // Input fields
      let id        = idField.val();
      let name      = nameField.val();
      let category  = categoryField.val();

      if(!id) return new Dialog('Please enter skus id').addCancelButton().text('OK');
      if(!name) return new Dialog('Please enter skus name').addCancelButton().text('OK');

      btnUpdate.prop('disabled', true);
      // vanName.prop('disabled', true);
      let loadingDialog = new Dialog('Updating... ');
      loadingDialog.overlay.off();
      var postData = {
        id,
        name,
        category
      };
      // console.log(postData); return false;
      $.post('/a/sku/update', postData).done(function(r){
        let dialog = new Dialog('SKUs updated successfully')
        dialog.addButton('primary', 'OK').click(function(){
          dialog.remove();
          btnCancel.trigger('click');
          location.reload();
        });
        // getlist();
      }).fail(function(e){
        console.error(e);
        errMsg = JSON.parse(e.responseText).errMsg;
        new Dialog(errMsg || 'An error occured.').addCancelButton().text('OK');
      }).always(function(){
        loadingDialog.remove();
        btnSave.prop('disabled', false);
      });
  });


});


  function getlist(){
    let dt = new DataTableCURD({
      api: '/a/sku/list',
      columns: ['id', 'name', 'category'],
      headerColumns: ['id', 'name', 'category'],
      data: 'skus',
      edit: true,
      delete:false
    });

    dt.search(getJsonQueries());
    window.onpopstate = function(event) {
      dt.search(getJsonQueries(), true);
    }
  }

  function editRow(doc_id){
    docId = doc_id;

    $.get(`/a/sku/o/${docId}`).done(function(d){
      console.log(d);
      if(!d || !d.sku) {
        return alert('SKUs ID not found.');
      }
      
      let { sku } = d;
      const listDiv           = $('.list');
      const createDiv         = $('.create');
      const formLabel         = $('#formLabel');
      const btnUpdate         = $('.update');
      const btnCancel         = $('.cancel');
      const btnSave           = $('.save');
      //Input Fields

      const idField         = $('#id');
      const nameField       = $('#name');
      const categoryField   = $('#category');

      listDiv.hide();
      createDiv.show();
      formLabel.text('Edit SKUs');

      btnUpdate.show();
      btnSave.hide();

      idField.val(sku.id).prop('readonly',true);
      nameField.val(sku.name);
      categoryField.val(sku.category);

    }).fail(function(x,s,e){
      error(x,s,e);
      alert('SKUs ID not found.')
    });
  }
  function deleteRow(doc_id){
    docId = doc_id;

    $.post(`/a/sku/delete`,{id:docId}).done(function(d){
      console.log(d);
      // if(!d || !d.site) {
      //   return alert('Site ID not found.');
      // }
      let dialog = new Dialog(d.msg)
      dialog.addButton('primary', 'OK').click(function(){
        dialog.remove();
        location.reload();
      });
    }).fail(function(x,s,e){
      error(x,s,e);
      alert('SKUs ID not found.')
    });
  }
