$(()=>{
  
  drawSummary();
  drawYearlySalesAndReturns();
  drawRecentTransactionTable();
  drawBestSellingProductChartAndTable();

  function drawSummary() {
    $.get(`a/dashboard/summary/list`).done(function(d){
      let data = d.data[0];
      $('#sales p.value').text(`RM ${data.total_sales}`);
      $('#returns p.value').text(`RM ${data.total_returns}`);
      $('#collections p.value').text(`RM ${data.total_collections}`);
    });
  }

  function drawYearlySalesAndReturns() {

    $.get(`a/dashboard/yearlySalesAndReturns/list`).done(function(d){

      const labels = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
      const data = {
        labels: labels,
        datasets: [{
          label: 'Sales',
          barThickness: 8,
          backgroundColor: 'rgba(35, 100, 170, 1)',
          borderColor: 'rgba(35, 100, 170)',
          borderWidth: 1
        },
        {
          label: 'Returns',
          barThickness: 8,
          backgroundColor: 'rgba(247, 37, 133, 1)',
          borderColor: 'rgba(247, 37, 133)',
          borderWidth: 1
        }]
      };
      
      let sales = [];
      for (label of labels) {
        let flag = false;
        for (record of d.data.yearlySales) {
          if (label == record.month) {
            sales.push(record.total_transaction_count);
            flag = true;
            break;
          }
        }

        if (flag == false) {
          sales.push(0);
        }
      }
      data.datasets[0].data = sales;

      let returns = [];
      for (label of labels) {
        let flag = false;
        for (record of d.data.yearlyReturns) {
          if (label == record.month) {
            returns.push(record.total_transaction_count);
            flag = true;
            break;
          }
        }

        if (flag == false) {
          returns.push(0);
        }
      }
      data.datasets[1].data = returns;

      const config = {
        type: 'bar',
        data: data,
        options: {
          plugins: {
            title: {
              display: true
            }
          },
          scales: {
            y: {
              beginAtZero: true,
              stacked: false
            }
          },
           maintainAspectRatio: false
        },
      };
      var salesAndReturnChart = new Chart(
        document.getElementById('salesAndReturnChart'),
        config
      );
    });

    
  }

  function drawRecentTransactionTable() {
    recentTransactionTable = new Table({
      parent: $('.recentTransaction'),
      id: 'recentTransactionTable',
      columns: [ 
        ['date', 'Date'],
        ['reference', 'Reference'],
        ['customer', 'Customer'],
        ['status', 'Status'],
        ['grand_total', 'Grand Total']
      ],
      format:{
        status: (v) => (v).toUpperCase(),
        grand_total: (v) => 'RM ' + Decimal(v).toFixed(2)
      },
      selectable: false,
      rowId: 'id',
    });

    $.get(`a/dashboard/recent_transaction/list`).done(function(d){
      recentTransactionTable.list(d.data);
    });
  }

  function drawBestSellingProductChartAndTable() {
    $.get(`a/dashboard/best_selling_product/list`).done(function(d){

      let skus = []
      let percentage = [];

      for (anItem of d.data) {
        skus.push(anItem.sku_id);
        percentage.push(Decimal(anItem.percentage * 100).toFixed(2));
      }

      let bestSellingProductData = {
        labels: skus,
        datasets: [{
          label: 'Best Selling Product Dataset',
          data: percentage,
          backgroundColor: [
            'rgb(247, 37, 133)',
            'rgb(114, 9, 183)',
            'rgb(58, 12, 163)',
            'rgb(67, 97, 238)',
            'rgb(76, 201, 240)'
          ],
          hoverOffset: 4
        }]
      };

      const bestSellingProductConfig = {
        type: 'doughnut',
        data: bestSellingProductData,
        options: {
          plugins: {
            title: {
              display: true
            },
            legend: {
              display: false
            }
          },
          responsive: true, 
          maintainAspectRatio: false
        },
      };
    
      var bestSellingProductChart = new Chart(
        $('#bestSellingProductChart'),
        bestSellingProductConfig
      );


      bestSellingProductTable = new Table({
        parent: $('.bestSellingProduct'),
        id: 'bestSellingProductTable',
        columns: [ 
          ['diamond', ''],
          ['sku_id', 'Product'],
          ['percentage', 'Percentage']
        ],
        format:{
          percentage: (v) => Decimal(v * 100).toFixed(2) + '%'
        },
        selectable: false,
        rowId: 'sku_id',
      });

      bestSellingProductTable.list(d.data);
    });
  }

  
});