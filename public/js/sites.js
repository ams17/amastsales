$(()=>{
  const btnCreateScreen   = $('#btnCreateScreen');
  const btnSave           = $('.save');
  const btnUpdate         = $('.update');
  const btnCancel         = $('.cancel');
  const listDiv           = $('.list');
  const createDiv         = $('.create');
  const formLabel         = $('#formLabel');

  //Input Fields

  const idField        = $('#id');
  const nameField      = $('#name');
  const addressField   = $('#address');
  const postcodeField  = $('#postcode');
  const cityField      = $('#city');
  const stateField     = $('#state');
  const statusField    = $('#status');
  const logoField      = $('#image');

  const allowedExtension = ['png'];

  logoField.click(function(e) {
    $("#imageUpload").click();
  });

  function fasterPreview( uploader ) {
    if ( uploader.files && uploader.files[0] ){
      let fileName = uploader.files[0].name;
      let isValidFile = validateFile(fileName);

      if (isValidFile)
      {
        var reader = new FileReader();
         reader.readAsDataURL(uploader.files[0]);
         reader.onload = function () {
           logoField.attr('src', reader.result );
           logoField.attr('isDefault', false);
         };
         reader.onerror = function (error) {
           console.log('Error: ', error);
         };
      } else {
        alert('Error! Allowed Extensions are : *.' + allowedExtension.join(', *.'));
      }
    }
  }

  function validateFile(fileName) 
  {
    var fileExtension = fileName.split('.').pop().toLowerCase();
    var isValidFile = false;

    for(var index in allowedExtension) {
      if(fileExtension === allowedExtension[index]) {
        isValidFile = true; 
        break;
      }
    }
    return isValidFile;
  }

  $("#imageUpload").change(function(){
    fasterPreview( this );
  });

  let statusDropDown = new DropDown({
    url: '/a/lookup/lov/list',
    id: '#status',
    group_id: 'true_false_status'
  });

  btnCreateScreen.click(function(){
    $.get('/a/site/getDetails').done(function(data){
      listDiv.hide();
      createDiv.show();
      formLabel.text('Add Site');
      btnUpdate.hide();
      btnSave.show();
      let states = data.states;
      var stateOptions = [];
      Object.keys(states).forEach(key => {
         stateOptions.push('<option value="'+
            states[key]+'">'+
            states[key]+ '</option>');
      });
      stateField.html(stateOptions.join('')).select2({
         width: '54%',
      });

      idField.val('').prop('readonly',false);
      nameField.val('');
      addressField.val('');
      postcodeField.val('');
      cityField.val('').select2({
         width: '54%',
      });;
      // stateField.val('');
      statusDropDown.setDefault();
      logoField.attr('src', '/img/no_image_available.svg');
      logoField.attr('isDefault', true);
    });
  });

  btnCancel.click(function(){
    createDiv.hide();
    listDiv.show();
  });

  btnSave.click(function(){

      // Input fields
      let id          = idField.val();
      let name        = nameField.val();
      let address     = addressField.val();
      let postcode    = postcodeField.val();
      let city        = cityField.val();
      let state       = stateField.val();
      let status      = statusField.val();
      let isDefault   = logoField.attr('isDefault');
      let logo        = logoField.attr('src');

      if(!id) return new Dialog('Please enter site id').addCancelButton().text('OK');
      if(!name) return new Dialog('Please enter site name').addCancelButton().text('OK');

      btnSave.prop('disabled', true);
      // vanName.prop('disabled', true);
      let loadingDialog = new Dialog('Saving... ');
      loadingDialog.overlay.off();
      var postData = {
        id,
        name,
        address,
        postcode,
        city,
        state,
        status,
        logo,
        isDefault
      };
      
      // console.log(postData); return false;
      $.post('/a/site/create', postData).done(function(r){
        let dialog = new Dialog('site saved successfully')
        dialog.addButton('primary', 'OK').click(function(){
          dialog.remove();
          btnCancel.trigger('click');
          location.reload();
        });
        // getlist();
      }).fail(function(e){
        console.error(e);
        errMsg = JSON.parse(e.responseText).errMsg;
        new Dialog(errMsg || 'An error occured.').addCancelButton().text('OK');
      }).always(function(){
        loadingDialog.remove();
        btnSave.prop('disabled', false);
      });
  });

  btnUpdate.click(function(){

      // Input fields
      let id          = idField.val();
      let name        = nameField.val();
      let address     = addressField.val();
      let postcode    = postcodeField.val();
      let city        = cityField.val();
      let state       = stateField.val();
      let status      = statusField.val();
      let isDefault   = logoField.attr('isDefault');
      let logo        = logoField.attr('src');

      if(!id) return new Dialog('Please enter site id').addCancelButton().text('OK');
      if(!name) return new Dialog('Please enter site name').addCancelButton().text('OK');

      btnUpdate.prop('disabled', true);
      // vanName.prop('disabled', true);
      let loadingDialog = new Dialog('Updating... ');
      loadingDialog.overlay.off();

      var postData = {
        id,
        name,
        address,
        postcode,
        city,
        state,
        status,
        logo,
        isDefault
      };
      // console.log(postData); return false;
      $.post('/a/site/update', postData).done(function(r){
        let dialog = new Dialog('Site updated successfully')
        dialog.addButton('primary', 'OK').click(function(){
          dialog.remove();
          btnCancel.trigger('click');
          location.reload();
        });
        // getlist();
      }).fail(function(e){
        console.error(e);
        errMsg = JSON.parse(e.responseText).errMsg;
        new Dialog(errMsg || 'An error occured.').addCancelButton().text('OK');
      }).always(function(){
        loadingDialog.remove();
        btnSave.prop('disabled', false);
      });
  });

  // $("#state").on("select2:select", function (e) { 
  //   var select_val = $(e.currentTarget).val();
  //   console.log(select_val)
  // });

  $('#state').on('change',function(){

     var state = $('#state').val();
     if(state != ""){
        $.get(`/a/site/getCities/`+state).done(function(data){
            let cities = data.cities;
            var cityOptions = [];
            Object.keys(cities).forEach(key => {
               cityOptions.push('<option value="'+
                  cities[key]+'">'+
                  cities[key]+ '</option>');
            });
            cityField.html(cityOptions.join('')).select2({
               width: '54%',
            });
        }).fail(function(x,s,e){
          error(x,s,e);
          alert('State not found.')
        });
     }
     
  });


  // function getlist(){
  //   let dt = new DataTable({
  //     api: '/a/site/list',
  //     // columns: ['id', 'name', 'address', 'postcode', 'city', 'state', 'created_by'],
  //     columns: [
  //       {
  //         "name" : "id",
  //       },
  //       {
  //         "name" :"name",
  //       },
  //       {
  //         "name" : "address",
  //       },
  //       { 
  //         "name" : "postcode",
  //       },
  //       { 
  //          "name" : "city",
  //       },
  //       { 
  //         "name" : "state",
  //       },
  //       {
  //         "name" : "created_by",
  //       }
  //     ],
  //     data: 'sites',
  //     result_link:result_link
  //   });

  //   dt.search(getJsonQueries());
  //   window.onpopstate = function(event) {
  //     dt.search(getJsonQueries(), true);
  //   }
  // }

  let result_link = function(doc_id){
    docId = doc_id;

    $.get(`/a/site/o/${docId}`).done(function(d){
      console.log(d);
      if(!d || !d.site) {
        return alert('Site ID not found.');
      }
      
      let { site } = d;
      listDiv.hide();
      createDiv.show();
      formLabel.text('Edit Site');

      btnUpdate.show();
      btnSave.hide();

      idField.val(site.id).prop('readonly',true);
      nameField.val(site.name);
      addressField.val(site.address);
      postcodeField.val(site.postcode);
      cityField.val(site.city);
      stateField.val(site.state);
      statusField.val(site.status);

    }).fail(function(x,s,e){
      error(x,s,e);
      alert('Site ID not found.')
    });
  }

  getlist();
});

  function getlist(){
    let dt = new DataTableCURD({
      api: '/a/site/list',
      columns: ['id', 'name', 'address', 'postcode', 'city', 'state', 'created_by'],
      headerColumns: ['id', 'name', 'address', 'postcode', 'city', 'state', 'created_by'],
      data: 'sites',
      edit: true,
      delete:true
      // act:true
    });

    dt.search(getJsonQueries());
    window.onpopstate = function(event) {
      dt.search(getJsonQueries(), true);
    }
  }

  function editRow(doc_id){
    docId = doc_id;

    $.get(`/a/site/o/${docId}`).done(function(d){
      console.log(d);
      if(!d || !d.site) {
        return alert('Site ID not found.');
      }
      
      let { site } = d;
      const listDiv           = $('.list');
      const createDiv         = $('.create');
      const formLabel         = $('#formLabel');
      const btnUpdate         = $('.update');
      const btnCancel         = $('.cancel');
      const btnSave           = $('.save');
      //Input Fields

      const idField        = $('#id');
      const nameField      = $('#name');
      const addressField   = $('#address');
      const postcodeField  = $('#postcode');
      const cityField      = $('#city');
      const stateField     = $('#state');
      const logoField      = $('#image');

      let statusDropDown = new DropDown({
        url: '/a/lookup/lov/list',
        id: '#status',
        group_id: 'true_false_status'
      });

      listDiv.hide();
      createDiv.show();
      formLabel.text('Edit Site');

      btnUpdate.show();
      btnSave.hide();

      idField.val(site.id).prop('readonly',true);
      nameField.val(site.name);
      addressField.val(site.address);
      postcodeField.val(site.postcode);


      let states = d.states;
      var stateOptions = [];
      Object.keys(states).forEach(key => {

        if(states[key] == site.state){
         stateOptions.push('<option value="'+
            states[key]+'" selected="selected">'+
            states[key]+ '</option>');
        }else{
         stateOptions.push('<option value="'+
            states[key]+'">'+
            states[key]+ '</option>');

        }
      });
      stateField.html(stateOptions.join('')).select2({
         width: '54%',
      });

      let cities = d.cities;
        var cityOptions = [];
        Object.keys(cities).forEach(key => {
          if(cities[key] == site.city){
           cityOptions.push('<option value="'+
              cities[key]+'" selected="selected">'+
              cities[key]+ '</option>');
          }else{            
           cityOptions.push('<option value="'+
              cities[key]+'">'+
              cities[key]+ '</option>');
          }
        });
        cityField.html(cityOptions.join('')).select2({
           width: '54%',
        });

      statusDropDown.setValue(site.active);
      if (site.logo != null){
        logoField.attr('src', site.logo);
        logoField.attr('isDefault', false);
      } else {
        logoField.attr('src', '/img/no_image_available.svg');
        logoField.attr('isDefault', true);
      }

    }).fail(function(x,s,e){
      error(x,s,e);
      alert('Site ID not found.')
    });
  }

  function deleteRow(doc_id){
    docId = doc_id;

    let dialog = new Dialog('Please confirm deletion')
    dialog.addCancelButton().text('Cancel')
    dialog.addButton('primary', 'OK').click(function(){
      dialog.remove();
      // btnCancel.trigger('click');
      // location.reload();

      $.post(`/a/site/delete`,{id:docId}).done(function(d){
        console.log(d);
        // if(!d || !d.site) {
        //   return alert('Site ID not found.');
        // }
        let dialog = new Dialog(d.msg)
        dialog.addButton('primary', 'OK').click(function(){
          dialog.remove();
          location.reload();
        });
      }).fail(function(x,s,e){
        error(x,s,e);
        alert('Site ID not found.')
      });
    });
  }

  $(document).ready(function () {
    $('th').each(function (col) {
        $(this).hover(
                function () {
                    $(this).addClass('focus');
                },
                function () {
                    $(this).removeClass('focus');
                }
        );
        $(this).click(function () {
            $(this).removeClass('sort-by');
            if ($(this).is('.asc')) {
                $(this).removeClass('asc');
                $(this).addClass('desc selected');
                sortOrder = -1;
            } else {
                $(this).addClass('asc selected');
                $(this).removeClass('desc');
                sortOrder = 1;
            }
            $(this).siblings().removeClass('asc selected');
            $(this).siblings().removeClass('desc selected');
            $(this).siblings('th:not(.action)').addClass('sort-by');
            var arrData = $('table').find('tbody >tr:has(td)').get();
            arrData.sort(function (a, b) {
                var val1 = $(a).children('td').eq(col).text().toUpperCase();
                var val2 = $(b).children('td').eq(col).text().toUpperCase();
                if ($.isNumeric(val1) && $.isNumeric(val2))
                    return sortOrder == 1 ? val1 - val2 : val2 - val1;
                else
                    return (val1 < val2) ? -sortOrder : (val1 > val2) ? sortOrder : 0;
            });
            $.each(arrData, function (index, row) {
                $('tbody').append(row);
            });
        });
    });
  });








