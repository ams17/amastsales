$(()=>{

  let dt = new DataTable({
    api: '/a/outlet/list',
    columns: ['id', 'name', 'price_group_id','active'],
    filters: {
      price_group: {name: 'Price Group', type: 'option', options:['All']},
      active: {name: 'Active', type: 'option',options:['All','Active','Inactive']}
    },
    data: 'outlets',
    result_link: (id) => {
      console.log(`/outlet/${id}`);
    }
  });

  dt.search(getJsonQueries());
  window.onpopstate = function(event) {
    dt.search(getJsonQueries(), true);
  }

  const btnCreateScreen   = $('#btnCreateScreen');
  const btnSave           = $('.save');
  const btnUpdate         = $('.update');
  const btnCancel         = $('.cancel');
  const listDiv           = $('.list');
  const createDiv         = $('.create');
  const formLabel         = $('#formLabel');
  
});
