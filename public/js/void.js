$(()=>{
  let sites = [];
  let xhr;
  let docId;

  let listScreen = $('.content section');
  let viewScreen = new Screen({
    id: 'viewScreen',
    h1: 'Void Invoice Details', 
    prevScreen: listScreen,
    hasFooter: true
  });

  docHeader = new PivotTable({
    parent: viewScreen.content,
    columns: [
      ['id', 'Void Invoice ID'],
      ['invoice_id', 'Voiding'],
      ['createdAt', 'Created At'],
      ['outlet_id', 'Outlet ID'],
      ['van_id', 'Van ID'],
      ['status', 'Status']
    ],
    format:{
      createdAt: (v) => { return (v)? dayjs(v).format(ntf) : '-'} 
    }
  });

  docDetails = new Table({
      parent: viewScreen.content,
      id: 'doc_details',
      title: 'Void Invoice',
      columns: [ 
        ['condition', 'Description'],
        ['sku_id', 'SKU'],
        ['uom_id', 'UOM'],
        'price',
        'quantity',
        'tax',
        ['line_total', 'Subtotal']
      ],
      format:{
        condition: (v) => 'SALES',
        price: (v) => Decimal(v).toFixed(2),
        tax: (v) => Decimal(v).toFixed(2),
        line_total: (v) => Decimal(v).toFixed(2),
        createdAt: (v) => { return (v)? dayjs(v).format(ntf) : '-'} 
      },
      selectable: false,
      rowId: 'id',
    });
  
  docDetails.tfoot = $(`<tfoot></tfoot>`).appendTo(docDetails.table);
  docDetails.clear = function(){
    this.tbody.empty();
    this.tfoot.empty();
    return this;
  }

  let result_link = function(doc_id){
    docId = doc_id;
    viewScreen.title.text(`${viewScreen.h1} - ${docId}`);
    viewScreen.show();
    docHeader.clear();
    docDetails.clear();

    if(xhr) xhr.abort();
    xhr = $.get(`a/finance/void/o/${docId}`).done(function(d){
      if(!d || !d.void_invoice) {
        return alert('Void Invoice ID not found.');
      }
      
      let {void_invoice, details} = d;
      console.log({void_invoice, details});
      docHeader.update(void_invoice);
      docDetails.list(details);
      let cs = 6;
      
      docDetails.tfoot.append(`<tr><td colspan='${cs}'>Sub Total</td><td class='subtotal'>${void_invoice.subtotal}</td></tr>`);
      docDetails.tfoot.append(`<tr><td colspan='${cs}'>Tax</td><td class='tax'>${void_invoice.tax}</td></tr>`);
      if(void_invoice.discount && parseFloat(void_invoice.discount) > 0){
        docDetails.tfoot.append(`<tr><td colspan='${cs}'>Discount</td><td class='discount'>${void_invoice.discount}</td></tr>`);  
      }
      docDetails.tfoot.append(`<tr><td colspan='${cs}'>Rounding</td><td class='rounding'>${void_invoice.cent_rounding}</td></tr>`);
      docDetails.tfoot.append(`<tr><td colspan='${cs}'>Grand Total</td class='grand_total'><td>${void_invoice.grand_total}</td></tr>`);

    }).fail(function(x,s,e){
      error(x,s,e);
      alert('Void Invoice ID not found.')
    });
  }

  function drawDataTable(){
    let dt = new DataTable({
      api: '/a/finance/void/list',
      columns: ['id', 'date', 'outlet_id', 'van_id', 'grand_total', ['invoice_id', 'Voiding']],
      filters: {
        searchby: {name: 'Search by', type: 'option', options: [['id','Void Invoice ID'], ['outlet_id', 'Outlet ID'], ['van_id','Van ID']]},
        date: {name: 'Date', type: 'date', dateOptions: [
            ['anytime', 'Any time'],
            ['today', 'Today'],
            ['weektodate', 'Week-to-date'],
            ['monthtodate', 'Month-to-date'],
            ['yeartodate', 'Year-to-date'],
            ['customdate', 'Custom date'],
            ['customrange', 'Custom range']
          ]}        
      },
      data: 'void_invoices',
      result_link: result_link
    });

    dt.search(getJsonQueries());
    window.onpopstate = function(event) {
      dt.search(getJsonQueries(), true);
    }
  }

  $.get('/a/site/list').done(function(data){    
    if(data.sites && data.sites.rows){
      sites = data.sites.rows.map(e => e.id);
    }
    drawDataTable();
  });

  let btnPrint = $(`<button type='button' id='printer' class='primary' class='btnPrint'>Print</button>`).appendTo(viewScreen.navRight);
  btnPrint.click(function(){
    let api = `/a/report/void_invoice?report_id=${docId}`;

    printJS({
      printable:  api,
      type: 'pdf',
          onError: function  (error) {
            alert('Print PDF failed');
          }
      })
  });
})