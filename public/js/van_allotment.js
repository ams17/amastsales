$(()=>{
  let selectedSite;
  let selectedDate;
  let selectedVans;
  let createSkus = [];
  let sites = [];
  const today = dayjs().format(df);
  let xhr;

  let dt;
  let divAct = $('article .act');

  let listScreen = $('.content section');
  let createScreen = new Screen({
    id: 'createScreen',
    h1: 'Create Allotment', 
    prevScreen: listScreen,
    hasFooter: true
  });
  let viewScreen;
  let viewDetailRows;
  let viewDetailSkus;

  function createScreenContent(createScreen){
    let fSite = new Field({ parent: createScreen.content, id: 'fSite', title: 'Select a site' });
    siteCards = new Cards({
      parent: fSite.label,
      id: 'siteCards'
    });
    $.get('/a/site/list/min').done(function(data){
      siteCards.list(data.sites);
    }).fail(function(x,s,e){
      error(x,s,e);
    });

    let fdate = $(`<div id='fdate' class='field'>
        <label>
          <p>Select allotment date</p>
          <input id='inDate' type='date' min='${today}'/>
        </label>
      </div>`).appendTo(createScreen.content);
    let inDate = $('#inDate', fdate);
    createScreen.fdate = fdate;
    createScreen.inDate = inDate;

    siteCards.cards.on('selected', function(){
      fdate.show();
    });

    siteCards.cards.on('deselected', function(){
      fdate.hide();
      inDate.val('');
      fvans.hide();
    })

    let fvans = $(`<div id='fvans' class='field'>
      <p class='label'>Select vans</p>
    </div>`);
    fvans.appendTo(createScreen.content);
    createScreen.fvans = fvans;

    let vanCards = new VanCards({
      parent: fvans,
	  multiselect: true,
      id: 'vanCards',
      statusIcons: {
        'allotted': '/img/allotted.svg',
        'default': '/img/allotted_not.svg'
      }
    });
    
    fdate.hide();
    fvans.hide();
    createScreen.vanCards = vanCards;

    inDate.on('change', function(){
      fqty.hide();
      createScreen.footer.hide();

      selectedSite = $('.card.selected', siteCards.cards).eq(0).data('id');
      selectedDate = inDate.val();
      if(!dayjs(selectedDate, df).isValid()){
        return alert('Please enter a valid date');
      }

      if(xhr) xhr.abort();
      xhr = $.get(`a/van/allot/list/vanStatus/${selectedSite}/${selectedDate}`).done(function(d){
        if(!d || !d.vans){
          return alert('Failed to get Van Allotment status;');
        }
        vanCards.list(d.vans);
        fvans.show();
        vanCards.btns.show();
      }).fail(function(x,s,e){
        error(x,s,e);
        return alert('Failed to get Van Allotment status;');
      });
    });

    vanCards.btnNext.click(function(e){
      e.stopPropagation();
      selectedVans = vanCards.getSelected();
      if(!selectedVans){
        return alert('Please select at least 1 van.');
      }
      if(selectedVans.length == 0){
        return alert('Please select at least 1 van.');
      }
      if(selectedVans.length == 1){
        $('.card:not(.selected)', vanCards.cards).addClass('hide');
        vanCards.btns.hide();

        // Prepare sku qty table
        skuTable.clear();
        if(xhr) xhr.abort();

        let api = 'a/van/allot/create/skulist';
        if(selectedVans[0].status == 'unallotted') api += `/planned/${selectedDate}/${selectedVans[0].van_id}`;
        
        xhr = $.get(api).done(function(d){
          if(!d || !d.skus) {
            return alert('Failed to get SKU list for planned allotment');
          }
          createSkus = d.skus;
          skuTable.list(d.skus);
          fqty.show();
          createScreen.footer.show();
        }).fail(function(x,s,e){
          error(x,s,e);
          return alert('Failed to get SKU list for planned allotment');
        });
      } else {
        // multiple vans, bulk create allotment.
        let unallottedVans = selectedVans.filter(v => v.status == 'unallotted').map(v => v.van_id);
        if(unallottedVans.length != selectedVans.length) return alert('Bulk allotment is only meant for vans without allotment for the selected date.\nThe bulk allotment follows the allotment plan.');
        let yesBulkCreate = confirm(`Bulk allotment follows the allotment plan.\nConfirm bulk allotment for these vans:\n${unallottedVans.join(', ')}.`);
        if(!yesBulkCreate) return;

        if(xhr) xhr.abort();
        xhr = $.ajax({
          type: 'POST',
          url: '/a/van/allot/bulkCreate', 
          contentType: "application/json;charset=utf-8",
          data: JSON.stringify({vans: unallottedVans, date: selectedDate})
        }).done(function(data){
          let unplanned_vans = data.unplanned_vans;
          let msg = '';

          let isBulkCreated = unallottedVans.length !== unplanned_vans.length;

          if(isBulkCreated){
            msg = 'Bulk allotment created.\n';
          }

          if(unplanned_vans && unplanned_vans.length > 0){
            msg += `Bulk allotment not created for these vans because planned quantity is zero: ${unplanned_vans.join(', ')}.`;
          }

          alert(msg);

          if(!isBulkCreated) return;
          
          createScreen.back();
          dt.search({searchby: 'allotId', keyword: data.allots.join(', ')}, true);
          // reset createScreen
          createScreen.footer.hide();
          createScreen.inDate.val('');
          createScreen.fvans.hide();
          createScreen.fqty.hide();

        }).fail(function(x,s,e){
          error(x,s,e);
        }).always(function(){
          $('body .content .inprogress').remove();
          btnCreate.prop('disabled', false);
        });

      }

      vanCards.content.click(function(){
        $('.card:not(.selected)', vanCards.cards).removeClass('hide');
        vanCards.btns.show();
        fqty.hide();
        createScreen.footer.hide();
        vanCards.content.off();
      })
    });
  }
  createScreenContent(createScreen);

  let fqty = $(`<div id='fqty' class='field'>
    <label>
      <p>Enter allotment quantity</p>
    </label>
  </div>`).appendTo(createScreen.content);
  createScreen.fqty = fqty;
  fqty.hide();

  let skuTable = new SkuTable({
    parent: fqty,
    title: 'Allotment',
    columns: [['sku_id', 'SKU ID'], ['uom', 'UOM']],
    quantityColumns: ['Allotment']
  });

  createScreen.footer.hide();
  let btnCreate = $(`<button class='create'>Create Allotment</button>`).appendTo(createScreen.footer);
  
  btnCreate.click(function(){
    if(selectedVans.length == 0) return alert('Must select at least 1 van.');
    if(selectedVans.length > 1) return alert('Cannot create allotment for more than 1 van.');

    btnCreate.prop('disabled', true);
    $('body .content').append(`<div class='inprogress'><p>Creating...</p></div>`);

    let allot = [];
    for(let d of skuTable.data){
      if(d.quantity[0] > 0){
        allot.push({sku_id: d.sku_id, quantity: d.quantity[0]})  
      }
    }

    let bodyData = {
      allot: allot, 
      date: selectedDate, 
      van_id: selectedVans[0].van_id,
      tag: (selectedVans[0].status == 'allotted')? 'additional': 'planned'
    }

    if(xhr) xhr.abort();
    xhr = $.ajax({
      type: 'POST',
      url: '/a/van/allot/create', 
      contentType: "application/json;charset=utf-8",
      data: JSON.stringify(bodyData)
    }).done(function(data){
      alert('Allotment created.');
      createScreen.back();
      dt.search({searchby: 'allotId', date: selectedDate, keyword: data.allotment.id}, true);

      // reset createScreen
      createScreen.footer.hide();
      createScreen.inDate.val('');
      createScreen.fvans.hide();
      createScreen.fqty.hide();

    }).fail(function(x,s,e){
      error(x,s,e);
    }).always(function(){
      $('body .content .inprogress').remove();
      btnCreate.prop('disabled', false);
    });
  });

  let btnCreateScreen = $('#btnCreateScreen');
  btnCreateScreen.click(function(){
    createScreen.show();
  });

  function createViewScreen(){
    viewScreen = new Screen({
      id: 'viewScreen',
      h1: 'View Allotment', 
      prevScreen: listScreen
    });

    let div1 = $(`<div id='div1'></div>`).appendTo(viewScreen.content);
    let div2 = $(`<div id='div2'></div>`).appendTo(viewScreen.content);
    
    viewDetailRows = new PivotTable({
      parent: div1,
      columns: [
        ['id', 'Allotment ID'], 
        ['van_id', 'Van ID'],
        ['date', 'Date'],
        ['tag', 'Tag'],
        ['delivery_order_id', 'Delivery Order ID']
      ]
    });

    viewEventHistory = new EventHistory({
      parent: div1,
      id: 'allotStatusHistory',
      title: 'Status History',
      format: `DD MMM 'YY, hh:mmA`
    })

    viewDetailSkus = new SkuTable({
      parent: div2,
      columns: [['sku_id', 'SKU ID'], ['uom_id', 'UOM']],
      quantityColumns: ['Requested Qty','Allotted Qty'],
      viewOnly: true
    });
  }
  
  function result_link(allotId){

    if(!viewScreen) createViewScreen();

    viewDetailRows.clear();
    viewDetailSkus.clear();
    viewEventHistory.clear();
    viewScreen.show();

    if(xhr) xhr.abort();
    xhr = $.get(`a/van/allot/o/${allotId}`).done(function(data){
      viewDetailRows.update(data.allotment);
      viewDetailSkus.list(data.details);

      let d = data.allotment;
      let history = [
        {status: 'Cancelled', by: d.cancelled_by, at: d.cancelled_at},
        {status: 'Loaded', by: d.loaded_by, at: d.loaded_at},
        {status: 'Packed', by: d.packed_by, at: d.packed_at},
        {status: 'Sent for picking', by: d.sent_for_picking_by, at: d.sent_for_picking_at},
        {status: 'Added to picklist', by: d.added_to_picklist_by, at: d.added_to_picklists_at},
        {status: 'Alloted', by: d.created_by, at: d.createdAt},
        {status: 'Approved', by: d.approved_by, at: d.approved_at},
        {status: 'Requested', by: d.requested_by, at: d.requested_at}
      ];
      viewEventHistory.list(history);

    }).fail(function(x,s,e){
      error(x,s,e);
    })
  }

  function drawDataTable(){
    dt = new DataTable({
      api: '/a/van/allot/list',
      columns: ['date', 'id', ['van_id', 'van id'], ['delivery_order_id', 'DO'], 'tag', 'status', ['created_at', 'Created At']],
      filters: {
        searchby: {name: 'Search By', type: 'option', options: [['vanId','Van ID'], ['allotId','Allotment ID'], ['do','DO']]},
        site_id: {name: 'Site ID', type: 'option', options:['All', ...sites]},
        date: {name: 'Date', type: 'date', dateOptions: [
            ['today', 'Today'],
            ['anytime', 'Any time'],
            ['weektodate', 'Week-to-date'],
            ['monthtodate', 'Month-to-date'],
            ['yeartodate', 'Year-to-date'],
            ['customdate', 'Custom date'],
            ['customrange', 'Custom range']
          ]},
        tag: {name: 'Tag', type: 'option', options: ['All', 'Planned', 'Additional']},
        status: {name: 'Status', type: 'option', options: ['All', 'Requested', 'Allotted', 'Packed', 'Loaded', 'Cancelled']}
      },
      format:{
        created_at: (v) => { return (v)? dayjs(v).format(df) : '-'} 
      },
      filterAlwaysShow: true,
      data: 'allotments',
      result_link: result_link,
      checkable: true,
      act: divAct
    });

    dt.search(getJsonQueries());
    window.onpopstate = function(event) {
      dt.search(getJsonQueries(), true);
    }

    let btnPicklist = new ActButton(divAct, 'btnPicklist', 'Add to Picklist', '/img/picking.svg');
    btnPicklist.hide();
    btnPicklist.click(function(){
      let checkedIds = dt.getCheckedIds();
      let yesConfirm = confirm('Add these allotments to picklist?\n' + checkedIds.join(', ') + '.');
      if(!yesConfirm) return;

      if(xhr) xhr.abort();
      btnPicklist.prop('disabled', true);
      xhr = $.ajax({
        type: 'POST',
        url: '/a/picklist/add', 
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({allotments: checkedIds})
      }).done(function(data){
        console.log(data);
        alert(`Allotments added to picklist ${data.picklist.id}`);
        dt.search({searchby: 'allotId', keyword: checkedIds.join(', ')});
      }).fail(function(x,s,e){
        error(x,s,e);
        console.error('Failed to add allotments to picklist.');
      }).always(function(){
        btnPicklist.prop('disabled', false);
      });
    });
  }

  $.get('/a/site/list').done(function(data){    
    if(data.sites && data.sites.rows){
      sites = data.sites.rows.map(e => e.id);
    }
    drawDataTable();
  });
})