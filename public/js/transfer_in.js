let receivingSkus, notReceivingSkus;
$(()=>{
  let xhr;
  let dt;
  let selectedDocId;
  let selectedSiteId;
  let selectedReferenceType;

  let listScreen = $('.content section');
  let createScreen;
  let siteCards, inpRemark;

  let viewScreen;
  let viewDetailRows;
  let viewReceivedSkus;
  let viewNonReceivedSkus;
  let btnCreateScreen = $('#btnCreateScreen');
  let btnCreate;
  let btnConfirm;
  let btnPrint;

  let expectedQty;
  let expectedQtyMap = new Map();
  const typeMap = {'ad': 'ad-hoc', 'tr': 'transfer', 'vr': 'van return'};


  function createScreenContent(){ 
    createScreen = new Screen({
      id: 'createScreen',
      h1: 'Create Transfer In', 
      prevScreen: listScreen,
      hasFooter: true
    });
    createScreen.footer.hide();

    let fSite = new Field({ parent: createScreen.content, id: 'fSite', title: 'Select a site' });
    siteCards = new Cards({
      parent: fSite.label,
      id: 'siteCards'
    });
    
    $.get('/a/site/list/min').done(function(data){
      siteCards.list(data.sites);
      if (selectedSiteId != '') {
        $(`div[data-id='${selectedSiteId}']`).click();
      }
    }).fail(function(x,s,e){
      error(x,s,e);
    });

    let fType = new Field({ parent: createScreen.content, id: 'fType', title: 'Select reference type'});
    fType.hide();
    typeCards = new Cards({
      parent: fType.label,
      id: 'typeCards'
    }, [
      {id: 'ad', display: 'Ad-hoc'}, 
      {id: 'tr', display: 'Transfer'}, 
      {id: 'vr', display: 'Van Return'}
    ]);

    let fRef = new Field({ parent: createScreen.content, id: 'fRef', title: 'Select reference ID'});
    fRef.hide();
    refCards = new Cards({
      parent: fRef.label,
      id: 'refCards'
    });

    let fAdRef = new Field({ parent: createScreen.content, id: 'fAdRef', title: 'Enter PO ID'});
    fAdRef.hide();
    inpAdRef = $(`<input id='inpAdRef' type='text'>`).appendTo(fAdRef.label);

    let fRemark = new Field({ parent: createScreen.content, id: 'fRemark', title: 'Enter remark'})
    fRemark.hide();
    inpRemark = $(`<input id='inpRemark' type='text'>`).appendTo(fRemark.label);

    let fQuantity = new Field({ parent: createScreen.content, id: 'fQuantity', title: 'Enter SKU quantity'});
    fQuantity.d1 = $(`<div class='d1'></div>`).appendTo(fQuantity.field);
    fQuantity.hide();
    receivingSkus = new SkuTable({
      parent: fQuantity.d1,
      id: 'receivingSkus',
      title: 'Receiving',
      columns: [['sku_id', 'SKU ID'], ['uom_id', 'UOM']],
      quantityColumns: ['Fresh', 'Damaged', 'Old', 'Recalled']
    });

    notReceivingSkus = new SkuTable({
      parent: fQuantity.d1,
      id: 'notReceivingSkus',
      title: 'Not Receiving',
      columns: [['sku_id', 'SKU ID'], ['uom_id', 'UOM']],
      quantityColumns: ['quantity']
    });

    receivingSkus.skutable.on('inputChange', function(e, skuid, x, y, qty){
      if(!expectedQty) return;
      if(qty < 0){
        qty = 0;
        $(`tr[data-id='${skuid}'] input:eq(${y})`, receivingSkus.skutable).val(qty);
        receivingSkus.data[x].quantity[y] = qty;
      }

      let expected = expectedQtyMap.get(skuid).quantity.reduce((a, v) => a + v);

      if(qty > expected){
        alert('Receiving quantity cannot be more than expected quantity.');
        qty = expected;
        $(`tr[data-id='${skuid}'] input:eq(${y})`, receivingSkus.skutable).val(qty);
        receivingSkus.data[x].quantity[y] = qty;
      }

      if(qty == expected){
        $(`tr[data-id='${skuid}'] input`, notReceivingSkus.skutable).val(0);
        notReceivingSkus.data[x].quantity[0] = 0;
        $(`tr[data-id='${skuid}'] input:not(:eq(${y}))`, receivingSkus.skutable).val(0);
        for(let i = 0; i < 4; i++){
          if(i == y) continue;
          receivingSkus.data[x].quantity[i] = 0;
        }
        return;
      }

      let nr = parseInt($(`tr[data-id='${skuid}'] input`, notReceivingSkus.skutable).val());
      let sum = 0;
      $(`tr[data-id='${skuid}'] input`, receivingSkus.skutable).each(function(){
        sum += parseInt($(this).val());
      });

      console.log({expected, sum, nr});

      if(sum + nr < expected){
        let _d = expected - sum;
        $(`tr[data-id='${skuid}'] input`, notReceivingSkus.skutable).val(_d);
        notReceivingSkus.data[x].quantity[0] = _d;
        return;
      }

      if(sum + nr > expected){
        let _q = sum - (expected - nr);
        console.log({nr, _q})
        if(nr > 0){
          let _v = (nr >= _q)? nr - _q : 0;
          $(`tr[data-id='${skuid}'] input`, notReceivingSkus.skutable).val(_v)
          notReceivingSkus.data[x].quantity[0] = _v;
          _q =- (nr >= _q)? 0 : _q - nr;
        }

        if(_q > 0){
          for(let i = 0; i < 4; i++){
            if(i == y) continue;
            let r = $(`tr[data-id='${skuid}'] input:eq(${i})`, receivingSkus.skutable);
            let val = parseInt(r.val());
            let _v = (val >= _q)? val - _q: 0;
            $(`tr[data-id='${skuid}'] input:eq(${i})`, receivingSkus.skutable).val(_v);
            receivingSkus.data[x].quantity[i] = _v;
            _q = (val >= _q)? 0 : _q - val;

            if(_q == 0) return;
          }
        }
      }
    });

    notReceivingSkus.skutable.on('inputChange', function(e, skuid, x, y, qty){
      if(!expectedQty) return;
      if(qty < 0){
        qty = 0;
        $(`tr[data-id='${skuid}'] input:eq(${y})`, notReceivingSkus.skutable).val(qty);
        notReceivingSkus.data[x].quantity[y] = qty;
      }

      let expected = expectedQtyMap.get(skuid).quantity.reduce((a, v) => a + v);
      if(qty > expected){
        alert('Non-receipt quantity cannot be more than expected quantity.');
        qty = expected;
        $(`tr[data-id='${skuid}'] input:eq(${y})`, notReceivingSkus.skutable).val(qty)
        notReceivingSkus.data[x].quantity[y] = qty;
      }

      let sum = 0;
      $(`tr[data-id='${skuid}'] input`, receivingSkus.skutable).each(function(){
        sum += parseInt($(this).val());
      });

      if(sum < expected){
        let fresh = $(`tr[data-id='${skuid}'] input:eq(0)`, receivingSkus.skutable);
        let _f = parseInt(fresh.val());
        fresh.val(_f + expected - sum);
        receivingSkus.data[x].quantity[0] = _f;
      }
      
      let _q = qty;
      for(let y = 0; y < 4; y++){
        let val = $(`tr[data-id='${skuid}'] input:eq(${y})`, receivingSkus.skutable).val();
        let _v = (val >= _q)? val - _q: 0;
        $(`tr[data-id='${skuid}'] input:eq(${y})`, receivingSkus.skutable).val(_v);
        receivingSkus.data[x].quantity[y] = _v;
        _q = (val >= _q)? 0 : _q - val;
        if(_q == 0) return;
      }

    });

    function deselectSwitch(level){
      switch(level){
        case 'site': 
          fType.hide();
          typeCards.deselect();
        case 'type':
          fRef.hide();
          refCards.deselect().clear();
          fAdRef.hide();
        case 'ref':
          fRemark.hide();
          fQuantity.hide();
      }
      expectedQty = null;
      expectedQtyMap.clear();
      receivingSkus.zerorize();
      notReceivingSkus.zerorize();
      createScreen.footer.hide();
    }

    siteCards.cards.on('selected', function(){
      fType.show();

      if (selectedReferenceType != '') {
        $(`div[data-id='${selectedReferenceType}']`).click();
      }
    });

    siteCards.cards.on('deselected', function(){
      deselectSwitch('site');
    });

    typeCards.cards.on('selected', function(ev, id){
      if(id == 'ad') {
        fAdRef.show();
        refCards.cards.trigger('selected');
        return;
      }

      let site_id = $('.card.selected', siteCards.cards).eq(0).data('id');
      let refs, url, err;

      if(id == 'tr'){
        url = `/a/inventory/transfer_out/list?to_site_id=${site_id}&status=in transit`;

        $.ajax({ type: 'GET', async: false, url: url, 
          error: function(x,s,e){ err = {x,s,e} },
          success: function(data) { refs = data.transferOut.rows } 
        });

        if(err){
          let {x,s,e} = err;
          error(x,s,e);
          typeCards.deselect();
          return;
        }

      } else if(id == 'vr') {
        url = `/a/inventory/van_return/list?site_id=${site_id}`;

        $.ajax({ type: 'GET', async: false, url: url, 
          error: function(x,s,e){ err = {x,s,e} },
          success: function(data) { refs = data.vanReturn.rows } 
        });

        if(err){
          let {x,s,e} = err;
          error(x,s,e);
          typeCards.deselect();
          return;
        }
      }

      if(!refs || refs.length == 0){
        let msg = 'No unreceived ';
        if(id == 'tr') {
          msg += 'transfer.';
          msg += "\nSelect 'ad-hoc' to proceed with receiving."
        } else if(id == 'vr'){
          msg += 'van returns.';
        } 
        alert(msg);
        typeCards.deselect();
        return; 
      }

      refCards.list(refs);
      fRef.show();

      if (selectedDocId != '') {
        $(`div[data-id='${selectedDocId}']`).click();
      }
    });

    typeCards.cards.on('deselected', function(){
      deselectSwitch('type');
    });

    refCards.cards.on('selected', function(){
      let type_id = $('.card.selected', typeCards.cards).eq(0).data('id');
      let ref_id =  $('.card.selected', refCards.cards).eq(0).data('id');
      if(!ref_id && type_id != 'ad'){
        alert('Please select reference id.');
        refCards.deselect();
        return; 
      }

      
      if(type_id == 'tr'){
        url = `/a/inventory/transfer_out/o/${ref_id}`;
        $.ajax({ type: 'GET', async: false, url: url, 
          error: function(x,s,e){ err = {x,s,e} },
          success: function(data) { 
            expectedQty = data.details 
            expectedQtyMap.clear();
            for(let e of expectedQty){
              expectedQtyMap.set(e.sku_id, e);
            }
          } 
        });

        btnConfirm.hide();

        btnCreate.show();
        fRemark.show();

      } else if(type_id == 'vr') {
        url = `/a/inventory/van_return/o/${ref_id}`;
        $.ajax({ type: 'GET', async: false, url: url, 
          error: function(x,s,e){ err = {x,s,e} },
          success: function(data) { 
            expectedQty = data.details 
            expectedQtyMap.clear();
            for(let e of expectedQty){
              expectedQtyMap.set(e.sku_id, e);
            }
          } 
        });

        btnConfirm.show();

        fRemark.hide();
        btnCreate.hide();
      } else if (type_id == 'ad'){
        btnConfirm.hide();
        fRemark.show();
      }

      fQuantity.show();

      if(xhr) xhr.abort();
      xhr = $.get('/a/sku/tablelist').done(function(data){
        let skus = data.skus;
        for(let i = 0; i < skus.length; i++){
          skus[i].quantity = [0,0,0,0];
        }
        receivingSkus.list(skus);
        notReceivingSkus.list(skus);

        receivingSkus.populate(expectedQty);
      }).fail(function(x,s,e){
        error(x,s,e);
      });
      
      createScreen.footer.show();
    });

    refCards.cards.on('deselected', function(){
      deselectSwitch('ref');
    });

    function resetCreateScreen(){
      expectedQty = null;
      expectedQtyMap.clear();
      inpAdRef.val('');
      inpRemark.val('');
      receivingSkus.zerorize();
      notReceivingSkus.zerorize();
      siteCards.cards.children().show().removeClass('selected');
      createScreen.footer.hide();
      fRemark.hide();
      fQuantity.hide();
    }

    btnCreate = $(`<button class='create'>Create Transfer In</button>`).appendTo(createScreen.footer);
    btnCreate.click(function(){
      let site_id = $('.card.selected', siteCards.cards).eq(0).data('id');
      if(!site_id || site_id.trim().length == 0){
        return alert('Please enter site id.');
      }

      let type_id = $('.card.selected', typeCards.cards).eq(0).data('id');
      let type = typeMap[type_id];

      console.log(type_id == 'ad');
      console.log(inpAdRef.val());
      let ref_id = (type_id == 'ad')? inpAdRef.val() : $('.card.selected', refCards.cards).eq(0).data('id');
      if(!ref_id || ref_id.trim().length == 0){
        return alert('Please enter reference ID');
      }

      let remark = inpRemark.val();
      if(!remark || remark.trim().length == 0){
        return alert('Please enter remark.');
      }

      let rSkus = receivingSkus.data;
      let nrSkus = notReceivingSkus.data;
      let total = 0;
      for(let s of rSkus){
        for(let q of s.quantity){
          let qty = parseInt(q);
          if(qty < 0) return alert('Quantity cannot be negative.');
          total += qty;
        }
      }
      if(total == 0) return alert('Total quantity cannot be 0.');

      // submit transfer in
      btnCreate.prop('disabled', true);
      if(xhr) xhr.abort();
      xhr = $.ajax({
        type: 'POST',
        url:'/a/inventory/transfer_in',
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({site_id, type, ref_id, remark, receivingSkus: rSkus, nonReceivingSkus: nrSkus})
      }).done(function(data){
        console.log(data);
        alert(data.msg);
        createScreen.btnBack.trigger('click');
        resetCreateScreen();
        dt.search({keyword: data.ti.id}, true);
      }).fail(function(x,s,e){
        error(x,s,e);
      }).always(function(){
        btnCreate.prop('disabled', false);
      })

    });

    btnConfirm = $(`<button class='create'>Confirm</button>`).appendTo(createScreen.footer);
    btnConfirm.click(function(){
      let site_id = $('.card.selected', siteCards.cards).eq(0).data('id');
      if(!site_id || site_id.trim().length == 0){
        return alert('Please enter site id.');
      }

      let type_id = $('.card.selected', typeCards.cards).eq(0).data('id');
      let type = typeMap[type_id];

      console.log(type_id == 'ad');
      console.log(inpAdRef.val());
      let ref_id = (type_id == 'ad')? inpAdRef.val() : $('.card.selected', refCards.cards).eq(0).data('id');
      if(!ref_id || ref_id.trim().length == 0){
        return alert('Please enter reference ID');
      }

      let remark = `van return adhoc`;

      let rSkus = receivingSkus.data;
      let nrSkus = notReceivingSkus.data;
      let total = 0;
      for(let s of rSkus){
        for(let q of s.quantity){
          let qty = parseInt(q);
          if(qty < 0) return alert('Quantity cannot be negative.');
          total += qty;
        }
      }
      if(total == 0) return alert('Total quantity cannot be 0.');

      // submit transfer in
      btnConfirm.prop('disabled', true);
      if(xhr) xhr.abort();
      xhr = $.ajax({
        type: 'POST',
        url:'/a/inventory/transfer_in',
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({site_id, type, ref_id, remark, receivingSkus: rSkus, nonReceivingSkus: nrSkus})
      }).done(function(data){
        console.log(data);
        alert(data.msg);
        createScreen.btnBack.trigger('click');
        resetCreateScreen();
        dt.search({keyword: data.ti.id}, true);
      }).fail(function(x,s,e){
        error(x,s,e);
      }).always(function(){
        btnConfirm.prop('disabled', false);
      })

    });
  }


  btnCreateScreen.click(function(){
    if(!createScreen) createScreenContent();
    createScreen.show();
  });

  function createViewScreen(){
    viewScreen = new Screen({
      id: 'viewScreen',
      h1: 'View Transfer In', 
      prevScreen: listScreen
    });

    viewDetailRows = new PivotTable({
      parent: viewScreen.content,
      columns: [
        ['id', 'Transfer In ID'], 
        ['site_id', 'Site ID'],
        ['type', 'Type'],
        ['ref_id', 'Reference ID'],
        ['date', 'Date'],
        ['remark', 'Remark'],
        ['created_by', 'Transfer In by']
      ]
    });    

    viewReceivedSkus = new SkuTable({
      parent: viewScreen.content,
      id: 'received',
      title: 'Received',
      columns: [['sku_id', 'SKU ID'], ['uom_id', 'UOM']],
      quantityColumns: ['Fresh', 'Damaged', 'Old', 'Recalled'],
      viewOnly: true
    });

    viewNonReceivedSkus = new SkuTable({
      parent: viewScreen.content,
      id: 'unreceived',
      title: 'Unreceived',
      columns: [['sku_id', 'SKU ID'], ['uom_id', 'UOM']],
      quantityColumns: ['Fresh', 'Damaged', 'Old', 'Recalled'],
      viewOnly: true
    });

    btnPrint = $(`<button type='button' id='printer' class='primary' class='btnPrint'>Print</button>`).appendTo(viewScreen.navRight);
    
    btnPrint.click(function(){
      let api = `/a/report/transfer_in?report_id=${docId}`;

      printJS({
        printable:  api,
        type: 'pdf',
            onError: function  (error) {
              alert('Print PDF failed');
            }
        })
    });

  }

  function result_link(tiid){
    docId = tiid;
    if(!viewScreen) createViewScreen();
    viewDetailRows.clear();
    viewReceivedSkus.clear();
    viewReceivedSkus.skutable.hide();
    viewNonReceivedSkus.clear();
    viewNonReceivedSkus.skutable.hide();
    viewReceivedSkus
    viewScreen.show();

    if(xhr) xhr.abort();
    xhr = $.get(`/a/inventory/transfer_in/o/${tiid}`).done(function(data){
      viewDetailRows.update(data.transfer_in);
      if(data.received && data.received.length > 0) {
        viewReceivedSkus.list(data.received);
        viewReceivedSkus.skutable.show();
      }
      
      if(data.unreceived && data.unreceived.length > 0) {
        viewNonReceivedSkus.list(data.unreceived);
        viewNonReceivedSkus.skutable.show();
      }
    }).fail(function(x,s,e){
      error(x,s,e);
    })
  }

  function drawDataTable(){
    dt = new DataTable({
      api: 'a/inventory/transfer_in/list',
      columns: ['date', 'site_id', 'id', 'created_by', ['created_at', 'Created At']],
      filters: {
        site_id: {name: 'Site ID', type: 'option', options:['All', ...sites]},
        date: {name: 'Date', type: 'date', dateOptions: [
            ['anytime', 'Any time'],
            ['today', 'Today'],
            ['weektodate', 'Week-to-date'],
            ['monthtodate', 'Month-to-date'],
            ['yeartodate', 'Year-to-date'],
            ['customdate', 'Custom date'],
            ['customrange', 'Custom range']
          ]}        
      },
      format:{
        created_at: (v) => { return (v)? dayjs(v).format(df) : '-'} 
      },
      data: 'transferIn',
      result_link: result_link
    });

    dt.search(getJsonQueries());
    window.onpopstate = function(event) {
      dt.search(getJsonQueries(), true);
    }
  }

  $.get('/a/site/list').done(function(data){    
    if(data.sites && data.sites.rows){
      sites = data.sites.rows.map(e => e.id);
    }
    drawDataTable();
  });

function openNewTransferIn(docId) {
  let url = `/a/notification/o/${docId}`;
  $.ajax({ type: 'GET', async: false, url: url, 
    error: function(x,s,e){ err = {x,s,e} },
    success: function(data) { 
      if (data.notification != null) {
        selectedSiteId = data.notification.receiver;
        selectedReferenceType = data.notification.type;
        selectedDocId = data.notification.id;

        btnCreateScreen.click();
      }
    } 
  });
  
}

 let jsonQueries = getJsonQueries();
  if (jsonQueries != null){
    let action = jsonQueries.action;
    let docId = jsonQueries.id;
    
    if (docId != null) {
      if (action == 'create') {
        openNewTransferIn(docId);
      } else {
        result_link(docId);
        // Override click listener for back button
        viewScreen.btnBack.unbind();
        viewScreen.btnBack.click(function(){
          history.back()
        });
      }
    }
  }

})