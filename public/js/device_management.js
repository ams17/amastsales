$(()=>{
	const df = 'YYYY-MM-DD';
	const btnCreateScreen   = $('#btnCreateScreen');
	const btnSave           = $('.save');
	const btnUpdate         = $('.update');
	const btnCancel         = $('.cancel');
	const listDiv           = $('.list');
	const createDiv         = $('.create');
	const formLabel         = $('#formLabel');
	const rowIdField        	= $('#rowId');
	const uuIDField         	= $('#uuID');
	const imeIDField       		= $('#imeID');
	const madeField       		= $('#made');
	const modelField    		= $('#model');
	const telNoField   			= $('#telNo');
	const yearPurchasedField    = $('#yearPurchased');
	const statusField    		= $('#status');

	btnCreateScreen.click(function(event){
		event.preventDefault();
		let loadingDialog = new Dialog('Loading... ');
      	loadingDialog.overlay.off();    

		$.get('/a/device_management/getDetails').done(function(r){
			uuIDField.val('');
			imeIDField.val('');
			telNoField.val('');
			// console.log(r);
			listDiv.hide();
		    createDiv.show();
		    formLabel.text('Create Device');
		    btnUpdate.hide();
		    btnSave.show();

		    let data 		= r.data;
		    let madeRows  	= data.madeRows;
		    let madeCount  	= data.madeCount;

		    let modelRows  	= data.modelRows;
		    let modelCount  = data.modelCount;

		    let yearRows  	= data.yearRows;
		    let yearCount  	= data.yearCount;

		    let statusRows  = data.statusRows;
		    let statusCount = data.statusCount;

			var madeOptions = [];
			for (var i = 0; i < madeCount; i++) {
				if(madeRows[i].is_default === true){
			        madeOptions.push('<option value="',
			          madeRows[i].id,'" selected="selected">',
			          madeRows[i].display, '</option>');
		    	}else{
		    		madeOptions.push('<option value="',
			          madeRows[i].id, '">',
			          madeRows[i].display, '</option>');
		    	}
		    }
		    console.log(madeOptions.join(''));
		    $("#made").html(madeOptions.join('')).select2({
				width: '54%',
			});

			var modelOptions = [];
			for (var i = 0; i < modelCount; i++) {
				if(modelRows[i].is_default === true){
		        	modelOptions.push('<option value="',
			          modelRows[i].id,'" selected="selected">',
			          modelRows[i].display, '</option>');
		        }else{
		        	modelOptions.push('<option value="',
			          modelRows[i].id, '">',
			          modelRows[i].display, '</option>');
		        }
		    }
		    $("#model").html(modelOptions.join('')).select2({
				width: '54%',
			});

			var yearOptions = [];
			for (var i = 0; i < yearCount; i++) {
				if(yearRows[i].is_default === true){
			        yearOptions.push('<option value="',
			          yearRows[i].id, '" selected="selected">',
			          yearRows[i].display, '</option>');
			    }else{
			    	yearOptions.push('<option value="',
			          yearRows[i].id, '">',
			          yearRows[i].display, '</option>');
			    }
		    }
		    $("#yearPurchased").html(yearOptions.join('')).select2({
				width: '54%',
			});

			var statusOptions = [];
			for (var i = 0; i < statusCount; i++) {
				if(statusRows[i].is_default === true){
			        statusOptions.push('<option value="',
			          statusRows[i].id, '" selected="selected">',
			          statusRows[i].display, '</option>');
			    }else{
			    	statusOptions.push('<option value="',
			          statusRows[i].id, '">',
			          statusRows[i].display, '</option>');
			    }
		    }
		    $("#status").html(statusOptions.join('')).select2({
				width: '54%',
			});

		}).fail(function(e){
		console.error(e);
			errMsg = JSON.parse(e.responseText).errMsg;
			new Dialog(errMsg || 'An error occured.').addCancelButton().text('OK');
		}).always(function(){
			loadingDialog.remove();
			btnSave.prop('disabled', false);
		});

	    // idField.val('');
	    // nameField.val('');
	    // roleField.val('');
	    // statusField.val('');
	    // passwordField.val('');
	    // deviceIDField.val('');
	    // allowNewDeviceField.val('');
  	});

	btnCancel.click(function(){
		createDiv.hide();
		listDiv.show();
	});



	btnSave.click(function(){
		// Input fields
		let uuID        	= uuIDField.val();
		let imeID      		= imeIDField.val();
		let made      		= madeField.val();
		let model      		= modelField.val();
		let telNo    		= telNoField.val();
		let yearPurchased   = yearPurchasedField.val();
		let status   		= statusField.val();

		if(!uuID) return new Dialog('Please enter UU ID').addCancelButton().text('OK');
		if(!imeID) return new Dialog('Please enter IME ID').addCancelButton().text('OK');
		if(!made) return new Dialog('Please select Made').addCancelButton().text('OK');
		if(!model) return new Dialog('Please select Model').addCancelButton().text('OK');
		if(!telNo) return new Dialog('Please enter Tel No').addCancelButton().text('OK');
		if(!yearPurchased) return new Dialog('Please enter year purchased').addCancelButton().text('OK');
		if(!status) return new Dialog('Please select a status');

		btnSave.prop('disabled', true);
		// vanName.prop('disabled', true);
		let loadingDialog = new Dialog('Saving... ');
		loadingDialog.overlay.off();
		var postData = {
			uuID,
			imeID,
			model,
			made,
			telNo,
			yearPurchased,
			status
		};
		// console.log(postData); return false;
		$.post('/a/device_management/create', postData).done(function(r){
			let dialog = new Dialog('Device saved successfully')
			dialog.addButton('primary', 'OK').click(function(){
				dialog.remove();
				btnCancel.trigger('click');
				location.reload();
			});
		}).fail(function(e){
			console.error(e);
			errMsg = JSON.parse(e.responseText).errMsg;
			new Dialog(errMsg || 'An error occured.').addCancelButton().text('OK');
		}).always(function(){
			loadingDialog.remove();
			btnSave.prop('disabled', false);
		});
	});

  	btnUpdate.click(function(){

		// Input fields
		let rowId        	= rowIdField.val();
		let uuID        	= uuIDField.val();
		let imeID      		= imeIDField.val();
		let made      		= madeField.val();
		let model      		= modelField.val();
		let telNo    		= telNoField.val();
		let yearPurchased   = yearPurchasedField.val();
		let status   		= statusField.val();

		if(!uuID) return new Dialog('Please enter UU ID').addCancelButton().text('OK');
		if(!imeID) return new Dialog('Please enter IME ID').addCancelButton().text('OK');
		if(!made) return new Dialog('Please select Made').addCancelButton().text('OK');
		if(!model) return new Dialog('Please select Model').addCancelButton().text('OK');
		if(!telNo) return new Dialog('Please enter Tel No').addCancelButton().text('OK');
		if(!yearPurchased) return new Dialog('Please enter year purchased').addCancelButton().text('OK');
		if(!status) return new Dialog('Please select a status');

		btnUpdate.prop('disabled', true);

		let loadingDialog = new Dialog('Updating... ');
		loadingDialog.overlay.off();

		var postData = {
			rowId,
			uuID,
			imeID,
			model,
			made,
			telNo,
			yearPurchased,
			status
		};
		// console.log(postData); return false;
		$.post('/a/device_management/update', postData).done(function(r){
			let dialog = new Dialog('Device updated successfully')
			dialog.addButton('primary', 'OK').click(function(){
			dialog.remove();
			btnCancel.trigger('click');
			location.reload();
		});
		// getlist();
		}).fail(function(e){
			console.error(e);
			errMsg = JSON.parse(e.responseText).errMsg;
			new Dialog(errMsg || 'An error occured.').addCancelButton().text('OK');
		}).always(function(){
			loadingDialog.remove();
			btnSave.prop('disabled', false);
		});
	});

	let dt = new DataTableCURD({
		api: '/a/device_management/list',
		columns: ['id','ime_id','year_purchased','tel_no','status'],
		headerColumns: ['UU ID','IME ID','Year Purchased','Tel No','Status'],
		data: 'deviceList',
		edit: true,
		delete:false
	});

	dt.search(getJsonQueries());
		window.onpopstate = function(event) {
		dt.search(getJsonQueries(), true);
	}

});

function editRow(doc_id){
    docId = doc_id;
    let loadingDialog = new Dialog('loading... ');
	loadingDialog.overlay.off();
    $.get(`/a/device_management/read/${docId}`).done(function(r){
    	loadingDialog.remove();
		if(!r || !r.data) {
			return alert('ID not found.');
		}

		const listDiv           = $('.list');
		const createDiv         = $('.create');
		const formLabel         = $('#formLabel');
		const btnUpdate         = $('.update');
		const btnCancel         = $('.cancel');
		const btnSave           = $('.save');
		//Input Fields

		const rowIdField        	= $('#rowId');
		const uuIDField         	= $('#uuID');
		const imeIDField       		= $('#imeID');
		const madeField       		= $('#made');
		const modelField    		= $('#model');
		const telNoField   			= $('#telNo');
		const yearPurchasedField    = $('#yearPurchased');
		const statusField    		= $('#status');


	    let data 		= r.data;
	    let madeRows  	= data.madeRows;
	    let madeCount  	= data.madeCount;

	    let modelRows  	= data.modelRows;
	    let modelCount  = data.modelCount;

	    let yearRows  	= data.yearRows;
	    let yearCount  	= data.yearCount;

	    let statusRows  = data.statusRows;
	    let statusCount = data.statusCount;

	    let device = data.device;
	    // console.log(device);

		var madeOptions = [];
		for (var i = 0; i < madeCount; i++) {
			if(device.made == madeRows[i].id){
		        madeOptions.push('<option value="',
		          madeRows[i].id,'" selected="selected">',
		          madeRows[i].display, '</option>');
	    	}else{
	    		madeOptions.push('<option value="',
		          madeRows[i].id, '">',
		          madeRows[i].display, '</option>');
	    	}
	    }
	    $("#made").html(madeOptions.join('')).select2({
			width: '54%',
		});

		var modelOptions = [];
		for (var i = 0; i < modelCount; i++) {
			if(device.model == modelRows[i].id){
	        	modelOptions.push('<option value="',
		          modelRows[i].id,'" selected="selected">',
		          modelRows[i].display, '</option>');
	        }else{
	        	modelOptions.push('<option value="',
		          modelRows[i].id, '">',
		          modelRows[i].display, '</option>');
	        }
	    }
	    $("#model").html(modelOptions.join('')).select2({
			width: '54%',
		});

		var yearOptions = [];
		for (var i = 0; i < yearCount; i++) {
			if(device.year_purchased == yearRows[i].id){
		        yearOptions.push('<option value="',
		          yearRows[i].id, '" selected="selected">',
		          yearRows[i].display, '</option>');
		    }else{
		    	yearOptions.push('<option value="',
		          yearRows[i].id, '">',
		          yearRows[i].display, '</option>');
		    }
	    }
	    $("#yearPurchased").html(yearOptions.join('')).select2({
			width: '54%',
		});

		var statusOptions = [];
		for (var i = 0; i < statusCount; i++) {
			if(device.status == statusRows[i].id){
		        statusOptions.push('<option value="',
		          statusRows[i].id, '" selected="selected">',
		          statusRows[i].display, '</option>');
		    }else{
		    	statusOptions.push('<option value="',
		          statusRows[i].id, '">',
		          statusRows[i].display, '</option>');
		    }
	    }
	    $("#status").html(statusOptions.join('')).select2({
			width: '54%',
		});


      listDiv.hide();
      createDiv.show();
      formLabel.text('Edit Device');

      btnUpdate.show();
      btnSave.hide();

      rowIdField.val(device.device_id).prop('readonly',true);

      uuIDField.val(device.device_id);
      imeIDField.val(device.ime_id);
      telNoField.val(device.tel_no);

    }).fail(function(x,s,e){
      error(x,s,e);
      alert('ID not found.')
    }).always(function(){
		loadingDialog.remove();
	});
 }

function deleteRow(doc_id){
    docId = doc_id;

    let dialog = new Dialog('Please confirm deletion')
    dialog.addCancelButton().text('Cancel')
    dialog.addButton('primary', 'OK').click(function(){
      dialog.remove();
      // btnCancel.trigger('click');
      // location.reload();
      $.post(`/a/price/delete`,{id:docId}).done(function(d){
        console.log(d);
        // if(!d || !d.user) {
        //   return alert('Site ID not found.');
        // }
        let dialog = new Dialog(d.msg)
        dialog.addButton('primary', 'OK').click(function(){
          dialog.remove();
          location.reload();
        });
      }).fail(function(x,s,e){
        error(x,s,e);
        alert('ID not found.')
      });
    });    
  }


$(document).ready(function () {
    $('th').each(function (col) {
        $(this).hover(
          function () {
              $(this).addClass('focus');
          },
          function () {
              $(this).removeClass('focus');
          }
        );
	    $(this).click(function () {
	        if ($(this).is('.asc')) {
	            $(this).removeClass('asc');
	            $(this).addClass('desc selected');
	            sortOrder = -1;
	        } else {
	            $(this).addClass('asc selected');
	            $(this).removeClass('desc');
	            sortOrder = 1;
	        }
	        $(this).siblings().removeClass('asc selected');
	        $(this).siblings().removeClass('desc selected');
	        var arrData = $('table').find('tbody >tr:has(td)').get();
	        // arrData.sort(function (a, b) {
	        //     var val1 = $(a).children('td').eq(col).text().toUpperCase();
	        //     var val2 = $(b).children('td').eq(col).text().toUpperCase();
	        //     if ($.isNumeric(val1) && $.isNumeric(val2))
	        //         return sortOrder == 1 ? val1 - val2 : val2 - val1;
	        //     else
	        //         return (val1 < val2) ? -sortOrder : (val1 > val2) ? sortOrder : 0;
	        // });
	        // $.each(arrData, function (index, row) {
	        //     $('tbody').append(row);
	        // });

	        var table = $('table').eq(0)
	        var rows = table.find('tr:gt(0)').toArray().sort(comparer($(this).index()))
	        this.asc = !this.asc
	        if (!this.asc){rows = rows.reverse()}
	        for (var i = 0; i < rows.length; i++){table.append(rows[i])}
	    });
    });

    function comparer(index) {
      return function(a, b) {
        var valA = getCellValue(a, index), valB = getCellValue(b, index)
         return $.isNumeric(valA) && $.isNumeric(valB) ? valA - valB : valA.toString().localeCompare(valB)
      }
    }
    function getCellValue(row, index){ return $(row).children('td').eq(index).text() }
});

