
$(document).ready(async ()=>{
  let particlesbg = $('#particles-js');
  let btnLogin = $('#btnLogin');
  
  let form = $('form');
  let username = $('#username');
  let password = $('#password');

  $('input', form)
    .on('focus', function(){ particlesbg.addClass('blur') })
    .on('focusout', function(){ particlesbg.removeClass('blur') });
  

  let logging_in = false;

  $('form').on('submit', async function(e){
    let form = this;
    e.preventDefault();

    if(logging_in) return false;
    logging_in = true;
    btnLogin.prop('disabled', true);
    btnLogin.text('Logging in...');

    form.submit();    
  });

  /*particlesJS.load('particles-js', '/js/particles.json', function() {
    console.log('callback - particles.js config loaded');
  });*/

});

