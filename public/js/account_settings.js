$(()=>{

  const cpass = $('#cpass');
  const cname = $('#cname');
  const cacc  = $('#cacc');

  const iname = $('#iname');
  const icurrpass = $('#icurrpass');
  const inewpass = $('#inewpass');
  const irepass = $('#irepass');

  const uid = $('#uid');
  const name = $('#name');
  const pass = $('#pass');
  const lastlogin = $('#lastlogin');
  const createdat = $('#createdat');
  const createdby = $('#createdby');

  const btnNameEdit = $('#name button', cacc);
  const btnPassEdit = $('#pass button', cacc);
  const btnNameSave = $('.save', cname);
  const btnNameCancel = $('.cancel', cname);
  const btnPassSave = $('.save', cpass);
  const btnPassCancel = $('.cancel', cpass);

  const df = 'ddd, D MMM YYYY h:mm:ss A';
  const passwordRegex = /(?=(.*[0-9]))(?=.*[\!@#$%^&*()\\[\]{}\-_+=~`|:;"'<>,./?])(?=.*[a-z])(?=(.*[A-Z]))(?=(.*)).{8,}/;

  let currentName = '';

  $.get('/a/user/self').done(function(r){
    if(!r || !r.user){
      let dialog = new Dialog("An error occured.");
      dialog.addCancelButton().text('OK');
      return;
    }

    let d = r.user;
    $('.value', uid).text(d.id);
    $('.value', name).text(d.name);
    $('.value', lastlogin).text(dayjs(d.last_login_at).format(df));
    $('.value', createdat).text(dayjs(d.createdAt).format(df));
    $('.value', createdby).text(d.created_by);
    currentName = d.name;

  }).fail(function(e){
    console.error(e);
    let errMsg = JSON.parse(e.responseText).errMsg;
    let dialog = new Dialog(errMsg || "An error occurred.");
    dialog.addCancelButton().text('OK');
  });

  btnNameEdit.click(function(){
    iname.val(currentName);
    cpass.hide();
    cacc.hide();
    cname.show();
  });

  btnNameCancel.click(function(){
    cpass.hide();
    cname.hide();
    cacc.show();
  });

  btnPassEdit.click(function(){
    icurrpass.val('');
    inewpass.val('');
    irepass.val('');
    cpass.show();
    cname.hide();
    cacc.hide();
  });

  btnPassCancel.click(function(){
    cpass.hide();
    cname.hide();
    cacc.show();
  });

  btnPassSave.click(function(){
    let currPassword = icurrpass.val();
    let newPassword = inewpass.val();
    let rePassword = irepass.val();

    if(!currPassword) return new Dialog('Please enter current password').addCancelButton().text('OK');
    if(!newPassword) return new Dialog('Please enter new password').addCancelButton().text('OK');
    if(!rePassword) return new Dialog('Please retype new password').addCancelButton().text('OK');
    if(rePassword != newPassword) return new Dialog('New Password does not match with retyped password').addCancelButton().text('OK');
    if(!passwordRegex.test(newPassword)) return new Dialog('Password does not meet complexity requirement').addCancelButton().text('OK');

    btnPassSave.prop('disabled', true);
    icurrpass.prop('disabled', true);
    inewpass.prop('disabled', true);
    irepass.prop('disabled', true);
    let loadingDialog = new Dialog('Saving... ');
    loadingDialog.overlay.off();

    $.post('/a/account/password', {currPassword, newPassword}).done(function(r){
      let dialog = new Dialog('Password saved successfully')
      dialog.addButton('primary', 'OK').click(function(){
        dialog.remove();
        btnPassCancel.trigger('click');
      });
    }).fail(function(e){
      console.error(e);
      errMsg = JSON.parse(e.responseText).errMsg;
      new Dialog(errMsg || 'An error occured.').addCancelButton().text('OK');
    }).always(function(){
      loadingDialog.remove();
      btnPassSave.prop('disabled', false);
      icurrpass.prop('disabled', false);
      inewpass.prop('disabled', false);
      irepass.prop('disabled', false);
    });
  });

  btnNameSave.click(function(){
    btnNameSave.prop('disabled', true);
    let username = iname.val();
    if(username == null || username == '') {
      btnNameSave.prop('disabled', false);
      return new Dialog('Please enter name').addCancelButton().text('OK');
    }

    iname.prop('disabled', true);
    $.post('/a/account/name', {name: username}).done(function(r){
      currentName = username;
      $('.value', name).text(username);
      let dialog = new Dialog('Name saved successfully')
      dialog.addButton('primary', 'OK').click(function(){
        dialog.remove();
        btnNameCancel.trigger('click');
      });
    }).fail(function(e){
      console.error(e);
      errMsg = JSON.parse(e.responseText).errMsg;
      new Dialog(errMsg || 'An error occured.').addCancelButton().text('OK');
    }).always(function(e){
      btnNameSave.prop('disabled', false);
      iname.prop('disabled', false);
    });

  })

})  
