$(()=>{

  let xhr;
  let icont = $('article .icont');
  let sitelist = getSitelist();
  let selectedSite, selectedSku;
  let invtTable;
  let sites;
  const ledgerApi = '/a/inventory/ledger/list';

  let mainSection = $('.content section');
  let ledgerScreen;
  let dt;

  const df = 'YYYY-MM-DD';

  ledgerScreen = new Screen({
    id: 'ledgerScreen',
    h1: 'Inventory Ledger',
    prevScreen: mainSection
  });

  $(`<article>
    <div class='top'>
    </div>
    <div class='searchCont'></div>
    <div class='actPager'>
      <div class='act'></div>
      <div class='pager'></div>
    </div>
  </article>
  <div class='results'></div>`).appendTo(ledgerScreen.content);

  function drawDataTable(){
    dt = new DataTable({
      api: ledgerApi,
      columns: [['site_id', 'Site ID'], 'date', 'operation', ['reference_id', 'reference'], 'sku_id', ['uom_id', 'UOM'], 'condition', 'movement', 'new_quantity', 'created_by'],
      filters: {
        site_id: {name: 'Site ID', type: 'option', options:['All', ...sites]},
        date: {name: 'Date', type: 'date', dateOptions: [
            ['anytime', 'Any time'],
            ['today', 'Today'],
            ['weektodate', 'Week-to-date'],
            ['monthtodate', 'Month-to-date'],
            ['yeartodate', 'Year-to-date'],
            ['customdate', 'Custom date'],
            ['customrange', 'Custom range']
          ]},
        condition:{name: 'Condition', type: 'option', options:['All', 'Fresh', 'Damaged', 'Old', 'Recalled']}
      },
      data: 'inventory_ledger',
      result_link: result_link
    });
  }

  $.get('/a/site/list').done(function(data){    
    if(data.sites && data.sites.rows){
      sites = data.sites.rows.map(e => e.id);
    }
    drawDataTable();
  });

  let fsite = new Field({ parent: icont, id: 'fsite', title: 'Select a site' });
  siteCards = new Cards({
    parent: fsite.label,
    id: 'siteCards'
  });
  siteCards.list(sitelist);
  
  let fInvt = new Field({ parent: icont, id: 'fInvt'});
  invtTable = new SkuTable({
    parent: icont,
    columns: [['sku_id', 'SKU ID'], ['uom_id', 'UOM']],
    quantityColumns: ['Fresh', 'Damaged', 'Old', 'Recalled'],
    viewOnly: true
  });
  fInvt.hide();
  invtTable.skutable.hide();

  siteCards.cards.on('selected', function(e, selectedId){
    selectedSite = selectedId;
    if(xhr) xhr.abort();
    xhr = $.get(`/a/inventory/list/${selectedSite}`).done(function(data){
      let statusText = `Inventory Status as of ${dayjs(data.inventory_status[0].last_updated).format(df)}`;
      fInvt.title.text(statusText);
      fInvt.show();  

      invtTable.list(data.inventory);
      invtTable.skutable.show();
      
      $('tr > *', invtTable.skutable).off().click(function(){
        selectedSku = $(this).parent().data('id');
        ledgerScreen.show();
        dt.search({ site_id: selectedSite, keyword: selectedSku }, true);
      });
    }).fail(function(x,s,e){
      error(x,s,e);
    })
  }); 

  siteCards.cards.on('deselected', function(){
    fsite.title.show();
    fInvt.hide();
    invtTable.skutable.hide();
    invtTable.clear();
    selectedSite = null;
    if(xhr) xhr.abort();
  });

  function result_link(toid){
    if(xhr) xhr.abort();
    xhr = $.get(`/a/inventory/ledger/o/${toid}`).done(function(data){
      if (data.details != null) {
        let operation = data.details.operation;
        let reference_id = data.details.reference_id;

        switch (operation){
          case 'transfer in':
            window.location.href = `/transfer_in?id=${reference_id}`;
            break;
          case 'transfer out':
            window.location.href = `/transfer_out?id=${reference_id}`;
            break;
          case 'write off':
            window.location.href = `/write_off?id=${reference_id}`;
            break;
        }
      }
    }).fail(function(x,s,e){
      error(x,s,e);
    })
  };
});

  