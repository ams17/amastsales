$(()=>{

  const btnCreateScreen   = $('#btnCreateScreen');
  const btnSave           = $('.save');
  const btnUpdate         = $('.update');
  const btnCancel         = $('.cancel');
  const listDiv           = $('.list');
  const createDiv         = $('.create');
  const formLabel         = $('#formLabel');

  //Input Fields

  const idField         = $('#id');
  const nameField       = $('#name');
  const roleField       = $('#role');
  const statusField     = $('#status');
  const passwordField   = $('#password');
  const deviceIDField   = $('#deviceID');
  const allowNewDeviceField   = $('#allowNewDevice');

  btnCreateScreen.click(function(){
    listDiv.hide();
    createDiv.show();
    formLabel.text('Add User');
    btnUpdate.hide();
    btnSave.show();

    idField.val('');
    nameField.val('');
    roleField.val('');
    statusField.val('');
    passwordField.val('');
    deviceIDField.val('');
    allowNewDeviceField.val('');
  });

  btnCancel.click(function(){
    createDiv.hide();
    listDiv.show();
  });

  btnSave.click(function(){

      // Input fields
      let id        = idField.val();
      let name      = nameField.val();
      let role      = roleField.val();
      let status    = statusField.val();
      let password    = passwordField.val();
      let deviceID    = deviceIDField.val();
      let allowNewDevice    = allowNewDeviceField.val();

      if(!id) return new Dialog('Please enter user id').addCancelButton().text('OK');
      if(!name) return new Dialog('Please enter user name').addCancelButton().text('OK');
      if(!password) return new Dialog('Please enter password').addCancelButton().text('OK');

      btnSave.prop('disabled', true);
      // vanName.prop('disabled', true);
      let loadingDialog = new Dialog('Saving... ');
      loadingDialog.overlay.off();
      var postData = {
        id,
        name,
        role,
        status,
        password,
        deviceID,
        allowNewDevice
      };
      // console.log(postData); return false;
      $.post('/a/user/create', postData).done(function(r){
        let dialog = new Dialog('user saved successfully')
        dialog.addButton('primary', 'OK').click(function(){
          dialog.remove();
          btnCancel.trigger('click');
          location.reload();
        });
        // getlist();
      }).fail(function(e){
        console.error(e);
        errMsg = JSON.parse(e.responseText).errMsg;
        new Dialog(errMsg || 'An error occured.').addCancelButton().text('OK');
      }).always(function(){
        loadingDialog.remove();
        btnSave.prop('disabled', false);
      });
  });

  btnUpdate.click(function(){

      // Input fields
      let id        = idField.val();
      let name      = nameField.val();
      let role      = roleField.val();
      let status    = statusField.val();
      let deviceID    = deviceIDField.val();
      let allowNewDevice    = allowNewDeviceField.val();

      if(!id) return new Dialog('Please enter user id').addCancelButton().text('OK');
      if(!name) return new Dialog('Please enter user name').addCancelButton().text('OK');

      btnUpdate.prop('disabled', true);
      // vanName.prop('disabled', true);
      let loadingDialog = new Dialog('Updating... ');
      loadingDialog.overlay.off();
      var postData = {
        id,
        name,
        role,
        status,
        deviceID,
        allowNewDevice
      };
      // console.log(postData); return false;
      $.post('/a/user/update', postData).done(function(r){
        let dialog = new Dialog('User updated successfully')
        dialog.addButton('primary', 'OK').click(function(){
          dialog.remove();
          btnCancel.trigger('click');
          location.reload();
        });
        // getlist();
      }).fail(function(e){
        console.error(e);
        errMsg = JSON.parse(e.responseText).errMsg;
        new Dialog(errMsg || 'An error occured.').addCancelButton().text('OK');
      }).always(function(){
        loadingDialog.remove();
        btnSave.prop('disabled', false);
      });
  });

  let result_link = function(doc_id){
    docId = doc_id;

    $.get(`/a/user/o/${docId}`).done(function(d){
      console.log(d);
      if(!d || !d.user) {
        return alert('User ID not found.');
      }
      
      let { user } = d;
      listDiv.hide();
      createDiv.show();
      formLabel.text('Edit User');

      btnUpdate.show();
      btnSave.hide();

      idField.val(user.id).prop('readonly',true);
      nameField.val(user.name);
      roleField.val(user.role);
      statusField.val(user.status);

    }).fail(function(x,s,e){
      error(x,s,e);
      alert('User ID not found.')
    });
  }

  getlist();
});

  
  function getlist(){
    let dt = new DataTableCURD({
      api: '/a/user/list',
      columns: ['id', 'role', 'name', 'active', 'last_login_at'],
      headerColumns: ['id', 'role', 'name', 'active', 'last_login_at'],
      format: {last_login_at: (v) => {
        return (v)? dayjs(v).format(df) : '-'}
      },
      data: 'users',
      edit: true,
      delete:true,
      filterAlwaysShow: true
    });

    dt.search(getJsonQueries());
    window.onpopstate = function(event) {
      dt.search(getJsonQueries(), true);
    }
  }

  function editRow(doc_id){
    docId = doc_id;

    $.get(`/a/user/o/${docId}`).done(function(d){
      console.log(d);
      if(!d || !d.user) {
        return alert('User ID not found.');
      }
      
      let { user } = d;
      const listDiv           = $('.list');
      const createDiv         = $('.create');
      const formLabel         = $('#formLabel');
      const btnUpdate         = $('.update');
      const btnCancel         = $('.cancel');
      const btnSave           = $('.save');
      const passwordDiv       = $('.password');
      //Input Fields

      const idField        = $('#id');
      const nameField      = $('#name');
      const roleField      = $('#role');
      const statusField   = $('#status');
      const deviceIDField   = $('#deviceID');
      const allowNewDeviceField   = $('#allowNewDevice');

      listDiv.hide();
      passwordDiv.hide();
      createDiv.show();
      formLabel.text('Edit User');

      btnUpdate.show();
      btnSave.hide();

      idField.val(user.id).prop('readonly',true);
      nameField.val(user.name);
      roleField.val(user.role);
      statusField.val((user.active == true)?(1):0);
      deviceIDField.val(user.device_id);
      allowNewDeviceField.val((user.allow_new_device == true)?(1):0);

    }).fail(function(x,s,e){
      error(x,s,e);
      alert('User ID not found.')
    });
  }
  function deleteRow(doc_id){
    docId = doc_id;

    let dialog = new Dialog('Please confirm deletion')
    dialog.addCancelButton().text('Cancel')
    dialog.addButton('primary', 'OK').click(function(){
      dialog.remove();
      // btnCancel.trigger('click');
      // location.reload();
      $.post(`/a/user/delete`,{id:docId}).done(function(d){
        console.log(d);
        // if(!d || !d.user) {
        //   return alert('Site ID not found.');
        // }
        let dialog = new Dialog(d.msg)
        dialog.addButton('primary', 'OK').click(function(){
          dialog.remove();
          location.reload();
        });
      }).fail(function(x,s,e){
        error(x,s,e);
        alert('User ID not found.')
      });
    });    
  }

  $(document).ready(function () {
    $('th').each(function (col) {
        $(this).hover(
          function () {
              $(this).addClass('focus');
          },
          function () {
              $(this).removeClass('focus');
          }
        );
        $(this).click(function () {
            if ($(this).is('.asc')) {
                $(this).removeClass('asc');
                $(this).addClass('desc selected');
                sortOrder = -1;
            } else {
                $(this).addClass('asc selected');
                $(this).removeClass('desc');
                sortOrder = 1;
            }
            $(this).siblings().removeClass('asc selected');
            $(this).siblings().removeClass('desc selected');
            var arrData = $('table').find('tbody >tr:has(td)').get();
            // arrData.sort(function (a, b) {
            //     var val1 = $(a).children('td').eq(col).text().toUpperCase();
            //     var val2 = $(b).children('td').eq(col).text().toUpperCase();
            //     if ($.isNumeric(val1) && $.isNumeric(val2))
            //         return sortOrder == 1 ? val1 - val2 : val2 - val1;
            //     else
            //         return (val1 < val2) ? -sortOrder : (val1 > val2) ? sortOrder : 0;
            // });
            // $.each(arrData, function (index, row) {
            //     $('tbody').append(row);
            // });

            var table = $('table').eq(0)
            var rows = table.find('tr:gt(0)').toArray().sort(comparer($(this).index()))
            this.asc = !this.asc
            if (!this.asc){rows = rows.reverse()}
            for (var i = 0; i < rows.length; i++){table.append(rows[i])}
        });
    });

    function comparer(index) {
      return function(a, b) {
        var valA = getCellValue(a, index), valB = getCellValue(b, index)
         return $.isNumeric(valA) && $.isNumeric(valB) ? valA - valB : valA.toString().localeCompare(valB)
      }
    }
    function getCellValue(row, index){ return $(row).children('td').eq(index).text() }
  });