$(()=>{
	const df = 'YYYY-MM-DD';
	const btnCreateScreen   = $('#btnCreateScreen');
	const btnSave           = $('.save');
	const btnUpdate         = $('.update');
	const btnCancel         = $('.cancel');
	const listDiv           = $('.list');
	const createDiv         = $('.create');
	const formLabel         = $('#formLabel');

	const rowIdField        	= $('#rowId');

	const sequenceField         	= $('#sequence');
	const displayValueField       	= $('#displayValue');
	const groupIDField       		= $('#groupID');
	const isDefaultField    		= $('#isDefault');
	const idField   				= $('#id');
	const activeField    			= $('#active');
	const valueField    			= $('#value');

	btnCreateScreen.click(function(event){
		event.preventDefault();		  

      	sequenceField.val('');
		displayValueField.val('');
		groupIDField.val('');
		isDefaultField.val('');
		idField.val('');
		activeField.val('');
		valueField.val('');

		listDiv.hide();
	    createDiv.show();
	    formLabel.text('Create LOV');
	    btnUpdate.hide();
	    btnSave.show();
  	});

	btnCancel.click(function(){
		createDiv.hide();
		listDiv.show();
	});



	btnSave.click(function(){

		let sequence = sequenceField.val();
		let displayValue = displayValueField.val();
		let groupID = groupIDField.val();
		let id = idField.val();
		let isDefault = isDefaultField.is( ":checked" );
		let active = activeField.is( ":checked" );
		let value = valueField.val();

		if(!sequence) return new Dialog('Please enter sequence').addCancelButton().text('OK');
		if(!displayValue) return new Dialog('Please enter display value').addCancelButton().text('OK');
		if(!groupID) return new Dialog('Please enter group ID').addCancelButton().text('OK');
		if(!id) return new Dialog('Please enter id').addCancelButton().text('OK');
		if(!value) return new Dialog('Please enter value').addCancelButton().text('OK');

		btnSave.prop('disabled', true);
		// vanName.prop('disabled', true);
		let loadingDialog = new Dialog('Saving... ');
		loadingDialog.overlay.off();
		var postData = {
			sequence,
			displayValue,
			groupID,
			id,
			value,
			isDefault,
			active
		};
		// console.log(postData); return false;
		$.post('/a/lov/create', postData).done(function(r){
			let dialog = new Dialog('LOV saved successfully')
			dialog.addButton('primary', 'OK').click(function(){
				dialog.remove();
				btnCancel.trigger('click');
				location.reload();
			});
		}).fail(function(e){
			console.error(e);
			errMsg = JSON.parse(e.responseText).errMsg;
			new Dialog(errMsg || 'An error occured.').addCancelButton().text('OK');
		}).always(function(){
			loadingDialog.remove();
			btnSave.prop('disabled', false);
		});
	});

  	btnUpdate.click(function(){

		// Input fields
		let rowId        	= rowIdField.val();

		let sequence = sequenceField.val();
		let displayValue = displayValueField.val();
		let groupID = groupIDField.val();
		let id = idField.val();
		let isDefault = isDefaultField.is( ":checked" );
		let active = activeField.is( ":checked" );
		let value = valueField.val();

		if(!sequence) return new Dialog('Please enter sequence').addCancelButton().text('OK');
		if(!displayValue) return new Dialog('Please enter display value').addCancelButton().text('OK');
		if(!groupID) return new Dialog('Please enter group ID').addCancelButton().text('OK');
		if(!id) return new Dialog('Please enter id').addCancelButton().text('OK');
		if(!value) return new Dialog('Please enter value').addCancelButton().text('OK');

		btnUpdate.prop('disabled', true);

		let loadingDialog = new Dialog('Updating... ');
		loadingDialog.overlay.off();

		var postData = {
			rowId,
			sequence,
			displayValue,
			groupID,
			id,
			value,
			isDefault,
			active
		};
		// console.log(postData); return false;
		$.post('/a/lov/update', postData).done(function(r){
			let dialog = new Dialog('Device updated successfully')
			dialog.addButton('primary', 'OK').click(function(){
			dialog.remove();
			btnCancel.trigger('click');
			location.reload();
		});
		// getlist();
		}).fail(function(e){
			console.error(e);
			errMsg = JSON.parse(e.responseText).errMsg;
			new Dialog(errMsg || 'An error occured.').addCancelButton().text('OK');
		}).always(function(){
			loadingDialog.remove();
			btnSave.prop('disabled', false);
		});
	});

	let dt = new DataTableCURD({
		api: '/a/lov/lists',
		columns: ['sequence','group_id','id','value','display','is_default','active'],
		headerColumns: ['seq','Group ID','ID','Value','Display','Is Default','Active'],
		data: 'data',
		edit: true,
		delete:false
	});

	dt.search(getJsonQueries());
		window.onpopstate = function(event) {
		dt.search(getJsonQueries(), true);
	}

});

function editRow(doc_id){
    docId = doc_id;
    let loadingDialog = new Dialog('loading... ');
	loadingDialog.overlay.off();
    $.get(`/a/lov/read/${docId}`).done(function(r){
    	loadingDialog.remove();
		if(!r || !r.data) {
			return alert('ID not found.');
		}

		const listDiv           = $('.list');
		const createDiv         = $('.create');
		const formLabel         = $('#formLabel');
		const btnUpdate         = $('.update');
		const btnCancel         = $('.cancel');
		const btnSave           = $('.save');
		//Input Fields

		const rowIdField        	= $('#rowId');

		const sequenceField         	= $('#sequence');
		const displayValueField       	= $('#displayValue');
		const groupIDField       		= $('#groupID');
		const isDefaultField    		= $('#isDefault');
		const idField   				= $('#id');
		const activeField    			= $('#active');
		const valueField    			= $('#value');


	    let data 		= r.data;

	    let lov = data.lovs;
	    // console.log(device);

		listDiv.hide();
		createDiv.show();
		formLabel.text('Edit LOV');

		btnUpdate.show();
		btnSave.hide();

		rowIdField.val(lov.id).prop('readonly',true);

		sequenceField.val(lov.sequence);
		displayValueField.val(lov.display);
		groupIDField.val(lov.group_id);
		idField.val(lov.id).prop('readonly',true);
		valueField.val(lov.value);
		activeField.prop( "checked", (lov.active) );
		isDefaultField.prop( "checked", (lov.is_default) );

    }).fail(function(x,s,e){
      error(x,s,e);
      alert('ID not found.')
    }).always(function(){
		loadingDialog.remove();
	});
 }

function deleteRow(doc_id){
    docId = doc_id;

    let dialog = new Dialog('Please confirm deletion')
    dialog.addCancelButton().text('Cancel')
    dialog.addButton('primary', 'OK').click(function(){
      dialog.remove();
      // btnCancel.trigger('click');
      // location.reload();
      $.post(`/a/lov/delete`,{id:docId}).done(function(d){
        console.log(d);
        // if(!d || !d.user) {
        //   return alert('Site ID not found.');
        // }
        let dialog = new Dialog(d.msg)
        dialog.addButton('primary', 'OK').click(function(){
          dialog.remove();
          location.reload();
        });
      }).fail(function(x,s,e){
        error(x,s,e);
        alert('ID not found.')
      });
    });    
  }


$(document).ready(function () {
    $('th').each(function (col) {
        $(this).hover(
          function () {
              $(this).addClass('focus');
          },
          function () {
              $(this).removeClass('focus');
          }
        );
	    $(this).click(function () {
	        if ($(this).is('.asc')) {
	            $(this).removeClass('asc');
	            $(this).addClass('desc selected');
	            sortOrder = -1;
	        } else {
	            $(this).addClass('asc selected');
	            $(this).removeClass('desc');
	            sortOrder = 1;
	        }
	        $(this).siblings().removeClass('asc selected');
	        $(this).siblings().removeClass('desc selected');
	        var arrData = $('table').find('tbody >tr:has(td)').get();
	        // arrData.sort(function (a, b) {
	        //     var val1 = $(a).children('td').eq(col).text().toUpperCase();
	        //     var val2 = $(b).children('td').eq(col).text().toUpperCase();
	        //     if ($.isNumeric(val1) && $.isNumeric(val2))
	        //         return sortOrder == 1 ? val1 - val2 : val2 - val1;
	        //     else
	        //         return (val1 < val2) ? -sortOrder : (val1 > val2) ? sortOrder : 0;
	        // });
	        // $.each(arrData, function (index, row) {
	        //     $('tbody').append(row);
	        // });

	        var table = $('table').eq(0)
	        var rows = table.find('tr:gt(0)').toArray().sort(comparer($(this).index()))
	        this.asc = !this.asc
	        if (!this.asc){rows = rows.reverse()}
	        for (var i = 0; i < rows.length; i++){table.append(rows[i])}
	    });
    });

    function comparer(index) {
      return function(a, b) {
        var valA = getCellValue(a, index), valB = getCellValue(b, index)
         return $.isNumeric(valA) && $.isNumeric(valB) ? valA - valB : valA.toString().localeCompare(valB)
      }
    }
    function getCellValue(row, index){ return $(row).children('td').eq(index).text() }
});

