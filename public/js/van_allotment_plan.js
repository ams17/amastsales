$(()=>{
  let vanId;
  let plan;
  let sites = [];
  let xhr;

  let listScreen = $('.content section');
  let viewScreen = new Screen({
    id: 'viewScreen',
    h1: 'Plan Allotment Details', 
    prevScreen: listScreen,
    hasFooter: true
  });

  let skuTable = new SkuTable({
    parent: viewScreen.content,
    title: 'Allotment Plan',
    columns: [['sku_id', 'SKU ID'], ['uom', 'UOM']],
    quantityColumns: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
  });

  let btnSave = $(`<button class='save'>Save</button>`).appendTo(viewScreen.footer);
  btnSave.click(function(){
    btnSave.prop('disabled', true);
    $('body .content').append(`<div class='inprogress'><p>Saving...</p></div>`);

    if(xhr) xhr.abort();
    xhr = $.ajax({
      type: 'POST',
      url: '/a/van/allot_plan/save', 
      contentType: "application/json;charset=utf-8",
      data: JSON.stringify({van_id: vanId, plan: skuTable.data})
    }).done(function(data){
      alert('Allotment Plan saved');
    }).fail(function(x,s,e){
      error(x,s,e);
    }).always(function(){
      $('body .content .inprogress').remove();
      btnSave.prop('disabled', false);
    });
  });

  let result_link = function(van_id){
    vanId = van_id;
    viewScreen.title.text(`${viewScreen.h1} - ${van_id}`);
    viewScreen.show();
    skuTable.clear();

    if(xhr) xhr.abort();
    xhr = $.get(`a/van/allot_plan/list/${van_id}`).done(function(d){
      if(!d || !d.plan) {
        return alert('Allotment Plan not found.');
      }
      plan = d.plan;
      skuTable.list(d.plan);
    }).fail(function(x,s,e){
      error(x,s,e);
      alert('Allotment Plan not found.')
    });
  }

  function drawDataTable(){
    let dt = new DataTable({
      api: '/a/van/list',
      columns: [['id', 'van id'], 'name', 'site_id','active'],
      filters: {
        site_id: {name: 'Site ID', type: 'option', options:['All', ...sites]},
        active: {name: 'Van Active', type: 'option',options:['All','Active','Inactive']}
      },
      data: 'vans',
      result_link: result_link
    });

    dt.search(getJsonQueries());
    window.onpopstate = function(event) {
      dt.search(getJsonQueries(), true);
    }
  }

  $.get('/a/site/list').done(function(data){    
    if(data.sites && data.sites.rows){
      sites = data.sites.rows.map(e => e.id);
    }
    drawDataTable();
  });
  
})