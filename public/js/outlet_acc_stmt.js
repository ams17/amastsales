$(()=>{
  let sites = [];
  let xhr;
  let docId;

  let listScreen = $('.content section');
  let viewScreen = new Screen({
    id: 'viewScreen',
    h1: 'Statement of Account Details', 
    prevScreen: listScreen,
    hasFooter: true
  });

  docHeader = new PivotTable({
    parent: viewScreen.content,
    title: 'Bill To',
    columns: [
      ['outlet_id', 'Outlet ID'],
      ['outlet_name', 'Outlet Name']
    ],
    format:{
      createdAt: (v) => { return (v)? dayjs(v).format(ntf) : '-'} 
    }
  });
  let val_Voided = $('#val_Voided');

  docDetails = new Table({
      parent: viewScreen.content,
      id: 'doc_details',
      title: 'Outstanding Invoices',
      columns: [ 
        ['invoice_id', 'Invoice No'],
        ['invoice_date', 'Invoice Date'],
        ['due_date', 'Due Date'],
        ['outstanding_amount', 'Total (RM)'],
        ['aging', 'Aging (Days)']
      ],
      format:{
        aging: (v) => Decimal(v).toFixed(0),
        outstanding_amount: (v) => Decimal(v).toFixed(2)
      },
      selectable: false,
      rowId: 'id',
    });
  
  docDetails.tfoot = $(`<tfoot></tfoot>`).appendTo(docDetails.table);
  docDetails.clear = function(){
    this.tbody.empty();
    this.tfoot.empty();
    return this;
  }

  let result_link = function(doc_id){
    docId = doc_id;
    viewScreen.title.text(`${viewScreen.h1} - ${docId}`);
    viewScreen.show();
    docHeader.clear();
    docDetails.clear();

    if(xhr) xhr.abort();
    xhr = $.get(`a/finance/outlet_acc_stmt/o/${docId}`).done(function(d){
      if(!d) {
        return alert('Statement of Account not found.');
      }
      
      let {header, details} = d;
      console.log(d);
      docHeader.update(header);
      docDetails.list(details);
      let cs = 5;
      
    }).fail(function(x,s,e){
      error(x,s,e);
      alert('Invoice ID not found.')
    });
  }

  function drawDataTable(){
    let dt = new DataTable({
      api: '/a/finance/outlet_acc_stmt/list',
      columns: [
        'site_id', 
        ['id', 'Outlet ID'], 
        'outlet_name', 
        ['total_outstanding', 'Total (RM)'],
        ['total_aging', 'Total Aging (Days)']
      ],
      format:{
        total_aging: (v) => Decimal(v).toFixed(0),
        total_outstanding: (v) => Decimal(v).toFixed(2),
      },
      filters: {
        searchby: {name: 'Search by', type: 'option', options: [['site_id','Site ID'], ['outlet_id', 'Outlet ID']]}
      },
      data: 'outletAccStmt',
      result_link: result_link
    });

    dt.search(getJsonQueries());
    window.onpopstate = function(event) {
      dt.search(getJsonQueries(), true);
    }
  }

  $.get('/a/site/list').done(function(data){    
    if(data.sites && data.sites.rows){
      sites = data.sites.rows.map(e => e.id);
    }
    drawDataTable();
  });

  let btnPrint = $(`<button type='button' id='printer' class='primary' class='btnPrint'>Print</button>`).appendTo(viewScreen.navRight);
  btnPrint.click(function(){
    let api = `/a/report/outlet_acc_stmt?outlet_id=${docId}`;

    printJS({
      printable:  api,
      type: 'pdf',
          onError: function  (error) {
            alert('Print PDF failed');
          }
      })
  });
})