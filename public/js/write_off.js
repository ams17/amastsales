$(()=>{
  let xhr;
  let dt;
  let docId;

  let listScreen = $('.content section');
  let createScreen;
  let siteCards, inpRemark, createSkus;

  let viewScreen;
  let viewDetailRows;
  let viewDetailSkus;

  let btnCreateScreen = $('#btnCreateScreen');
  let btnCreate;
  let btnPrint;

  let jsonQueries = getJsonQueries();
  if (jsonQueries != null){
    let docId = jsonQueries.id;
    if (docId != null) {
      result_link(docId);
      // Override click listener for back button
      viewScreen.btnBack.unbind();
      viewScreen.btnBack.click(function(){
        history.back()
      });
    }
  }

  function createScreenContent(){
    createScreen = new Screen({
      id: 'createScreen',
      h1: 'Create Write-off', 
      prevScreen: listScreen,
      hasFooter: true
    });
    createScreen.footer.hide();

    let fSite = new Field({ parent: createScreen.content, id: 'fSite', title: 'Select a site' });
    siteCards = new Cards({
      parent: fSite.label,
      id: 'siteCards'
    });

    $.get('/a/site/list/min').done(function(data){
      siteCards.list(data.sites);
    }).fail(function(x,s,e){
      error(x,s,e);
    });

    let fRemark = new Field({ parent: createScreen.content, id: 'fRemark', title: 'Enter remark'})
    fRemark.hide();
    inpRemark = $(`<input id='inpRemark' type='text'>`).appendTo(fRemark.label);

    let fQuantity = new Field({ parent: createScreen.content, id: 'fQuantity', title: 'Enter SKU quantity'})
    fQuantity.hide();
    createSkus = new SkuTable({
      parent: fQuantity.field,
      title: 'Transfer In',
      columns: [['sku_id', 'SKU ID'], ['uom_id', 'UOM']],
      quantityColumns: ['Fresh', 'Damaged', 'Old', 'Recalled']
    });

    siteCards.cards.on('selected', function(){
      fRemark.show();
      fQuantity.show();
      createScreen.footer.show();
    });

    siteCards.cards.on('deselected', function(){
      fRemark.hide();
      fQuantity.hide();
      createScreen.footer.hide();
    })

    function resetCreateScreen(){
      inpRemark.val('');
      createSkus.zerorize();
      siteCards.cards.children().show().removeClass('selected');
      createScreen.footer.hide();
      fRemark.hide();
      fQuantity.hide();
      console.log(createSkus.data);
    }

    let btnCreate = $(`<button class='create'>Create Write-off</button>`).appendTo(createScreen.footer);
    btnCreate.click(function(){
      let site_id = $('.card.selected', siteCards.cards).eq(0).data('id');
      if(!site_id || site_id.trim().length == 0){
        return alert('Please enter site id.');
      }

      let remark = inpRemark.val();
      if(!remark || remark.trim().length == 0){
        return alert('Please enter remark.');
      }

      let skus = createSkus.data;
      let total = 0;
      for(let s of skus){
        for(let q of s.quantity){
          let qty = parseInt(q);
          if(qty < 0) return alert('Quantity cannot be negative.');
          total += qty;
        }
      }

      if(total == 0) return alert('Total quantity cannot be 0.');

      // submit transfer out
      btnCreate.prop('disbaled', true);
      if(xhr) xhr.abort();
      xhr = $.ajax({
        type: 'POST',
        url: '/a/inventory/write_off',
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({site_id, remark, skus})
      }).done(function(data){
        console.log(data);
        alert(data.msg);
        createScreen.btnBack.trigger('click');
        resetCreateScreen();
        dt.search({keyword: data.writeOff.id}, true);
      }).fail(function(x,s,e){
        error(x,s,e);
      }).always(function(){
        btnCreate.prop('disabled', false);
      }); 
    });
  }
  
  btnCreateScreen.click(function(){
    console.log('aha');
    if(!createScreen) createScreenContent();
    createScreen.show();

    if(xhr) xhr.abort();
    xhr = $.get('/a/sku/tablelist').done(function(data){
      let skus = data.skus;
      for(let i = 0; i < skus.length; i++){
        skus[i].quantity = [0,0,0,0];
      }
      createSkus.list(skus);
    }).fail(function(x,s,e){
      error(x,s,e);
    });
  });

  function createViewScreen(){
    viewScreen = new Screen({
      id: 'viewScreen',
      h1: 'View Write Off', 
      prevScreen: listScreen
    });

    viewDetailRows = new PivotTable({
      parent: viewScreen.content,
      columns: [
        ['id', 'Write Off ID'], 
        ['site_id', 'Site ID'],
        ['date', 'Date'],
        ['remark', 'Remark'],
        ['created_by', 'Written Off by']
      ]
    });    

    viewDetailSkus = new SkuTable({
      parent: viewScreen.content,
      columns: [['sku_id', 'SKU ID'], ['uom_id', 'UOM']],
      quantityColumns: ['Fresh', 'Damaged', 'Old', 'Recalled'],
      viewOnly: true
    });

    btnPrint = $(`<button type='button' id='printer' class='primary' class='btnPrint'>Print</button>`).appendTo(viewScreen.navRight);
    
    btnPrint.click(function(){
      let api = `/a/report/write_off?report_id=${docId}`;

      printJS({
        printable:  api,
        type: 'pdf',
            onError: function  (error) {
              alert('Print PDF failed');
            }
        })
    });
  }

  function result_link(woid){
    docId = woid;
    if(!viewScreen) createViewScreen();
    viewDetailRows.clear();
    viewDetailSkus.clear();
    viewScreen.show();

    if(xhr) xhr.abort();
    xhr = $.get(`/a/inventory/write_off/o/${woid}`).done(function(data){
      viewDetailRows.update(data.write_off);
      viewDetailSkus.list(data.details);
    }).fail(function(x,s,e){
      error(x,s,e);
    })
  }

  function drawDataTable(){
    dt = new DataTable({
      api: 'a/inventory/write_off/list',
      columns: ['date', 'site_id', 'id', 'created_by', ['created_at', 'Created At']],
      filters: {
        site_id: {name: 'Site ID', type: 'option', options:['All', ...sites]},
        date: {name: 'Date', type: 'date', dateOptions: [
            ['anytime', 'Any time'],
            ['today', 'Today'],
            ['weektodate', 'Week-to-date'],
            ['monthtodate', 'Month-to-date'],
            ['yeartodate', 'Year-to-date'],
            ['customdate', 'Custom date'],
            ['customrange', 'Custom range']
          ]}        
      },
      format:{
        created_at: (v) => { return (v)? dayjs(v).format(df) : '-'} 
      },
      data: 'writeOff',
      result_link: result_link
    });

    dt.search(getJsonQueries());
    window.onpopstate = function(event) {
      dt.search(getJsonQueries(), true);
    }
  }

  $.get('/a/site/list').done(function(data){    
    if(data.sites && data.sites.rows){
      sites = data.sites.rows.map(e => e.id);
    }
    drawDataTable();
  });


})