$(()=>{
	const df = 'YYYY-MM-DD';
	const btnCreateScreen   = $('#btnCreateScreen');
	const btnSave           = $('.save');
	const btnUpdate         = $('.update');
	const btnCancel         = $('.cancel');
	const listDiv           = $('.list');
	const createDiv         = $('.create');
	const formLabel         = $('#formLabel');

	const skuField         		= $('#sku');
	const rowIdField         	= $('#rowId');
	const uomField       		= $('#uom');
	const priceField       		= $('#price');
	const effectiveDateField    = $('#effectiveDate');
	const priceGroupField   	= $('#priceGroup');

	btnCreateScreen.click(function(event){
		event.preventDefault();
		let loadingDialog = new Dialog('Loading... ');
      	loadingDialog.overlay.off();    

		$.get('/a/price/getDetails').done(function(r){
			
			// console.log(r);
			priceField.val('');
			effectiveDateField.val('');
			listDiv.hide();
		    createDiv.show();
		    formLabel.text('Create Price');
		    btnUpdate.hide();
		    btnSave.show();
		    let data = r.data;
		    let skuRows  = data.skuRows;
		    let skuCount  = data.skuCount;
		    let uomRows  = data.uomRows;
		    let uomCount  = data.uomCount;
		    let priceGroupRows  = data.priceGroupRows;
		    let priceGroupCount  = data.priceGroupCount;

		 //    $.each(skuRows, function() {
			//     $skuDropDown.append($("<option />").val(this.id).text(this.name));
			// });
			var skuOptions = [];
			for (var i = 0; i < skuCount; i++) {
		        skuOptions.push('<option value="',
		          skuRows[i].id, '">',
		          skuRows[i].name, '</option>');
		    }
		    $("#sku").html(skuOptions.join('')).select2();

			var uomOptions = [];
			for (var i = 0; i < uomCount; i++) {

				if(uomRows[i].is_default === true){
					uomOptions.push('<option value="',
			            uomRows[i].value, '" selected="selected">',
			             uomRows[i].display, '</option>');
				}else{
					uomOptions.push('<option value="',
		                uomRows[i].value, '">',
		                uomRows[i].display, '</option>');
				}		        
		    }
		    $("#uom").html(uomOptions.join('')).select2();

			var priceGroupOptions = [];
			for (var i = 0; i < priceGroupCount; i++) {
				if(priceGroupRows[i].is_default === true){
			        priceGroupOptions.push('<option value="',
			          priceGroupRows[i].value, '" selected="selected">',
			          priceGroupRows[i].display, '</option>');			        
		    	}else{
		    		priceGroupOptions.push('<option value="',
			          priceGroupRows[i].value, '">',
			          priceGroupRows[i].display, '</option>');
		    	}
		    }
		    $("#priceGroup").html(priceGroupOptions.join('')).select2();

			// let dialog = new Dialog('user saved successfully')
			// dialog.addButton('primary', 'OK').click(function(){
			//   dialog.remove();
			//   btnCancel.trigger('click');
			//   location.reload();
			// });
		}).fail(function(e){
		console.error(e);
			errMsg = JSON.parse(e.responseText).errMsg;
			new Dialog(errMsg || 'An error occured.').addCancelButton().text('OK');
		}).always(function(){
			loadingDialog.remove();
			btnSave.prop('disabled', false);
		});

	    // idField.val('');
	    // nameField.val('');
	    // roleField.val('');
	    // statusField.val('');
	    // passwordField.val('');
	    // deviceIDField.val('');
	    // allowNewDeviceField.val('');
  	});

	btnCancel.click(function(){
		createDiv.hide();
		listDiv.show();
	});



btnSave.click(function(){
	// Input fields
	let sku        		= skuField.val();
	let uom      		= uomField.val();
	let priceGroup      = priceGroupField.val();
	let price    		= priceField.val();
	let effectiveDate   = effectiveDateField.val();

	if(!sku) return new Dialog('Please select sku').addCancelButton().text('OK');
	if(!uom) return new Dialog('Please select uom').addCancelButton().text('OK');
	if(!priceGroup) return new Dialog('Please select priceGroup').addCancelButton().text('OK');
	if(!price) return new Dialog('Please enter price').addCancelButton().text('OK');
	if(!effectiveDate) return new Dialog('Please enter a Effective Date');
  	if(!dayjs(effectiveDate, df).isValid()) return new Dialog('Please enter a valid date format.');

	btnSave.prop('disabled', true);
	// vanName.prop('disabled', true);
	let loadingDialog = new Dialog('Saving... ');
	loadingDialog.overlay.off();
	var postData = {
		sku,
		uom,
		priceGroup,
		price,
		effectiveDate
	};
	// console.log(postData); return false;
	$.post('/a/price/create', postData).done(function(r){
		let dialog = new Dialog('Price saved successfully')
		dialog.addButton('primary', 'OK').click(function(){
			dialog.remove();
			btnCancel.trigger('click');
			location.reload();
		});
		// getlist();
		}).fail(function(e){
			console.error(e);
			errMsg = JSON.parse(e.responseText).errMsg;
			new Dialog(errMsg || 'An error occured.').addCancelButton().text('OK');
		}).always(function(){
			loadingDialog.remove();
			btnSave.prop('disabled', false);
		});
	});

  	btnUpdate.click(function(){

		// Input fields
		let rowId        	= rowIdField.val();
		let sku        		= skuField.val();
		let uom      		= uomField.val();
		let priceGroup      = priceGroupField.val();
		let price    		= priceField.val();
		let effectiveDate   = effectiveDateField.val();

		if(!sku) return new Dialog('Please select sku').addCancelButton().text('OK');
		if(!uom) return new Dialog('Please select uom').addCancelButton().text('OK');
		if(!priceGroup) return new Dialog('Please select priceGroup').addCancelButton().text('OK');
		if(!price) return new Dialog('Please enter price').addCancelButton().text('OK');
		if(!effectiveDate) return new Dialog('Please enter a Effective Date');
	  	if(!dayjs(effectiveDate, df).isValid()) return new Dialog('Please enter a valid date format.');

		btnUpdate.prop('disabled', true);
		// vanName.prop('disabled', true);
		let loadingDialog = new Dialog('Updating... ');
		loadingDialog.overlay.off();
		var postData = {
			rowId,
			sku,
			uom,
			priceGroup,
			price,
			effectiveDate
		};
		// console.log(postData); return false;
		$.post('/a/price/update', postData).done(function(r){
			let dialog = new Dialog('Price updated successfully')
			dialog.addButton('primary', 'OK').click(function(){
			dialog.remove();
			btnCancel.trigger('click');
			location.reload();
		});
		// getlist();
		}).fail(function(e){
			console.error(e);
			errMsg = JSON.parse(e.responseText).errMsg;
			new Dialog(errMsg || 'An error occured.').addCancelButton().text('OK');
		}).always(function(){
			loadingDialog.remove();
			btnSave.prop('disabled', false);
		});
	});

	let dt = new DataTableCURD({
		api: '/a/price/list',
		columns: ['effective_at','price_group_id','sku_id', 'uom_id','price'],
		headerColumns: ['effective_date','price_group_id','sku_id', 'uom_id','price'],
		data: 'prices',
		edit: true,
		delete:false
	});

	dt.search(getJsonQueries());
		window.onpopstate = function(event) {
		dt.search(getJsonQueries(), true);
	}

});

function editRow(doc_id){
    docId = doc_id;
    let loadingDialog = new Dialog('loading... ');
	loadingDialog.overlay.off();
    $.get(`/a/price/read/${docId}`).done(function(r){
    	loadingDialog.remove();
		if(!r || !r.data) {
			return alert('ID not found.');
		}

		// let { price } = d;
		const listDiv           = $('.list');
		const createDiv         = $('.create');
		const formLabel         = $('#formLabel');
		const btnUpdate         = $('.update');
		const btnCancel         = $('.cancel');
		const btnSave           = $('.save');
		//Input Fields

		const rowIdField        	= $('#rowId');
		const skuField      		= $('#sku');
		const uomField      		= $('#uom');
		const priceGroupField   	= $('#priceGroup');
		const priceField   			= $('#price');
		const effectiveDateField  	= $('#effectiveDate');


	    let data = r.data;
		let skuRows  = data.skuRows;
		let skuCount  = data.skuCount;
		let uomRows  = data.uomRows;
		let uomCount  = data.uomCount;
		let priceGroupRows  = data.priceGroupRows;
		let priceGroupCount  = data.priceGroupCount;
		let price = data.price;

		//    $.each(skuRows, function() {
		//     $skuDropDown.append($("<option />").val(this.id).text(this.name));
		// });
		var skuOptions = [];
		for (var i = 0; i < skuCount; i++) {
			if(skuRows[i].id === price.sku_id){
		    	skuOptions.push('<option value="',
		      		skuRows[i].id, '" selected="selected">',
		      		skuRows[i].name, '</option>');
			}else{
				skuOptions.push('<option value="',
		      		skuRows[i].id, '">',
		      		skuRows[i].name, '</option>');
			}
		}
		$("#sku").html(skuOptions.join('')).select2({
			width: '54%',
		});

		var uomOptions = [];
		for (var i = 0; i < uomCount; i++) {

			if(uomRows[i].value === price.uom_id){
				uomOptions.push('<option value="',
		            uomRows[i].value, '" selected="selected">',
		             uomRows[i].display, '</option>');
			}else{
				uomOptions.push('<option value="',
		            uomRows[i].value, '">',
		            uomRows[i].display, '</option>');
			}		        
		}
		$("#uom").html(uomOptions.join('')).select2({
			width: '54%',
		});

		var priceGroupOptions = [];
		for (var i = 0; i < priceGroupCount; i++) {
			if(priceGroupRows[i].is_default === price.price_group_id){
		        priceGroupOptions.push('<option value="',
		          priceGroupRows[i].value, '" selected="selected">',
		          priceGroupRows[i].display, '</option>');			        
			}else{
				priceGroupOptions.push('<option value="',
		          priceGroupRows[i].value, '">',
		          priceGroupRows[i].display, '</option>');
			}
		}
		$("#priceGroup").html(priceGroupOptions.join('')).select2({
			width: '54%',
		});


      listDiv.hide();
      createDiv.show();
      formLabel.text('Edit Price');

      btnUpdate.show();
      btnSave.hide();

      rowIdField.val(price.id).prop('readonly',true);
      skuField.val(price.sku_id);
      uomField.val(price.uom_id);
      priceGroupField.val(price.price_group_id);
      priceField.val(price.price);
      effectiveDateField.val(price.effective_at);

    }).fail(function(x,s,e){
      error(x,s,e);
      alert('ID not found.')
    }).always(function(){
		loadingDialog.remove();
	});
 }
function deleteRow(doc_id){
    docId = doc_id;

    let dialog = new Dialog('Please confirm deletion')
    dialog.addCancelButton().text('Cancel')
    dialog.addButton('primary', 'OK').click(function(){
      dialog.remove();
      // btnCancel.trigger('click');
      // location.reload();
      $.post(`/a/price/delete`,{id:docId}).done(function(d){
        console.log(d);
        // if(!d || !d.user) {
        //   return alert('Site ID not found.');
        // }
        let dialog = new Dialog(d.msg)
        dialog.addButton('primary', 'OK').click(function(){
          dialog.remove();
          location.reload();
        });
      }).fail(function(x,s,e){
        error(x,s,e);
        alert('ID not found.')
      });
    });    
  }

$(document).ready(function () {
    $('th').each(function (col) {
        $(this).hover(
          function () {
              $(this).addClass('focus');
          },
          function () {
              $(this).removeClass('focus');
          }
        );
        $(this).click(function () {
            if ($(this).is('.asc')) {
                $(this).removeClass('asc');
                $(this).addClass('desc selected');
                sortOrder = -1;
            } else {
                $(this).addClass('asc selected');
                $(this).removeClass('desc');
                sortOrder = 1;
            }
            $(this).siblings().removeClass('asc selected');
            $(this).siblings().removeClass('desc selected');
            var arrData = $('table').find('tbody >tr:has(td)').get();
            // arrData.sort(function (a, b) {
            //     var val1 = $(a).children('td').eq(col).text().toUpperCase();
            //     var val2 = $(b).children('td').eq(col).text().toUpperCase();
            //     if ($.isNumeric(val1) && $.isNumeric(val2))
            //         return sortOrder == 1 ? val1 - val2 : val2 - val1;
            //     else
            //         return (val1 < val2) ? -sortOrder : (val1 > val2) ? sortOrder : 0;
            // });
            // $.each(arrData, function (index, row) {
            //     $('tbody').append(row);
            // });

            var table = $('table').eq(0)
            var rows = table.find('tr:gt(0)').toArray().sort(comparer($(this).index()))
            this.asc = !this.asc
            if (!this.asc){rows = rows.reverse()}
            for (var i = 0; i < rows.length; i++){table.append(rows[i])}
        });
    });

    function comparer(index) {
      return function(a, b) {
        var valA = getCellValue(a, index), valB = getCellValue(b, index)
         return $.isNumeric(valA) && $.isNumeric(valB) ? valA - valB : valA.toString().localeCompare(valB)
      }
    }
    function getCellValue(row, index){ return $(row).children('td').eq(index).text() }
  });