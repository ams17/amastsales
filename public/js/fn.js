const nf = 'D MMM YYYY'; // natural format
const df = 'YYYY-MM-DD'; // date format
const tf = 'YYYY-MM-DD h:mm A'; // timestamp format
const ntf = 'ddd, D MMM YYYY, h:mm A';

const getJsonQueries = function(){
  let search = location.search.substring(1);
  if(search.length == 0) return;
  let q;
  try{
    q = JSON.parse('{"' + decodeURI(search).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g,'":"') + '"}');
  } catch(e){
    //console.error(e);
  }
  return q;
}

const getSitelist = function(){
  return JSON.parse($.ajax({
    type: 'GET',
    url: '/a/site/list/min',
    async: false
  }).responseText).sites;
}

const error = function(x={},s={},e={}){
  console.error({x,s,e});
  if(x.status == 401){
    window.location.href = '/login';
  }
  let errMsg = JSON.parse(x.responseText).errMsg || x.status  + ' ' + e;
  alert(errMsg);
}

Number.prototype.format = function(n, x) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
};

class ActButton{
  constructor(parent, id, label, imgSrc){
    this.parent = parent || $('.act'); 

    this.btn = $(`<button type='button'>
        <img src='${imgSrc}'>
        <div class='bg'></div>
        <div class='label'>${label || id || 'Button'}</div>
      </button>`).appendTo(this.parent);

    return this.btn;
  }
}

class Screen{
  constructor(options){
    if(options){
      this.id = options.id;
      this.class = options.class;
      this.h1 = options.h1;
      this.prevScreen = options.prevScreen;
    }
    let _this = this;
    
    this.parent = $('body > .content');
    this.screen = $(`<section class='screen'></section>`).hide();

    if(this.id) this.screen.prop('id', this.id);
    if(this.class) this.screen.addClass(this.class);

    this.nav = $(`<div class='nav'></div>`).appendTo(this.screen);
    this.navLeft = $(`<div class='left'></div>`).appendTo(this.nav);
    this.navRight = $(`<div class='right'></div>`).appendTo(this.nav);
    this.cont = $(`<div class='cont'></div>`).appendTo(this.screen);
    this.content = $(`<div class='content'></div>`).appendTo(this.cont);
    this.screen.appendTo(this.parent);
    
    this.btnBack = $(`<button id='btnBackScreen'><img src='/img/back_arrow_grey.svg'></button>`)
      .appendTo(this.navLeft)
      .click(function(){
        _this.hide();
        _this.prevScreen.show();
      });

    this.title = $(`<h1>${this.h1}</h1>`).appendTo(this.navLeft);

    if(Boolean(options.hasFooter)){
      this.footer = $(`<div class='footer'></div>`).appendTo(this.cont);
    }

    return this;
  }

  back(){
    this.btnBack.trigger('click');
    return this;
  }

  show(){
    this.screen.show();
    this.prevScreen.hide();
    return this;
  }

  hide(){
    this.screen.hide();
    return this;
  }

  remove(){
    this.screen.remove();
    return this;
  }

}

class DetailRows{
  constructor(options, data){
    this.parent = $('body');
    this.data;

    if(options){
      if('parent' in options) this.parent = options.parent;
      this.format = options.format;
      this.columns = options.columns;
      this.next = options.next;
      this.data = data;
    }

    this.rows = $(`<div class='drows'></div>`).appendTo(this.parent);
    this.nvl = ('nvl' in options)? options.nvl : '-';
    let _this = this;

    for(let col of this.columns){
      let colId = Array.isArray(col)? col[0] : col;
      let colDisplay = Array.isArray(col)? col[1]: col;
      let row = $(`<div class='row'></div>`).appendTo(this.rows);
      let left = $(`<div class='left'></div>`).appendTo(row);
      let rh = $(`<p class='title'>${colDisplay}</p>`).appendTo(left);
      let rd = $(`<p class='detail'></p>`).appendTo(left);
      this['c_'+colId] = rd;

      if(this.next && colId in this.next){
        row.addClass('next');
        $(`<div class='right'><img src='/img/chev_right.svg'></div>`).appendTo(row);
        row.click(function(){
          _this.next[colId]();
          $('body').trigger(`${colId}_click`);
        });
      }
    }

    if(data) this.update(data);

    this.rows.appendTo(this.parent);
    return this;
  }

  update(data){
    if(data) this.data = data;

    for(let col in this.data){
      let c_col = `c_${col}`;
      console.log(c_col);
      if(c_col in this) {
        let val = (this.data[col] !== null)? (this.format[col])? this.format[col](this.data[col]) : this.data[col] : this.nvl;
        //console.log(this[c_col].text())
        this[c_col].text(val);
      }
    }
  }

  clear(){
    for(let col of this.columns){
      let colId = Array.isArray(col)? col[0] : col;
      console.log('c_' + colId);
      this['c_' + colId].text('');
    }    
  }

  show(){
    this.rows.show();
    return this;
  }

  hide(){
    this.rows.hide();
    return this;
  }

  remove(){
    this.rows.remove();
    return this;
  }
}

class PivotTable{
  constructor(options, data){
    this.parent = $('body');

    if(options){
      if('parent' in options) this.parent = options.parent;
      this.title = options.title;
      this.columns = options.columns;
      this.format = options.format || {};
    }

    this.ptable = $(`<div class='ptable'></div>`).appendTo(this.parent);
    if(options.id) this.ptable.prop('id', id);
    if(options.class) this.ptable.addClass(options.class);

    this.title = $(`<div class='title'>${this.title || 'Details'}</div>`).appendTo(this.ptable);
    this.cont = $(`<div class='cont'></div>`).appendTo(this.ptable)
    this.table = $(`<table></table>`).appendTo(this.cont);
    this.tbody = $(`<tbody></tbody>`).appendTo(this.table);
    this.nvl = ('nvl' in options)? options.nvl : '-';

    for(let col of this.columns){
      let colId = Array.isArray(col)? col[0] : col;
      let colDisplay = Array.isArray(col)? col[1] : col;
      let tr = $(`<tr><th class='col'>${colDisplay}</td></tr>`).appendTo(this.tbody);
      this['c_' + colId] = $(`<td id='val_${colDisplay}' class='val'></td>`).appendTo(tr);
    }

    if(data) this.update(data);

    return this;
  }

  update(data){
    for(let col in data){
      if('c_'+col in this) {
        let val = (data[col] !== null)? (this.format[col])? this.format[col](data[col]) : data[col] : this.nvl;
        this['c_'+col].text(val);
      }
    }
  }

  clear(){
    for(let col of this.columns){
      let colId = Array.isArray(col)? col[0]: col;
      this['c_' + colId].text('');
    }    
  }
}

class Table{
  constructor(options, data){
    this.parent = $('body');
    this.data;
    this.selectable = false;

    if(options){
      if('parent' in options) this.parent = options.parent;
      if('title' in options) this._title = options.title;
      if('id' in options) this.id = options.id;
      if('selectable' in options) this.selectable = options.selectable || false;
      this.rowId = options.rowId;
      this.format = options.format || {};
      this.columns = options.columns || [];
      this.rowId = options.rowId;
      this.result_link = options.result_link;
    }

    this.rtable = $(`<div class='table'></div>`).appendTo(this.parent);
    if(this.id) this.rtable.prop('id', this.id);
    if(this.selectable) this.rtable.addClass('selectable');
    if (this.title) this.title = $(`<div class='title'><span>${this._title}<span></div>`).appendTo(this.rtable);
    this.cont = $(`<div class='cont'></div>`).appendTo(this.rtable);
    
    this.table = $(`<table></table>`);
    this.thead = $(`<thead></thead>`).appendTo(this.table);
    this.tbody = $(`<tbody></tbody>`).appendTo(this.table);
    let thead_tr = $('<tr></tr>').appendTo(this.thead);
    
    for(let col of this.columns){
      let cls = Array.isArray(col)? col[0]: col;
      let c = Array.isArray(col)? col[1]: col;
      $(`<th class='${cls}'>${c}</th>`).appendTo(thead_tr);
    }

    if(data) this.update();
    this.table.appendTo(this.cont);
    return this;
  }

  list(data){
    this.clear();
    if(data) this.data = data;
    for(let d of this.data){      
      let data_id = (this.rowId && d[this.rowId])? `data-id='${d[this.rowId]}'` : '' ;
      let tr = $(`<tr ${data_id}></tr>`).appendTo(this.tbody);
      for(let c of this.columns){
        if(Array.isArray(c)){
          $(`<td class='${c[0]}'>${ this.format[c[0]]? this.format[c[0]](d[c[0]]) : d[c[0]] || '' }</td>`).appendTo(tr);
        } else {
          $(`<td class='${c}'>${ this.format[c]? this.format[c](d[c]) : d[c] }</td>`).appendTo(tr);
        }
      }

    }

    let _this = this;
    if(this.result_link){
      $('td', this.tbody).click(function(){
        $(this).parent().data('id');
        let row_id = $(this).parent().data('id');
        _this.result_link(row_id);
      });
    }

    return this;
  }

  clear(){
    this.tbody.empty();
    return this;
  }
}

class SkuTable{
  constructor(options, data){
    this.parent = $('body');
    this.viewOnly = false;
    this.data;
    this._title = 'SKU List';

    if(options){
      if('parent' in options) this.parent = options.parent;
      if('title' in options) this._title = options.title;
      if('subTitle' in options) this._subTitle = options.subTitle;
      if('id' in options) this.id = options.id;
      this.format = options.format;
      this.columns = options.columns || [];
      this.quantityColumns = options.quantityColumns || [];
      this.viewOnly = options.viewOnly;
    }

    this.skutable = $(`<div class='skutable'></div>`).appendTo(this.parent);
    if(this.id) this.skutable.prop('id', this.id);
    if(this.viewOnly) this.skutable.addClass('viewOnly');
    this.title = $(`<div class='title'>${this._title}</div>`).appendTo(this.skutable);
    this.cont = $(`<div class='cont'></div>`).appendTo(this.skutable);
    

    this.table = $(`<table></table>`);
    this.thead = $(`<thead></thead>`).appendTo(this.table);
    this.tbody = $(`<tbody></tbody>`).appendTo(this.table);

    if (this._subTitle != null) {
      let subTitle_tr = $('<tr></tr>').appendTo(this.thead);
      for(let col of this._subTitle){
        let colspan = Array.isArray(col)? col[0]: col;
        let title = Array.isArray(col)? col[1]: col;
        
        $(`<th colspan=${colspan} class='title'>${title}</th>`).appendTo(subTitle_tr);
      }
    }
    
    let thead_tr = $('<tr></tr>').appendTo(this.thead);

    for(let col of this.columns){
      let c = Array.isArray(col)? col[1]: col;
      $(`<th>${c}</th>`).appendTo(thead_tr);
    }

    for(let col of this.quantityColumns){
      let c = Array.isArray(col)? col[1]: col;
      $(`<th class='qty'>${c}</th>`).appendTo(thead_tr);
    }

    if(data) this.update();
    //this.tabs.appendTo(this.parent);
    this.table.appendTo(this.cont);
    return this;
  }

  populate(data){
    if(!data) return this;
    this.zerorize();
    for(let d of data){
      $(`tr[data-id='${d.sku_id}'] input`, this.table).each(function(i){
        $(this).val(d.quantity[i]);
        $(this).trigger('change');
      });
    }

    return this;
  }

  list(data){
    if(data) this.data = JSON.parse(JSON.stringify(data));
    let x = 0;
    let y = 0;
    for(let d of this.data){
      let tr = $(`<tr data-id= '${d.sku_id}'></tr>`).appendTo(this.tbody);
      for(let col of this.columns){
        let c = Array.isArray(col)? col[0]: col;
        $(`<td class='${c}'>${d[c]}</td>`).appendTo(tr);
      }
      if(!d.quantity) this.data[x].quantity = this.quantityColumns.map(e => 0);
      for(let y = 0; y < this.quantityColumns.length; y++){
        if(this.viewOnly){
          $(`<td class='qty'>${d.quantity[y]}</td>`).appendTo(tr);
        }else {
          $(`<td class='qty'><input class="form-control" type="number" type='text' tabindex='${x+1+(this.data.length * y)}'' data-xy='${x},${y}' value='${d.quantity[y]}'></td>`).appendTo(tr);  
        }
      }
      x++;
    }

    let _this = this;
    $('.qty input', this.tbody).on('change', function(){
      console.log('change');
      let val = parseInt($(this).val() || 0);
      $(this).val(val);

      let xy = $(this).data('xy').split(',');
      let x = xy[0];
      let y = xy[1];
      _this.data[x].quantity[y] = parseInt($(this).val() || 0);
      let sku_id = $(this).parent().parent().data('id');

      _this.skutable.trigger('inputChange', [sku_id, x, y, val]);
      //console.log(_this.data[xy[0]].quantity);
    });

    return this;
  }

  clear(){
    this.tbody.empty();
    return this;
  }

  show(){
    this.skutable.show();
    return this;
  }

  hide(){
    this.skutable.hide();
    return this;
  }

  zerorize(){
    $('input', this.table).val(0);
    for(let x = 0; x < this.data.length; x++){
      for(let y = 0; y < this.data[x].quantity.length; y++){
        this.data[x].quantity[y] = 0;
      }
    }
    return this;
  }
}

class DataTable{
  constructor(options){
    this.api = options.api;
    this.method = options.method || 'get';
    this.filters = options.filters;
    this.order = options.order;
    this.columns = options.columns;
    this.format = options.format || {};
    this.data = options.data;
    this.result_link  = options.result_link;
    this.filterAlwaysShow = options.filterAlwaysShow || false;
    this.checkable = options.checkable || false;
    this.act = options.act;
    this.nvl = options.nvl || '-';
    this.xhr;
    this.hasPushState = false;
    let _this = this;

    if (options.parent) {
      this.parent = options.parent;
      this.searchCont = $('.searchCont', this.parent);
      this.results = $('.results', this.parent);
      this.pager = $('.pager', this.parent);

    } else {
      this.searchCont = $('.searchCont');
      this.results = $('.results');
      this.pager = $('.pager');
    }
    
    this.page = 1;
    const defaultDateOptions = [
        ['anytime', 'Any time'],
        ['today', 'Today'],
        ['weektodate', 'Week-to-date'],
        ['monthtodate', 'Month-to-date'],
        ['yeartodate', 'Year-to-date'],
        ['customdate', 'Custom date'],
        ['customrange', 'Custom range']
      ];

    // searchbar
    if(this.searchCont){
      this.barCont = $(`<div class='barCont'></div>`).appendTo(this.searchCont)
      this.bar = $(`<div class='bar' title='[ Alt + &#92; ]'></div>`).appendTo(this.barCont);
      this.input = $(`<input type='text' placeholder='Search'/>`).appendTo(this.bar);
      this.btnSearch = $(`<button id='btnSearch' class='icon' title='[ Enter ]'><img src='/img/search.svg'></button>`).appendTo(this.barCont);

      this.filtersCont = $(`<div class='filtersCont' style='display:none'></div>`).appendTo(this.searchCont);
      if(this.filterAlwaysShow) this.filtersCont.show();

      let hideFilters = function(){
        if(!_this.filterAlwaysShow){
          _this.filtersCont.slideUp('fast', function(){
            if(_this.searchCont.hasClass('focused')){
              _this.searchCont.removeClass('focused');
              //_this.search();  
            }
          });
        }
        _this.input.blur();
      }

      this.input
        .on('focus', function(){
          _this.searchCont.addClass('focused');
          if(_this.filters && !_this.filterAlwaysShow){
            _this.filtersCont.slideDown('fast')
          }
        })
        .on('keypress', function(e){
          if(e.which == 13) {
            _this.search();
          }
        });

      this.btnSearch.click(function(e){
        _this.search();
      })

      $(document).keydown(function(e){
        //if(e.altKey && e.keyCode == 83){
        if(e.altKey && e.which == 220){
          _this.input.select().focus();
        } else if( e.key === 'Escape'){
          console.log('escape')
          _this.input.blur();
          hideFilters();
        }
      });

      let filters;
      //if(Array.isArray(this.filters) && this.filters.length > 0){
      if(this.filters){
        filters = $(`<div class='filters'></div`).appendTo(this.filtersCont);
      }

      for(let filter in this.filters){
        let f = this.filters[filter];
        if(f.type === 'option' && f.options && f.options.length > 0){
          let icon = `<img class='icon' src='/img/dropdown.svg'>`;
          let dropdown = `<div class='options'>${f.options.map((e) => {
            if(Array.isArray(e)){
              return `<p class='option' data-value='${e[0]}'>${e[1]}</p>`  
            }
            return `<p class='option' data-value='${e}'>${e}</p>`        
          }).join('')}</div>`;

          let optIsArray = Array.isArray(f.options[0]);
          let _f = $(`<div class='filter' data-type='${f.type}' data-param='${filter}'>
              <div class='title'>
                <p>${f.name}: <span class='selection' data-value='${optIsArray? f.options[0][0]: f.options[0]}'>${optIsArray? f.options[0][1]: f.options[0]}</span></p>
                ${icon}
              </div>${dropdown}
            </div>`).appendTo(filters);
          $('.option', _f).click(function(){
            let val = $(this).data('value');
            let txt = $(this).text();
            $('.selection', _f).text(txt).data('value', val);
          });

        } else if(f.type=='date'){
          let icon = `<img class='icon' src='/img/dropdown.svg'>`;
          let dateOptions = f.dateOptions || defaultDateOptions ;
          let dropdown = `<div class='options'>${dateOptions.map(e => `<p class='option' data-value='${e[0]}'>${e[1]}</p>`).join('')}</div>`;
          
          let customDate = `<div class='custom date'>
            <div>
               <div><p>Date</p><input type='date' class='onedate'></div>
            </div>
            <button type='button' class='primary apply'>Apply</button>
          </div>`;

          let customRange = `<div class='custom range'>
            <div>
               <div><p>Start Date</p><input type='date' class='startdate'></div>
               <div><p>End Date</p><input type='date' class='enddate'></div>
            </div>
            <button type='button' class='primary apply'>Apply</button>
          </div>`;

          let _f = $(`<div class='filter' data-type='${f.type}' data-param='${filter}'>
            <div class='title'>
              <p>${f.name}: <span class='selection' data-value='${dateOptions[0][0]}'>${dateOptions[0][1]}</span></p>
              ${icon}
            </div>
            ${dropdown}
            ${customDate}
            ${customRange}
          </div>`).appendTo(filters);

          $('.option', _f).click(function(){
            let val;
            let txt = $(this).text();
            let opt = $(this).data('value');
            let selection = $('.selection', _f);

            console.log({opt})

            if(opt == 'anytime'){
              val = '';
            }else if(opt == 'today'){
              val = dayjs().format(df);
            }else if(opt == 'weektodate'){
              val = dayjs().startOf('isoWeek').format(df) + ',' + dayjs().format(df);
            }else if(opt == 'monthtodate'){
              val = dayjs().startOf('month').format(df) + ',' + dayjs().format(df);
            }else if(opt == 'yeartodate'){
              val = dayjs().startOf('year').format(df) + ',' + dayjs().format(df);
            }
            
            if(!opt.startsWith('custom')) {
              selection.text(txt).data('value', val);
              return;
            }

            if(opt == 'customdate'){
              let cd = $('.custom.date', _f);
              cd.addClass('open');
              let od = $('input.onedate', cd);
                
              let curr_val = selection.data('cd');
              if(curr_val && dayjs(curr_val, 'YYYY-MM-DD').isValid()){
                od.val(curr_val);
              }else{
                od.val(dayjs().format(df));  
              }

              $('.apply', cd).click(function(){
                let d = dayjs(od.val()).format(df);
                selection.text(d).data('value', d);
                selection.data('cd', d);
                cd.removeClass('open');
              });

            }else if(opt == 'customrange'){
              let cr = $('.custom.range', _f);
              cr.addClass('open');
              let sd = $('.startdate', cr);
              let ed = $('.enddate', cr);

              let curr_val = selection.data('cr') || 'x,x';
              let _sd = curr_val.split(',')[0];
              let _ed = curr_val.split(',')[1];

              if(!dayjs(_sd, df).isValid() || !dayjs(_ed, df).isValid()){
                sd.val(dayjs().subtract(1,'d').format(df));
                ed.val(dayjs().format(df));  
              } else {
                sd.val(_sd);
                ed.val(_ed);
              }
              
              $('.apply', cr).click(function(){
                let s = dayjs(sd.val()).format(df);
                let e = dayjs(ed.val()).format(df);
                selection.text(`${dayjs(s).format(nf)} — ${dayjs(e).format(nf)}`).data('value', `${s},${e}`);
                selection.data('cr', `${s},${e}`);
                cr.removeClass('open');
              });
            }

          });
        }
      }

      $('.filter', this.filtersCont).click(function(){
        let tgt = $(event.target);
        if(tgt.closest('.custom').length === 0){
          $(this).toggleClass('open');  
        }
      });

      $(document).on('click', function (event) {
        let tgt = $(event.target);
        if(tgt.closest(_this.searchCont).length === 0) {
          hideFilters();
        }

        $('.filter', _this.filtersCont)
          .not(tgt.closest('.filter', _this.filtersCont))          
          .removeClass('open');

        if(!tgt.hasClass('option')){
          $('.custom', _this.filtersCont)
          .not(tgt.closest('.custom', _this.filtersCont))
          .removeClass('open')  
        }
        
      });
    }

    // pager
    if(this.pager){
      $(
        `<div class='counter'>
          <span class='lower'>0</span>‒<span class='upper'>0</span> of <span class='total'>0</span>
        </div>
        <div class='stepper'>
          <button class='stepDown'><img src='/img/chev_left.svg'></button>
          <button class='stepUp'><img src='/img/chev_right.svg'></button>
        </div>`
      ).appendTo(this.pager);
      this.counterLower = $('.lower', this.pager);
      this.counterUpper = $('.upper', this.pager);
      this.counterTotal = $('.total', this.pager);
      this.stepDown = $('.stepDown', this.pager).click(function(){
        _this.page--;
        _this.search();
      });
      this.stepUp = $('.stepUp', this.pager).click(function(){
        _this.page++;
        _this.search();
      });
    }

    // results
    if(this.results){
      this.table = $(`<table></table>`).appendTo(this.results);
      let thead = `<thead><tr>${this.columns.map(c => {
        if(Array.isArray(c)){
          return `<th class='${c[0]}'>` + c[1].replaceAll('_',' ').toUpperCase() + '</th>'
        }else {
          return `<th class='${c}'>` + c.replaceAll('_',' ').toUpperCase() + '</th>'
        }
      })} </tr></thead>`;
      this.thead = $(thead).appendTo(this.table);

      if(this.checkable){
        $('tr', this.thead).prepend(`<th></th>`);
      }

      this.tbody = $(`<tbody></tbody>`).appendTo(this.table);
    }

    // if action div
    if(this.act){
      if(this.checkable){
        this.act.append(`<div class='chkAll'>
          <input id='checkAll' type='checkbox'>
          <label for='checkAll'></label>
        </div>`);
        
        this.checkAll = $('#checkAll', this.act);
        this.checkAll.click(function(){
          let isChecked = $(this).prop('checked');
          $('.chk input[type="checkbox"]', _this.table).prop('checked', isChecked);
          if(isChecked){
            $('tr', _this.tbody).addClass('checked');
            _this.act.children().show();
          } else {
            $('tr', _this.tbody).removeClass('checked');
            _this.act.children().not('.chkAll').hide();
          }
        });
      }
    }

    return this;
  }

  showLoading(){
    this.results.prepend(`<div class='loading'><p>Loading...</p></div>`);
  }

  hideLoading(){
    $('.loading', this.results).remove();
  }

  showNoResults(){
    this.hideTable();
    this.results.append(`<div class='noresults'><p>No results found. Try a different search criteria</p></div>`);
  }

  hideNoResults(){
    $('.noresults', this.results).remove();
  }

  showTable(){
    this.table.removeClass('hide');
    this.pager.removeClass('hide');
    if(this.act) this.act.removeClass('hide');
  }

  hideTable(){
    this.table.addClass('hide');
    this.pager.addClass('hide');
    if(this.act) this.act.addClass('hide');
  }

  updatePager(data){
    let {count, limit, offset, rows} = data || {};
    let cLower = (offset == null)? 0: offset + 1;
    let cUpper = (offset == null && !rows)? 0: offset + rows.length;
    let cTotal = (count == null)? 0: count;

    this.counterLower.text(cLower);
    this.counterUpper.text(cUpper);
    this.counterTotal.text(cTotal);
    this.page = Math.ceil(cUpper / (limit || 0));

    if(cLower <= 1) {
      this.stepDown.prop('disabled', true);
    } else {
      this.stepDown.prop('disabled', false);
    }
    if(cUpper >= cTotal) {
      this.stepUp.prop('disabled', true);
    } else {
      this.stepUp.prop('disabled', false);
    }

    /*
    if(cTotal <= 0){
      this.pager.hide();
    } else {
      this.pager.show();
    }*/
  }

  search(params, isPopState){
    //this.hideTable();
    this.hideNoResults();
    this.showLoading();

    if(this.xhr) this.xhr.abort(0);

    let _this = this;
    let queries = [];
    let keyword = this.input.val();

    if(!params){
      if(keyword) queries.push(`keyword=${keyword}`);
      $('.filter', this.filtersCont).each(function(){
        let v = $('.selection', this).data('value');
        if(v && !['all', 'anytime'].includes(v.toLowerCase())){
          let val; 
          if($(this).data('type') == 'date'){
            if(v == 'today'){
              v = dayjs().format(df);
            }else if(v == 'weektodate'){
              v = dayjs().startOf('isoWeek').format(df) + ',' + dayjs().format(df);
            }else if(v == 'monthtodate'){
              v = dayjs().startOf('month').format(df) + ',' + dayjs().format(df);
            }else if(v == 'yeartodate'){
              v = dayjs().startOf('year').format(df) + ',' + dayjs().format(df);
            }
          }
          queries.push($(this).data('param') + '=' + v);  
        }
      });
      if(this.page > 1) queries.push(`page=${this.page}`);
    } else {
      for(let param in params){
        if(![...Object.keys(this.filters), 'keyword', 'page'].includes(param)) continue;

        let val = params[param] || '';
        //console.log({params, param, val});

        //if(val) queries.push(`${param}=${val}`);
        if(param === 'keyword') {
          if(params.keyword != null) queries.push(`keyword=${val}`);
          this.input.val(val);
        }

        let tf = this.filters[param];
        if(tf && tf.type==='option' && tf.options && tf.options.map(e => {return Array.isArray(e)? e[0].toLowerCase(): e.toLowerCase()}).includes(val.toLowerCase())){
          let fTxt = Array.isArray(tf.options[0])? tf.options.filter(e => e[0] === val)[0][1]: val ;
          $(`.filter[data-param=${param}] .selection`, this.filtersCont).data('value', val).text(fTxt);
          if(val) queries.push(`${param}=${val}`);
        } else if(tf && tf.type==='date'){
          let dates = val.split(',');
          if(dates.length > 2){
            dates = [dates[0], dates[1]];  
          }
          for(let d of dates){
            if(!dayjs(d, df).isValid){
              dates = ['Anytime'];
              break;
            }
          }
          let sel =$(`.filter[data-param=${param}] .selection`, this.filtersCont)
          sel.data('value', dates.join(','))
          sel.text(dates.map(d => {
            let a = dayjs(d, df);
            if(a.isValid()) return a.format(nf);
            return dayjs().format(nf);
          }).join(' — '));

          if(val) {
            if(val == 'today'){
              val = dayjs().format(df);
            }else if(val == 'weektodate'){
              val = dayjs().startOf('isoWeek').format(df) + ',' + dayjs().format(df);
            }else if(val == 'monthtodate'){
              val = dayjs().startOf('month').format(df) + ',' + dayjs().format(df);
            }else if(val == 'yeartodate'){
              val = dayjs().startOf('year').format(df) + ',' + dayjs().format(df);
            }
            queries.push(`${param}=${val}`);
          }
        }
      }
    }

    //update url
    if (!isPopState && history.pushState){
      var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname;
      if(queries.length > 0) { 
        newurl += '?' + queries.join('&');
      }

      // prevents first push state, creating unnecessary need for double back click.
      if(this.hasPushState){
        window.history.pushState({path:newurl},null,newurl);
      } else {
        this.hasPushState = true;
      }

    }

    let uri  = this.api + `?` + queries.join('&');
    this.xhr = $[this.method](uri).done(function(rs){

      let data = rs[_this.data];
      if(!data || !data.rows || data.rows == 0){
        return _this.showNoResults();
      }
      
      //draw rows
      let tbody = '';
      for(let d of data.rows){
        tbody += `<tr data-id='${d.id}'>`;
        if(_this.checkable){
          tbody += `<td class='chk'><input id='o_${d.id}' type='checkbox'><label for='o_${d.id}'></label></td>`;
        }

        for(let c of _this.columns){
          if(Array.isArray(c)){
            tbody += `<td class='${c[0]}'>${ _this.format[c[0]]? _this.format[c[0]](d[c[0]]) : d[c[0]] || '' }</td>`;  
          } else {
            tbody += `<td class='${c}'>${ _this.format[c]? _this.format[c](d[c]) : d[c] }</td>`;    
          }
        }
        tbody += '</tr>';
      }

      _this.tbody.html(tbody);
      _this.showTable();

      if(_this.checkable){
        $('.chk input').click(function(e){
          e.stopPropagation();
          $(this).parent().parent().toggleClass('checked');
          let checkedCount = $('.chk input:checked', _this.table).length;
          _this.checkAll.prop('checked', checkedCount === $('.chk input').length);
          (_this.act && checkedCount > 0)? _this.act.children().show() : _this.act.children().not('.chkAll').hide();
        });
      }

      if(_this.result_link){
        $('tr > *:not(.chk)', _this.tbody).click(function(){
          $(this).parent().data('id');
          let row_id = $(this).parent().data('id');
          _this.result_link(row_id);
        });
      }

      if(_this.act){
        _this.checkAll.prop('checked', false);
        _this.act.children().not('.chkAll').hide();
      }

      if(_this.pager){
        _this.updatePager(rs[_this.data]);
      }
      
    }).fail(function(x,s,e){
      error(x,s,e);
    }).always(function(){
      _this.hideLoading();
    });
  }

  getCheckedIds(){
    if(!this.checkable) return null;

    let checkedIds = [];
    $('input:checked', this.tbody).each(function(){
      checkedIds.push($(this).prop('id').substring(2));
    });
    return checkedIds;
  }
}

class DataTableCURD{
  constructor(options){
    this.api = options.api;
    this.method = options.method || 'get';
    this.filters = options.filters;
    this.order = options.order;
    this.columns = options.columns;
    this.headerColumns = options.headerColumns;
    this.format = options.format || {};
    this.data = options.data;
    this.result_link  = options.result_link;
    this.filterAlwaysShow = options.filterAlwaysShow || false;
    this.checkable = options.checkable || false;
    this.act = options.act;
    this.nvl = options.nvl || '-';
    this.xhr;
    this.hasPushState = false;
    let _this = this;

    this.parent = $('body');
    this.searchCont = $('.searchCont');
    this.results = $('.results');
    this.pager = $('.pager');
    this.page = 1;
    this.delete = options.delete;
    this.edit  = options.edit;
    const defaultDateOptions = [
        ['anytime', 'Any time'],
        ['today', 'Today'],
        ['weektodate', 'Week-to-date'],
        ['monthtodate', 'Month-to-date'],
        ['yeartodate', 'Year-to-date'],
        ['customdate', 'Custom date'],
        ['customrange', 'Custom range']
      ];

    // searchbar
    if(this.searchCont){
      this.barCont = $(`<div class='barCont'></div>`).appendTo(this.searchCont)
      this.bar = $(`<div class='bar' title='[ Alt + &#92; ]'></div>`).appendTo(this.barCont);
      this.input = $(`<input type='text' placeholder='Search'/>`).appendTo(this.bar);
      this.btnSearch = $(`<button id='btnSearch' class='icon' title='[ Enter ]'><img src='/img/search.svg'></button>`).appendTo(this.barCont);

      this.filtersCont = $(`<div class='filtersCont' style='display:none'></div>`).appendTo(this.searchCont);
      if(this.filterAlwaysShow) this.filtersCont.show();

      let hideFilters = function(){
        if(!_this.filterAlwaysShow){
          _this.filtersCont.slideUp('fast', function(){
            if(_this.searchCont.hasClass('focused')){
              _this.searchCont.removeClass('focused');
              //_this.search();  
            }
          });
        }
        _this.input.blur();
      }

      this.input
        .on('focus', function(){
          _this.searchCont.addClass('focused');
          if(_this.filters && !_this.filterAlwaysShow){
            _this.filtersCont.slideDown('fast')
          }
        })
        .on('keypress', function(e){
          if(e.which == 13) {
            _this.search();
          }
        });

      this.btnSearch.click(function(e){
        _this.search();
      })

      $(document).keydown(function(e){
        //if(e.altKey && e.keyCode == 83){
        if(e.altKey && e.which == 220){
          _this.input.select().focus();
        } else if( e.key === 'Escape'){
          console.log('escape')
          _this.input.blur();
          hideFilters();
        }
      });

      let filters;
      //if(Array.isArray(this.filters) && this.filters.length > 0){
      if(this.filters){
        filters = $(`<div class='filters'></div`).appendTo(this.filtersCont);
      }

      for(let filter in this.filters){
        let f = this.filters[filter];
        if(f.type === 'option' && f.options && f.options.length > 0){
          let icon = `<img class='icon' src='/img/dropdown.svg'>`;
          let dropdown = `<div class='options'>${f.options.map((e) => {
            if(Array.isArray(e)){
              return `<p class='option' data-value='${e[0]}'>${e[1]}</p>`  
            }
            return `<p class='option' data-value='${e}'>${e}</p>`        
          }).join('')}</div>`;

          let optIsArray = Array.isArray(f.options[0]);
          let _f = $(`<div class='filter' data-type='${f.type}' data-param='${filter}'>
              <div class='title'>
                <p>${f.name}: <span class='selection' data-value='${optIsArray? f.options[0][0]: f.options[0]}'>${optIsArray? f.options[0][1]: f.options[0]}</span></p>
                ${icon}
              </div>${dropdown}
            </div>`).appendTo(filters);
          $('.option', _f).click(function(){
            let val = $(this).data('value');
            let txt = $(this).text();
            $('.selection', _f).text(txt).data('value', val);
          });

        } else if(f.type=='date'){
          let icon = `<img class='icon' src='/img/dropdown.svg'>`;
          let dateOptions = f.dateOptions || defaultDateOptions ;
          let dropdown = `<div class='options'>${dateOptions.map(e => `<p class='option' data-value='${e[0]}'>${e[1]}</p>`).join('')}</div>`;
          
          let customDate = `<div class='custom date'>
            <div>
               <div><p>Date</p><input type='date' class='onedate'></div>
            </div>
            <button type='button' class='primary apply'>Apply</button>
          </div>`;

          let customRange = `<div class='custom range'>
            <div>
               <div><p>Start Date</p><input type='date' class='startdate'></div>
               <div><p>End Date</p><input type='date' class='enddate'></div>
            </div>
            <button type='button' class='primary apply'>Apply</button>
          </div>`;

          let _f = $(`<div class='filter' data-type='${f.type}' data-param='${filter}'>
            <div class='title'>
              <p>${f.name}: <span class='selection' data-value='${dateOptions[0][0]}'>${dateOptions[0][1]}</span></p>
              ${icon}
            </div>
            ${dropdown}
            ${customDate}
            ${customRange}
          </div>`).appendTo(filters);

          $('.option', _f).click(function(){
            let val;
            let txt = $(this).text();
            let opt = $(this).data('value');
            let selection = $('.selection', _f);

            console.log({opt})

            if(opt == 'anytime'){
              val = '';
            }else if(opt == 'today'){
              val = dayjs().format(df);
            }else if(opt == 'weektodate'){
              val = dayjs().startOf('isoWeek').format(df) + ',' + dayjs().format(df);
            }else if(opt == 'monthtodate'){
              val = dayjs().startOf('month').format(df) + ',' + dayjs().format(df);
            }else if(opt == 'yeartodate'){
              val = dayjs().startOf('year').format(df) + ',' + dayjs().format(df);
            }
            
            if(!opt.startsWith('custom')) {
              selection.text(txt).data('value', val);
              return;
            }

            if(opt == 'customdate'){
              let cd = $('.custom.date', _f);
              cd.addClass('open');
              let od = $('input.onedate', cd);
                
              let curr_val = selection.data('cd');
              if(curr_val && dayjs(curr_val, 'YYYY-MM-DD').isValid()){
                od.val(curr_val);
              }else{
                od.val(dayjs().format(df));  
              }

              $('.apply', cd).click(function(){
                let d = dayjs(od.val()).format(df);
                selection.text(d).data('value', d);
                selection.data('cd', d);
                cd.removeClass('open');
              });

            }else if(opt == 'customrange'){
              let cr = $('.custom.range', _f);
              cr.addClass('open');
              let sd = $('.startdate', cr);
              let ed = $('.enddate', cr);

              let curr_val = selection.data('cr') || 'x,x';
              let _sd = curr_val.split(',')[0];
              let _ed = curr_val.split(',')[1];

              if(!dayjs(_sd, df).isValid() || !dayjs(_ed, df).isValid()){
                sd.val(dayjs().subtract(1,'d').format(df));
                ed.val(dayjs().format(df));  
              } else {
                sd.val(_sd);
                ed.val(_ed);
              }
              
              $('.apply', cr).click(function(){
                let s = dayjs(sd.val()).format(df);
                let e = dayjs(ed.val()).format(df);
                selection.text(`${dayjs(s).format(nf)} — ${dayjs(e).format(nf)}`).data('value', `${s},${e}`);
                selection.data('cr', `${s},${e}`);
                cr.removeClass('open');
              });
            }

          });
        }
      }

      $('.filter', this.filtersCont).click(function(){
        let tgt = $(event.target);
        if(tgt.closest('.custom').length === 0){
          $(this).toggleClass('open');  
        }
      });

      $(document).on('click', function (event) {
        let tgt = $(event.target);
        if(tgt.closest(_this.searchCont).length === 0) {
          hideFilters();
        }

        $('.filter', _this.filtersCont)
          .not(tgt.closest('.filter', _this.filtersCont))          
          .removeClass('open');

        if(!tgt.hasClass('option')){
          $('.custom', _this.filtersCont)
          .not(tgt.closest('.custom', _this.filtersCont))
          .removeClass('open')  
        }
        
      });
    }

    // pager
    if(this.pager){
      $(
        `<div class='counter'>
          <span class='lower'>0</span>‒<span class='upper'>0</span> of <span class='total'>0</span>
        </div>
        <div class='stepper'>
          <button class='stepDown'><img src='/img/chev_left.svg'></button>
          <button class='stepUp'><img src='/img/chev_right.svg'></button>
        </div>`
      ).appendTo(this.pager);
      this.counterLower = $('.lower', this.pager);
      this.counterUpper = $('.upper', this.pager);
      this.counterTotal = $('.total', this.pager);
      this.stepDown = $('.stepDown', this.pager).click(function(){
        _this.page--;
        _this.search();
      });
      this.stepUp = $('.stepUp', this.pager).click(function(){
        _this.page++;
        _this.search();
      });
    }

    // results
    if(this.results){
      this.table = $(`<table></table>`).appendTo(this.results);
      let thead = `<thead><tr>${this.headerColumns.map(c => {
        if(Array.isArray(c)){
          return `<th scope='col' class='${c[0]} sort-by'>` + c[1].replaceAll('_',' ').toUpperCase() + '</th>'
        }else {
          return `<th scope='col' class='${c} sort-by'>` + c.replaceAll('_',' ').toUpperCase() + '</th>'
        }
      })} <th class='action'>ACTION</th></tr></thead>`;
      this.thead = $(thead).appendTo(this.table);

      if(this.checkable){
        $('tr', this.thead).prepend(`<th></th>`);
      }

      this.tbody = $(`<tbody></tbody>`).appendTo(this.table);
    }

    // if action div
    if(this.act){
      if(this.checkable){
        this.act.append(`<div class='chkAll'>
          <input id='checkAll' type='checkbox'>
          <label for='checkAll'></label>
        </div>`);
        
        this.checkAll = $('#checkAll', this.act);
        this.checkAll.click(function(){
          let isChecked = $(this).prop('checked');
          $('.chk input[type="checkbox"]', _this.table).prop('checked', isChecked);
          if(isChecked){
            $('tr', _this.tbody).addClass('checked');
            _this.act.children().show();
          } else {
            $('tr', _this.tbody).removeClass('checked');
            _this.act.children().not('.chkAll').hide();
          }
        });
      }
    }

    return this;
  }

  showLoading(){
    this.results.prepend(`<div class='loading'><p>Loading...</p></div>`);
  }

  hideLoading(){
    $('.loading', this.results).remove();
  }

  showNoResults(){
    this.hideTable();
    this.results.append(`<div class='noresults'><p>No results found. Try a different search criteria</p></div>`);
  }

  hideNoResults(){
    $('.noresults', this.results).remove();
  }

  showTable(){
    this.table.removeClass('hide');
    this.pager.removeClass('hide');
    if(this.act) this.act.removeClass('hide');
  }

  hideTable(){
    this.table.addClass('hide');
    this.pager.addClass('hide');
    if(this.act) this.act.addClass('hide');
  }

  updatePager(data){
    let {count, limit, offset, rows} = data || {};
    let cLower = (offset == null)? 0: offset + 1;
    let cUpper = (offset == null && !rows)? 0: offset + rows.length;
    let cTotal = (count == null)? 0: count;

    this.counterLower.text(cLower);
    this.counterUpper.text(cUpper);
    this.counterTotal.text(cTotal);
    this.page = Math.ceil(cUpper / (limit || 0));

    if(cLower <= 1) {
      this.stepDown.prop('disabled', true);
    } else {
      this.stepDown.prop('disabled', false);
    }
    if(cUpper >= cTotal) {
      this.stepUp.prop('disabled', true);
    } else {
      this.stepUp.prop('disabled', false);
    }

    /*
    if(cTotal <= 0){
      this.pager.hide();
    } else {
      this.pager.show();
    }*/
  }

  search(params, isPopState){
    //this.hideTable();
    this.hideNoResults();
    this.showLoading();

    if(this.xhr) this.xhr.abort(0);

    let _this = this;
    let queries = [];
    let keyword = this.input.val();

    if(!params){
      if(keyword) queries.push(`keyword=${keyword}`);
      $('.filter', this.filtersCont).each(function(){
        let v = $('.selection', this).data('value');
        if(v && !['all', 'anytime'].includes(v.toLowerCase())){
          let val; 
          if($(this).data('type') == 'date'){
            if(v == 'today'){
              v = dayjs().format(df);
            }else if(v == 'weektodate'){
              v = dayjs().startOf('isoWeek').format(df) + ',' + dayjs().format(df);
            }else if(v == 'monthtodate'){
              v = dayjs().startOf('month').format(df) + ',' + dayjs().format(df);
            }else if(v == 'yeartodate'){
              v = dayjs().startOf('year').format(df) + ',' + dayjs().format(df);
            }
          }
          queries.push($(this).data('param') + '=' + v);  
        }
      });
      if(this.page > 1) queries.push(`page=${this.page}`);
    } else {
      for(let param in params){
        if(![...Object.keys(this.filters), 'keyword', 'page'].includes(param)) continue;

        let val = params[param] || '';
        //console.log({params, param, val});

        //if(val) queries.push(`${param}=${val}`);
        if(param === 'keyword') {
          if(params.keyword != null) queries.push(`keyword=${val}`);
          this.input.val(val);
        }

        let tf = this.filters[param];
        if(tf && tf.type==='option' && tf.options && tf.options.map(e => {return Array.isArray(e)? e[0].toLowerCase(): e.toLowerCase()}).includes(val.toLowerCase())){
          let fTxt = Array.isArray(tf.options[0])? tf.options.filter(e => e[0] === val)[0][1]: val ;
          $(`.filter[data-param=${param}] .selection`, this.filtersCont).data('value', val).text(fTxt);
          if(val) queries.push(`${param}=${val}`);
        } else if(tf && tf.type==='date'){
          let dates = val.split(',');
          if(dates.length > 2){
            dates = [dates[0], dates[1]];  
          }
          for(let d of dates){
            if(!dayjs(d, df).isValid){
              dates = ['Anytime'];
              break;
            }
          }
          let sel =$(`.filter[data-param=${param}] .selection`, this.filtersCont)
          sel.data('value', dates.join(','))
          sel.text(dates.map(d => {
            let a = dayjs(d, df);
            if(a.isValid()) return a.format(nf);
            return dayjs().format(nf);
          }).join(' — '));

          if(val) {
            if(val == 'today'){
              val = dayjs().format(df);
            }else if(val == 'weektodate'){
              val = dayjs().startOf('isoWeek').format(df) + ',' + dayjs().format(df);
            }else if(val == 'monthtodate'){
              val = dayjs().startOf('month').format(df) + ',' + dayjs().format(df);
            }else if(val == 'yeartodate'){
              val = dayjs().startOf('year').format(df) + ',' + dayjs().format(df);
            }
            queries.push(`${param}=${val}`);
          }
        }
      }
    }

    //update url
    if (!isPopState && history.pushState){
      var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname;
      if(queries.length > 0) { 
        newurl += '?' + queries.join('&');
      }

      // prevents first push state, creating unnecessary need for double back click.
      if(this.hasPushState){
        window.history.pushState({path:newurl},null,newurl);
      } else {
        this.hasPushState = true;
      }

    }

    let uri  = this.api + `?` + queries.join('&');
    this.xhr = $[this.method](uri).done(function(rs){

      let data = rs[_this.data];
      if(!data || !data.rows || data.rows == 0){
        return _this.showNoResults();
      }
      
      //draw rows
      let tbody = '';
      for(let d of data.rows){
        tbody += `<tr data-id='${d.id}'>`;
        if(_this.checkable){
          tbody += `<td class='chk'><input id='o_${d.id}' type='checkbox'><label for='o_${d.id}'></label></td>`;
        }

        for(let c of _this.columns){
          if(Array.isArray(c)){
            tbody += `<td class='${c[0]}'>${ _this.format[c[0]]? _this.format[c[0]](d[c[0]]) : d[c[0]] || '' }</td>`;  
          } else {
            tbody += `<td class='${c}'>${ _this.format[c]? _this.format[c](d[c]) : d[c] }</td>`;    
          }
        }
        tbody += `<td class='actions'>`;
        if(_this.edit){
          tbody += `<button type='button' 
                 onclick='editRow("${d.id}");'
                 class='btn btn-default'>Edit</button>`;
        }
        if(_this.delete){
          tbody += `<button type='button'
                  onclick='deleteRow("${d.id}");'
                  class='fa fa-default'>Delete</button>`;
        }
        tbody += '</td></tr>';
      }

      _this.tbody.html(tbody);
      _this.showTable();

      if(_this.checkable){
        $('.chk input').click(function(e){
          e.stopPropagation();
          $(this).parent().parent().toggleClass('checked');
          let checkedCount = $('.chk input:checked', _this.table).length;
          _this.checkAll.prop('checked', checkedCount === $('.chk input').length);
          (_this.act && checkedCount > 0)? _this.act.children().show() : _this.act.children().not('.chkAll').hide();
        });
      }

      if(_this.result_link){
        $('tr > *:not(.chk):not(.actions)', _this.tbody).click(function(){
          $(this).parent().data('id');
          let row_id = $(this).parent().data('id');
          _this.result_link(row_id);
        });
      }

      if(_this.act){
        _this.checkAll.prop('checked', false);
        _this.act.children().not('.chkAll').hide();
      }

      if(_this.pager){
        _this.updatePager(rs[_this.data]);
      }
      
    }).fail(function(x,s,e){
      error(x,s,e);
    }).always(function(){
      _this.hideLoading();
    });
  }

  getCheckedIds(){
    if(!this.checkable) return null;

    let checkedIds = [];
    $('input:checked', this.tbody).each(function(){
      checkedIds.push($(this).prop('id').substring(2));
    });
    return checkedIds;
  }
}

class Dialog{
  constructor(title, message){
    this.parent = $('body');
    this.modalCont = $(`<div class='modalCont'></div>`).appendTo(this.parent);
    this.modal = $(`<div class='modal'></div>`).appendTo(this.modalCont);
    this.title = $(`<h1>${title||''}</h1>`).appendTo(this.modal);
    this.message = $(`<p>${message||''}</p>`).appendTo(this.modal);
    this.buttons = $(`<div class='buttons'></div>`).appendTo(this.modal);
    this.overlay = $(`<div class='overlay'></div>`).appendTo(this.modalCont);
    let _this = this;

    $('body').css('overflow', 'hidden');
    this.overlay.click(function(){
      $('body').css('overflow', 'visible');
      _this.modalCont.remove();
    })
  }

  addButton(cls, label){
    let button = $(`<button type='button'>${label}</button>`).appendTo(this.buttons);
    button.addClass(cls);
    return button;
  }

  addCancelButton(){
    let _this = this;
    let cancelButton = $(`<button class='secondary' type='button'>Cancel</button>`).appendTo(this.buttons);
    cancelButton.click(function(){
      _this.modalCont.remove();
	  $('body').css('overflow', 'visible');
    })
    return cancelButton;
  }

  addLink(cls, href, label){
    let link = $(`<a class='button' href='${href}'>${label}</a>`).appendTo(this.buttons);
    link.addClass(cls);
    return link;
  }

  remove(){
	  $('body').css('overflow', 'visible');
    return this.modalCont.remove();
  }
}

class EventHistory{
  constructor(options, data){
    let _this = this;
    this.parent = $('body');
    this.dateFormat =  'YYYY-MM-DD (ddd) h:mmA';

    if(options){
      if(options.parent) this.parent = options.parent;
      if(options.title) this._title = options.title || 'Status History';
      if(options.dateFormat) this.dateFormat = options.dateFormat
    }

    this.content = $(`<div class='eventHistory'></div>`).appendTo(this.parent);
    if(options.id) this.content.prop('id', options.id);
    if(options.class) this.content.addClass(options.class);

    this.title = $(`<div class='title'>${this._title}</div>`).appendTo(this.content);
    this.events = $(`<div class='events'></div>`).appendTo(this.content);

    if(this.data) this.list(data);
  }

  list(data){
    if(data) this.data = data;
    
    let html = '';
    let count = 0;
    for(let i = 0; i < this.data.length; i++){
      let d = this.data[i];
      if(!d.at) continue;

      let by = d.by? ` <span class='by'>by ${d.by}</span>`: '';
      html += `<div class='event ${d.status.toLowerCase()}'>
        <div class='accent'>
          <div class='head'></div>
          <div class='tail'></div>
        </div>
        <div class='details'>
          <div class='desc'>
            <p><span class='status'>${d.status}</span>${by}</p>
          </div>
          <div class='timestamp'>
            <p>${dayjs(d.at).format(this.dateFormat)}</p>
          </div>
        </div>
      </div>`;
      count++;
    }

    this.events.html(html);
    return this;
  }

  clear(){
    this.events.empty();
    return this;
  }
}

class Field{
  constructor(options, data){
    let _this = this;
    this.parent = $('body');
    this._title = '';
    if(options){
      if(options.parent) this.parent = options.parent;
      if(options.title) this._title = options.title || '';
    }

    this.field = $(`<div class='field'></div>`).appendTo(this.parent);
    if(options.id) this.field.prop('id', options.id);
    if(options.class) this.field.addClass(options.class);

    this.label = $(`<label></label>`).appendTo(this.field);
    this.title = $(`<p>${this._title}</p>`).appendTo(this.label);

    return this;
  }

  hide(){
    this.field.hide();
  }

  show(){
    this.field.show();
  }
}

class Cards{
  constructor(options, data){
    let _this = this;
    this.parent = $('body');
    this.multiselect = false;

    if(options){
      if(options.parent) this.parent = options.parent;
      if(options.multiselect) this.multiselect = options.multiselect;
    }

    this.cont = $(`<div class='content'></div>`).appendTo(this.parent);
    if(options.id) this.cont.prop('id', options.id);
    if(options.class) this.cont.addClass(options.class);

    this.cards = $(`<div class='cards'></div>`).appendTo(this.cont);
    this.btns = $(`<div class='btns'></div>`).appendTo(this.cont);
    this.bLeft = $(`<div class='left'></div>`).appendTo(this.btns);
    this.bRight = $(`<div class='right'></div>`).appendTo(this.btns);

    if(this.multiselect){
      this.btnSelectAll = $(`<button type='button' class='selectAll'>Select All</button>`).appendTo(this.bLeft);
      this.btnSelectAll.click(function(){
        _this.cards.children().addClass('selected');
      });
      this.btnDeselectAll = $(`<button type='button' class='deselectAll'>Deselect All</button>`).appendTo(this.bRight);
      this.btnDeselectAll.click(function(){
        _this.cards.children().removeClass('selected');
      });
      this.btnNext = $(`<button type='button' class='next'>Next</button>`).appendTo(this.bRight);
    }

    if(data) this.list(data);
  }

  list(data){
    if(data) this.data = data;
    if(!this.data || this.data.length == 0) return;

    let html = '';
    for(let d of this.data){
      html += `<div class='card' data-id='${d.id}'>
        <div>
          <p class='id'>${d.display || d.id}</p>
        </div>
      </div>`
    }

    let _this = this;
    this.cards.html(html);
    if(this.multiselect){
      this.cards.children().click(function(){
        $(this).toggleClass('selected');
      });
    } else {
      this.cards.children().click(function(){
        let isSelected = $(this).hasClass('selected');
        $(this).toggleClass('selected');
        if(isSelected) {
          _this.cards.children().not($(this)).show();
          _this.cards.trigger('deselected');
        } else {
          _this.cards.children().not($(this)).removeClass('selected').hide();
          _this.cards.trigger('selected', [$(this).data('id')]);
        }
      });
    }
    return this;
  }

  deselect(){
    this.cards.children().removeClass('selected').show();
    return this;
  }

  clear(){
    this.cards.empty();
    return this;
  }

  hide(){
    this.cards.hide();
    return this;
  }

  show(){
    this.cards.show();
    return this;
  }
}

class VanCards{
  constructor(options, data){
    let _this = this;
    this.parent = $('body');
    this.defaultIconHtml = `<div class='imgPlaceholder'></div>`;
    

    if(options){
      if(options.parent) this.parent = options.parent;
      if(options.statusIcons) this.statusIcons = options.statusIcons;
      if(options.defaultIconHtml) this.defaultIconHtml = options.defaultIconHtml;
      if(options.multiselect) this.multiselect = options.multiselect;
    }

    this.content = $(`<div class='vanCards'></div>`).appendTo(this.parent);
    if(options.id) this.content.prop('id', options.id);
    if(options.class) this.content.addClass(options.class);
    
    this.cards = $(`<div class='cards'></div>`).appendTo(this.content);
    this.btns = $(`<div class='btns'></div>`).appendTo(this.content);
    this.bLeft = $(`<div class='left'></div>`).appendTo(this.btns);
    this.bRight = $(`<div class='right'></div>`).appendTo(this.btns);

    if(this.multiselect){
      this.btnSelectAll = $(`<button type='button' class='selectAll'>Select All</button>`).appendTo(this.bLeft);
      this.btnSelectAll.click(function(){
        _this.cards.children().addClass('selected');
      });
      this.btnDeselectAll = $(`<button type='button' class='deselectAll'>Deselect All</button>`).appendTo(this.bLeft);
      this.btnDeselectAll.click(function(){
        _this.cards.children().removeClass('selected');
      });

      this.btnNext = $(`<button type='button' class='next'>Next</button>`).appendTo(this.bRight);
    }
    if(data) this.list(data);
  }

  list(data){

    if(data) this.data = data;
    if(!this.data || this.data.length == 0) return;

    let html = '';
    for(let van of this.data){
      let icon;
      if(van.status && this.statusIcons){
        if(this.statusIcons[van.status]){
          icon = `<img src='${this.statusIcons[van.status]}' />`;
        } else if (this.statusIcons['default']){
          icon = `<img src='${this.statusIcons['default']}' />`;
        }
      } 
      if(!icon){
        icon = this.defaultIconHtml;
      }
      html += `<div class='card' data-id='${van.id}'' data-status='${van.status|| ''}'>
        <div>
          <p class='id'>${van.id}</p>
          <div>${icon}</div>
        </div>
      </div>`
    }

    let _this = this;
    this.cards.html(html);
    if(this.multiselect){
      this.cards.children().click(function(){
        $(this).toggleClass('selected');
      });
    } else {
      this.cards.children().click(function(){
        let isSelected = $(this).hasClass('selected');
        $(this).toggleClass('selected');
        if(isSelected) {
          _this.cards.children().not($(this)).show();
          _this.cards.trigger('deselected');
        } else {
          _this.cards.children().not($(this)).removeClass('selected').hide();
          _this.cards.trigger('selected', [$(this).data('id')]);
        }
      });
    }
  }

  clear(){
    this.cards.children().off();
    this.cards.empty();
  }

  getSelected(){
    let selected = [];
    $('.card.selected', this.cards).each(function(){
      selected.push({
        van_id: $(this).data('id'),
        status: $(this).data('status')
      });
    });
    return selected;
  }

  slideUp(){
    this.content.slideUp('fast');
  }

  slideDown(){
    this.content.slideDown('fast');
  }

  show(){
    this.content.show();
  }

  hide(){
    this.content.hide();
  }
}

class SortableTable{
  constructor(options, data){
    this.parent = $('body');
    this.viewOnly = false;
    this._title = 'Sortable Table';
    this.data = data;

    if(options){
      if('parent' in options) this.parent = options.parent;
      if('title' in options) this._title = options.title;
      if('onUpdate' in options) this.onUpdateMethod = options.onUpdate;
      if('onDelete' in options) this.onDeleteMethod = options.onDelete;
      // if('id' in options) this.id = options.id;
      this.format = options.format;
      this.columns = options.columns || [];
      this.quantityColumns = options.quantityColumns || [];
      this.viewOnly = options.viewOnly;
    }

    this.sortabletable = $(`<div class='sortabletable'></div>`).appendTo(this.parent);
    
    if(this.viewOnly) this.sortabletable.addClass('viewOnly');
    this.title = $(`<div class='title'>${this._title}</div>`).appendTo(this.sortabletable);
    this.cont = $(`<div class='cont'></div>`).appendTo(this.sortabletable);

    this.table = $(`<table></table>`);
    this.thead = $(`<thead></thead>`).appendTo(this.table);
    this.tbody = $(`<tbody></tbody>`).appendTo(this.table);
    let thead_tr = $('<tr></tr>').appendTo(this.thead);

    for(let col of this.quantityColumns){
      let c = Array.isArray(col)? col[1]: col;
      $(`<th class='qty'>${c}</th>`).appendTo(thead_tr);
    }

    this.table.appendTo(this.cont);

    if(Boolean(options.hasFooter)){
      this.footer = $(`<div class='footer'></div>`).appendTo(this.sortabletable);
    }

    return this;
  }

  list(data){
    if(data) this.data = JSON.parse(JSON.stringify(data));
    let x = 0;
    let y = 0;
    this.container = $(`<tr></tr>`);

    for(let col of this.quantityColumns){
      let c = Array.isArray(col)? col[1]: col;
      c = c.toUpperCase();

      let found = false;

      for(let d of this.data){
        if (d.day == c) {
          this.td = $(`<td data-id='${d.day}'></td>`);
          for(let y = 0; y < d.visit_list.length; y++){

            this.card = $(`<div class='card' data-xy='${d.day},${d.visit_list[y]}'></div>`);
            this.p = $(`<p>${d.visit_list[y]}</p>`)
              .appendTo(this.card);

            this.p.appendTo(this.card);

            let _this = this;

            this.btnClose = $(`<span id="close">&times;</span>`)
              .appendTo(this.card)
              .click(function(){
                _this.onDeleteMethod($(this));    
              });

            this.card.appendTo(this.td);
          }
          x++;

          this.td.sortable({
            onUpdate: this.onUpdateMethod
          });
          this.td.appendTo(this.container); 

          found = true;
          break;
        }
      }
      
      if (found) {
        this.container.appendTo(this.tbody);

      } else {
        this.td = $(`<td></td>`);
        this.td.appendTo(this.container); 
      }
    }

    return this;
  }

  update(event){
    let foundDay = $(event.to).data('id');
    this.refresh(foundDay, event.to);
  }

  delete(_this){
    let foundDay = _this.parent().parent().data('id');
    let dayList = _this.parent().parent();

    _this.parent().remove();
    this.refresh(foundDay, dayList);
  }

  refresh(selectedDay, newList){
    let newVisitList = [];

    $('div > p', newList).each(function() {
      let xy = $(this).parent().data('xy').split(',');
      let x = xy[0];
      let y = xy[1];

      newVisitList.push(y);
    });
    
    for(let d of this.data){
      if (d.day == selectedDay){
        d.visit_list = newVisitList;
        break;
      }
    }
  }

  clear(){
    this.tbody.empty();
    return this;
  }

  show(){
    this.sortabletable.show();
    return this;
  }

  showNoResults(){
    this.hide();
    this.parent.append(`<div class='noresults'><p>No visit plans available.</p></div>`);
  }

  hideNoResults(){
    $('.noresults', this.results).remove();
  }

  hide(){
    this.sortabletable.hide();
    this.hideNoResults();
    return this;
  }

}

class DropDown{
  constructor(options){
    if(options){
      this.id = options.id;
      this.url = options.url;
      this.group_id = options.group_id;
    }

    let _this = this;

    $(`${_this.id}`).select2({
      placeholder: 'Select Option',
      width: '54%',
      debug: false,
      ajax: {
          url: this.url,
          dataType: 'json',
          data: function(params) {
              return {
                  group_id: _this.group_id,
                  keywords: params.term,
                  page: params.page || 1
              }
          },
          processResults: function (data, params) {
            params.page = params.page || 1;
            
            return {
              results: data.results,
              pagination: {
                more: data.pagination.more
              }
            };
          },
          cache: false
      }
    });
  }

  setDefault(){
    var select = $(`${this.id}`);
    $.ajax({
      type: 'GET',
      url: `${this.url}?group_id=${this.group_id}&is_default=true`
    }).then(function (data) {
      if (data.results != null && data.results.length > 0){
        var option = new Option(data.results[0].text, data.results[0].id, true, true);
        select.append(option).trigger('change');

        select.trigger({
          type: 'select2:select',
          params: {
            data: data
          }
        });
      }
    });
  }

  setValue(selected_id){
    var select = $(`${this.id}`);
    $.ajax({
      type: 'GET',
      url: `${this.url}?group_id=${this.group_id}&selected_id=${selected_id}`
    }).then(function (data) {
      if (data.results != null && data.results.length > 0){
        var option = new Option(data.results[0].text, data.results[0].id, true, true);
        select.append(option).trigger('change');

        select.trigger({
          type: 'select2:select',
          params: {
            data: data
          }
        });
      }
    });
  }
}

