$(()=>{

  const btnCreateScreen   = $('#btnCreateScreen');
  const btnSave           = $('.save');
  const btnUpdate         = $('.update');
  const btnCancel         = $('.cancel');
  const listDiv           = $('.list');
  const createDiv         = $('.create');
  const formLabel         = $('#formLabel');

  let sites = [];
  let xhr;
  let selectedVanId;
  let dt;
  let createScreen;
  let siteCards;
  let fVanId;
  let fVanName;

  //Input Fields

  const siteIdField        = $('#siteId');
  const idField            = $('#id');
  const nameField          = $('#name');
  const territoryTypeField = $('#territoryType');
  const shiftField         = $('#shift');
  const planUploadField    = $('#planUpload');
  const statusField        = $('#status');

  let listScreen = $('.content section');
  let optionScreen = new Screen({
      id: 'optionScreen',
      h1: 'Van Details', 
      prevScreen: listScreen,
      hasFooter: true
    });

  let fSite = new Field({ parent: optionScreen.content, id: 'fOption', title: 'Select an option' });
  let optionCards = new Cards({
    parent: fSite.label,
    id: 'optionCards'
  }, [ 
    {id: 'vp', display: 'Visit Plan'},
    {id: 'ap', display: 'Allotment Plan'}
  ]);

  let detailRows = new DetailRows({
    parent: optionScreen.content,
    columns: [
      ['id', 'ID'],
      ['site_id', 'Site ID'],
      ['name', 'Name'],
      ['active', 'Active'],
      ['created_by', 'Created by'],
      ['createdAt', 'Created at']
    ],
    format: {
      createdAt:     (v) => { return (v)? dayjs(v).format(ntf) : '-'} 
    }
  });

  let visitPlanTable = new SortableTable({
    parent: optionScreen.content,
    id: 'visitPlanTable',
    title: 'Visit Plans',
    columns: [['outlet_id', 'OUTLET ID']],
    quantityColumns: ['MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN'],
    onUpdate: visitPlanUpdate,
    onDelete: visitPlanDelete,
    hasFooter: true
  });

  let skuTable = new SkuTable({
    parent: optionScreen.content,
    title: 'Allotment Plan',
    columns: [['sku_id', 'SKU ID'], ['uom', 'UOM']],
    quantityColumns: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
  });

  btnCreateScreen.click(function(){
    listDiv.hide();
    createDiv.show();
    formLabel.text('Add Van');
    btnUpdate.hide();
    btnSave.show();

    let siteIdDropDown = new DropDown({
      url: '/a/lookup/site/list',
      id: '#siteId',
      group_id: 'site_id'
    });

    let territoryTypeDropDown = new DropDown({
      url: '/a/lookup/lov/list',
      id: '#territoryType',
      group_id: 'van_territories'
    });

    let shiftDropDown = new DropDown({
      url: '/a/lookup/lov/list',
      id: '#shift',
      group_id: 'van_shifts'
    });

    let planUploadDropDown = new DropDown({
      url: '/a/lookup/lov/list',
      id: '#planUpload',
      group_id: 'plan_upload'
    });

    let statusDropDown = new DropDown({
      url: '/a/lookup/lov/list',
      id: '#status',
      group_id: 'true_false_status'
    });

    siteIdDropDown.setDefault();
    territoryTypeDropDown.setDefault();
    shiftDropDown.setDefault();
    planUploadDropDown.setDefault();
    statusDropDown.setDefault();

    siteIdField.prop('readonly', false);
    idField.val('').prop('readonly', false);
    nameField.val('');
  });

  btnSave.click(function(){

      // Input fields
      let site_id         = siteIdField.val();
      let id              = idField.val();
      let name            = nameField.val();
      let territory_type  = territoryTypeField.val();
      let shift           = shiftField.val();
      let plan_upload     = planUploadField.val();
      let status          = statusField.val();

      if(!siteId)   return new Dialog('Please enter site id').addCancelButton().text('OK');
      if(!id)   return new Dialog('Please enter van id').addCancelButton().text('OK');
      if(!name) return new Dialog('Please enter van name').addCancelButton().text('OK');
      if(!territoryType) return new Dialog('Please select territory type').addCancelButton().text('OK');
      if(!shift) return new Dialog('Please select shift').addCancelButton().text('OK');
      if(!planUpload) return new Dialog('Please select plan upload').addCancelButton().text('OK');
      if(!status) return new Dialog('Please select status').addCancelButton().text('OK');

      btnSave.prop('disabled', true);
      // vanName.prop('disabled', true);
      let loadingDialog = new Dialog('Saving... ');
      loadingDialog.overlay.off();
      var postData = {
        site_id,
        id,
        name,
        territory_type,
        shift,
        plan_upload,
        status
      };
      // console.log(postData); return false;
      $.post('/a/van/create', postData).done(function(r){
        let dialog = new Dialog('Van created successfully')
        dialog.addButton('primary', 'OK').click(function(){
          dialog.remove();
          btnCancel.trigger('click');
          location.reload();
        });
        // getlist();
      }).fail(function(e){
        console.error(e);
        errMsg = JSON.parse(e.responseText).errMsg;
        new Dialog(errMsg || 'An error occured.').addCancelButton().text('OK');
      }).always(function(){
        loadingDialog.remove();
        btnSave.prop('disabled', false);
      });
  });

  btnCancel.click(function(){
    createDiv.hide();
    listDiv.show();
  });

  function resetCreateScreen(){
    inpVanId.val('');
    inpVanName.val('');
    siteCards.cards.children().show().removeClass('selected');
    createScreen.footer.hide();

    fVanId.hide();
    fVanName.hide();
  }

  function visitPlanUpdate(event)
  {
    visitPlanTable.update(event);
  }

  function visitPlanDelete(_this)
  {
    let xy = _this.parent().data('xy').split(',');
    let x = xy[0];
    let y = xy[1];

    let dialog = new Dialog(`Are you sure you want remove ${y} from ${x}?`);

    dialog.addButton('warning', 'Yes').click(function(){
      visitPlanTable.delete(_this);
      dialog.remove();
    });

    dialog.addButton('light', 'Cancel').click(function(){
      dialog.remove();
    });
  }

  let btnUpdateVisitPlan = $(`<button class='updateVisitPlan'>Update</button>`).appendTo(visitPlanTable.footer);
  btnUpdateVisitPlan.click(function(){
    btnUpdateVisitPlan.prop('disabled', true);
    $('#optionCards').append(`<div class='inprogress'><p>Saving...</p></div>`);

    if(xhr) xhr.abort();
    xhr = $.ajax({
      type: 'POST',
      url: '/a/van/visit_plan/update', 
      contentType: "application/json;charset=utf-8",
      data: JSON.stringify({van_id: selectedVanId, plan: visitPlanTable.data})
    }).done(function(data){
      alert('Visit Plan saved');
    }).fail(function(x,s,e){
      error(x,s,e);
    }).always(function(){
      $('body .content .inprogress').remove();
      btnUpdateVisitPlan.prop('disabled', false);
    });
  });

  function drawDataTable(){
    dt = new DataTableCURD({
      api: '/a/van/list',
      columns: ['id', 'name', 'site_id','active'],
      headerColumns: ['id', 'name', 'site_id','active'],
      filters: {
        site_id: {name: 'Site ID', type: 'option', options:['All', ...sites]},
        active: {name: 'Active', type: 'option',options:['All','Active','Inactive']}
      },
      data: 'vans',
      edit: true,
      result_link: selectionScreen
    });

    dt.search(getJsonQueries());
    window.onpopstate = function(event) {
      dt.search(getJsonQueries(), true);
    }
  }

  $.get('/a/site/list').done(function(data){    
    if(data.sites && data.sites.rows){
      sites = data.sites.rows.map(e => e.id);
    }

    drawDataTable();
  });

  function selectionScreen(selectedId){
    optionScreen.title.text(`${optionScreen.h1} - ${selectedId}`);

    btnSaveAllotmentPlan.hide();
    optionCards.deselect();
    detailRows.hide();
    visitPlanTable.hide();
    skuTable.hide();
    optionScreen.show();
    selectedVanId = selectedId;
  }

  function openVisitPlanScreen(){
    btnUpdateVisitPlan.show();
    visitPlanTable.clear();

    if(xhr) xhr.abort();
    xhr = $.get(`a/van/visit_plan/list/${selectedVanId}`).done(function(d){
      if(!d || d.plan.length == 0) {
        visitPlanTable.showNoResults();
      }
      else
      {
        plan = d.plan;
        visitPlanTable.list(d.plan);
        visitPlanTable.show();
      }
      
    }).fail(function(x,s,e){
      error(x,s,e);
      alert('Visit Plan not found.')
    });

  }

  function openAllotmentPlanScreen(){
    skuTable.clear();
    skuTable.show();
    btnSaveAllotmentPlan.show();

    if(xhr) xhr.abort();
    xhr = $.get(`a/van/allot_plan/list/${selectedVanId}`).done(function(d){
      if(!d || !d.plan) {
        return alert('Allotment Plan not found.');
      }
      plan = d.plan;
      skuTable.list(d.plan);
    }).fail(function(x,s,e){
      error(x,s,e);
      alert('Allotment Plan not found.')
    });
  }

  let btnSaveAllotmentPlan = $(`<button class='save'>Save</button>`).appendTo(optionScreen.footer);
  btnSaveAllotmentPlan.click(function(){
      btnSaveAllotmentPlan.prop('disabled', true);
      $('body .content').append(`<div class='inprogress'><p>Saving...</p></div>`);

      if(xhr) xhr.abort();
      xhr = $.ajax({
        type: 'POST',
        url: '/a/van/allot_plan/save', 
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify({van_id: selectedVanId, plan: skuTable.data})
      }).done(function(data){
        alert('Allotment Plan saved');
      }).fail(function(x,s,e){
        error(x,s,e);
      }).always(function(){
        $('body .content .inprogress').remove();
        btnSaveAllotmentPlan.prop('disabled', false);
      });
    });



  optionCards.cards.on('selected', function(e, selectedId){
    switch(selectedId){
      case 'vp':
        openVisitPlanScreen();
        break;
      case 'ap':
        openAllotmentPlanScreen(selectedId);
        break;
    }
    fSite.title.hide();
  }); 

  optionCards.cards.on('deselected', function(){
    fSite.title.show();
    optionCards.deselect();
    detailRows.hide();
    visitPlanTable.hide();
    skuTable.hide();
    btnSaveAllotmentPlan.hide();
  });

  btnUpdate.click(function(){

    // Input fields
    let id             = idField.val();
    let name           = nameField.val();
    let territory_type = territoryTypeField.val();
    let shift          = shiftField.val();
    let plan_upload    = planUploadField.val();
    let status         = statusField.val();

    if(!id) return new Dialog('Please enter van id').addCancelButton().text('OK');
    if(!name) return new Dialog('Please enter van name').addCancelButton().text('OK');
    if(!territory_type) return new Dialog('Please select territory type').addCancelButton().text('OK');
    if(!shift) return new Dialog('Please select shift').addCancelButton().text('OK');
    if(!plan_upload) return new Dialog('Please select plan upload').addCancelButton().text('OK');
    if(!status) return new Dialog('Please select van status').addCancelButton().text('OK');

    btnUpdate.prop('disabled', true);
    // vanName.prop('disabled', true);
    let loadingDialog = new Dialog('Updating... ');
    loadingDialog.overlay.off();
    var postData = {
      id,
      name,
      territory_type,
      shift,
      plan_upload,
      status
    };
    // console.log(postData); return false;
    $.post('/a/van/update', postData).done(function(r){
      let dialog = new Dialog('Van updated successfully')
      dialog.addButton('primary', 'OK').click(function(){
        dialog.remove();
        btnCancel.trigger('click');
        location.reload();
      });
      // getlist();
    }).fail(function(e){
      console.error(e);
      errMsg = JSON.parse(e.responseText).errMsg;
      new Dialog(errMsg || 'An error occured.').addCancelButton().text('OK');
    }).always(function(){
      loadingDialog.remove();
      btnSave.prop('disabled', false);
    });
  });

});

function editRow(doc_id){
  docId = doc_id;

  $.get(`/a/van/o/${docId}`).done(function(d){
    console.log(d);
    if(!d || !d.van) {
      return alert('Van ID not found.');
    }
    
    let { van } = d;
    const listDiv           = $('.list');
    const createDiv         = $('.create');
    const formLabel         = $('#formLabel');
    const btnUpdate         = $('.update');
    const btnSave           = $('.save');
    const btnCancel         = $('.cancel');
    //Input Fields

    const siteIdField       = $('#siteId');
    const idField           = $('#id');
    const nameField         = $('#name');

    listDiv.hide();
    createDiv.show();
    formLabel.text('Edit Van');

    btnSave.hide();
    btnUpdate.show();

    siteIdField.val(van.site_id).prop('readonly',true);
    idField.val(van.id).prop('readonly',true);
    nameField.val(van.name);

    let siteIdDropDown = new DropDown({
      url: '/a/lookup/site/list',
      id: '#siteId',
      group_id: 'site_id'
    });

    let territoryTypeDropDown = new DropDown({
      url: '/a/lookup/lov/list',
      id: '#territoryType',
      group_id: 'van_territories'
    });

    let shiftDropDown = new DropDown({
      url: '/a/lookup/lov/list',
      id: '#shift',
      group_id: 'van_shifts'
    });

    let planUploadDropDown = new DropDown({
      url: '/a/lookup/lov/list',
      id: '#planUpload',
      group_id: 'plan_upload'
    });

    let statusDropDown = new DropDown({
      url: '/a/lookup/lov/list',
      id: '#status',
      group_id: 'true_false_status'
    });

    siteIdDropDown.setValue(van.site_id)
    territoryTypeDropDown.setValue(van.territory_type);
    shiftDropDown.setValue(van.shift);
    planUploadDropDown.setValue(van.plan_upload);
    statusDropDown.setValue(van.active);

  }).fail(function(x,s,e){
    error(x,s,e);
    alert('Van ID not found.')
  });
}

function deleteRow(doc_id){
  docId = doc_id;
  
  $.post(`/a/van/delete`,{id:docId}).done(function(d){
    console.log(d);
    let dialog = new Dialog(d.msg)
    dialog.addButton('primary', 'OK').click(function(){
      dialog.remove();
      location.reload();
    });
  }).fail(function(x,s,e){
    error(x,s,e);
    alert('Van ID not found.')
  });
};