const moment = require('moment');
const df = 'YYYY-MM-DD';

let Helper = {};

Helper.getDates = function(query){
  let {date} = query;
  let dates = date.split(',');

  if(dates.length == 0){
    return [moment().format(df)];
  }else if (dates.length == 1) {
    return [dates[0]];
  }

  dates = [dates[0],dates[1]];
  console.log(date);
  return dates;
}

Helper.validDates = function(dates){
  for(let date of dates){
    if (!moment(date, df).isValid()){
      return false;
    }
  }
  return true;
}


module.exports = Helper;